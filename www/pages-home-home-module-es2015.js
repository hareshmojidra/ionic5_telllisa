(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-home-home-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/home/components/boards/boards.component.html":
/*!**********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/home/components/boards/boards.component.html ***!
  \**********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ng-container  *ngIf=\"board\">\r\n <app-ios-speech-fab [showFab]=\"showFab\"></app-ios-speech-fab>\r\n\r\n  <ion-item-sliding  (ionDrag)=\"dragged($event, board)\">\r\n    <ion-item-options side=\"start\">\r\n    </ion-item-options>\r\n    \r\n    <ion-item [detail]=\"false\" (click)=\"openSpeechRecognition();\">\r\n      <ion-thumbnail>\r\n        <!-- <app-aspect-ratio [ratio]=\"{w: 1, h: 1}\" *ngIf=\"board.backgroundImage\"> -->\r\n          <app-image-shell class=\"user-image\" *ngIf=\"board.backgroundImage\" animation=\"spinner\" [src]=\"board.backgroundImage\" [alt]=\"'item image'\"></app-image-shell>\r\n        <!-- </app-aspect-ratio> -->\r\n        <ng-container *ngIf=\"board.backgroundColor\">\r\n          <div class=\"color-box\" [ngStyle]=\"{'background-color': (board.backgroundColor === 'default' ? defaultColor : board.backgroundColor)}\"></div>\r\n        </ng-container>\r\n      </ion-thumbnail>\r\n      <ion-label class=\"ion-text-wrap\">\r\n        <h2>\r\n          {{ board?.name }}\r\n          <span class=\"date\">\r\n            <ion-badge color=\"danger\">{{ board?.recordings?.length }}</ion-badge>\r\n          </span>\r\n        </h2>\r\n      </ion-label>\r\n    </ion-item>\r\n\r\n\r\n    <ion-item-options side=\"end\">\r\n      <!-- <ion-item-option (click)=\"gotoCreateRecord(board)\">\r\n        <ion-icon name=\"create-outline\" ></ion-icon>\r\n      </ion-item-option> -->\r\n      <!-- <ion-item-option>\r\n        <ion-icon name=\"copy-outline\"></ion-icon>\r\n      </ion-item-option>\r\n      <ion-item-option>\r\n        <ion-icon name=\"trash-outline\"></ion-icon>\r\n      </ion-item-option> -->\r\n    </ion-item-options>\r\n  </ion-item-sliding>\r\n</ng-container>\r\n\r\n\r\n\r\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/home/home.page.html":
/*!*********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/home/home.page.html ***!
  \*********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\r\n  <ion-toolbar color=\"skip-btn\" class=\"header-toolbar\">\r\n    <ion-buttons slot=\"start\">\r\n      <ion-menu-button color=\"light\"></ion-menu-button>\r\n    </ion-buttons>\r\n    <app-aspect-ratio [ratio]=\"{w:100, h:12}\">\r\n      <app-image-shell style=\"width:150px; height: 34px; margin: 0 auto;\" animation=\"spinner\" [src]=\"'assets/tl-logo-simpel-white.png'\" [alt]=\"'walkthrough'\"></app-image-shell>\r\n    </app-aspect-ratio>\r\n    <ion-buttons slot=\"end\">\r\n      <!-- <div (click)=\"test();\">\r\n        <app-image-shell style=\"width:24px; height: 24px; margin: 0 auto;\" animation=\"spinner\" [src]=\"'assets/tl-images/bx-add-board-24-px.svg'\" [alt]=\"'walkthrough'\"></app-image-shell>\r\n      </div> -->\r\n    </ion-buttons>\r\n  </ion-toolbar>\r\n  <ion-toolbar class=\"filters-toolbar\">\r\n    <ion-row class=\"searchbar-row\" align-items-center>\r\n      <ion-col>\r\n        <ion-searchbar mode=\"md\" class=\"items-searchbar\" animated  (ngModelChange)=\"searchLoader = true\" [(ngModel)]=\"searchQuery\" (ionChange)=\"searchList($event)\" placeholder=\"Search\"></ion-searchbar>\r\n      </ion-col>\r\n    </ion-row>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content [fullscreen]=\"true\">\r\n  <ion-refresher #refresher slot=\"fixed\" (ionRefresh)=\"refresh($event)\">\r\n    <ion-refresher-content></ion-refresher-content>\r\n  </ion-refresher>\r\n  <div class=\"ion-padding content-header\">\r\n    <h1>Boards</h1>\r\n  </div>\r\n  <ion-list lines=\"none\">\r\n    <ng-container *ngIf=\"boards$ | async; let boards; else LoadingSpinner;\">\r\n      <div class=\"spin\"  *ngIf=\"!boards.length\">\r\n        <div class=\"ion-text-center ion-padding\" >\r\n            <app-image-shell style=\"width:220px; height: 220px; margin: 0 auto;\" animation=\"spinner\" [src]=\"'assets/tl-images/boards-svg.svg'\" [alt]=\"'walkthrough'\"></app-image-shell>\r\n          <h5>No boards found yet</h5>\r\n          <p>\r\n            To add boards click the add icon in the top right corner.\r\n          </p>\r\n        </div>\r\n      </div>\r\n    <app-boards *ngFor=\"let board of boards | name: (searchService.searchedWord$ | async)\" [board]=\"board\" [boards]=\"boards\"></app-boards>\r\n    </ng-container>\r\n   \r\n    <ng-template #LoadingSpinner>\r\n      <div class=\"spin\">\r\n        <ion-spinner name=\"bubbles\"></ion-spinner>\r\n    </div>\r\n    </ng-template>\r\n  \r\n  \r\n  </ion-list>\r\n</ion-content>");

/***/ }),

/***/ "./src/app/pages/home/components/boards/boards.component.scss":
/*!********************************************************************!*\
  !*** ./src/app/pages/home/components/boards/boards.component.scss ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (":host {\n  --image-shell-border-radius: 5px;\n}\n\nion-item {\n  --padding-start: 0;\n  --inner-padding-end: 0;\n  margin: 6px 12px;\n  border-radius: 16px;\n  border: solid 1px #e2e6f1;\n  background-color: #fbfcfd;\n}\n\nion-label {\n  margin-top: 16px;\n  margin-bottom: 16px;\n}\n\nion-item h2 {\n  margin: 0;\n  font-family: \"Inter\";\n  font-size: 15px;\n  font-weight: bold;\n  font-stretch: normal;\n  font-style: normal;\n  line-height: 1.33;\n  letter-spacing: normal;\n  text-align: left;\n  color: #000000;\n}\n\nion-item p {\n  text-overflow: ellipsis;\n  overflow: hidden;\n  white-space: nowrap;\n  width: 95%;\n}\n\nion-item .date {\n  float: right;\n  align-items: center;\n  display: flex;\n}\n\nion-item ion-icon {\n  color: #c9c9ca;\n}\n\nion-item ion-note {\n  font-size: 15px;\n  margin-right: 8px;\n  font-weight: normal;\n}\n\nion-item ion-note.md {\n  margin-right: 14px;\n}\n\n.dot {\n  display: block;\n  height: 12px;\n  width: 12px;\n  border-radius: 50%;\n  align-self: start;\n  margin: 16px 10px 16px 16px;\n}\n\n.dot-unread {\n  background: var(--ion-color-primary);\n}\n\nion-footer ion-title {\n  font-size: 11px;\n  font-weight: normal;\n}\n\nion-thumbnail {\n  padding: 0.5em;\n  margin: 0 0.5em;\n}\n\nion-thumbnail app-aspect-ration app-image-shell img {\n  border-radius: 10px;\n}\n\nion-item {\n  --background: transparent;\n}\n\nion-item .color-box {\n  height: 100%;\n  width: 100%;\n  border-radius: 10px;\n}\n\nion-item ion-thumbnail .user-image ::ng-deep img {\n  border-radius: 10px;\n}\n\nion-item-options ion-item-option {\n  --background: #fbfcfd;\n  --color: #8da1b5;\n  margin: 5px;\n  border-radius: 16px;\n  border: solid 1px #e2e6f1;\n  background-color: #fbfcfd;\n  width: 58px;\n  height: 58px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvaG9tZS9jb21wb25lbnRzL2JvYXJkcy9ib2FyZHMuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxnQ0FBQTtBQUNGOztBQUNBO0VBQ0Usa0JBQUE7RUFDQSxzQkFBQTtFQUNBLGdCQUFBO0VBQ0EsbUJBQUE7RUFDQSx5QkFBQTtFQUNBLHlCQUFBO0FBRUY7O0FBQ0E7RUFDRSxnQkFBQTtFQUNBLG1CQUFBO0FBRUY7O0FBQ0E7RUFDRSxTQUFBO0VBQ0Esb0JBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxvQkFBQTtFQUNBLGtCQUFBO0VBQ0EsaUJBQUE7RUFDQSxzQkFBQTtFQUNBLGdCQUFBO0VBQ0EsY0FBQTtBQUVGOztBQUNBO0VBQ0UsdUJBQUE7RUFDQSxnQkFBQTtFQUNBLG1CQUFBO0VBQ0EsVUFBQTtBQUVGOztBQUNBO0VBQ0UsWUFBQTtFQUNBLG1CQUFBO0VBQ0EsYUFBQTtBQUVGOztBQUNBO0VBQ0UsY0FBQTtBQUVGOztBQUNBO0VBQ0UsZUFBQTtFQUNBLGlCQUFBO0VBQ0EsbUJBQUE7QUFFRjs7QUFDQTtFQUNFLGtCQUFBO0FBRUY7O0FBQ0E7RUFDRSxjQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7RUFDQSxrQkFBQTtFQUNBLGlCQUFBO0VBQ0EsMkJBQUE7QUFFRjs7QUFDQTtFQUNFLG9DQUFBO0FBRUY7O0FBQ0E7RUFDRSxlQUFBO0VBQ0EsbUJBQUE7QUFFRjs7QUFDQTtFQUNFLGNBQUE7RUFDQSxlQUFBO0FBRUY7O0FBR007RUFDRSxtQkFBQTtBQURSOztBQU1BO0VBQ0UseUJBQUE7QUFIRjs7QUFJRTtFQUNFLFlBQUE7RUFDQSxXQUFBO0VBQ0EsbUJBQUE7QUFGSjs7QUFLSTtFQUNFLG1CQUFBO0FBSE47O0FBU0U7RUFDRSxxQkFBQTtFQUNBLGdCQUFBO0VBQ0EsV0FBQTtFQUNBLG1CQUFBO0VBQ0EseUJBQUE7RUFDQSx5QkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0FBTkoiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9ob21lL2NvbXBvbmVudHMvYm9hcmRzL2JvYXJkcy5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIjpob3N0IHtcclxuICAtLWltYWdlLXNoZWxsLWJvcmRlci1yYWRpdXM6IDVweDtcclxufVxyXG5pb24taXRlbSB7XHJcbiAgLS1wYWRkaW5nLXN0YXJ0OiAwO1xyXG4gIC0taW5uZXItcGFkZGluZy1lbmQ6IDA7XHJcbiAgbWFyZ2luOiA2cHggMTJweDtcclxuICBib3JkZXItcmFkaXVzOiAxNnB4O1xyXG4gIGJvcmRlcjogc29saWQgMXB4ICNlMmU2ZjE7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZiZmNmZDtcclxufVxyXG5cclxuaW9uLWxhYmVsIHtcclxuICBtYXJnaW4tdG9wOiAxNnB4O1xyXG4gIG1hcmdpbi1ib3R0b206IDE2cHg7XHJcbn1cclxuXHJcbmlvbi1pdGVtIGgyIHtcclxuICBtYXJnaW46IDA7XHJcbiAgZm9udC1mYW1pbHk6IFwiSW50ZXJcIjtcclxuICBmb250LXNpemU6IDE1cHg7XHJcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgZm9udC1zdHJldGNoOiBub3JtYWw7XHJcbiAgZm9udC1zdHlsZTogbm9ybWFsO1xyXG4gIGxpbmUtaGVpZ2h0OiAxLjMzO1xyXG4gIGxldHRlci1zcGFjaW5nOiBub3JtYWw7XHJcbiAgdGV4dC1hbGlnbjogbGVmdDtcclxuICBjb2xvcjogIzAwMDAwMDtcclxufVxyXG5cclxuaW9uLWl0ZW0gcCB7XHJcbiAgdGV4dC1vdmVyZmxvdzogZWxsaXBzaXM7XHJcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICB3aGl0ZS1zcGFjZTogbm93cmFwO1xyXG4gIHdpZHRoOiA5NSU7XHJcbn1cclxuXHJcbmlvbi1pdGVtIC5kYXRlIHtcclxuICBmbG9hdDogcmlnaHQ7XHJcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG59XHJcblxyXG5pb24taXRlbSBpb24taWNvbiB7XHJcbiAgY29sb3I6ICNjOWM5Y2E7XHJcbn1cclxuXHJcbmlvbi1pdGVtIGlvbi1ub3RlIHtcclxuICBmb250LXNpemU6IDE1cHg7XHJcbiAgbWFyZ2luLXJpZ2h0OiA4cHg7XHJcbiAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcclxufVxyXG5cclxuaW9uLWl0ZW0gaW9uLW5vdGUubWQge1xyXG4gIG1hcmdpbi1yaWdodDogMTRweDtcclxufVxyXG5cclxuLmRvdCB7XHJcbiAgZGlzcGxheTogYmxvY2s7XHJcbiAgaGVpZ2h0OiAxMnB4O1xyXG4gIHdpZHRoOiAxMnB4O1xyXG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcclxuICBhbGlnbi1zZWxmOiBzdGFydDtcclxuICBtYXJnaW46IDE2cHggMTBweCAxNnB4IDE2cHg7XHJcbn1cclxuXHJcbi5kb3QtdW5yZWFkIHtcclxuICBiYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSk7XHJcbn1cclxuXHJcbmlvbi1mb290ZXIgaW9uLXRpdGxlIHtcclxuICBmb250LXNpemU6IDExcHg7XHJcbiAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcclxufVxyXG5cclxuaW9uLXRodW1ibmFpbCB7XHJcbiAgcGFkZGluZzogMC41ZW07XHJcbiAgbWFyZ2luOiAwIDAuNWVtO1xyXG4gIC8vIGJvcmRlci1yYWRpdXM6IDEwcHg7XHJcbiAgLy8gLS1ib3JkZXItcmFkaXVzOiAxMHB4O1xyXG4gIGFwcC1hc3BlY3QtcmF0aW9uIHtcclxuICAgIGFwcC1pbWFnZS1zaGVsbCB7XHJcbiAgICAgIGltZyB7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogMTBweDtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxufVxyXG5pb24taXRlbSB7XHJcbiAgLS1iYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcclxuICAuY29sb3ItYm94IHtcclxuICAgIGhlaWdodDogMTAwJTtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgYm9yZGVyLXJhZGl1czogMTBweDtcclxuICB9XHJcbiAgaW9uLXRodW1ibmFpbCAudXNlci1pbWFnZSB7XHJcbiAgICA6Om5nLWRlZXAgaW1nIHtcclxuICAgICAgYm9yZGVyLXJhZGl1czogMTBweDtcclxuICAgIH1cclxuICB9XHJcbn1cclxuXHJcbmlvbi1pdGVtLW9wdGlvbnMge1xyXG4gIGlvbi1pdGVtLW9wdGlvbiB7XHJcbiAgICAtLWJhY2tncm91bmQ6ICNmYmZjZmQ7XHJcbiAgICAtLWNvbG9yOiAjOGRhMWI1O1xyXG4gICAgbWFyZ2luOiA1cHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiAxNnB4O1xyXG4gICAgYm9yZGVyOiBzb2xpZCAxcHggI2UyZTZmMTtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmYmZjZmQ7XHJcbiAgICB3aWR0aDogNThweDtcclxuICAgIGhlaWdodDogNThweDtcclxuICB9XHJcbn1cclxuIl19 */");

/***/ }),

/***/ "./src/app/pages/home/components/boards/boards.component.ts":
/*!******************************************************************!*\
  !*** ./src/app/pages/home/components/boards/boards.component.ts ***!
  \******************************************************************/
/*! exports provided: BoardsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BoardsComponent", function() { return BoardsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _capacitor_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @capacitor/core */ "./node_modules/@capacitor/core/dist/esm/index.js");
/* harmony import */ var _core_helpers__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @core/helpers */ "./src/app/core/helpers/index.ts");
/* harmony import */ var _core_models__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @core/models */ "./src/app/core/models/index.ts");
/* harmony import */ var _core_services__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @core/services */ "./src/app/core/services/index.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _shared_lib_services__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @shared-lib/services */ "./src/app/shared/services/index.ts");









const { isNative } = _capacitor_core__WEBPACK_IMPORTED_MODULE_3__["Capacitor"];
let BoardsComponent = class BoardsComponent {
    constructor(router, recordingsService, messagesService, toast, mailService, platform, languageService) {
        this.router = router;
        this.recordingsService = recordingsService;
        this.messagesService = messagesService;
        this.toast = toast;
        this.mailService = mailService;
        this.platform = platform;
        this.languageService = languageService;
        this.defaultColor = _core_models__WEBPACK_IMPORTED_MODULE_5__["defaultColor"];
        this.longPressActive = false;
        this.showFab = false;
    }
    ngOnInit() {
        const { $id: boardId } = this.board;
        this.messagesService.setBoardId(boardId);
        this.recordingsService.initSpeechRecognition();
    }
    openSpeechRecognition() {
        if (isNative) {
            if (this.platform.is("ios")) {
                this.showFab = true;
            }
            const { languageTag } = this.languageService.getSelectedLanguage || {};
            const options = {
                language: languageTag ? languageTag : "en-US",
            };
            this.recordingsService.options = options;
            this.recordingsService.speechRecognition
                .startListening(options)
                .subscribe((matches) => this.handleSpeechToText(matches), (onerror) => {
                console.log("one error", onerror);
            });
        }
        else {
            this.toast.showBottomShort("Please run on actual device", 3000);
        }
    }
    handleSpeechToText(matches) {
        console.log("matches", matches);
        const { email } = this.board;
        const record = matches.join(", ");
        const message = {
            title: Object(_core_helpers__WEBPACK_IMPORTED_MODULE_4__["convertDateToFirebaseTimestamp"])(),
            creationDate: Object(_core_helpers__WEBPACK_IMPORTED_MODULE_4__["convertDateToFirebaseTimestamp"])(),
            text: record,
        };
        const mail = {
            to: email,
            message: {
                subject: "New card",
                html: `${record}`,
            },
        };
        Object(_core_helpers__WEBPACK_IMPORTED_MODULE_4__["tryCatchHelper"])(this.mailService.addMail(mail));
        Object(_core_helpers__WEBPACK_IMPORTED_MODULE_4__["tryCatchHelper"])(this.messagesService.addMessage(message));
        this.toast.showBottomShort("Stored Message.", 3000);
        if (this.platform.is("ios")) {
            this.showFab = !this.showFab;
        }
    }
    showRecordings(board) {
        alert(JSON.stringify(board.recordings));
    }
    gotoPage(board, url) {
        this.messagesService.setBoard(board);
        this.router.navigate(url);
    }
    dragged(event, board) {
        this.recordingsService.recordings = board.recordings;
        if (event.detail.amount < 0) {
            this.gotoPage(board, ["recordings"]);
        }
        else {
            this.gotoPage(board, ["recordings", "create-record"]);
        }
    }
};
BoardsComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _core_services__WEBPACK_IMPORTED_MODULE_6__["RecordingsService"] },
    { type: _core_services__WEBPACK_IMPORTED_MODULE_6__["MessagesService"] },
    { type: _shared_lib_services__WEBPACK_IMPORTED_MODULE_8__["ToastService"] },
    { type: _core_services__WEBPACK_IMPORTED_MODULE_6__["MailService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["Platform"] },
    { type: _core_services__WEBPACK_IMPORTED_MODULE_6__["LanguageService"] }
];
BoardsComponent.propDecorators = {
    board: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"] }],
    boards: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"] }]
};
BoardsComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "app-boards",
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./boards.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/home/components/boards/boards.component.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./boards.component.scss */ "./src/app/pages/home/components/boards/boards.component.scss")).default]
    }),
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
        _core_services__WEBPACK_IMPORTED_MODULE_6__["RecordingsService"],
        _core_services__WEBPACK_IMPORTED_MODULE_6__["MessagesService"],
        _shared_lib_services__WEBPACK_IMPORTED_MODULE_8__["ToastService"],
        _core_services__WEBPACK_IMPORTED_MODULE_6__["MailService"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["Platform"],
        _core_services__WEBPACK_IMPORTED_MODULE_6__["LanguageService"]])
], BoardsComponent);



/***/ }),

/***/ "./src/app/pages/home/home-routing.module.ts":
/*!***************************************************!*\
  !*** ./src/app/pages/home/home-routing.module.ts ***!
  \***************************************************/
/*! exports provided: HomePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePageRoutingModule", function() { return HomePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _home_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./home.page */ "./src/app/pages/home/home.page.ts");




const routes = [
    {
        path: "",
        component: _home_page__WEBPACK_IMPORTED_MODULE_3__["HomePage"],
    },
];
let HomePageRoutingModule = class HomePageRoutingModule {
};
HomePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], HomePageRoutingModule);



/***/ }),

/***/ "./src/app/pages/home/home.module.ts":
/*!*******************************************!*\
  !*** ./src/app/pages/home/home.module.ts ***!
  \*******************************************/
/*! exports provided: HomePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePageModule", function() { return HomePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _shared_lib_shared_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @shared-lib/shared.module */ "./src/app/shared/shared.module.ts");
/* harmony import */ var _components_boards_boards_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./components/boards/boards.component */ "./src/app/pages/home/components/boards/boards.component.ts");
/* harmony import */ var _home_routing_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./home-routing.module */ "./src/app/pages/home/home-routing.module.ts");
/* harmony import */ var _home_page__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./home.page */ "./src/app/pages/home/home.page.ts");









let HomePageModule = class HomePageModule {
};
HomePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _home_routing_module__WEBPACK_IMPORTED_MODULE_7__["HomePageRoutingModule"],
            _shared_lib_shared_module__WEBPACK_IMPORTED_MODULE_5__["SharedModule"],
        ],
        declarations: [_home_page__WEBPACK_IMPORTED_MODULE_8__["HomePage"], _components_boards_boards_component__WEBPACK_IMPORTED_MODULE_6__["BoardsComponent"]],
    })
], HomePageModule);



/***/ }),

/***/ "./src/app/pages/home/home.page.scss":
/*!*******************************************!*\
  !*** ./src/app/pages/home/home.page.scss ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (":host {\n  --page-margin: var(--app-fair-margin);\n  --page-background: var(--app-background);\n  --toolbar-background: #1d2a64;\n  --ion-toolbar-background: transparent;\n}\n\n.header-toolbar {\n  padding: 16px;\n}\n\n.filters-toolbar {\n  padding: 14px;\n  background-color: #e7f0f7;\n}\n\n.filters-toolbar .searchbar-row {\n  --ion-grid-column-padding: 0px;\n}\n\n.filters-toolbar .searchbar-row ion-searchbar.items-searchbar {\n  padding: 0px;\n  height: 100%;\n  contain: content;\n  border-radius: 16px;\n  --background: #ffffff;\n}\n\n.filters-toolbar .searchbar-row .call-to-action-col {\n  -webkit-padding-start: var(--page-margin);\n          padding-inline-start: var(--page-margin);\n  max-width: -webkit-fit-content;\n  max-width: -moz-fit-content;\n  max-width: fit-content;\n  flex-shrink: 0;\n  flex-grow: 0;\n  display: flex;\n  justify-content: flex-end;\n}\n\n.filters-toolbar .searchbar-row .call-to-action-col .filters-btn {\n  --padding-start: 0px;\n  --padding-end: 0px;\n  margin: 0px;\n  font-size: 18px;\n  height: initial;\n}\n\n.filters-toolbar .range-item-row {\n  --ion-grid-column-padding: 0px;\n  margin-top: var(--page-margin);\n  padding-top: var(--page-margin);\n  box-shadow: inset 0 6px 3px -8px var(--ion-color-darkest);\n}\n\n.filters-toolbar .range-item-row .range-header {\n  display: flex;\n  justify-content: space-between;\n  padding-bottom: calc(var(--page-margin) / 2);\n}\n\n.filters-toolbar .range-item-row .range-header .range-value {\n  font-size: 12px;\n  font-weight: 600;\n  letter-spacing: 0.2px;\n  color: var(--ion-color-medium);\n}\n\n.filters-toolbar .range-item-row .range-header .range-label {\n  font-size: 14px;\n  font-weight: 500;\n  letter-spacing: 0.2px;\n  color: var(--ion-color-medium);\n}\n\n.filters-toolbar .range-item-row .range-control {\n  --ion-text-color: var(--ion-color-medium);\n  padding-top: 0px;\n  padding-bottom: 0px;\n}\n\n.boards-listing-content {\n  --background: var(--page-background);\n}\n\n.boards-listing-content .items-list .list-item {\n  --padding-start: var(--page-margin);\n  --padding-end: 0px;\n  --inner-padding-start: 0px;\n  --inner-padding-end: var(--page-margin);\n  --inner-padding-top: calc(var(--page-margin) / 2);\n  --inner-padding-bottom: calc(var(--page-margin) / 2);\n}\n\n.boards-listing-content .items-list .list-item .user-row {\n  --ion-grid-column-padding: 0px;\n  width: 100%;\n}\n\n.boards-listing-content .items-list .list-item .user-row .user-image-wrapper {\n  -webkit-padding-end: calc(var(--page-margin) / 2);\n          padding-inline-end: calc(var(--page-margin) / 2);\n}\n\n.boards-listing-content .items-list .list-item .user-row .user-data-wrapper {\n  -webkit-padding-start: calc(var(--page-margin) / 2);\n          padding-inline-start: calc(var(--page-margin) / 2);\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n}\n\n.boards-listing-content .items-list .list-item .user-row .user-data-wrapper .user-info:not(:last-child) {\n  margin-bottom: calc(var(--page-margin) / 2);\n}\n\n.boards-listing-content .items-list .list-item .user-row .user-data-wrapper .user-info .user-name {\n  margin: 0px;\n  margin-bottom: calc(var(--page-margin) / 4);\n  font-size: 16px;\n}\n\n.boards-listing-content .items-list .list-item .user-row .user-data-wrapper .user-info .user-age {\n  margin: 0px;\n  color: rgba(var(--ion-color-dark-rgb), 0.4);\n  font-size: 14px;\n}\n\n.boards-listing-content .empty-list-message {\n  margin: calc(var(--page-margin) * 3);\n  color: rgba(var(--ion-color-dark-rgb), 0.4);\n  text-align: center;\n}\n\n.content-header {\n  font-family: \"Inter\";\n  font-size: 28px;\n  font-weight: normal;\n  font-stretch: normal;\n  font-style: normal;\n  line-height: 1.21;\n  letter-spacing: normal;\n  text-align: left;\n  color: #263238;\n  padding-bottom: 0;\n}\n\n.content-header h1 {\n  margin-top: 0;\n}\n\n.spin {\n  display: flex;\n  align-items: center;\n  justify-content: center;\n  place-items: center;\n  height: 50vh;\n}\n\n.spin h5 {\n  font-family: \"Inter\";\n  font-size: 20px;\n  font-weight: 300;\n  font-stretch: normal;\n  font-style: normal;\n  line-height: 1.25;\n  letter-spacing: normal;\n  color: #263238;\n  width: 100%;\n}\n\n.spin p {\n  width: 100%;\n  font-family: \"Inter\";\n  font-size: 13px;\n  font-weight: normal;\n  font-stretch: normal;\n  font-style: normal;\n  line-height: 1.54;\n  letter-spacing: normal;\n  text-align: center;\n  color: #8da1b5;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvaG9tZS9ob21lLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFFQTtFQUNFLHFDQUFBO0VBQ0Esd0NBQUE7RUFDQSw2QkFBQTtFQUNBLHFDQUFBO0FBREY7O0FBSUE7RUFDRSxhQUFBO0FBREY7O0FBS0E7RUFDRSxhQUFBO0VBQ0EseUJBQUE7QUFGRjs7QUFJRTtFQUNFLDhCQUFBO0FBRko7O0FBSUk7RUFDRSxZQUFBO0VBR0EsWUFBQTtFQUVBLGdCQUFBO0VBQ0EsbUJBQUE7RUFDQSxxQkFBQTtBQUxOOztBQVFJO0VBQ0UseUNBQUE7VUFBQSx3Q0FBQTtFQUNBLDhCQUFBO0VBQUEsMkJBQUE7RUFBQSxzQkFBQTtFQUNBLGNBQUE7RUFDQSxZQUFBO0VBRUEsYUFBQTtFQUNBLHlCQUFBO0FBUE47O0FBU007RUFDRSxvQkFBQTtFQUNBLGtCQUFBO0VBRUEsV0FBQTtFQUNBLGVBQUE7RUFFQSxlQUFBO0FBVFI7O0FBY0U7RUFDRSw4QkFBQTtFQUVBLDhCQUFBO0VBQ0EsK0JBQUE7RUFFQSx5REFBQTtBQWRKOztBQWdCSTtFQUNFLGFBQUE7RUFDQSw4QkFBQTtFQUNBLDRDQUFBO0FBZE47O0FBZ0JNO0VBQ0UsZUFBQTtFQUNBLGdCQUFBO0VBQ0EscUJBQUE7RUFDQSw4QkFBQTtBQWRSOztBQWlCTTtFQUNFLGVBQUE7RUFDQSxnQkFBQTtFQUNBLHFCQUFBO0VBQ0EsOEJBQUE7QUFmUjs7QUFtQkk7RUFFRSx5Q0FBQTtFQUVBLGdCQUFBO0VBQ0EsbUJBQUE7QUFuQk47O0FBd0JBO0VBQ0Usb0NBQUE7QUFyQkY7O0FBd0JJO0VBQ0UsbUNBQUE7RUFDQSxrQkFBQTtFQUNBLDBCQUFBO0VBQ0EsdUNBQUE7RUFDQSxpREFBQTtFQUNBLG9EQUFBO0FBdEJOOztBQXdCTTtFQUNFLDhCQUFBO0VBRUEsV0FBQTtBQXZCUjs7QUF5QlE7RUFDRSxpREFBQTtVQUFBLGdEQUFBO0FBdkJWOztBQTBCUTtFQUNFLG1EQUFBO1VBQUEsa0RBQUE7RUFDQSxhQUFBO0VBQ0Esc0JBQUE7RUFDQSx1QkFBQTtBQXhCVjs7QUEyQlk7RUFDRSwyQ0FBQTtBQXpCZDs7QUE0Qlk7RUFDRSxXQUFBO0VBQ0EsMkNBQUE7RUFDQSxlQUFBO0FBMUJkOztBQTZCWTtFQUNFLFdBQUE7RUFDQSwyQ0FBQTtFQUNBLGVBQUE7QUEzQmQ7O0FBbUNFO0VBQ0Usb0NBQUE7RUFDQSwyQ0FBQTtFQUNBLGtCQUFBO0FBakNKOztBQXFDQTtFQUNFLG9CQUFBO0VBQ0EsZUFBQTtFQUNBLG1CQUFBO0VBQ0Esb0JBQUE7RUFDQSxrQkFBQTtFQUNBLGlCQUFBO0VBQ0Esc0JBQUE7RUFDQSxnQkFBQTtFQUNBLGNBQUE7RUFDQSxpQkFBQTtBQWxDRjs7QUFtQ0U7RUFDRSxhQUFBO0FBakNKOztBQXFDQTtFQUNFLGFBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSxZQUFBO0FBbENGOztBQW9DRTtFQUNFLG9CQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0VBQ0Esb0JBQUE7RUFDQSxrQkFBQTtFQUNBLGlCQUFBO0VBQ0Esc0JBQUE7RUFDQSxjQUFBO0VBQ0EsV0FBQTtBQWxDSjs7QUFvQ0U7RUFDRSxXQUFBO0VBQ0Esb0JBQUE7RUFDQSxlQUFBO0VBQ0EsbUJBQUE7RUFDQSxvQkFBQTtFQUNBLGtCQUFBO0VBQ0EsaUJBQUE7RUFDQSxzQkFBQTtFQUNBLGtCQUFBO0VBQ0EsY0FBQTtBQWxDSiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2hvbWUvaG9tZS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIvLyBDdXN0b20gdmFyaWFibGVzXHJcbi8vIE5vdGU6ICBUaGVzZSBvbmVzIHdlcmUgYWRkZWQgYnkgdXMgYW5kIGhhdmUgbm90aGluZyB0byBkbyB3aXRoIElvbmljIENTUyBDdXN0b20gUHJvcGVydGllc1xyXG46aG9zdCB7XHJcbiAgLS1wYWdlLW1hcmdpbjogdmFyKC0tYXBwLWZhaXItbWFyZ2luKTtcclxuICAtLXBhZ2UtYmFja2dyb3VuZDogdmFyKC0tYXBwLWJhY2tncm91bmQpO1xyXG4gIC0tdG9vbGJhci1iYWNrZ3JvdW5kOiAjMWQyYTY0O1xyXG4gIC0taW9uLXRvb2xiYXItYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XHJcbn1cclxuXHJcbi5oZWFkZXItdG9vbGJhciB7XHJcbiAgcGFkZGluZzogMTZweDtcclxufVxyXG5cclxuLy8gTm90ZTogIEFsbCB0aGUgQ1NTIHZhcmlhYmxlcyBkZWZpbmVkIGJlbG93IGFyZSBvdmVycmlkZXMgb2YgSW9uaWMgZWxlbWVudHMgQ1NTIEN1c3RvbSBQcm9wZXJ0aWVzXHJcbi5maWx0ZXJzLXRvb2xiYXIge1xyXG4gIHBhZGRpbmc6IDE0cHg7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2U3ZjBmNztcclxuXHJcbiAgLnNlYXJjaGJhci1yb3cge1xyXG4gICAgLS1pb24tZ3JpZC1jb2x1bW4tcGFkZGluZzogMHB4O1xyXG5cclxuICAgIGlvbi1zZWFyY2hiYXIuaXRlbXMtc2VhcmNoYmFyIHtcclxuICAgICAgcGFkZGluZzogMHB4O1xyXG5cclxuICAgICAgLy8gb3ZlcnJpZGUgSW9uaWMgZml4ZWQgaGVpZ2h0XHJcbiAgICAgIGhlaWdodDogMTAwJTtcclxuICAgICAgLy8gTGVhcm4gbW9yZSBhYm91dCBDU1MgY29udGFpbiBwcm9wZXJ0eSBoZXJlOiBodHRwczovL3Rlcm12YWRlci5naXRodWIuaW8vY3NzLWNvbnRhaW4vXHJcbiAgICAgIGNvbnRhaW46IGNvbnRlbnQ7XHJcbiAgICAgIGJvcmRlci1yYWRpdXM6IDE2cHg7XHJcbiAgICAgIC0tYmFja2dyb3VuZDogI2ZmZmZmZjtcclxuICAgIH1cclxuXHJcbiAgICAuY2FsbC10by1hY3Rpb24tY29sIHtcclxuICAgICAgcGFkZGluZy1pbmxpbmUtc3RhcnQ6IHZhcigtLXBhZ2UtbWFyZ2luKTtcclxuICAgICAgbWF4LXdpZHRoOiBmaXQtY29udGVudDtcclxuICAgICAgZmxleC1zaHJpbms6IDA7XHJcbiAgICAgIGZsZXgtZ3JvdzogMDtcclxuXHJcbiAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgIGp1c3RpZnktY29udGVudDogZmxleC1lbmQ7XHJcblxyXG4gICAgICAuZmlsdGVycy1idG4ge1xyXG4gICAgICAgIC0tcGFkZGluZy1zdGFydDogMHB4O1xyXG4gICAgICAgIC0tcGFkZGluZy1lbmQ6IDBweDtcclxuXHJcbiAgICAgICAgbWFyZ2luOiAwcHg7XHJcbiAgICAgICAgZm9udC1zaXplOiAxOHB4O1xyXG4gICAgICAgIC8vIG92ZXJyaWRlIElvbmljIGZpeGVkIGhlaWdodFxyXG4gICAgICAgIGhlaWdodDogaW5pdGlhbDtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgLnJhbmdlLWl0ZW0tcm93IHtcclxuICAgIC0taW9uLWdyaWQtY29sdW1uLXBhZGRpbmc6IDBweDtcclxuXHJcbiAgICBtYXJnaW4tdG9wOiB2YXIoLS1wYWdlLW1hcmdpbik7XHJcbiAgICBwYWRkaW5nLXRvcDogdmFyKC0tcGFnZS1tYXJnaW4pO1xyXG4gICAgLy8gYm94LXNoYWRvdyBhdCB0aGUgdG9wXHJcbiAgICBib3gtc2hhZG93OiBpbnNldCAwIDZweCAzcHggLThweCB2YXIoLS1pb24tY29sb3ItZGFya2VzdCk7XHJcblxyXG4gICAgLnJhbmdlLWhlYWRlciB7XHJcbiAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxuICAgICAgcGFkZGluZy1ib3R0b206IGNhbGModmFyKC0tcGFnZS1tYXJnaW4pIC8gMik7XHJcblxyXG4gICAgICAucmFuZ2UtdmFsdWUge1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgICAgICBmb250LXdlaWdodDogNjAwO1xyXG4gICAgICAgIGxldHRlci1zcGFjaW5nOiAwLjJweDtcclxuICAgICAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLW1lZGl1bSk7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIC5yYW5nZS1sYWJlbCB7XHJcbiAgICAgICAgZm9udC1zaXplOiAxNHB4O1xyXG4gICAgICAgIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbiAgICAgICAgbGV0dGVyLXNwYWNpbmc6IDAuMnB4O1xyXG4gICAgICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItbWVkaXVtKTtcclxuICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC5yYW5nZS1jb250cm9sIHtcclxuICAgICAgLy8gb3ZlcnJpZGUgdGhlIHBpbiBjb2xvclxyXG4gICAgICAtLWlvbi10ZXh0LWNvbG9yOiB2YXIoLS1pb24tY29sb3ItbWVkaXVtKTtcclxuXHJcbiAgICAgIHBhZGRpbmctdG9wOiAwcHg7XHJcbiAgICAgIHBhZGRpbmctYm90dG9tOiAwcHg7XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcblxyXG4uYm9hcmRzLWxpc3RpbmctY29udGVudCB7XHJcbiAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1wYWdlLWJhY2tncm91bmQpO1xyXG5cclxuICAuaXRlbXMtbGlzdCB7XHJcbiAgICAubGlzdC1pdGVtIHtcclxuICAgICAgLS1wYWRkaW5nLXN0YXJ0OiB2YXIoLS1wYWdlLW1hcmdpbik7XHJcbiAgICAgIC0tcGFkZGluZy1lbmQ6IDBweDtcclxuICAgICAgLS1pbm5lci1wYWRkaW5nLXN0YXJ0OiAwcHg7XHJcbiAgICAgIC0taW5uZXItcGFkZGluZy1lbmQ6IHZhcigtLXBhZ2UtbWFyZ2luKTtcclxuICAgICAgLS1pbm5lci1wYWRkaW5nLXRvcDogY2FsYyh2YXIoLS1wYWdlLW1hcmdpbikgLyAyKTtcclxuICAgICAgLS1pbm5lci1wYWRkaW5nLWJvdHRvbTogY2FsYyh2YXIoLS1wYWdlLW1hcmdpbikgLyAyKTtcclxuXHJcbiAgICAgIC51c2VyLXJvdyB7XHJcbiAgICAgICAgLS1pb24tZ3JpZC1jb2x1bW4tcGFkZGluZzogMHB4O1xyXG5cclxuICAgICAgICB3aWR0aDogMTAwJTtcclxuXHJcbiAgICAgICAgLnVzZXItaW1hZ2Utd3JhcHBlciB7XHJcbiAgICAgICAgICBwYWRkaW5nLWlubGluZS1lbmQ6IGNhbGModmFyKC0tcGFnZS1tYXJnaW4pIC8gMik7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAudXNlci1kYXRhLXdyYXBwZXIge1xyXG4gICAgICAgICAgcGFkZGluZy1pbmxpbmUtc3RhcnQ6IGNhbGModmFyKC0tcGFnZS1tYXJnaW4pIC8gMik7XHJcbiAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG5cclxuICAgICAgICAgIC51c2VyLWluZm8ge1xyXG4gICAgICAgICAgICAmOm5vdCg6bGFzdC1jaGlsZCkge1xyXG4gICAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IGNhbGModmFyKC0tcGFnZS1tYXJnaW4pIC8gMik7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIC51c2VyLW5hbWUge1xyXG4gICAgICAgICAgICAgIG1hcmdpbjogMHB4O1xyXG4gICAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IGNhbGModmFyKC0tcGFnZS1tYXJnaW4pIC8gNCk7XHJcbiAgICAgICAgICAgICAgZm9udC1zaXplOiAxNnB4O1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAudXNlci1hZ2Uge1xyXG4gICAgICAgICAgICAgIG1hcmdpbjogMHB4O1xyXG4gICAgICAgICAgICAgIGNvbG9yOiByZ2JhKHZhcigtLWlvbi1jb2xvci1kYXJrLXJnYiksIDAuNCk7XHJcbiAgICAgICAgICAgICAgZm9udC1zaXplOiAxNHB4O1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICAuZW1wdHktbGlzdC1tZXNzYWdlIHtcclxuICAgIG1hcmdpbjogY2FsYyh2YXIoLS1wYWdlLW1hcmdpbikgKiAzKTtcclxuICAgIGNvbG9yOiByZ2JhKHZhcigtLWlvbi1jb2xvci1kYXJrLXJnYiksIDAuNCk7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgfVxyXG59XHJcblxyXG4uY29udGVudC1oZWFkZXIge1xyXG4gIGZvbnQtZmFtaWx5OiBcIkludGVyXCI7XHJcbiAgZm9udC1zaXplOiAyOHB4O1xyXG4gIGZvbnQtd2VpZ2h0OiBub3JtYWw7XHJcbiAgZm9udC1zdHJldGNoOiBub3JtYWw7XHJcbiAgZm9udC1zdHlsZTogbm9ybWFsO1xyXG4gIGxpbmUtaGVpZ2h0OiAxLjIxO1xyXG4gIGxldHRlci1zcGFjaW5nOiBub3JtYWw7XHJcbiAgdGV4dC1hbGlnbjogbGVmdDtcclxuICBjb2xvcjogIzI2MzIzODtcclxuICBwYWRkaW5nLWJvdHRvbTogMDtcclxuICBoMSB7XHJcbiAgICBtYXJnaW4tdG9wOiAwO1xyXG4gIH1cclxufVxyXG5cclxuLnNwaW4ge1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICBwbGFjZS1pdGVtczogY2VudGVyO1xyXG4gIGhlaWdodDogNTB2aDtcclxuXHJcbiAgaDUge1xyXG4gICAgZm9udC1mYW1pbHk6IFwiSW50ZXJcIjtcclxuICAgIGZvbnQtc2l6ZTogMjBweDtcclxuICAgIGZvbnQtd2VpZ2h0OiAzMDA7XHJcbiAgICBmb250LXN0cmV0Y2g6IG5vcm1hbDtcclxuICAgIGZvbnQtc3R5bGU6IG5vcm1hbDtcclxuICAgIGxpbmUtaGVpZ2h0OiAxLjI1O1xyXG4gICAgbGV0dGVyLXNwYWNpbmc6IG5vcm1hbDtcclxuICAgIGNvbG9yOiAjMjYzMjM4O1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgfVxyXG4gIHAge1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBmb250LWZhbWlseTogXCJJbnRlclwiO1xyXG4gICAgZm9udC1zaXplOiAxM3B4O1xyXG4gICAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcclxuICAgIGZvbnQtc3RyZXRjaDogbm9ybWFsO1xyXG4gICAgZm9udC1zdHlsZTogbm9ybWFsO1xyXG4gICAgbGluZS1oZWlnaHQ6IDEuNTQ7XHJcbiAgICBsZXR0ZXItc3BhY2luZzogbm9ybWFsO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgY29sb3I6ICM4ZGExYjU7XHJcbiAgfVxyXG59XHJcbiJdfQ== */");

/***/ }),

/***/ "./src/app/pages/home/home.page.ts":
/*!*****************************************!*\
  !*** ./src/app/pages/home/home.page.ts ***!
  \*****************************************/
/*! exports provided: HomePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePage", function() { return HomePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _core_services__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @core/services */ "./src/app/core/services/index.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _shared_lib_services__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @shared-lib/services */ "./src/app/shared/services/index.ts");





let HomePage = class HomePage {
    constructor(authService, boardService, searchService, languageService) {
        this.authService = authService;
        this.boardService = boardService;
        this.searchService = searchService;
        this.languageService = languageService;
        this.searchLoader = false;
    }
    ngOnInit() {
        this.searchQuery = "";
        const { uid } = this.authService.currentUser || {};
        if (uid) {
            this.boardService.setUserId(uid);
            this.boards$ = this.boardService.getBoards(uid);
        }
        this.searchInputValueSubscription = this.searchService.searchedWord$.subscribe((searched) => {
            this.searchQuery = searched;
        });
        this.handleDefaultLanguage();
    }
    ngOnDestroy() {
        if (this.searchInputSubscription) {
            this.searchInputSubscription.unsubscribe();
        }
        if (this.searchInputValueSubscription) {
            this.searchInputValueSubscription.unsubscribe();
        }
    }
    ngAfterViewInit() { }
    refresh(ev) {
        setTimeout(() => {
            ev.detail.complete();
        }, 3000);
    }
    searchList(event) {
        this.searchService.setSearchedWord(event.detail.value);
        this.searchLoader = false;
    }
    handleDefaultLanguage() {
        this.languageService.setTranslations();
    }
};
HomePage.ctorParameters = () => [
    { type: _core_services__WEBPACK_IMPORTED_MODULE_2__["FirebaseAuthService"] },
    { type: _core_services__WEBPACK_IMPORTED_MODULE_2__["BoardsService"] },
    { type: _shared_lib_services__WEBPACK_IMPORTED_MODULE_4__["SearchService"] },
    { type: _core_services__WEBPACK_IMPORTED_MODULE_2__["LanguageService"] }
];
HomePage.propDecorators = {
    refresher: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"], args: ["refresher",] }],
    ngOnDestroy: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"], args: ["window:beforeunload",] }]
};
HomePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "app-home",
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./home.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/home/home.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./home.page.scss */ "./src/app/pages/home/home.page.scss")).default]
    }),
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_core_services__WEBPACK_IMPORTED_MODULE_2__["FirebaseAuthService"],
        _core_services__WEBPACK_IMPORTED_MODULE_2__["BoardsService"],
        _shared_lib_services__WEBPACK_IMPORTED_MODULE_4__["SearchService"],
        _core_services__WEBPACK_IMPORTED_MODULE_2__["LanguageService"]])
], HomePage);



/***/ })

}]);
//# sourceMappingURL=pages-home-home-module-es2015.js.map