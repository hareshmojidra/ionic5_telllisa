(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-recordings-recordings-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/recordings/recordings.page.html":
    /*!*********************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/recordings/recordings.page.html ***!
      \*********************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppPagesRecordingsRecordingsPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header>\r\n  <ion-toolbar color=\"skip-btn\" class=\"header-toolbar\">\r\n    <ion-buttons slot=\"start\">\r\n      <ion-back-button></ion-back-button>\r\n      <!-- <ion-menu-button color=\"light\"></ion-menu-button> -->\r\n    </ion-buttons>\r\n    <app-aspect-ratio [ratio]=\"{w:100, h:12}\">\r\n      <app-image-shell style=\"width:150px; height: 34px; margin: 0 auto;\" animation=\"spinner\" [src]=\"'assets/tl-logo-simpel-white.png'\" [alt]=\"'walkthrough'\"></app-image-shell>\r\n    </app-aspect-ratio>\r\n    <ion-buttons slot=\"end\">\r\n      <div>&nbsp;</div>\r\n    </ion-buttons>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <div class=\"ion-padding content-header\">\r\n    <ng-container *ngIf=\"board\">\r\n      <h1>{{ board?.name }}</h1>\r\n      <h5>Recordings</h5>\r\n    </ng-container>\r\n  </div>\r\n  <ion-list lines=\"none\">\r\n\r\n    <ng-container *ngIf=\"messages$ | async; let messages; else LoadingSpinner;\">\r\n      <ion-item [detail]=\"false\" *ngFor=\"let message of messages\">\r\n        <ion-label class=\"ion-padding\">\r\n          <h2>{{ message?.title.toDate() | date: 'medium'}}</h2>\r\n          <p>{{ message?.text }}</p>\r\n        </ion-label>\r\n      </ion-item>\r\n    </ng-container>\r\n\r\n<ng-template #LoadingSpinner>\r\n  <div class=\"spin\">\r\n    <ion-spinner name=\"bubbles\"></ion-spinner>\r\n </div>\r\n</ng-template>\r\n  </ion-list>\r\n</ion-content>\r\n";
      /***/
    },

    /***/
    "./src/app/pages/recordings/recordings-routing.module.ts":
    /*!***************************************************************!*\
      !*** ./src/app/pages/recordings/recordings-routing.module.ts ***!
      \***************************************************************/

    /*! exports provided: RecordingsPageRoutingModule */

    /***/
    function srcAppPagesRecordingsRecordingsRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "RecordingsPageRoutingModule", function () {
        return RecordingsPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _recordings_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./recordings.page */
      "./src/app/pages/recordings/recordings.page.ts");

      var routes = [{
        path: '',
        component: _recordings_page__WEBPACK_IMPORTED_MODULE_3__["RecordingsPage"]
      }, {
        path: 'create-record',
        loadChildren: function loadChildren() {
          return __webpack_require__.e(
          /*! import() | create-record-create-record-module */
          "create-record-create-record-module").then(__webpack_require__.bind(null,
          /*! ./create-record/create-record.module */
          "./src/app/pages/recordings/create-record/create-record.module.ts")).then(function (m) {
            return m.CreateRecordPageModule;
          });
        }
      }];

      var RecordingsPageRoutingModule = function RecordingsPageRoutingModule() {
        _classCallCheck(this, RecordingsPageRoutingModule);
      };

      RecordingsPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], RecordingsPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/pages/recordings/recordings.module.ts":
    /*!*******************************************************!*\
      !*** ./src/app/pages/recordings/recordings.module.ts ***!
      \*******************************************************/

    /*! exports provided: RecordingsPageModule */

    /***/
    function srcAppPagesRecordingsRecordingsModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "RecordingsPageModule", function () {
        return RecordingsPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _shared_lib_shared_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @shared-lib/shared.module */
      "./src/app/shared/shared.module.ts");
      /* harmony import */


      var _recordings_routing_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./recordings-routing.module */
      "./src/app/pages/recordings/recordings-routing.module.ts");
      /* harmony import */


      var _recordings_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ./recordings.page */
      "./src/app/pages/recordings/recordings.page.ts");

      var RecordingsPageModule = function RecordingsPageModule() {
        _classCallCheck(this, RecordingsPageModule);
      };

      RecordingsPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _recordings_routing_module__WEBPACK_IMPORTED_MODULE_6__["RecordingsPageRoutingModule"], _shared_lib_shared_module__WEBPACK_IMPORTED_MODULE_5__["SharedModule"]],
        declarations: [_recordings_page__WEBPACK_IMPORTED_MODULE_7__["RecordingsPage"]]
      })], RecordingsPageModule);
      /***/
    },

    /***/
    "./src/app/pages/recordings/recordings.page.scss":
    /*!*******************************************************!*\
      !*** ./src/app/pages/recordings/recordings.page.scss ***!
      \*******************************************************/

    /*! exports provided: default */

    /***/
    function srcAppPagesRecordingsRecordingsPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "ion-item {\n  --padding-start: 0;\n  --inner-padding-end: 0;\n  margin: 6px 12px;\n  border-radius: 16px;\n  border: solid 1px #e2e6f1;\n  background-color: #fbfcfd;\n}\nion-item ion-label {\n  margin-inline: 0;\n}\n.content-header {\n  font-family: \"Inter\";\n  font-size: 28px;\n  font-weight: normal;\n  font-stretch: normal;\n  font-style: normal;\n  line-height: 1.21;\n  letter-spacing: normal;\n  text-align: left;\n  color: #263238;\n  padding-bottom: 0;\n}\n.content-header h1 {\n  margin-top: 0;\n}\n.spin {\n  display: flex;\n  align-items: center;\n  justify-content: center;\n  place-items: center;\n  height: 50vh;\n}\n.spin h5 {\n  font-family: \"Inter\";\n  font-size: 20px;\n  font-weight: 300;\n  font-stretch: normal;\n  font-style: normal;\n  line-height: 1.25;\n  letter-spacing: normal;\n  color: #263238;\n  width: 100%;\n}\n.spin p {\n  width: 100%;\n  font-family: \"Inter\";\n  font-size: 13px;\n  font-weight: normal;\n  font-stretch: normal;\n  font-style: normal;\n  line-height: 1.54;\n  letter-spacing: normal;\n  text-align: center;\n  color: #8da1b5;\n}\n.header-toolbar {\n  padding: 16px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvcmVjb3JkaW5ncy9yZWNvcmRpbmdzLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGtCQUFBO0VBQ0Esc0JBQUE7RUFDQSxnQkFBQTtFQUNBLG1CQUFBO0VBQ0EseUJBQUE7RUFDQSx5QkFBQTtBQUNGO0FBQUU7RUFDRSxnQkFBQTtBQUVKO0FBRUE7RUFDRSxvQkFBQTtFQUNBLGVBQUE7RUFDQSxtQkFBQTtFQUNBLG9CQUFBO0VBQ0Esa0JBQUE7RUFDQSxpQkFBQTtFQUNBLHNCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxjQUFBO0VBQ0EsaUJBQUE7QUFDRjtBQUFFO0VBQ0UsYUFBQTtBQUVKO0FBRUE7RUFDRSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0EsWUFBQTtBQUNGO0FBQ0U7RUFDRSxvQkFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtFQUNBLG9CQUFBO0VBQ0Esa0JBQUE7RUFDQSxpQkFBQTtFQUNBLHNCQUFBO0VBQ0EsY0FBQTtFQUNBLFdBQUE7QUFDSjtBQUNFO0VBQ0UsV0FBQTtFQUNBLG9CQUFBO0VBQ0EsZUFBQTtFQUNBLG1CQUFBO0VBQ0Esb0JBQUE7RUFDQSxrQkFBQTtFQUNBLGlCQUFBO0VBQ0Esc0JBQUE7RUFDQSxrQkFBQTtFQUNBLGNBQUE7QUFDSjtBQUdBO0VBQ0UsYUFBQTtBQUFGIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvcmVjb3JkaW5ncy9yZWNvcmRpbmdzLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1pdGVtIHtcclxuICAtLXBhZGRpbmctc3RhcnQ6IDA7XHJcbiAgLS1pbm5lci1wYWRkaW5nLWVuZDogMDtcclxuICBtYXJnaW46IDZweCAxMnB4O1xyXG4gIGJvcmRlci1yYWRpdXM6IDE2cHg7XHJcbiAgYm9yZGVyOiBzb2xpZCAxcHggI2UyZTZmMTtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmJmY2ZkO1xyXG4gIGlvbi1sYWJlbCB7XHJcbiAgICBtYXJnaW4taW5saW5lOiAwO1xyXG4gIH1cclxufVxyXG5cclxuLmNvbnRlbnQtaGVhZGVyIHtcclxuICBmb250LWZhbWlseTogXCJJbnRlclwiO1xyXG4gIGZvbnQtc2l6ZTogMjhweDtcclxuICBmb250LXdlaWdodDogbm9ybWFsO1xyXG4gIGZvbnQtc3RyZXRjaDogbm9ybWFsO1xyXG4gIGZvbnQtc3R5bGU6IG5vcm1hbDtcclxuICBsaW5lLWhlaWdodDogMS4yMTtcclxuICBsZXR0ZXItc3BhY2luZzogbm9ybWFsO1xyXG4gIHRleHQtYWxpZ246IGxlZnQ7XHJcbiAgY29sb3I6ICMyNjMyMzg7XHJcbiAgcGFkZGluZy1ib3R0b206IDA7XHJcbiAgaDEge1xyXG4gICAgbWFyZ2luLXRvcDogMDtcclxuICB9XHJcbn1cclxuXHJcbi5zcGluIHtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgcGxhY2UtaXRlbXM6IGNlbnRlcjtcclxuICBoZWlnaHQ6IDUwdmg7XHJcblxyXG4gIGg1IHtcclxuICAgIGZvbnQtZmFtaWx5OiBcIkludGVyXCI7XHJcbiAgICBmb250LXNpemU6IDIwcHg7XHJcbiAgICBmb250LXdlaWdodDogMzAwO1xyXG4gICAgZm9udC1zdHJldGNoOiBub3JtYWw7XHJcbiAgICBmb250LXN0eWxlOiBub3JtYWw7XHJcbiAgICBsaW5lLWhlaWdodDogMS4yNTtcclxuICAgIGxldHRlci1zcGFjaW5nOiBub3JtYWw7XHJcbiAgICBjb2xvcjogIzI2MzIzODtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gIH1cclxuICBwIHtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgZm9udC1mYW1pbHk6IFwiSW50ZXJcIjtcclxuICAgIGZvbnQtc2l6ZTogMTNweDtcclxuICAgIGZvbnQtd2VpZ2h0OiBub3JtYWw7XHJcbiAgICBmb250LXN0cmV0Y2g6IG5vcm1hbDtcclxuICAgIGZvbnQtc3R5bGU6IG5vcm1hbDtcclxuICAgIGxpbmUtaGVpZ2h0OiAxLjU0O1xyXG4gICAgbGV0dGVyLXNwYWNpbmc6IG5vcm1hbDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIGNvbG9yOiAjOGRhMWI1O1xyXG4gIH1cclxufVxyXG5cclxuLmhlYWRlci10b29sYmFyIHtcclxuICBwYWRkaW5nOiAxNnB4O1xyXG59XHJcbiJdfQ== */";
      /***/
    },

    /***/
    "./src/app/pages/recordings/recordings.page.ts":
    /*!*****************************************************!*\
      !*** ./src/app/pages/recordings/recordings.page.ts ***!
      \*****************************************************/

    /*! exports provided: RecordingsPage */

    /***/
    function srcAppPagesRecordingsRecordingsPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "RecordingsPage", function () {
        return RecordingsPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _core_services__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @core/services */
      "./src/app/core/services/index.ts");
      /* harmony import */


      var lodash__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! lodash */
      "./node_modules/lodash/lodash.js");
      /* harmony import */


      var lodash__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_4__);

      var RecordingsPage = /*#__PURE__*/function () {
        function RecordingsPage(recordingsService, messagesService, router) {
          _classCallCheck(this, RecordingsPage);

          this.recordingsService = recordingsService;
          this.messagesService = messagesService;
          this.router = router;
        }

        _createClass(RecordingsPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            this.board = this.messagesService.getBoard;

            if (Object(lodash__WEBPACK_IMPORTED_MODULE_4__["isEmpty"])(this.board)) {
              this.router.navigate(["home"], {
                replaceUrl: true
              });
            }

            this.messages$ = this.messagesService.getMessages();
          }
        }, {
          key: "ngOnDestroy",
          value: function ngOnDestroy() {
            this.recordingsService.recordings = null;
          }
        }]);

        return RecordingsPage;
      }();

      RecordingsPage.ctorParameters = function () {
        return [{
          type: _core_services__WEBPACK_IMPORTED_MODULE_3__["RecordingsService"]
        }, {
          type: _core_services__WEBPACK_IMPORTED_MODULE_3__["MessagesService"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
        }];
      };

      RecordingsPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "app-recordings",
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./recordings.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/recordings/recordings.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./recordings.page.scss */
        "./src/app/pages/recordings/recordings.page.scss"))["default"]]
      }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_core_services__WEBPACK_IMPORTED_MODULE_3__["RecordingsService"], _core_services__WEBPACK_IMPORTED_MODULE_3__["MessagesService"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])], RecordingsPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=pages-recordings-recordings-module-es5.js.map