(function () {
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-auth-auth-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/auth/login/login.page.html":
    /*!****************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/auth/login/login.page.html ***!
      \****************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppPagesAuthLoginLoginPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-content class=\"login-content\">\r\n  <app-aspect-ratio [ratio]=\"{w:100, h:17}\">\r\n    <app-image-shell style=\"width:150px; height: 34px; margin: 0 auto;\" animation=\"spinner\"\r\n      [src]=\"'assets/tl-logo-simpel-white.png'\" [alt]=\"'walkthrough'\"></app-image-shell>\r\n  </app-aspect-ratio>\r\n\r\n  <div class=\"form-container\">\r\n    <h2 class=\"auth-title\">\r\n      Hey there! Welcome back.\r\n    </h2>\r\n    <p>New here? <a [routerLink]=\"['/auth/signup']\">Create an account.</a></p>\r\n    <form [formGroup]=\"loginForm\" (ngSubmit)=\"signInWithEmail()\">\r\n      <ion-list class=\"inputs-list\" lines=\"none\">\r\n        <ion-item class=\"input-item\">\r\n          <ion-label position=\"stacked\">\r\n            <p>Email</p>\r\n          </ion-label>\r\n          <ion-input [(ngModel)]=\"email\" autocomplete=\"off\" (click)=\"$event.target.select()\" type=\"email\"\r\n            placeholder=\"name@domain.com\" formControlName=\"email\" clearInput autocapitalize=\"off\" inputmode=\"email\">\r\n          </ion-input>\r\n        </ion-item>\r\n        <div class=\"error-container\">\r\n          <ng-container *ngFor=\"let validation of validation_messages.email\">\r\n            <div class=\"error-message\"\r\n              *ngIf=\"loginForm.get('email').hasError(validation.type) && (loginForm.get('email').dirty || loginForm.get('email').touched)\">\r\n              <ion-icon name=\"information-circle-outline\"></ion-icon>\r\n              <span>{{ validation.message }}</span>\r\n            </div>\r\n          </ng-container>\r\n        </div>\r\n        <div class=\"label-row\">\r\n          <ion-label position=\"stacked\">\r\n            <p>Password <a class=\"pulled-right\">Forgot your password?</a></p>\r\n          </ion-label>\r\n        </div>\r\n\r\n        <ion-item class=\"tl-auth-item\">\r\n          <app-show-hide-password>\r\n            <ion-input autocomplete=\"off\" (click)=\"$event.target.select()\" type=\"password\"\r\n              placeholder=\"at least 8 characters\" formControlName=\"password\"></ion-input>\r\n          </app-show-hide-password>\r\n        </ion-item>\r\n        <div class=\"error-container\">\r\n          <ng-container *ngFor=\"let validation of validation_messages.password\">\r\n            <div class=\"error-message\"\r\n              *ngIf=\"loginForm.get('password').hasError(validation.type) && (loginForm.get('password').dirty || loginForm.get('password').touched)\">\r\n              <ion-icon name=\"information-circle-outline\"></ion-icon>\r\n              <span>{{ validation.message }}</span>\r\n            </div>\r\n          </ng-container>\r\n        </div>\r\n      </ion-list>\r\n      <ion-button class=\"login-btn\" type=\"submit\" expand=\"block\" [disabled]=\"!loginForm.valid\">Sign in</ion-button>\r\n      <div class=\"error-container\" *ngIf=\"submitError\">\r\n        <div class=\"error-message\">\r\n          <ion-icon name=\"information-circle-outline\"></ion-icon>\r\n          <span>{{ submitError }}</span>\r\n        </div>\r\n      </div>\r\n      <div class=\"separator\">Or</div>\r\n    </form>\r\n    <!-- <div class=\"social-auth-options\">\r\n      <ion-button class=\"social-auth-btn facebook-auth-btn\" expand=\"block\" color=\"facebook\" (click)=\"doFacebookLogin()\">Log In with Facebook</ion-button>\r\n      <ion-button class=\"social-auth-btn google-auth-btn\" expand=\"block\" color=\"google\" (click)=\"doGoogleLogin()\">Log In with Google</ion-button>\r\n      <ion-button class=\"social-auth-btn twitter-auth-btn\" expand=\"block\" color=\"twitter\" (click)=\"doTwitterLogin()\">Log In with Twitter</ion-button>\r\n    </div> -->\r\n    <div class=\"social-logins d-flex justify-content-between\">\r\n      <ion-button type=\"button\" class=\"google-signin\" color=\"grey-primary\" (click)=\"doGoogleLogin()\">\r\n        <ion-icon name=\"logo-google\"></ion-icon>\r\n        <ion-label>Sign in with Google</ion-label>\r\n      </ion-button>\r\n      <ion-button type=\"button\" class=\"fb-signin\" color=\"lighter-grey\" (click)=\"doFacebookLogin()\">\r\n        <ion-icon name=\"logo-facebook\"></ion-icon>\r\n      </ion-button>\r\n      <ion-button type=\"button\" class=\"twitter-signin\" color=\"lighter-grey\" (click)=\"doTwitterLogin()\">\r\n        <ion-icon name=\"logo-twitter\"></ion-icon>\r\n      </ion-button>\r\n    </div>\r\n  </div>\r\n\r\n</ion-content>";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/auth/privacy-policy/components/privacy-english/privacy-english.component.html":
    /*!*******************************************************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/auth/privacy-policy/components/privacy-english/privacy-english.component.html ***!
      \*******************************************************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppPagesAuthPrivacyPolicyComponentsPrivacyEnglishPrivacyEnglishComponentHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<div id=\"englishPrivacy\">\r\n  <h3 class=\"legal-title\">1. An overview of data protection</h3>\r\n  <p class=\"legal-text\">\r\n    General\r\n  </p>\r\n  \r\n  <p class=\"legal-text\">\r\n    The following gives a simple overview of what happens to your personal information when you visit our website. Personal information is any data with which you could be personally identified. Detailed information on the subject of data protection can be found in our privacy policy found below.\r\n  </p>\r\n\r\n  <p class=\"legal-text\">\r\n    Data collection on our website\r\n  </p>\r\n  <p class=\"legal-text\">\r\n    Who is responsible for the data collection on this website?\r\n  </p>\r\n  <p class=\"legal-text\">\r\n    The data collected on this website are processed by the website operator. The operator’s contact details can be found in the website’s required legal notice.\r\n  </p>\r\n  <p class=\"legal-text\">\r\n    How do we collect your data?\r\n  </p>\r\n  <p class=\"legal-text\">\r\n    Some data are collected when you provide it to us. This could, for example, be data you enter on a contact form.\r\n  </p>\r\n  <p class=\"legal-text\">\r\n    Other data are collected automatically by our IT systems when you visit the website. These data are primarily technical data such as the browser and operating system you are using or when you accessed the page. These data are collected automatically as soon as you enter our website.\r\n  </p>\r\n  <p class=\"legal-text\">\r\n    What do we use your data for?\r\n  </p>\r\n  <p class=\"legal-text\">\r\n    Part of the data is collected to ensure the proper functioning of the website. Other data can be used to analyze how visitors use the site.\r\n  </p>\r\n  <p class=\"legal-text\">\r\n    What rights do you have regarding your data?\r\n  </p>\r\n  <p class=\"legal-text\">\r\n    You always have the right to request information about your stored data, its origin, its recipients, and the purpose of its collection at no charge. You also have the right to request that it be corrected, blocked, or deleted. You can contact us at any time using the address given in the legal notice if you have further questions about the issue of privacy and data protection. You may also, of course, file a complaint with the competent regulatory authorities.\r\n  </p>\r\n  <p class=\"legal-text\">\r\n    Analytics and third-party tools\r\n  </p>\r\n  <p class=\"legal-text\">\r\n    When visiting our website, statistical analyses may be made of your surfing behavior. This happens primarily using cookies and analytics. The analysis of your surfing behavior is usually anonymous, i.e. we will not be able to identify you from this data. You can object to this analysis or prevent it by not using certain tools. Details can be found in our privacy policy under the heading “Third-party modules and analytics.”\r\n  </p>\r\n  <p class=\"legal-text\">\r\n    You can object to this analysis. We will inform you below about how to exercise your options in this regard.\r\n  </p>\r\n\r\n  <h3 class=\"legal-title\">2. General information and mandatory information</h3>\r\n  \r\n  <h3 class=\"legal-title\">Data protection</h3>\r\n\r\n  <p class=\"legal-text\">\r\n    The operators of this website take the protection of your personal data very seriously. We treat your personal data as confidential and in accordance with the statutory data protection regulations and this privacy policy.\r\n  </p>\r\n  \r\n  <p class=\"legal-text\">\r\n    If you use this website, various pieces of personal data will be collected. Personal information is any data with which you could be personally identified. This privacy policy explains what information we collect and what we use it for. It also explains how and for what purpose this happens.\r\n  </p>\r\n  <p class=\"legal-text\">\r\n    Please note that data transmitted via the internet (e.g. via email communication) may be subject to security breaches. Complete protection of your data from third-party access is not possible.\r\n  </p>\r\n\r\n  <h3 class=\"legal-title\">Notice concerning the party responsible for this website</h3>\r\n\r\n  <p class=\"legal-text\">\r\n    G-TAC Software UG (haftungsbeschränkt)\r\n  </p>\r\n  \r\n  <p class=\"legal-text\">\r\n    Schmiedstrasse 7\r\n  </p>\r\n  <p class=\"legal-text\">\r\n    67734 Katzweiler\r\n  </p>\r\n  <p class=\"legal-text\">\r\n    Telephone: +49 (6301) 6049758\r\n  </p>\r\n  \r\n  <p class=\"legal-text\">\r\n    Email: suppor@telllisa.com\r\n  </p>\r\n  <p class=\"legal-text\">\r\n    The responsible party is the natural or legal person who alone or jointly with others decides on the purposes and means of processing personal data (names, email addresses, etc.).\r\n  </p>\r\n\r\n  <h3 class=\"legal-title\">Revocation of your consent to the processing of your data</h3>\r\n\r\n  <p class=\"legal-text\">\r\n    Many data processing operations are only possible with your express consent. You may revoke your consent at any time with future effect. An informal email making this request is sufficient. The data processed before we receive your request may still be legally processed.\r\n  </p>\r\n\r\n  <h3 class=\"legal-title\">Right to file complaints with regulatory authorities</h3>\r\n\r\n  <p class=\"legal-text\">\r\n    If there has been a breach of data protection legislation, the person affected may file a complaint with the competent regulatory authorities. The competent regulatory authority for matters related to data protection legislation is the data protection officer of the German state in which our company is headquartered. A list of data protection officers and their contact details can be found at the following link: https://www.bfdi.bund.de/DE/Infothek/Anschriften_Links/anschriften_links-node.html.\r\n  </p>\r\n\r\n  \r\n  <h3 class=\"legal-title\">Right to data portability</h3>\r\n\r\n  <p class=\"legal-text\">\r\n    You have the right to have data which we process based on your consent or in fulfillment of a contract automatically delivered to yourself or to a third party in a standard, machine-readable format. If you require the direct transfer of data to another responsible party, this will only be done to the extent technically feasible.\r\n  </p>\r\n\r\n  <h3 class=\"legal-title\">SSL or TLS encryption</h3>\r\n\r\n  <p class=\"legal-text\">\r\n    This site uses SSL or TLS encryption for security reasons and for the protection of the transmission of confidential content, such as the inquiries you send to us as the site operator. You can recognize an encrypted connection in your browser’s address line when it changes from “http://” to “https://” and the lock icon is displayed in your browser’s address bar.\r\n  </p>\r\n\r\n  <p class=\"legal-text\">\r\n    If SSL or TLS encryption is activated, the data you transfer to us cannot be read by third parties.\r\n  </p>\r\n\r\n  <h3 class=\"legal-title\">Encrypted payments on this website</h3>\r\n\r\n  <p class=\"legal-text\">\r\n    If you enter into a contract which requires you to send us your payment information (e.g. account number for direct debits), we will require this data to process your payment.\r\n  </p>\r\n  \r\n  <p class=\"legal-text\">\r\n    Payment transactions using common means of payment (Visa/MasterCard, direct debit) are only made via encrypted SSL or TLS connections. You can recognize an encrypted connection in your browser’s address line when it changes from “http://” to “https://” and the lock icon in your browser line is visible.\r\n  </p>\r\n\r\n  \r\n  <p class=\"legal-text\">\r\n    In the case of encrypted communication, any payment details you submit to us cannot be read by third parties.\r\n  </p>\r\n\r\n  <h3 class=\"legal-title\">Information, blocking, deletion</h3>\r\n\r\n  <p class=\"legal-text\">\r\n    As permitted by law, you have the right to be provided at any time with information free of charge about any of your personal data that is stored as well as its origin, the recipient and the purpose for which it has been processed. You also have the right to have this data corrected, blocked or deleted. You can contact us at any time using the address given in our legal notice if you have further questions on the topic of personal data.\r\n  </p>\r\n\r\n  <h3 class=\"legal-title\">Opposition to promotional emails</h3>\r\n\r\n  <p class=\"legal-text\">\r\n    We hereby expressly prohibit the use of contact data published in the context of website legal notice requirements with regard to sending promotional and informational materials not expressly requested. The website operator reserves the right to take specific legal action if unsolicited advertising material, such as email spam, is received.\r\n  </p>\r\n\r\n  <h3 class=\"legal-title\">3. Data collection on our website</h3>\r\n  <h3 class=\"legal-title\">Cookies</h3>\r\n\r\n  <p class=\"legal-text\">\r\n    Some of our web pages use cookies. Cookies do not harm your computer and do not contain any viruses. Cookies help make our website more user-friendly, efficient, and secure. Cookies are small text files that are stored on your computer and saved by your browser.\r\n  </p>\r\n\r\n  <p class=\"legal-text\">\r\n    Most of the cookies we use are so-called “session cookies.” They are automatically deleted after your visit. Other cookies remain in your device’s memory until you delete them. These cookies make it possible to recognize your browser when you next visit the site.\r\n  </p>\r\n\r\n  <p class=\"legal-text\">\r\n    You can configure your browser to inform you about the use of cookies so that you can decide on a case-by-case basis whether to accept or reject a cookie. Alternatively, your browser can be configured to automatically accept cookies under certain conditions or to always reject them, or to automatically delete cookies when closing your browser. Disabling cookies may limit the functionality of this website.\r\n  </p>\r\n  \r\n  <p class=\"legal-text\">\r\n    Cookies which are necessary to allow electronic communications or to provide certain functions you wish to use (such as the shopping cart) are stored pursuant to Art. 6 paragraph 1, letter f of GDPR. The website operator has a legitimate interest in the storage of cookies to ensure an optimized service provided free of technical errors. If other cookies (such as those used to analyze your surfing behavior) are also stored, they will be treated separately in this privacy policy.\r\n  </p>\r\n  <p class=\"legal-text\">\r\n    To manage your cookie consent:\r\n  </p>\r\n\r\n  <ion-button>\r\n    Revoke Consent\r\n  </ion-button>\r\n\r\n  <h3 class=\"legal-title\">Server log files</h3>\r\n\r\n  <p class=\"legal-text\">\r\n    The website provider automatically collects and stores information that your browser automatically transmits to us in “server log files”. These are:\r\n  </p>\r\n\r\n  <ul>\r\n    <li>\r\n      Browser type and browser version\r\n    </li>\r\n    <li>\r\n      Operating system used\r\n    </li>\r\n    <li>\r\n      Referrer URL\r\n    </li>\r\n    <li>\r\n      Host name of the accessing computer\r\n    </li>\r\n    <li>\r\n      Time of the server request\r\n    </li>\r\n    <li>\r\n      IP address\r\n    </li>\r\n  </ul>\r\n\r\n  <p class=\"legal-text\">\r\n    These data will not be combined with data from other sources.\r\n  </p>\r\n  <p class=\"legal-text\">\r\n    The basis for data processing is Art. 6 (1) (b) GDPR, which allows the processing of data to fulfill a contract or for measures preliminary to a contract.\r\n  </p>\r\n\r\n  <h3 class=\"legal-title\">\r\n    Contact form\r\n  </h3>\r\n\r\n  <p class=\"legal-text\">\r\n    Should you send us questions via the contact form, we will collect the data entered on the form, including the contact details you provide, to answer your question and any follow-up questions. We do not share this information without your permission.\r\n  </p>\r\n  <p class=\"legal-text\">\r\n    We will, therefore, process any data you enter onto the contact form only with your consent per Art. 6 (1)(a) GDPR. You may revoke your consent at any time. An informal email making this request is sufficient. The data processed before we receive your request may still be legally processed.\r\n  </p>\r\n  \r\n  <p class=\"legal-text\">\r\n    We will retain the data you provide on the contact form until you request its deletion, revoke your consent for its storage, or the purpose for its storage no longer pertains (e.g. after fulfilling your request). Any mandatory statutory provisions, especially those regarding mandatory data retention periods, remain unaffected by this provision.\r\n  </p>\r\n\r\n  <h3 class=\"legal-title\">\r\n    Registration on this website\r\n  </h3>\r\n\r\n  <p class=\"legal-text\">\r\n    You can register on our website in order to access additional functions offered here. The input data will only be used for the purpose of using the respective site or service for which you have registered. The mandatory information requested during registration must be provided in full. Otherwise, we will reject your registration.\r\n  </p>\r\n  <p class=\"legal-text\">\r\n    To inform you about important changes such as those within the scope of our site or technical changes, we will use the email address specified during registration.\r\n  </p>\r\n  <p class=\"legal-text\">\r\n    We will process the data provided during registration only based on your consent per Art. 6 (1)(a) GDPR. You may revoke your consent at any time with future effect. An informal email making this request is sufficient. The data processed before we receive your request may still be legally processed.\r\n  </p>\r\n  <p class=\"legal-text\">\r\n    We will continue to store the data collected during registration for as long as you remain registered on our website. Statutory retention periods remain unaffected.\r\n  </p>\r\n\r\n  <h3 class=\"legal-title\">\r\n    Processing of data (customer and contract data)\r\n  </h3>\r\n  <p class=\"legal-text\">\r\n    We collect, process, and use personal data only insofar as it is necessary to establish, or modify legal relationships with us (master data). This is done based on Art. 6 (1) (b) GDPR, which allows the processing of data to fulfill a contract or for measures preliminary to a contract. We collect, process and use your personal data when accessing our website (usage data) only to the extent required to enable you to access our service or to bill you for the same.\r\n\r\n  </p>\r\n  <p class=\"legal-text\">\r\n    Collected customer data shall be deleted after completion of the order or termination of the business relationship. Legal retention periods remain unaffected.\r\n  </p>\r\n\r\n  <h3 class=\"legal-title\">\r\n    Data transferred when signing up for services and digital content\r\n  </h3>\r\n\r\n  <p class=\"legal-text\">\r\n    We transmit personally identifiable data to third parties only to the extent required to fulfill the terms of your contract with us, for example, to banks entrusted to process your payments.\r\n  </p>\r\n  <p class=\"legal-text\">\r\n    Your data will not be transmitted for any other purpose unless you have given your express permission to do so. Your data will not be disclosed to third parties for advertising purposes without your express consent.\r\n  </p>  \r\n  <p class=\"legal-text\">\r\n    The basis for data processing is Art. 6 (1) (b) GDPR, which allows the processing of data to fulfill a contract or for measures preliminary to a contract.\r\n  </p>  \r\n\r\n  <h3 class=\"legal-title\">\r\n    4. Analytics and advertising\r\n  </h3>\r\n  <h3 class=\"legal-title\">\r\n    Google Analytics\r\n  </h3>\r\n\r\n  <p class=\"legal-text\">\r\n    This website uses Google Analytics, a web analytics service. It is operated by Google Inc., 1600 Amphitheatre Parkway, Mountain View, CA 94043, USA.\r\n  </p>\r\n  \r\n  <p class=\"legal-text\">\r\n    Google Analytics uses so-called “cookies”. These are text files that are stored on your computer and that allow an analysis of the use of the website by you. The information generated by the cookie about your use of this website is usually transmitted to a Google server in the USA and stored there.\r\n  </p>\r\n  <p class=\"legal-text\">\r\n    Google Analytics cookies are stored based on Art. 6 (1) (f) GDPR. The website operator has a legitimate interest in analyzing user behavior to optimize both its website and its advertising.\r\n  </p>\r\n\r\n  <h3 class=\"legal-title\">\r\n    IP anonymization\r\n  </h3>\r\n\r\n  <p class=\"legal-text\">\r\n    We have activated the IP anonymization feature on this website. Your IP address will be shortened by Google within the European Union or other parties to the Agreement on the European Economic Area prior to transmission to the United States. Only in exceptional cases is the full IP address sent to a Google server in the US and shortened there. Google will use this information on behalf of the operator of this website to evaluate your use of the website, to compile reports on website activity, and to provide other services regarding website activity and Internet usage for the website operator. The IP address transmitted by your browser as part of Google Analytics will not be merged with any other data held by Google.\r\n  </p>\r\n\r\n  <h3 class=\"legal-title\">\r\n    Browser plugin\r\n  </h3>\r\n\r\n  <p class=\"legal-text\">\r\n    You can prevent these cookies being stored by selecting the appropriate settings in your browser. However, we wish to point out that doing so may mean you will not be able to enjoy the full functionality of this website. You can also prevent the data generated by cookies about your use of the website (incl. your IP address) from being passed to Google, and the processing of these data by Google, by downloading and installing the browser plugin available at the following link: https://tools.google.com/dlpage/gaoptout?hl=en.\r\n  </p>\r\n\r\n  <h3 class=\"legal-title\">\r\n    Objecting to the collection of data\r\n  </h3>\r\n\r\n  <p class=\"legal-text\">\r\n    You can prevent the collection of your data by Google Analytics by clicking on the following link. An opt-out cookie will be set to prevent your data from being collected on future visits to this site: Disable Google Analytics.\r\n  </p>\r\n\r\n  <p class=\"legal-text\">\r\n    For more information about how Google Analytics handles user data, see Google’s privacy policy: https://support.google.com/analytics/answer/6004245?hl=en.\r\n  </p>\r\n\r\n  <h3 class=\"legal-title\">\r\n    Outsourced data processing\r\n  </h3>\r\n\r\n  <p class=\"legal-text\">\r\n    We have entered into an agreement with Google for the outsourcing of our data processing and fully implement the strict requirements of the German data protection authorities when using Google Analytics.\r\n  </p>\r\n\r\n  \r\n  <h3 class=\"legal-title\">\r\n    Demographic data collection by Google Analytics\r\n  </h3>\r\n\r\n  <p class=\"legal-text\">\r\n    This website uses Google Analytics’ demographic features. This allows reports to be generated containing statements about the age, gender, and interests of site visitors. This data comes from interest-based advertising from Google and third-party visitor data. This collected data cannot be attributed to any specific individual person. You can disable this feature at any time by adjusting the ads settings in your Google account or you can forbid the collection of your data by Google Analytics as described in the section “Refusal of data collection”.\r\n  </p>\r\n\r\n  <h3 class=\"legal-title\">\r\n    Google Analytics Remarketing\r\n  </h3>\r\n\r\n  <p class=\"legal-text\">\r\n    Our websites use the features of Google Analytics Remarketing combined with the cross-device capabilities of Google AdWords and DoubleClick. This service is provided by Google Inc., 1600 Amphitheater Parkway, Mountain View, CA 94043, USA.\r\n  </p>\r\n  \r\n  <p class=\"legal-text\">\r\n    This feature makes it possible to link target audiences for promotional marketing created with Google Analytics Remarketing to the cross-device capabilities of Google AdWords and Google DoubleClick. This allows advertising to be displayed based on your personal interests, identified based on your previous usage and surfing behavior on one device (e.g. your mobile phone), on other devices (such as a tablet or computer).\r\n  </p>\r\n  <p class=\"legal-text\">\r\n    Once you have given your consent, Google will associate your web and app browsing history with your Google Account for this purpose. That way, any device that signs in to your Google Account can use the same personalized promotional messaging.\r\n  </p>\r\n\r\n  <p class=\"legal-text\">\r\n    To support this feature, Google Analytics collects Google-authenticated IDs of users that are temporarily linked to our Google Analytics data to define and create audiences for cross-device ad promotion.\r\n  </p>\r\n  <p class=\"legal-text\">\r\n    You can permanently opt out of cross-device remarketing/targeting by turning off personalized advertising in your Google Account; follow this link: https://www.google.com/settings/ads/onweb/.\r\n  </p>\r\n  <p class=\"legal-text\">\r\n    The aggregation of the data collected in your Google Account data is based solely on your consent, which you may give or withdraw from Google per Art. 6 (1) (a) GDPR. For data collection operations not merged into your Google Account (for example, because you do not have a Google Account or have objected to the merge), the collection of data is based on Art. 6 (1) (f) GDPR. The website operator has a legitimate interest in analyzing anonymous user behavior for promotional purposes.\r\n  </p>\r\n  <p class=\"legal-text\">\r\n    For more information and the Google Privacy Policy, go to: https://www.google.com/policies/technologies/ads/.\r\n  </p>\r\n\r\n  <h3 class=\"legal-title\">Google AdWords and Google Conversion Tracking</h3>\r\n\r\n  <p class=\"legal-text\">\r\n    This website uses Google AdWords. AdWords is an online advertising program from Google Inc., 1600 Amphitheater Parkway, Mountain View, CA 94043, United States (“Google”).\r\n  </p>\r\n  \r\n  <p class=\"legal-text\">\r\n    As part of Google AdWords, we use so-called conversion tracking. When you click on an ad served by Google, a conversion tracking cookie is set. Cookies are small text files that your internet browser stores on your computer. These cookies expire after 30 days and are not used for personal identification of the user. Should the user visit certain pages of the website and the cookie has not yet expired, Google and the website can tell that the user clicked on the ad and proceeded to that page.\r\n\r\n  </p>\r\n  <p class=\"legal-text\">\r\n    Each Google AdWords advertiser has a different cookie. Thus, cookies cannot be tracked using the website of an AdWords advertiser. The information obtained using the conversion cookie is used to create conversion statistics for the AdWords advertisers who have opted for conversion tracking. Customers are told the total number of users who clicked on their ad and were redirected to a conversion tracking tag page. However, advertisers do not obtain any information that can be used to personally identify users. If you do not want to participate in tracking, you can opt-out of this by easily disabling the Google Conversion Tracking cookie by changing your browser settings. In doing so, you will not be included in the conversion tracking statistics.\r\n\r\n  </p>\r\n  \r\n  <p class=\"legal-text\">\r\n    Conversion cookies are stored based on Art. 6 (1) (f) GDPR. The website operator has a legitimate interest in analyzing user behavior to optimize both its website and its advertising.\r\n  </p>\r\n  <p class=\"legal-text\">\r\n    For more information about Google AdWords and Google Conversion Tracking, see the Google Privacy Policy: https://www.google.de/policies/privacy/.\r\n  </p>\r\n  <p class=\"legal-text\">\r\n    You can configure your browser to inform you about the use of cookies so that you can decide on a case-by-case basis whether to accept or reject a cookie. Alternatively, your browser can be configured to automatically accept cookies under certain conditions or to always reject them, or to automatically delete cookies when closing your browser. Disabling cookies may limit the functionality of this website.\r\n  </p>\r\n\r\n  <h3 class=\"legal-title\">\r\n    Data protection statement for the use of Twitter / conversion measurement with „Twitter-Pixel” / „Custom Audiences”\r\n  </h3>\r\n\r\n  <p class=\"legal-text\">\r\n    In particular, using the “Twitter pixel” set on our web pages, we can track users’ actions after they’ve seen or clicked on a Twitter ad. This procedure is used to evaluate the effectiveness of Twitter advertisements for statistical purposes and market research purposes, and can contribute to the optimisation of future advertising measures. The data collected is anonymous to us, so we can not draw conclusions about the identity of the users. However, the data is stored and processed by Twitter, so that a connection to the respective user profile is possible and Twitter can use the data for its own advertising purposes, in compliance with the Twitter data protection statement. You can enable Twitter as well as its affiliates to place advertisements on and outside of Twitter. A cookie may also be stored on your computer for these purposes. For further information in this regard, please refer to the data protection statement of Twitter under http://twitter.com/privacy.\r\n  </p>\r\n\r\n  <p class=\"legal-text\">You can change your data protection settings and consents with regard to Twitter in the Account Settings under: http://twitter.com/account/settings.</p>\r\n\r\n  <h3 class=\"legal-title\">Facebook Pixel</h3>\r\n\r\n  <p class=\"legal-text\">Our website measures conversions using visitor action pixels from Facebook, Facebook Inc., 1601 S. California Ave, Palo Alto, CA 94304, USA (“Facebook”).\r\n\r\n  </p>\r\n  <p class=\"legal-text\">These allow the behavior of site visitors to be tracked after they click on a Facebook ad to reach the provider’s website. This allows an analysis of the effectiveness of Facebook advertisements for statistical and market research purposes and their future optimization.\r\n\r\n  </p>\r\n  <p class=\"legal-text\">The data collected is anonymous to us as operators of this website and we cannot use it to draw any conclusions about our users’ identities. However, the data are stored and processed by Facebook, which may make a connection to your Facebook profile and which may use the data for its own advertising purposes, as stipulated in the Facebook privacy policy. This will allow Facebook to display ads both on Facebook and on third-party sites. We have no control over how this data is used.\r\n\r\n  </p>\r\n  <p class=\"legal-text\">Check out Facebook’s privacy policy to learn more about protecting your privacy: https://www.facebook.com/about/privacy/.\r\n\r\n  </p>\r\n  <p class=\"legal-text\">You can also deactivate the custom audiences remarketing feature in the Ads Settings section at https://www.facebook.com/ads/preferences/?entry_product=ad_settings_screen. You will first need to log into Facebook.\r\n\r\n  </p>\r\n  <p class=\"legal-text\">If you do not have a Facebook account, you can opt out of usage-based advertising from Facebook on the website of the European Interactive Digital Advertising Alliance: http://www.youronlinechoices.com/de/praferenzmanagement/.\r\n\r\n  </p>\r\n\r\n  <h3 class=\"legal-title\">5. Final Provisions\r\n  </h3>\r\n  <h3 class=\"legal-title\">Compliance With Laws\r\n  </h3>\r\n  <p class=\"legal-text\">We will disclose your Personal Information where required to do so by law or subpoena or if we believe that such action is necessary to comply with the law and the reasonable requests of law enforcement or to protect the security or integrity of our Service.\r\n  </p>\r\n\r\n  <h3 class=\"legal-title\">Business Transaction\r\n  </h3>\r\n\r\n  <p class=\"legal-text\">If Crowdy.ai is involved in a merger, acquisition or asset sale, your Personal Information may be transferred. We will provide notice before your Personal Information is transferred and becomes subject to a different Privacy Policy.\r\n\r\n  </p>\r\n\r\n  <h3 class=\"legal-title\">\r\n    International Transfer\r\n  </h3>\r\n\r\n  <p class=\"legal-text\">Your information, including Personal Information, may be transferred to — and maintained on — computers located outside of your state, province, country or other governmental jurisdiction where the data protection laws may differ than those from your jurisdiction.\r\n\r\n  </p>\r\n  <p class=\"legal-text\">If you are located outside Germany and choose to provide information to us, please note that we transfer the information, including Personal Information, to Germany and process it there.\r\n\r\n  </p>\r\n  <p class=\"legal-text\">Your consent to this Privacy Policy followed by your submission of such information represents your agreement to that transfer.\r\n\r\n  </p>\r\n\r\n  <h3 class=\"legal-title\">\r\n    Links To Other Sites\r\n\r\n\r\n  </h3>\r\n\r\n  <p class=\"legal-text\">Our Service may contain links to other sites that are not operated by us. If you click on a third party link, you will be directed to that third party’s site. We strongly advise you to review the Privacy Policy of every site you visit.\r\n\r\n  </p>\r\n  <p class=\"legal-text\">We have no control over, and assume no responsibility for the content, privacy policies or practices of any third party sites or services.\r\n\r\n  </p>\r\n\r\n  <h3 class=\"legal-title\">Children’s Privacy\r\n\r\n  </h3>\r\n\r\n  <p class=\"legal-text\">Our Service does not address anyone under the age of 13 (“Children”).\r\n\r\n  </p>\r\n  <p class=\"legal-text\">We do not knowingly collect personally identifiable information from children under 13. If you are a parent or guardian and you are aware that your Children has provided us with Personal Information, please contact us. If we become aware that we have collected Personal Information from a child under age 13 without verification of parental consent, we take steps to remove that information from our servers.\r\n\r\n  </p>\r\n</div>";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/auth/privacy-policy/components/privacy-german/privacy-german.component.html":
    /*!*****************************************************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/auth/privacy-policy/components/privacy-german/privacy-german.component.html ***!
      \*****************************************************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppPagesAuthPrivacyPolicyComponentsPrivacyGermanPrivacyGermanComponentHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<div id=\"germanPrivacy\">\r\n  <p class=\"legal-text\">\r\n    Die folgenden Hinweise geben einen einfachen Überblick darüber, was mit Ihren personenbezogenen Daten passiert, wenn Sie unsere Webseite besuchen. Personenbezogene Daten sind alle Daten, mit denen Sie persönlich identifiziert werden können. Ausführliche Informationen zum Thema Datenschutz entnehmen Sie unserer unter diesem Text aufgeführten Datenschutzerklärung.\r\n  </p>\r\n  \r\n  <h3 class=\"legal-title\">Vertragspartner und Hinweise zum Vertragsschluss</h3>\r\n\r\n  <p class=\"legal-text\">\r\n    Die Datenverarbeitung auf dieser Webseite erfolgt durch\r\n  </p>\r\n\r\n  <h3 class=\"legal-title\">G-TAC Software UG (haftungsbeschränkt)</h3>\r\n  <h3 class=\"legal-title\">Schmiedstrasse 7</h3>\r\n  <h3 class=\"legal-title\">67734 Katzweiler</h3>\r\n  <h3 class=\"legal-title\">Tel.: +49 (6301) 6049758</h3>\r\n  <h3 class=\"legal-title\">E-Mail: support at telllisa dot com</h3>\r\n  <h3 class=\"legal-title\">Wie erfassen wir Ihre Daten?</h3>\r\n\r\n  <p class=\"legal-text\">\r\n    Ihre Daten werden zum einen dadurch erhoben, dass Sie sie selbst mitteilen. Hierbei kann es sich um Daten handeln, die Sie in ein Kontaktformular oder bei einer Newsletterbestellung eingeben.\r\n  </p>\r\n  <p class=\"legal-text\">\r\n    Andere Daten werden automatisch beim Besuch der Webseite durch unsere IT-Systeme erfasst. Das sind vor allem technische Daten (z.B. Internetbrowser, Betriebssystem oder Uhrzeit des Seitenaufrufs). Die Erfassung dieser Daten erfolgt automatisch, sobald Sie unsere Webseite betreten.\r\n  </p>\r\n\r\n  <h3 class=\"legal-title\">\r\n    Analyse Ihrer Daten\r\n  </h3>\r\n\r\n  <p class=\"legal-text\">\r\n    Beim Besuch unserer Webseite kann Ihr Surf-Verhalten statistisch ausgewertet werden. Das geschieht vor allem mit Cookies und mit sogenannten Analysewerkzeugen. Die Analyse Ihres Surf-Verhaltens erfolgt anonym, und kann nicht zu Ihnen zurückverfolgt werden. Sie können dieser Analyse widersprechen oder sie durch die Nichtbenutzung bestimmter Tools verhindern. Details hierzu entnehmen Sie unserer Datenschutzerklärung unter der Überschrift „Drittmodule und Analysetools“.\r\n  </p>\r\n\r\n  <h3 class=\"legal-title\">Wofür nutzen wir Ihre Daten?</h3>\r\n  <p class=\"legal-text\">\r\n    Ein Teil der Daten wird erhoben, um eine fehlerfreie Bereitstellung der Webseite zu gewährleisten.\r\n  </p>\r\n\r\n  <p class=\"legal-text\">\r\n    Mit anderen Daten können wir Ihr Nutzerverhalten im Internet statistisch analysieren (z.B. über sogenannte Cookies). Derartige Analysen erfolgen anonym und können nicht zu Ihrer Person zurückverfolgt werden. Sie können dieser Analyse widersprechen. Details hierzu entnehmen Sie unserer Datenschutzerklärung unter der Überschrift „Drittmodule und Analysetools“.\r\n  </p>\r\n\r\n  <h3 class=\"legal-title\">Welche Rechte haben Sie bezüglich Ihrer Daten?</h3>\r\n\r\n  <p class=\"legal-text\">\r\n    Sie haben jederzeit das Recht unentgeltlich Auskunft über Herkunft, Empfänger und Zweck Ihrer gespeicherten personenbezogenen Daten zu bekommen. Sie haben außerdem ein Recht die Berichtigung, Sperrung oder Löschung dieser Daten zu verlangen. Hierzu sowie zu weiteren Fragen zum Thema personenbezogene Daten können Sie sich jederzeit unter der im Impressum angegebenen Adresse an uns wenden. Des Weiteren steht Ihnen ein Beschwerderecht bei der zuständigen Aufsichtsbehörde zu.\r\n  </p>\r\n\r\n  <h3 class=\"legal-title\">\r\n    Erfolgt die Übertragung Ihrer Daten verschlüsselt?\r\n  </h3>\r\n\r\n  <p class=\"legal-text\">\r\n    Ja. Diese Webseite nutzt eine Verschlüsselung. Diese soll verhindern, dass Unbefugte auf Ihre Daten zugreifen können\r\n  </p>\r\n\r\n  <h3 class=\"legal-title\">Datenschutz auf einen Blick</h3>\r\n  <h3 class=\"legal-title\">INHALTSÜBERSICHT</h3>\r\n\r\n  <ol>\r\n    <li>Allgemeine Hinweise und Pflichtinformationen</li>\r\n    <li>Datenerfassung auf unserer Webseite</li>\r\n    <li>Drittmodule und Analysetools</li>\r\n    <li>Sonstige Informationen</li>\r\n  </ol>\r\n\r\n  <h3 class=\"legal-title\">DETAILS</h3>\r\n  <h3 class=\"legal-title\">1. Allgemeine Hinweise und Pflichtinformationen</h3>\r\n\r\n  <p class=\"legal-text\">\r\n    Die Betreiber dieser Seiten nehmen den Schutz Ihrer persönlichen Daten sehr ernst. Wir behandeln Ihre personenbezogenen Daten vertraulich und entsprechend der gesetzlichen Datenschutzvorschriften sowie dieser Datenschutzerklärung.\r\n  </p>\r\n  <p class=\"legal-text\">\r\n    Wenn Sie diese Webseite benutzen, werden verschiedene personenbezogene Daten erhoben. Personenbezogene Daten sind Daten, mit denen Sie persönlich identifiziert werden können. Die vorliegende Datenschutzerklärung erläutert, welche Daten wir erheben und wofür wir sie nutzen. Sie erläutert auch, wie und zu welchem Zweck das geschieht.\r\n  </p>\r\n  <p class=\"legal-text\">\r\n    Wir weisen darauf hin, dass die Datenübertragung im Internet (z.B. bei der Kommunikation per E-Mail) Sicherheitslücken aufweisen kann. Ein lückenloser Schutz der Daten vor dem Zugriff durch Dritte ist nicht möglich.\r\n  </p>\r\n\r\n  <h3 class=\"legal-title\">Verantwortliche Stelle </h3>\r\n\r\n  <p class=\"legal-text\">\r\n    Die verantwortliche Stelle für die Datenverarbeitung auf dieser Webseite ergibt sich aus dem Impressum. Verantwortliche Stelle ist die natürliche oder juristische Person die allein oder gemeinsam mit anderen über die Zwecke und Mittel der Verarbeitung von personenbezogenen Daten (z.B. Namen, E-Mail-Adressen o. Ä.) entscheidet.\r\n  </p>\r\n  <p class=\"legal-text\">\r\n    Widerruf Ihrer Einwilligung zur Datenverarbeitung\r\n  </p>\r\n  <p class=\"legal-text\">\r\n    Viele Datenverarbeitungsvorgänge sind nur mit Ihrer Einwilligung möglich. Diese werden wir vor Beginn der Einwilligung ausdrücklich bei Ihnen einholen. Sie können diese Einwilligung jederzeit widerrufen. Dazu reicht eine formlose Mitteilung per E-Mail an uns. Die Rechtmäßigkeit der bis zum Widerruf erfolgten Datenverarbeitungsvorgänge bleibt vom Widerruf unberührt.\r\n  </p>\r\n\r\n  <h3 class=\"legal-title\">Beschwerderecht bei der zuständigen Aufsichtsbehörde</h3>\r\n  \r\n\r\n  <p class=\"legal-text\">\r\n    Der Webseitenbesucher wird darauf hingewiesen, dass ihm im Falle datenschutzrechtlicher Verstöße ein Beschwerderecht bei der zuständigen Aufsichtsbehörde zusteht. Zuständige Aufsichtsbehörde in datenschutzrechtlichen Fragen ist der Landesdatenschutzbeauftragte des Bundeslandes, in dem unser Unternehmen seinen Hauptsitz hat. Eine Liste der Datenschutzbeauftragten sowie deren Kontaktdaten können folgendem Link entnommen werden:\r\n  </p>\r\n  \r\n  <p class=\"legal-text\">\r\n    https://www.bfdi.bund.de/DE/Infothek/Anschriften_Links/anschriften_links-node.html  \r\n  </p>\r\n\r\n  <h3 class=\"legal-title\">Recht auf Datenübertragbarkeit</h3>\r\n\r\n  <p class=\"legal-text\">\r\n    Sie haben das Recht Daten, die wir auf Grundlage Ihrer Einwilligung oder in Erfüllung eines Vertrags automatisiert verarbeiten an sich oder an einen anderen Verantwortlichen in einem gängigen, maschinenlesbaren Format aushändigen zu lassen. Sofern Sie die direkte Übertragung der Daten an einen anderen Verantwortlichen verlangen, erfolgt dies nur, soweit es technisch machbar ist.\r\n  </p>\r\n  \r\n  <h3 class=\"legal-title\">SSL- bzw. TLS-Verschlüsselung</h3>\r\n\r\n\r\n  <p class=\"legal-text\">\r\n    Diese Seite nutzt aus Gründen der Sicherheit und zum Schutz der Übertragung vertraulicher Inhalte, wie zum Beispiel der Anfragen, die Sie an uns als Seitenbetreiber senden, eine SSL-bzw. TLS-Verschlüsselung. Eine verschlüsselte Verbindung erkennen Sie daran, dass die Adresszeile des Browsers von „http://“ auf „https://“ wechselt und an dem Schloss-Symbol in Ihrer Browserzeile.\r\n  </p>\r\n  <p class=\"legal-text\">\r\n    Wenn die SSL- bzw. TLS-Verschlüsselung aktiviert ist, können die Daten, die Sie an uns übermitteln, nicht von Dritten mitgelesen werden.\r\n  </p>\r\n\r\n  <h3 class=\"legal-title\">\r\n    Verschlüsselter Zahlungsverkehr auf dieser Webseite\r\n  </h3>\r\n  <p class=\"legal-text\">\r\n    Nach dem Abschluss eines entgeltpflichtigen Vertrags sind Sie vertraglich verpflichtet, uns Ihre Zahlungsdaten (z.B. Kontonummer bei Einzugsermächtigung) zu übermitteln. Diese Daten werden zur Zahlungsabwicklung benötigt. Stellen Sie uns diese Daten nicht bereit, können wir ggf. von einem geschlossenen Vertrag zurücktreten und Schadensersatz von Ihnen verlangen.\r\n  </p>\r\n  \r\n  <p class=\"legal-text\">\r\n    Der Zahlungsverkehr über die gängigen Zahlungsmittel (Visa/Mastercard, Lastschriftverfahren) erfolgt ausschließlich über eine verschlüsselte SSL- bzw. TLS-Verbindung. Eine verschlüsselte Verbindung erkennen Sie daran, dass die Adresszeile des Browsers von “http://” auf “https://” wechselt und an dem Schloss-Symbol in Ihrer Browserzeile.\r\n  </p>\r\n  <p class=\"legal-text\">\r\n    Bei verschlüsselter Kommunikation können Ihre Zahlungsdaten, die Sie an uns übermitteln, nicht von Dritten mitgelesen werden.\r\n  </p>\r\n\r\n  <h3 class=\"legal-title\">Auskunft, Sperrung, Löschung</h3>\r\n\r\n  <p class=\"legal-text\">\r\n    Sie haben im Rahmen der geltenden gesetzlichen Bestimmungen jederzeit das Recht auf unentgeltliche Auskunft über Ihre gespeicherten personenbezogenen Daten, deren Herkunft und Empfänger und den Zweck der Datenverarbeitung und ggf. ein Recht auf Berichtigung, Sperrung oder Löschung dieser Daten. Hierzu sowie zu weiteren Fragen zum Thema personenbezogene Daten können Sie sich jederzeit unter der im Impressum angegebenen Adresse an uns wenden.\r\n  </p>\r\n\r\n  <h3 class=\"legal-title\">Widerspruch Werbe-Mails</h3>\r\n\r\n  <p class=\"legal-text\">\r\n    Der Nutzung von im Rahmen der Impressumspflicht veröffentlichten Kontaktdaten zur Übersendung von nicht ausdrücklich angeforderter Werbung und Informationsmaterialien wird hiermit widersprochen. Die Betreiber der Seiten behalten sich ausdrücklich rechtliche Schritte im Falle der unverlangten Zusendung von Werbeinformationen, etwa durch Spam-E-Mails, vor.\r\n  </p>\r\n\r\n  \r\n  <h3 class=\"legal-title\">Änderung dieser Datenschutzerklärung</h3>\r\n\r\n  <p class=\"legal-text\">\r\n    Wir behalten uns das Recht vor, diese Datenschutzbestimmungen unter Einhaltung der gesetzlichen Vorgaben jederzeit zu ändern.\r\n  </p>\r\n\r\n  <h3 class=\"legal-title\">Geltendes Recht</h3>\r\n\r\n  <p class=\"legal-text\">\r\n    Es gilt das Recht der Bundesrepublik Deutschland unter Ausschluss des UN-Kaufrechts.\r\n  </p>\r\n\r\n  <h3 class=\"legal-title\">2. Datenerfassung auf unserer Webseite</h3>\r\n\r\n  <h3 class=\"legal-title\">Cookies</h3>\r\n\r\n  <p class=\"legal-text\">\r\n    Die Internetseiten verwenden teilweise so genannte Cookies. Cookies richten auf Ihrem Rechner keinen Schaden an und enthalten keine Viren. Cookies dienen dazu, unser Angebot nutzerfreundlicher, effektiver und sicherer zu machen. Cookies sind kleine Textdateien, die auf Ihrem Rechner abgelegt werden und die Ihr Browser speichert.\r\n  </p>\r\n  \r\n  <p class=\"legal-text\">\r\n    Sie können Ihren Browser so einstellen, dass Sie über das Setzen von Cookies informiert werden und Cookies nur im Einzelfall erlauben, die Annahme von Cookies für bestimmte Fälle oder generell ausschließen sowie das automatische Löschen der Cookies beim Schließen des Browsers aktivieren. Bei der Deaktivierung von Cookies kann die Funktionalität dieser Website eingeschränkt sein.\r\n  </p>\r\n\r\n  \r\n  <p class=\"legal-text\">\r\n    Die meisten der von uns verwendeten Cookies sind so genannte “Session-Cookies”. Sie werden nach Ende Ihres Besuchs automatisch gelöscht. Andere Cookies bleiben auf Ihrem Endgerät gespeichert, bis Sie diese löschen. Diese Cookies ermöglichen es uns, Ihren Browser beim nächsten Besuch wiederzuerkennen.\r\n  </p>\r\n\r\n\r\n  <p class=\"legal-text\">\r\n    Cookies, die zur Durchführung des elektronischen Kommunikationsvorgangs oder zur Bereitstellung bestimmter, von Ihnen erwünschter Funktionen (z.B. Warenkorbfunktion) erforderlich sind, werden auf Grundlage von Art. 6 Abs. 1 lit. f DSGVO gespeichert. Der Webseitenbetreiber hat ein berechtigtes Interesse an der Speicherung der Cookies zur technisch fehlerfreien und optimalen Bereitstellung seiner Dienste. Soweit andere Cookies (z.B. Cookies zur Analyse Ihres Surfverhaltens) gespeichert werden, werden diese in dieser Datenschutzerklärung gesondert behandelt.\r\n  </p>\r\n\r\n  <ion-button>\r\n    Revoke Consent\r\n  </ion-button>\r\n\r\n\r\n  <h3 class=\"legal-title\">Server-Log-Files</h3>\r\n\r\n  <p class=\"legal-text\">\r\n    Der Provider der Seiten erhebt und speichert automatisch Informationen in so genannten Server-Log Files, die Ihr Browser automatisch an uns übermittelt. Dies sind:\r\n  </p>\r\n\r\n  <ul>\r\n    <li>Browsertyp und Browserversion</li>\r\n    <li>verwendetes Betriebssystem</li>\r\n    <li>Referrer URL</li>\r\n    <li>Hostname des zugreifenden Rechners</li>\r\n    <li>Uhrzeit der Serveranfrage</li>\r\n    <li>IP-Adresses</li>\r\n  </ul>\r\n\r\n  <p class=\"legal-text\">\r\n    Eine Zusammenführung dieser Daten mit anderen Datenquellen wird nicht vorgenommen. Wir behalten uns vor, diese Daten nachträglich zu prüfen, wenn uns konkrete Anhaltspunkte für eine rechtswidrige Nutzung bekannt werden. Die Erfassung dieser Daten erfolgt auf Grundlage von Art. 6 Abs. 1 lit. f DSGVO. Der Webseitenbetreiber hat ein berechtigtes Interesse an der technisch fehlerfreien und optimalen Darstellung seiner Webseite – hierzu müssen die Server-Log-Files erfasst werden.\r\n  </p>\r\n\r\n  <h3 class=\"legal-title\">Kontaktformular</h3>\r\n\r\n  <p class=\"legal-text\">\r\n    Wenn Sie uns per Kontaktformular Anfragen zukommen lassen, werden Ihre Angaben aus dem Anfrageformular inklusive der von Ihnen dort angegebenen Kontaktdaten zwecks Bearbeitung der Anfrage und für den Fall von Anschlussfragen bei uns gespeichert. Diese Daten geben wir nicht ohne Ihre Einwilligung weiter.\r\n  </p>\r\n  \r\n  <p class=\"legal-text\">\r\n    Die Verarbeitung der in das Kontaktformular eingegebenen Daten erfolgt ausschließlich auf Grundlage Ihrer Einwilligung (Art. 6 Abs. 1 lit. a DSGVO). Sie können diese Einwilligung jederzeit widerrufen. Dazu reicht eine formlose Mitteilung per E-Mail an uns. Die Rechtmäßigkeit der bis zum Widerruf erfolgten Datenverarbeitungsvorgänge bleibt vom Widerruf unberührt.\r\n  </p>\r\n\r\n  <p class=\"legal-text\">\r\n    Die von Ihnen im Kontaktformular eingegebenen Daten verbleiben bei uns, bis Sie uns zur Löschung auffordern, Ihre Einwilligung zur Speicherung widerrufen oder der Zweck für die Datenspeicherung entfällt (z.B. nach abgeschlossener Bearbeitung Ihrer Anfrage). Zwingende gesetzliche Bestimmungen – insb. Aufbewahrungsfristen – bleiben unberührt.\r\n  </p>\r\n\r\n\r\n  <h3 class=\"legal-title\">Registrierung auf dieser Webseite</h3>\r\n\r\n  <p class=\"legal-text\">Sie können sich auf unserer Webseite registrieren, um zusätzliche Funktionen auf der Seite zu nutzen. Die dazu eingegebenen Daten verwenden wir nur zum Zwecke der Nutzung des jeweiligen Angebotes oder Dienstes, für den Sie sich registriert haben. Die bei der Registrierung abgefragten Pflichtangaben müssen vollständig angegeben werden. Anderenfalls werden wir die Registrierung ablehnen.\r\n\r\n  </p>\r\n  <p class=\"legal-text\">Für wichtige Änderungen etwa beim Angebotsumfang oder bei technisch notwendigen Änderungen nutzen wir die bei der Registrierung angegebene E-Mail-Adresse, um Sie auf diesem Wege zu informieren.\r\n\r\n  </p>\r\n  <p class=\"legal-text\">\r\n    Die Verarbeitung der bei der Registrierung eingegebenen Daten erfolgt auf Grundlage Ihrer Einwilligung (Art. 6 Abs. 1 lit. a DSGVO). Sie können eine von Ihnen erteilte Einwilligung jederzeit widerrufen. Dazu reicht eine formlose Mitteilung per E-Mail an uns. Die Rechtmäßigkeit der bereits erfolgten Datenverarbeitungsvorgänge bleibt vom Widerruf unberührt.\r\n  </p>\r\n  <p class=\"legal-text\">\r\n    Die bei der Registrierung erfassten Daten werden von uns gespeichert, solange Sie auf unserer Webseite registriert sind und werden anschließend gelöscht. Gesetzliche Aufbewahrungsfristen bleiben unberührt.\r\n  </p>\r\n\r\n  <h3 class=\"legal-title\">\r\n    Verarbeiten von Daten (Kunden- und Vertragsdaten)\r\n  </h3>\r\n\r\n  <p class=\"legal-text\">\r\n    Wir erheben, verarbeiten und nutzen personenbezogene Daten nur, soweit sie für die Begründung, inhaltliche Ausgestaltung oder Änderung des Rechtsverhältnisses erforderlich sind (Bestandsdaten). Dies erfolgt auf Grundlage von Art. 6 Abs. 1 lit. b DSGVO, der die Verarbeitung von Daten zur Erfüllung eines Vertrags oder vorvertraglicher Maßnahmen gestattet. Personenbezogene Daten über die Inanspruchnahme unserer Internetseiten (Nutzungsdaten) erheben, verarbeiten und nutzen wir nur, soweit dies erforderlich ist, um dem Nutzer die Inanspruchnahme des Dienstes zu ermöglichen oder abzurechnen.\r\n\r\n  </p>\r\n  <p class=\"legal-text\">\r\n    Die erhobenen Kundendaten werden nach Abschluss des Auftrags oder Beendigung der Geschäftsbeziehung gelöscht nicht jedoch vor Ablauf der gesetzlichen Aufbewahrungsfristen gelöscht.\r\n\r\n  </p>\r\n  \r\n  <p class=\"legal-text\">\r\n    Grundlage für die Datenverarbeitung ist Art. 6 Abs. 1 lit. b DSGVO, der die Verarbeitung von Daten zur Erfüllung eines Vertrags oder zur Durchführung vorvertraglicher Maßnahmen gestattet.\r\n\r\n  </p>\r\n\r\n  <p class=\"legal-text\">Datenübermittlung bei Vertragsschluss für Dienstleistungen und digitalen Inhalten\r\n\r\n  </p>\r\n  <p class=\"legal-text\">Wir übermitteln personenbezogene Daten an Dritte nur dann, wenn dies im Rahmen der Vertragsabwicklung notwendig ist, etwa an das mit der Zahlungsabwicklung beauftragte Kreditinstitut.\r\n\r\n  </p>\r\n  <p class=\"legal-text\">Eine weitergehende Übermittlung der Daten erfolgt nicht bzw. nur dann, wenn Sie der Übermittlung ausdrücklich zugestimmt haben. Eine Weitergabe Ihrer Daten an Dritte ohne ausdrückliche Einwilligung, etwa zu Zwecken der Werbung, erfolgt nicht.\r\n\r\n  </p>\r\n  <p class=\"legal-text\">Grundlage für die Datenverarbeitung ist Art. 6 Abs. 1 lit. b DSGVO, der die Verarbeitung von Daten zur Erfüllung eines Vertrags oder vorvertraglicher Maßnahmen gestattet.\r\n  </p>\r\n\r\n\r\n  <h3 class=\"legal-title\">\r\n    3. Drittmodule und Analysetools\r\n  </h3>\r\n  \r\n  <h3 class=\"legal-title\">\r\n    Google Analytics\r\n  </h3>\r\n\r\n\r\n  <p class=\"legal-text\">\r\n    Diese Website nutzt Funktionen des Webanalysedienstes Google Analytics. Anbieter ist die Google Inc., 1600 Amphitheatre Parkway Mountain View, CA 94043, USA.\r\n\r\n  </p>\r\n  <p class=\"legal-text\">\r\n    Google Analytics verwendet so genannte “Cookies”. Das sind Textdateien, die auf Ihrem Computer gespeichert werden und die eine Analyse der Benutzung der Website durch Sie ermöglichen. Die durch den Cookie erzeugten Informationen über Ihre Benutzung dieser Website werden in der Regel an einen Server von Google in den USA übertragen und dort gespeichert.\r\n\r\n  </p>\r\n  <p class=\"legal-text\">\r\n    Mehr Informationen zum Umgang mit Nutzerdaten bei Google Analytics finden Sie in der Datenschutzerklärung von Google:\r\n\r\n  </p>\r\n  <p class=\"legal-text\">\r\n    https://support.google.com/analytics/answer/6004245?hl=de\r\n  </p>\r\n  <p class=\"legal-text\">\r\n    Browser Plugin\r\n  </p>\r\n  <p class=\"legal-text\">\r\n    Sie können die Speicherung der Cookies durch eine entsprechende Einstellung Ihrer Browser-Software verhindern; wir weisen Sie jedoch darauf hin, dass Sie in diesem Fall gegebenenfalls nicht sämtliche Funktionen dieser Website vollumfänglich werden nutzen können. Sie können darüber hinaus die Erfassung der durch den Cookie erzeugten und auf Ihre Nutzung der Website bezogenen Daten (inkl. Ihrer IP-Adresse) an Google sowie die Verarbeitung dieser Daten durch Google verhindern, indem Sie das unter dem folgenden Link verfügbare Browser-Plugin herunterladen und installieren: https://tools.google.com/dlpage/gaoptout?hl=de\r\n  </p>\r\n\r\n  <h3 class=\"legal-title\">\r\n    Demografische Merkmale bei Google Analytics\r\n\r\n  </h3>\r\n\r\n  <p class=\"legal-text\">\r\n    Diese Website nutzt die Funktion “demografische Merkmale” von Google Analytics. Dadurch können Berichte erstellt werden, die Aussagen zu Alter, Geschlecht und Interessen der Seitenbesucher enthalten. Diese Daten stammen aus interessenbezogener Werbung von Google sowie aus Besucherdaten von Drittanbietern. Diese Daten können keiner bestimmten Person zugeordnet werden. Sie können diese Funktion jederzeit über die Anzeigeneinstellungen in Ihrem Google-Konto deaktivieren oder die Erfassung Ihrer Daten durch Google Analytics wie im Punkt “Widerspruch gegen Datenerfassung” dargestellt generell untersagen.\r\n\r\n  </p>\r\n  <p class=\"legal-text\">\r\n    Auftragsdatenverarbeitung\r\n\r\n  </p>  \r\n  <p class=\"legal-text\">\r\n    Wir haben mit Google einen Vertrag zur Auftragsdatenverarbeitung abgeschlossen und setzen die strengen Vorgaben der deutschen Datenschutzbehörden bei der Nutzung von Google Analytics vollständig um.\r\n\r\n  </p>  \r\n\r\n  <p class=\"legal-text\">\r\n    IP-Anonymisierung\r\n\r\n  </p>\r\n  \r\n  <p class=\"legal-text\">\r\n    Wir nutzen die Funktion “Aktivierung der IP-Anonymisierung” auf dieser Webseite. Dadurch wird Ihre IP-Adresse von Google jedoch innerhalb von Mitgliedstaaten der Europäischen Union oder in anderen Vertragsstaaten des Abkommens über den Europäischen Wirtschaftsraum zuvor gekürzt. Nur in Ausnahmefällen wird die volle IP-Adresse an einen Server von Google in den USA übertragen und dort gekürzt. Im Auftrag des Betreibers dieser Website wird Google diese Informationen benutzen, um Ihre Nutzung der Website auszuwerten, um Reports über die Websiteaktivitäten zusammenzustellen und um weitere mit der Websitenutzung und der Internetnutzung verbundene Dienstleistungen gegenüber dem Websitebetreiber zu erbringen. Die im Rahmen von Google Analytics von Ihrem Browser übermittelte IP-Adresse wird nicht mit anderen Daten von Google zusammengeführt.\r\n\r\n  </p>\r\n  <p class=\"legal-text\">\r\n    Rechtsgrundlage\r\n  </p>\r\n  <p class=\"legal-text\">\r\n    Die Speicherung von Google-Analytics-Cookies erfolgt auf Grundlage von Art. 6 Abs. 1 lit. f DSGVO. Der Webseitenbetreiber hat ein berechtigtes Interesse an der Analyse des Nutzerverhaltens, um sowohl sein Webangebot als auch seine Werbung zu optimieren.\r\n\r\n  </p>\r\n\r\n  <h3 class=\"legal-title\">\r\n    Facebook-Pixel\r\n  </h3>\r\n\r\n  <p class=\"legal-text\">\r\n    Unsere Webseite nutzt zur Konversionsmessung das Besucheraktions-Pixel von Facebook, Facebook Inc. 1601 S. California Ave, Palo Alto, CA 94304, USA (“Facebook”).\r\n\r\n  </p>\r\n  <p class=\"legal-text\">\r\n    So kann das Verhalten der Seitenbesucher nachverfolgt werden, nachdem diese durch Klick auf eine Facebook-Werbeanzeige auf die Webseite des Anbieters weitergeleitet wurden. Dadurch können die Wirksamkeit der Facebook-Werbeanzeigen für statistische und Marktforschungszwecke ausgewertet werden und zukünftige Werbemaßnahmen optimiert werden.\r\n  </p>\r\n\r\n  <p class=\"legal-text\">\r\n    Die erhobenen Daten sind für uns als Betreiber dieser Webseite anonym, wir können keine Rückschlüsse auf die Identität der Nutzer ziehen. Die Daten werden aber von Facebook gespeichert und verarbeitet, sodass eine Verbindung zum jeweiligen Nutzerprofil möglich ist und Facebook die Daten für eigene Werbezwecke, entsprechend der Facebook- Datenverwendungsrichtlinie (https://www.facebook.com/about/privacy/) verwenden kann. Dadurch kann Facebook das Schalten von Werbeanzeigen auf Seiten von Facebook sowie außerhalb von Facebook ermöglichen. Diese Verwendung der Daten kann von uns als Seitenbetreiber nicht beeinflusst werden.\r\n  </p>\r\n\r\n  <h3 class=\"legal-title\">\r\n    Datenschutzerklärung für die Nutzung von Twitter / Konversionsmessung mit dem „Twitter-Pixel“ / „Custom Audiences“\r\n  </h3>\r\n\r\n  <p class=\"legal-text\">\r\n    Auf unseren Seiten sind Funktionen des Dienstes Twitter eingebunden. Diese Funktionen werden angeboten durch die Twitter Inc., 1355 Market Street, Suite 900, San Francisco, CA 94103, USA. Durch das Benutzen von Twitter und der Funktion “Re-Tweet” werden die von Ihnen besuchten Webseiten mit Ihrem Twitter-Account verknüpft und anderen Nutzern bekannt gegeben. Dabei werden auch Daten an Twitter übertragen. Wir weisen darauf hin, dass wir als Anbieter der Seiten keine Kenntnis vom Inhalt der übermittelten Daten sowie deren Nutzung durch Twitter erhalten.\r\n  </p>\r\n  <p class=\"legal-text\">\r\n    Mithilfe des auf unseren Seiten gesetzten „Twitter-Pixels“ können wir insbesondere Aktionen von Nutzern nachverfolgen, nachdem diese eine Twitter-Werbeanzeige gesehen oder angeklickt haben. Dieses Verfahren dient dazu, die Wirksamkeit der Twitter-Werbeanzeigen für statistische Zwecke und Marktforschungszwecke auszuwerten und kann dazu beitragen, zukünftige Werbemaßnahmen zu optimieren.\r\n  </p>\r\n  <p class=\"legal-text\">\r\n    Die erhobenen Daten sind für uns anonym, sodass wir keine Rückschlüsse auf die Identität der Nutzer ziehen können. Allerdings werden die Daten von Twitter gespeichert und verarbeitet, sodass eine Verbindung zum jeweiligen Nutzerprofil möglich ist und Twitter die Daten für eigene Werbezwecke, entsprechend der Twitter-Datenschutzerklärung verwenden kann. Sie können Twitter sowie dessen Partnern das Schalten von Werbeanzeigen auf und außerhalb von Twitter ermöglichen. Es kann ferner zu diesen Zwecken ein Cookie auf Ihrem Rechner gespeichert werden. Weitere Informationen hierzu finden Sie in der Datenschutzerklärung von Twitter unter http://twitter.com/privacy.\r\n  </p>\r\n  \r\n  <p class=\"legal-text\">\r\n    Ihre Datenschutzeinstellungen und Einwilligungen bei Twitter können Sie in den Konto-Einstellungen unter: http://twitter.com/account/settings ändern.\r\n  </p>\r\n\r\n\r\n  <h3 class=\"legal-title\">\r\n    Google Analytics Remarketing\r\n  </h3>\r\n\r\n  <p class=\"legal-text\">\r\n    Unsere Webseiten nutzen die Funktionen von Google Analytics Remarketing in Verbindung mit den geräteübergreifenden Funktionen von Google AdWords und Google DoubleClick. Anbieter ist die Google Inc. 1600 Amphitheatre Parkway Mountain View, CA 94043, USA.\r\n  </p>\r\n\r\n  <p class=\"legal-text\">\r\n    Diese Funktion ermöglicht es die mit Google Analytics Remarketing erstellten Werbe-Zielgruppen mit den geräteübergreifenden Funktionen von Google-AdWords und Google-DoubleClick zu verknüpfen. Auf diese Weise können interessenbezogene, personalisierte Werbebotschaften, die in Abhängigkeit Ihres früheren Nutzungs- und Surfverhaltens auf einem Endgerät (z.B. Handy) an Sie angepasst wurden auch auf einem anderen Ihrer Endgeräte (z.B. Tablet oder PC) angezeigt werden.\r\n  </p>\r\n  <p class=\"legal-text\">\r\n    Haben Sie eine entsprechende Einwilligung erteilt, verknüpft Google zu diesem Zweck Ihren Web- und App-Browserverlauf mit Ihrem Google-Konto. Auf diese Weise können auf jedem Endgerät auf dem Sie sich mit Ihrem Google-Konto anmelden, dieselben personalisierten Werbebotschaften geschaltet werden.\r\n\r\n  </p>\r\n\r\n  <p class=\"legal-text\">\r\n    Zur Unterstützung dieser Funktion erfasst Google Analytics google-authentifizierte ID´s der Nutzer, die vorübergehend mit unseren Google-Analytics-Daten verknüpft werden, um Zielgruppen für die geräteübergreifende Anzeigenwerbung zu definieren und zu erstellen.\r\n  </p>\r\n  <p class=\"legal-text\">\r\n    Sie können dem geräteübergreifenden Remarketing/Targeting dauerhaft widersprechen, indem Sie personalisierte Werbung in Ihrem Google-Konto deaktivieren; folgen Sie hierzu diesem Link:\r\n\r\n  </p>\r\n  <p class=\"legal-text\">\r\n    https://www.google.com/settings/ads/onweb/\r\n  </p>\r\n\r\n  <p class=\"legal-text\">Weitergehende Informationen und die Datenschutzbestimmungen finden Sie in der Datenschutzerklärung von Google unter:</p>\r\n  <p class=\"legal-text\">http://www.google.com/policies/technologies/ads/</p>\r\n  <p class=\"legal-text\">Die Zusammenfassung der erfassten Daten in Ihrem Google-Konto Daten erfolgt ausschließlich auf Grundlage Ihrer Einwilligung, die Sie bei Google abgeben oder widerrufen können (Art. 6 Abs. 1 lit. a DSGVO). Bei Datenerfassungsvorgängen, die nicht in Ihrem Google-Konto zusammengeführt werden (z.B. weil Sie kein Google-Konto haben oder der Zusammenführung widersprochen haben) beruht die Erfassung der Daten auf Art. 6 Abs. 1 lit. f DSGVO. Das berechtigte Interesse ergibt sich daraus, dass der Webseitenbetreiber ein Interesse an der anonymisierten Analyse der Webseitenbesucher zu Werbezwecken hat.</p>\r\n\r\n\r\n  <h3 class=\"legal-title\">\r\n    Google AdWords und Google Conversion-Tracking\r\n  </h3>\r\n\r\n  <p class=\"legal-text\">\r\n    Diese Webseite verwendet Google AdWords. AdWords ist ein Online-Werbeprogramm der Google Inc., 1600 Amphitheatre Parkway, Mountain View, CA 94043, United States (“Google”).\r\n  </p>\r\n\r\n  <p class=\"legal-text\">Im Rahmen von Google AdWords nutzen wir das so genannte Conversion-Tracking. Wenn Sie auf eine von Google geschaltete Anzeige klicken wird ein Cookie für das Conversion-Tracking gesetzt. Bei Cookies handelt es sich um kleine Textdateien, die der Internet-Browser auf dem Computer des Nutzers ablegt. Diese Cookies verlieren nach 30 Tagen ihre Gültigkeit und dienen nicht der persönlichen Identifizierung der Nutzer. Besucht der Nutzer bestimmte Seiten dieser Website und das Cookie ist noch nicht abgelaufen, können Google und wir erkennen, dass der Nutzer auf die Anzeige geklickt hat und zu dieser Seite weitergeleitet wurde.\r\n\r\n  </p>\r\n  <p class=\"legal-text\">Jeder Google AdWords-Kunde erhält ein anderes Cookie. Die Cookies können nicht über die Websites von AdWords-Kunden nachverfolgt werden. Die mithilfe des Conversion-Cookies eingeholten Informationen dienen dazu, Conversion-Statistiken für AdWords-Kunden zu erstellen, die sich für Conversion-Tracking entschieden haben. Die Kunden erfahren die Gesamtanzahl der Nutzer, die auf ihre Anzeige geklickt haben und zu einer mit einem Conversion-Tracking-Tag versehenen Seite weitergeleitet wurden. Sie erhalten jedoch keine Informationen, mit denen sich Nutzer persönlich identifizieren lassen. Wenn Sie nicht am Tracking teilnehmen möchten, können Sie dieser Nutzung widersprechen, indem Sie das Cookie des Google Conversion-Trackings über ihren Internet-Browser unter Nutzereinstellungen leicht deaktivieren. Sie werden sodann nicht in die Conversion-Tracking Statistiken aufgenommen.\r\n\r\n  </p>\r\n  <p class=\"legal-text\">Die Speicherung von „Coversion-Cookies“ erfolgt auf Grundlage von Art. 6 Abs. 1 lit. f DSGVO. Der Webseitenbetreiber hat ein berechtigtes Interesse an der Analyse des Nutzerverhaltens, um sowohl sein Webangebot als auch seine Werbung zu optimieren.\r\n\r\n  </p>\r\n  <p class=\"legal-text\">Mehr Informationen zu Google AdSense und Google Conversion-Tracking finden Sie in den Datenschutzbestimmungen von Google:\r\n\r\n  </p>\r\n  <p class=\"legal-text\">\r\n    http://www.google.de/policies/privacy/\r\n  </p>\r\n  <p class=\"legal-text\">\r\n    Sie können Ihren Browser so einstellen, dass Sie über das Setzen von Cookies informiert werden und Cookies nur im Einzelfall erlauben, die Annahme von Cookies für bestimmte Fälle oder generell ausschließen sowie das automatische Löschen der Cookies beim Schließen des Browsers aktivieren. Bei der Deaktivierung von Cookies kann die Funktionalität dieser Website eingeschränkt sein.\r\n  </p>\r\n\r\n  \r\n  <h3 class=\"legal-title\">\r\n    4. Sonstige Informationen\r\n  </h3>\r\n  \r\n  <h3 class=\"legal-title\">\r\n    Einhaltung von Gesetzen\r\n\r\n  </h3>\r\n\r\n\r\n  <p class=\"legal-text\">\r\n    Wir werden Ihre persönlichen Daten offenlegen, wenn dies gesetzlich vorgeschrieben oder durch eine Vorladung verfügt wird oder wenn wir glauben, dass diese Maßnahmen erforderlich sind, um das Gesetz und die angemessenen Forderungen der Strafverfolgungsbehörden zu erfüllen oder die Sicherheit oder Integrität unseres Dienstes zu schützen.\r\n\r\n  </p>\r\n\r\n  <h3 class=\"legal-title\">\r\n    Business Transaktionen\r\n\r\n  </h3>\r\n\r\n  <p class=\"legal-text\">\r\n    Falls Crowdy.ai an einer Fusion, an einer Übernahme oder einem Vermögensverkauf beteiligt ist, können Ihre personenbezogenen Daten übertragen werden. Wir informieren Sie, bevor Ihre personenbezogenen Daten übertragen werden und einer anderen Datenschutzerklärung unterliegen.\r\n\r\n  </p>\r\n  \r\n  \r\n\r\n  <h3 class=\"legal-title\">\r\n    Internationaler Transfer\r\n\r\n\r\n  </h3>\r\n\r\n  <p class=\"legal-text\">\r\n    Ihre Informationen einschließlich personenbezogener Daten können an Computer weitergegeben werden, die sich außerhalb Ihres Staates, Ihrer Provinz, Ihres Landes oder im Geltungsbereich einer anderen staatlichen Gerichtsbarkeit befinden, wo die Datenschutzgesetze von den für Ihren Standort gültigen Gesetzen abweichen können.\r\n\r\n  </p>\r\n  \r\n  <p class=\"legal-text\">\r\n    Wenn Sie sich außerhalb von Germany befinden und uns Informationen zur Verfügung stellen, beachten Sie bitte, dass wir die Informationen, einschließlich personenbezogener Daten, nach Germany übertragen und dort verarbeiten.\r\n\r\n\r\n  </p>\r\n  <p class=\"legal-text\">\r\n    Ihre Zustimmung zu dieser Datenschutzerklärung, gefolgt von Ihrer Einreichung solcher Daten entspricht Ihrer Zustimmung zu dieser Übertragung.\r\n\r\n\r\n  </p>\r\n  \r\n  \r\n  <h3 class=\"legal-title\">Links zu anderen Seiten\r\n\r\n  </h3>\r\n\r\n  <p class=\"legal-text\">\r\n    Unser Service kann Links zu anderen Websites enthalten, die nicht von uns betrieben werden. Wenn Sie auf einen Link eines Drittanbieters klicken, werden Sie an die Website des Dritten weitergeleitet. Wir empfehlen Ihnen dringend, die Datenschutzrichtlinien jeder Website, die Sie besuchen, zu überprüfen.\r\n  </p>\r\n  <p class=\"legal-text\">\r\n    Wir haben keine Kontrolle über und übernehmen keine Verantwortung für, die Inhalte, Datenschutzrichtlinien oder Praktiken von Websites oder Diensten von Drittanbietern.\r\n  </p>\r\n  \r\n\r\n  <h3 class=\"legal-title\">Datenschutz und Kinder</h3>\r\n  \r\n  <p class=\"legal-text\">Unser Service richtet sich nicht an Personen unter 13 Jahren (“Kinder”).  </p>\r\n\r\n  <p class=\"legal-text\">Wir sammeln nicht wissentlich personenbezogene Daten von Kindern unter 13 Jahren. Wenn Sie ein Elternteil oder Vormund sind und wissen, dass Ihr Kind uns persönliche Daten übermittelt hat, kontaktieren Sie uns bitte. Wenn wir uns bewusst sind, dass wir personenbezogene Daten von Kindern unter 13 Jahren ohne Zustimmung der Eltern gesammelt haben, ergreifen wir Maßnahmen, um diese Informationen von unseren Servern zu entfernen.\r\n\r\n  </p>\r\n\r\n  <h3 class=\"legal-title\">BStand: Februar 2021\r\n\r\n\r\n  </h3>\r\n\r\n</div>";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/auth/privacy-policy/privacy-policy.page.html":
    /*!**********************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/auth/privacy-policy/privacy-policy.page.html ***!
      \**********************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppPagesAuthPrivacyPolicyPrivacyPolicyPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header>\r\n  <ion-toolbar color=\"primary\">\r\n    <ion-buttons slot=\"end\">\r\n      <ion-button (click)=\"dismiss()\">Close</ion-button>\r\n    </ion-buttons>\r\n    <ion-title>\r\n      Privacy Policy\r\n    </ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content class=\"legal-content\">\r\n\r\n  <ion-segment (ionChange)=\"segmentChanged($event)\" value=\"english\">\r\n    <ion-segment-button value=\"english\">\r\n      <ion-label>English</ion-label>\r\n    </ion-segment-button>\r\n    <ion-segment-button value=\"german\">\r\n      <ion-label>German</ion-label>\r\n    </ion-segment-button>\r\n  </ion-segment>\r\n\r\n  <ng-container *ngIf=\"isEnglish\">\r\n    <app-privacy-english></app-privacy-english>\r\n\r\n  </ng-container>\r\n  \r\n  <ng-container *ngIf=\"!isEnglish\">\r\n    <app-privacy-german></app-privacy-german>\r\n  </ng-container>\r\n\r\n \r\n \r\n</ion-content>\r\n";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/auth/signup/signup.page.html":
    /*!******************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/auth/signup/signup.page.html ***!
      \******************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppPagesAuthSignupSignupPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-content class=\"signup-content\">\r\n  <app-aspect-ratio [ratio]=\"{w:100, h:17}\">\r\n    <app-image-shell style=\"width:150px; height: 34px; margin: 0 auto;\" animation=\"spinner\" [src]=\"'assets/tl-logo-simpel-white.png'\" [alt]=\"'walkthrough'\"></app-image-shell>\r\n  </app-aspect-ratio>\r\n\r\n  <div class=\"form-container\">\r\n    <h2 class=\"auth-title\">\r\n      Sign up to Tell Lisa\r\n    </h2>\r\n    <p>Already a member? <a [routerLink]=\"['/auth/login']\">Sign in.</a></p>\r\n\r\n    <form [formGroup]=\"signupForm\" (ngSubmit)=\"signUpWithEmail()\">\r\n      <ion-list class=\"inputs-list\" lines=\"none\">\r\n        <ion-grid>\r\n          <ion-row>\r\n            <ion-col size=\"6\" style=\"padding-left:0;\">\r\n              <ion-item class=\"input-item\">\r\n                <ion-label position=\"stacked\"><p>Firstname</p></ion-label>\r\n                <ion-input type=\"text\" placeholder=\"Your fist name\" formControlName=\"firstName\" clearInput autocapitalize=\"off\" inputmode=\"firstName\"></ion-input>\r\n              </ion-item>\r\n              <div class=\"error-container\">\r\n                <ng-container *ngFor=\"let validation of validation_messages.firstName\">\r\n                  <div class=\"error-message\" *ngIf=\"signupForm.get('firstName').hasError(validation.type) && (signupForm.get('firstName').dirty || signupForm.get('firstName').touched)\">\r\n                    <ion-icon name=\"information-circle-outline\"></ion-icon>\r\n                    <span>{{ validation.message }}</span>\r\n                  </div>\r\n                </ng-container>\r\n              </div>\r\n            </ion-col>\r\n            \r\n            <ion-col size=\"6\" style=\"padding-right:0;\">\r\n              <ion-item class=\"input-item\">\r\n                <ion-label position=\"stacked\"><p>Lastname</p></ion-label>\r\n                <ion-input type=\"text\" placeholder=\"Your last name\" formControlName=\"lastName\" clearInput autocapitalize=\"off\" inputmode=\"lastName\"></ion-input>\r\n              </ion-item>\r\n              <div class=\"error-container\">\r\n                <ng-container *ngFor=\"let validation of validation_messages.lastName\">\r\n                  <div class=\"error-message\" *ngIf=\"signupForm.get('lastName').hasError(validation.type) && (signupForm.get('lastName').dirty || signupForm.get('lastName').touched)\">\r\n                    <ion-icon name=\"information-circle-outline\"></ion-icon>\r\n                    <span>{{ validation.message }}</span>\r\n                  </div>\r\n                </ng-container>\r\n              </div>\r\n            </ion-col>\r\n\r\n          </ion-row>\r\n        </ion-grid>\r\n        \r\n        <ion-item class=\"input-item\">\r\n          <ion-label position=\"stacked\"><p>Email</p></ion-label>\r\n          <ion-input type=\"email\" placeholder=\"Email\" formControlName=\"email\" clearInput autocapitalize=\"off\" inputmode=\"email\"></ion-input>\r\n        </ion-item>\r\n        <div class=\"error-container\">\r\n          <ng-container *ngFor=\"let validation of validation_messages.email\">\r\n            <div class=\"error-message\" *ngIf=\"signupForm.get('email').hasError(validation.type) && (signupForm.get('email').dirty || signupForm.get('email').touched)\">\r\n              <ion-icon name=\"information-circle-outline\"></ion-icon>\r\n              <span>{{ validation.message }}</span>\r\n            </div>\r\n          </ng-container>\r\n        </div>\r\n       \r\n        <div class=\"label-row\">\r\n          <ion-label position=\"stacked\"><p>Country</p></ion-label>\r\n        </div>\r\n        <ion-item class=\"input-item\">\r\n          <ion-select formControlName=\"country\" placeholder=\"Where you live\" cancelText=\"Cancel\" okText=\"OK\">\r\n            <ion-select-option *ngFor=\"let item of countries\" [value]=\"item\" >{{item.name}}</ion-select-option>\r\n          </ion-select>\r\n        </ion-item>\r\n  \r\n       \r\n  \r\n        <div formGroupName=\"matching_passwords\">\r\n          <div class=\"label-row\">\r\n            <ion-label position=\"stacked\"><p>Password</p></ion-label>\r\n          </div>\r\n          <ion-item class=\"tl-auth-item\">\r\n\r\n            <app-show-hide-password>\r\n              <ion-input type=\"password\" placeholder=\"Password\" formControlName=\"password\"></ion-input>\r\n            </app-show-hide-password>\r\n          </ion-item>\r\n          <div class=\"error-container\">\r\n            <ng-container *ngFor=\"let validation of validation_messages.password\">\r\n              <div class=\"error-message\" *ngIf=\"signupForm.get('matching_passwords').get('password').hasError(validation.type) && (signupForm.get('matching_passwords').get('password').dirty || signupForm.get('matching_passwords').get('password').touched)\">\r\n                <ion-icon name=\"information-circle-outline\"></ion-icon>\r\n                <span>{{ validation.message }}</span>\r\n              </div>\r\n            </ng-container>\r\n          </div>\r\n\r\n          <div class=\"label-row\">\r\n            <ion-label position=\"stacked\"><p>Confirm Password</p></ion-label>\r\n          </div>\r\n  \r\n          <ion-item class=\"tl-auth-item\">\r\n\r\n            <app-show-hide-password>\r\n              <ion-input type=\"password\" placeholder=\"Confirm Password\" formControlName=\"confirm_password\"></ion-input>\r\n            </app-show-hide-password>\r\n          </ion-item>\r\n          <div class=\"error-container\">\r\n            <ng-container *ngFor=\"let validation of validation_messages.confirm_password\">\r\n              <div class=\"error-message\" *ngIf=\"signupForm.get('matching_passwords').get('confirm_password').hasError(validation.type) && (signupForm.get('matching_passwords').get('confirm_password').dirty || signupForm.get('matching_passwords').get('confirm_password').touched)\">\r\n                <ion-icon name=\"information-circle-outline\"></ion-icon>\r\n                <span>{{ validation.message }}</span>\r\n              </div>\r\n            </ng-container>\r\n          </div>\r\n        </div>\r\n        <div class=\"error-container\">\r\n          <ng-container *ngFor=\"let validation of validation_messages.matching_passwords\">\r\n            <div class=\"error-message\" *ngIf=\"signupForm.get('matching_passwords').hasError(validation.type) && (signupForm.get('matching_passwords').get('confirm_password').dirty || signupForm.get('matching_passwords').get('confirm_password').touched)\">\r\n              <ion-icon name=\"information-circle-outline\"></ion-icon>\r\n              <span>{{ validation.message }}</span>\r\n            </div>\r\n          </ng-container>\r\n        </div>\r\n      </ion-list>\r\n      <ion-list class=\"inputs-list\" lines=\"none\">\r\n        <ion-item class=\"input-item terms-item\">\r\n          <ion-checkbox formControlName=\"terms\"></ion-checkbox>\r\n          <div class=\"legal-stuff\">\r\n            By creating an account you agree to our <a class=\"legal-action\" (click)=\"showPrivacyModal()\">Privacy Policy</a> and <a class=\"legal-action\" (click)=\"showTermsModal()\">Terms of use</a>.\r\n          </div>\r\n        </ion-item>\r\n        <div class=\"error-container\">\r\n          <ng-container *ngFor=\"let validation of validation_messages.terms\">\r\n            <div class=\"error-message\" *ngIf=\"signupForm.get('terms').hasError(validation.type) && (signupForm.get('terms').dirty || signupForm.get('terms').touched)\">\r\n              <ion-icon name=\"information-circle-outline\"></ion-icon>\r\n              <span>{{ validation.message }}</span>\r\n            </div>\r\n          </ng-container>\r\n        </div>\r\n      </ion-list>\r\n  \r\n      <ion-button class=\"signup-btn\" type=\"submit\" expand=\"block\" [disabled]=\"!signupForm.valid\">Sign Up</ion-button>\r\n  \r\n    </form>\r\n    <div class=\"separator\">Or</div>\r\n\r\n    <div class=\"social-logins d-flex justify-content-between\">\r\n      <ion-button type=\"button\" class=\"google-signin\" color=\"grey-primary\" (click)=\"doGoogleSignup()\">\r\n        <ion-icon name=\"logo-google\"></ion-icon>\r\n        <ion-label>Sign in with Google</ion-label>\r\n      </ion-button>\r\n      <ion-button type=\"button\" class=\"fb-signin\" color=\"lighter-grey\" (click)=\"doFacebookSignup()\">\r\n        <ion-icon name=\"logo-facebook\"></ion-icon>\r\n      </ion-button>\r\n      <ion-button type=\"button\" class=\"twitter-signin\" color=\"lighter-grey\" (click)=\"doTwitterSignup()\">\r\n        <ion-icon name=\"logo-twitter\"></ion-icon>\r\n      </ion-button>\r\n    </div>\r\n  \r\n    \r\n  </div>\r\n  \r\n</ion-content>\r\n";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/auth/terms-of-service/terms-of-service.page.html":
    /*!**************************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/auth/terms-of-service/terms-of-service.page.html ***!
      \**************************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppPagesAuthTermsOfServiceTermsOfServicePageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header>\r\n  <ion-toolbar color=\"primary\">\r\n    <ion-buttons slot=\"end\">\r\n      <ion-button (click)=\"dismiss()\">Close</ion-button>\r\n    </ion-buttons>\r\n    <ion-title>\r\n      Terms Of Service\r\n    </ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content class=\"legal-content\">\r\n  <h3 class=\"legal-header\">General Terms and Conditions of the Company Trust UG (haftungsbeschraenkt)</h3>\r\n  \r\n  <h3 class=\"legal-title\">General</h3>\r\n  <p class=\"legal-text\">\r\n    These General Terms and Conditions of Business (“GTC”) elucidate the purchase of goods and services over the website telllisa.com (hereinafter the “Website”) operated by G-TAC Software (hereinafter “us”, “we” or “our”). Furthermore, they regulate the use of the website.\r\n  </p>\r\n  <h3 class=\"legal-title\">\r\n    Contract partner and instructions for the conclusion of the contract\r\n  </h3>\r\n  <p class=\"legal-text\">\r\n    The exclusive vendor and contract partner for non-german market of the goods and services offered over the website telllisa.com is the Paddle.com Market Ltd, 15 Bermondsey Square, SE1 3UN London, United Kingdom (hereinafter “Paddle”). The contract conclusion over us, in our capacity as the operator of this website, is therefore neither feasible nor valid. Valid are the terms and conditions separately specified by Paddle prior to the conclusion of the contract. The General Terms and Conditions (GTC) of Paddle, shall in the case of contradiction, be deemed to take precedence over the General Terms and Conditions (GTC) specified herein.\r\n  </p>\r\n\r\n  <p class=\"legal-text\">\r\n    The offers provisioned on our website exclusively constitute an invitation to submit a contract offer to Paddle. In order to purchase goods or services over our website, you must first select the desired goods / services. After selecting the goods you will be redirected to Paddle. Only after being redirected to Paddle, is it possible for you to submit a binding contract offer by clicking on the “Pay” button. The acceptance of the contract is concluded, insofar as you are prompted for payment by Paddle, or insofar as you are enabled to download the digital content.\r\n  </p>\r\n  <p class=\"legal-text\">\r\n    When placing an offer via the online order form on our website, the contract text is not stored by us.\r\n  </p>\r\n  <p class=\"legal-text\">\r\n    Prior to the binding submission of the order, you can at any given time correct your entries using the usual keyboard and mouse functions. In addition, all entries prior to the binding submission of the order are displayed again in a confirmation window and can also be corrected there using the usual keyboard and mouse functions.\r\n  </p>\r\n  <p class=\"legal-text\">\r\n    The following languages are available for the conclusion of the contract: German and English.\r\n  </p>\r\n  <p class=\"legal-text\">\r\n    The processing of the payment is effected over Paddle. You will be informed by Paddle, about the payment methods and payment details. Valid are the prices listed on our website at the time of the placement of the order.\r\n  </p>\r\n  <p class=\"legal-text\">\r\n    After the payment, the subscribed / purchased services are provisioned, ready for you to download and use.\r\n  </p>\r\n\r\n\r\n  <h3 class=\"legal-title\">Exclusion of consumers / private individuals</h3>\r\n  <p class=\"legal-text\">\r\n    The services offered over this website are exclusively reserved for entrepreneurs / companies. Consumers / private individuals are not entitled to procure our services.\r\n  </p>\r\n  \r\n  <h3 class=\"legal-title\">Services and rights of use</h3>\r\n  <p class=\"legal-text\">\r\n    Customers can procure software (hereinafter referred to as “Service”) for a specified period of time, over the website. The Service is available to the customer in the latest version over the Internet against a fee. The current functional scope of the Service results from its current performance specification on our website. We shall immediately remedy all software faults, according to the given technical possibilities. A software fault is deemed to exist, insofar as the software does not fulfil the functions specified in the performance specification, returns erroneous results or otherwise does not function properly, so that the use of the software is rendered impossible or limited.\r\n  </p>\r\n  <p class=\"legal-text\">\r\n    The customer is granted the non-exclusive and non-transferable right to use the procured Service, as intended, during the term of the contract. The customer may only edit the software, insofar as this is covered by the intended use of the software according to the current performance specification. The customer may only reproduce the software, insofar as this is covered by the intended use of the software, in accordance with the current performance specification. The customer is not entitled to make the software available for use to third parties against payment or free of charge. The subletting of the software on the part of the customer is therefore expressly prohibited.\r\n  </p>\r\n\r\n  <h3 class=\"legal-title\">Warranty</h3>\r\n  <p class=\"legal-text\">\r\n    Valid for the procured goods / services are the statutory warranty provisions for defects.\r\n  </p>\r\n  \r\n  <h3 class=\"legal-title\">Subscriptions</h3>\r\n\r\n  <p class=\"legal-text\">Some parts of the Service are billed on a subscription basis (hereinafter “Subscription(s)”, “Subscription Service(s)”). They are billed in advance on a recurring and periodic basis (hereinafter “Billing Period”). The duration of the Billing Period is either one month or one year, depending on which Subscription Service you selected at the time of the procurement.\r\n  </p>\r\n  <p class=\"legal-text\">At the end of each billing cycle, the Subscription automatically renews pursuant to the exact same terms, unless terminated at the latest on the last day before the start of a new billing cycle. Upon termination, your right of use will be terminated, at the end of the pro rata already paid for, Subscription Service(s). Insofar as you want to cancel your account, you can manually do this over our Service dashboard. Your authorised regular payment, with regard to our Reseller Paddle, will automatically be terminated subject to the notice.\r\n  </p>\r\n  <p class=\"legal-text\">A valid payment method, including credit card or PayPal, is required to process the payment for your Subscription. You must provide your payment details including the full name, address, state, postal code, telephone number and valid payment method information at the time of the conclusion of the contract. By submitting this payment details, you automatically entitle Paddle to direct debit all Subscription fees for your account, through this means of payment.\r\n  </p>\r\n  <p class=\"legal-text\">If for any reason automatic direct debit fails, Paddle will issue an electronic invoice stating that within a specified period of time, you will be required to manually pay the full amount corresponding to the billing period indicated on the invoice.\r\n  </p>\r\n\r\n  <h3 class=\"legal-title\">Free trial Subscription Service</h3>\r\n  <p class=\"legal-text\">You can subscribe for the provision of a free trial version for a limited period of time (hereinafter “Free Trial”, “Trial-Subscription Service”).\r\n  </p>\r\n  <p class=\"legal-text\">You may need to enter your billing information to sign up for the free trial. This information is required to prevent misuse of the free trial feature (for example, by way of multiple registrations).\r\n  </p>\r\n  <p class=\"legal-text\">Insofar as you enter your billing information, when you sign up for the free trial version, G-TAC will not debit you until the term of the free trial version expires. Insofar as you have not cancelled the Subscription, you will automatically be charged the applicable fee for the Subscription Service you selected, on the last day of the term of the free trial version.\r\n  </p>\r\n  <p class=\"legal-text\">The free trial versions can be changed or discontinued at any time.\r\n  </p>\r\n\r\n  <h3 class=\"legal-title\">Change of fees </h3>\r\n  <p class=\"legal-text\">\r\n    <strong>Trust</strong> reserves the right, to at its sole discretion and at any time, but no more than once within 365 days, change the fees for the Subscriptions. Any change to the Subscription fees will take effect at the end of the current billing cycle.\r\n  </p>\r\n  <p class=\"legal-text\">\r\n    <strong>Trust</strong> shall provide you with adequate notice of any change to the Subscription fees, in order to grant you the opportunity to cancel your Subscription before this change takes effect.\r\n  </p>\r\n  <p class=\"legal-text\">\r\n    Your continued use of the Service after the change in Subscription fees has come into force, shall be deemed to constitute your granted consent to pay the changed Subscription fee.\r\n  </p>\r\n\r\n  <h3 class=\"legal-title\">Refunds</h3>\r\n  <p class=\"legal-text\">\r\n    Specific requests for refunds for Subscriptions may be reviewed by <strong>G-TAC</strong> on a case-by-case basis and granted at the sole discretion of <strong>G-TAC</strong>.\r\n  </p>\r\n  <h3 class=\"legal-title\">Content</h3>\r\n  <p class=\"legal-text\">\r\n    Our Service permits you to post, link, store, share and otherwise distribute specific information, text, graphics, videos or other material (hereinafter “Content”). You are responsible for the Content you send to the Service, including its legality, reliability and adequacy.\r\n  </p>\r\n  \r\n  <p class=\"legal-text\">\r\n    By sending Content to the Service, you grant us the right and license to use, modify, show publicly, publicly display, reproduce and distribute such Content, on and through the Service. You retain all rights to the Content you submit to us, on or through the Service, and you are responsible for the protection of such rights. You herewith expressly declare that this license includes the right to make your Content available to other users of the Service, who may also use the Content under these Terms and Conditions.\r\n  </p>\r\n\r\n  <p class=\"legal-text\">\r\n    You herewith expressly declare and warrant that: (i) the Content is yours (you own it), or you have the right to use it and grant us the rights and licenses in accordance with these Terms of Use, and (ii) the publishing of your Content, on or through the Service, does not violate the rights, privacy, publication rights, copyrights, contract rights or other rights, of any given third party.\r\n  </p>\r\n\r\n  <h3 class=\"legal-title\">\r\n    Accounts\r\n  </h3>\r\n\r\n  <p class=\"legal-text\">\r\n    When you create an account with us, you must provide us with information that is accurate, complete and up-to-date at all times. Failure to comply with these terms constitutes a breach of the Terms of Use which may result in immediate termination of your account on our Service.\r\n  </p>\r\n  <p class=\"legal-text\">You are responsible for the safeguarding of the password that you use to access the Service, and for all activities or actions executed under your password, regardless of whether your password was created for our Service or for a third-party service.\r\n  </p>\r\n  <p class=\"legal-text\">You are not permitted to disclose your password to third parties. You must notify us immediately, insofar as you become aware of any breach of the security or unauthorised use of your account.\r\n  </p>\r\n  <p class=\"legal-text\">You may not use as a username: the name of another person or entity that is not lawfully available for use; a name or a trademark without appropriate authorization, for which the rights are owned by third parties or organisations, other than yourself; or a name that is offensive, vulgar or obscene.\r\n  </p>\r\n\r\n  <h3 class=\"legal-title\">\r\n    Copyright guidelines\r\n  </h3>\r\n\r\n  <p class=\"legal-text\">\r\n    We respect the intellectual property rights of third parties. It is our policy to respond to any allegation, pursuant to which it is adduced that Content posted on the Service violates the copyright or other intellectual property rights of any given third party (hereinafter “Infringement”).\r\n  </p>\r\n  <p class=\"legal-text\">\r\n    Insofar as you own a copyright or are authorised by the copyright owner, and you believe that the copyrighted work has been copied in a manner that constitutes an infringement of the copyright, on account of its publication through the Service, you must notify the competent “Copyright Infringement” department in writing under <strong>dmca at telllisa dot com</strong> and enclose with your message a detailed description of the alleged Infringement.\r\n  </p>\r\n  <p class=\"legal-text\">\r\n    You may be held responsible for any damages (including costs and attorney’s fees) for a false claim that any Content whatsoever constitutes an Infringement of your copyright.\r\n  </p>\r\n\r\n  <h3 class=\"legal-title\">\r\n    Intellectual property\r\n  </h3>\r\n\r\n  <p class=\"legal-text\">\r\n    The Service and its original Content (excluding the Content provisioned by users), its features and functionality are and remain the property of <strong>G-TAC</strong> and its licensors. The Service is protected by copyright, trademarks and other laws of the State of Rhineland-Palatinate, Germany as well as other countries. Our trademarks and trade names may not be used in connection with products or services without the prior written consent of <strong>G-TAC</strong>.\r\n  </p>\r\n\r\n  <h3 class=\"legal-title\">\r\n    Links to other websites\r\n  </h3>\r\n\r\n  <p class=\"legal-text\">\r\n    Our Service may entail links to third-party websites or services that are not the property of <strong>G-TAC</strong>.\r\n  </p>\r\n  <p class=\"legal-text\">\r\n    <strong>G-TAC</strong> neither has control, nor accepts responsibility for the content, data protection provisions or practices of any third party websites or services. You further expressly declare that <strong>G-TAC</strong> is not responsible or liable, directly or indirectly, for any damage or loss arising from or in connection with the use or reliance on such content, goods or services, on or through such websites or services.\r\n  </p>\r\n  <p class=\"legal-text\">\r\n    We strongly recommend that you read through, the terms and conditions of use and the data protection policies of third-party websites or services, which you visit.\r\n  </p>\r\n\r\n  <h3 class=\"legal-title\">\r\n    Termination\r\n  </h3>\r\n\r\n  <p class=\"legal-text\">\r\n    We reserve the right to terminate or suspend your account immediately, without notice or liability, for any reason whatsoever, and without limitation, insofar as you violate these General Terms and Conditions (GTC).\r\n  </p>\r\n  <p class=\"legal-text\">\r\n    Upon termination, your right of use will be discontinued immediately. If you want to cancel your account, you can simply terminate your use of the Service.\r\n  </p>\r\n\r\n  <h3 class=\"legal-title\">\r\n    Liability\r\n  </h3>\r\n\r\n  <p class=\"legal-text\">\r\n    We expressly point out that you are solely responsible for the compliance with the statutory provisions when using our software. This applies in particular to compliance with the data protection law. The following liability rules remain unaffected, wherefrom.\r\n  </p>\r\n  <p class=\"legal-text\">\r\n    We are liable to the customer for all contractual, quasi-contractual and statutory, as well as tort claims for damages and reimbursement of expenses, as follows:\r\n  </p>\r\n  <p class=\"legal-text\">\r\n    We are unreservedly liable, irrespective of any legal reason whatsoever, in the case of intent or gross negligence, intentional or negligent injury to life, body or health, on the basis of an issued warranty, unless otherwise stipulated or due to mandatory liability.\r\n  </p>\r\n  <p class=\"legal-text\">\r\n    Insofar as we are negligently in breach of an essential contractual obligation, the liability shall be limited to the contractually typical, foreseeable damage, unless liability is unlimited in accordance with the preceding clause. Essential contractual obligations are obligations, which the contract imposes on the vendor according to the content of the vendor, for the purpose of achieving the purpose of the contract, the fulfilment of which enables the proper execution of the contract in the first place, and on the compliance of which the customer may regularly rely.\r\n  </p>\r\n\r\n  <p class=\"legal-text\">\r\n    Any other further reaching liability on our part is otherwise expressly excluded. The above liability provisions, also apply with regard to our liability for our vicarious agents and legal representatives.\r\n  </p>\r\n  <p class=\"legal-text\">\r\n    You herewith expressly indemnify us from any claims of third parties – including any associated costs of legal defence in their applicable statutory amount – that are asserted against us, on the basis of legal or contractually adverse actions on your part.\r\n  </p>\r\n\r\n\r\n  <h3 class=\"legal-title\">\r\n    Changes to these General Terms and Conditions (GTC)\r\n  </h3>\r\n\r\n  <p class=\"legal-text\">\r\n    We reserve the right to change these Terms of Use for factually justified reasons (such as changes in case law, legislation, market conditions or our corporate strategy) and in compliance with a reasonable period. Existing customers will be notified thereof by e-mail, no later than four weeks, before the change takes effect. Insofar as the existing customer does not raise an objection within the period specified in the change notification, the consent of the customer with regard to the change shall be deemed as granted. The notification of the intended change of these Terms of Use, shall expressly emphasise the specified period of objection and the consequences of the omission of an objection.\r\n  </p>\r\n\r\n  <h3 class=\"legal-title\">\r\n    Final provisions\r\n  </h3>\r\n\r\n  <p class=\"legal-text\">\r\n    Applicable is the law of the Federal Republic of Germany, under the exclusion of the UN sales law, insofar as this choice of law does not lead to a consumer being thereby deprived of mandatory consumer protection standards.\r\n  </p>\r\n  <p class=\"legal-text\">\r\n    Insofar as the customer is a merchant, a legal entity under public law or a special fund under public law, the competent court is the court with jurisdiction at our registered office, unless an exclusive place of jurisdiction is justified for the dispute. This also applies insofar as the customer is not resident in the European Union. For details of the headquarters of our company, please refer to the heading of these General Terms and Conditions (GTC).\r\n  </p>\r\n  <p class=\"legal-text\">\r\n    Insofar as any provision of this contract is or becomes invalid or unenforceable, the remaining provisions of this contract shall remain unaffected, wherefrom.\r\n  </p>\r\n\r\n  <h3 class=\"legal-title\">\r\n    Contact\r\n  </h3>\r\n\r\n  <p class=\"legal-text\">\r\n    Please do not hesitate to contact us, in case you have any questions that concern these provisions.\r\n  </p>\r\n\r\n  <h3 class=\"legal-title\">\r\n    Data protection statement for the use of Twitter / conversion measurement with „Twitter-Pixel” / „Custom Audiences”\r\n  </h3>\r\n\r\n  <p class=\"legal-text\">\r\n    Functions of the Twitter service are incorporated into our web pages. These functions are provisioned through the Twitter Inc., 1355 Market Street, Suite 900, San Francisco, CA 94103, USA. By using Twitter and the “Re-Tweet” function, the web pages you visit are linked to your Twitter account and shared with other users. This data is also thereby transmitted to Twitter. We expressly point out that we, in our capacity as the operator and provider of this website, are not aware of the content of the transmitted data and its use by Twitter.\r\n  </p>\r\n  <p class=\"legal-text\">\r\n    In particular, using the “Twitter pixel” set on our web pages, we can track users’ actions after they’ve seen or clicked on a Twitter ad. This procedure is used to evaluate the effectiveness of Twitter advertisements for statistical purposes and market research purposes, and can contribute to the optimisation of future advertising measures. The data collected is anonymous to us, so we can not draw conclusions about the identity of the users. However, the data is stored and processed by Twitter, so that a connection to the respective user profile is possible and Twitter can use the data for its own advertising purposes, in compliance with the Twitter data protection statement. You can enable Twitter as well as its affiliates to place advertisements on and outside of Twitter. A cookie may also be stored on your computer for these purposes. For further information in this regard, please refer to the data protection statement of Twitter under http://twitter.com/privacy.\r\n  </p>\r\n  <p class=\"legal-text\">\r\n    You can change your data protection settings and consents with regard to Twitter in the Account Settings under: http://twitter.com/account/settings.\r\n  </p>\r\n\r\n  <h4 class=\"ion-text-center\">Last update: February 2021</h4>\r\n</ion-content>\r\n";
      /***/
    },

    /***/
    "./src/app/core/validators/checkbox-checked.validator.ts":
    /*!***************************************************************!*\
      !*** ./src/app/core/validators/checkbox-checked.validator.ts ***!
      \***************************************************************/

    /*! exports provided: CheckboxCheckedValidator */

    /***/
    function srcAppCoreValidatorsCheckboxCheckedValidatorTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "CheckboxCheckedValidator", function () {
        return CheckboxCheckedValidator;
      });

      var CheckboxCheckedValidator = /*#__PURE__*/function () {
        function CheckboxCheckedValidator() {
          _classCallCheck(this, CheckboxCheckedValidator);
        }

        _createClass(CheckboxCheckedValidator, null, [{
          key: "minSelectedCheckboxes",
          value: function minSelectedCheckboxes(min) {
            var validator = function validator(formArray) {
              var totalSelected = formArray.controls // get a list of checkbox values (boolean)
              .map(function (control) {
                return control.value;
              }) // total up the number of checked checkboxes
              .reduce(function (prev, next) {
                return next ? prev + next : prev;
              }, 0); // if the total is not greater than the minimum, return the error message

              return totalSelected >= min ? null : {
                required: true
              };
            };

            return validator;
          }
        }]);

        return CheckboxCheckedValidator;
      }();
      /***/

    },

    /***/
    "./src/app/core/validators/index.ts":
    /*!******************************************!*\
      !*** ./src/app/core/validators/index.ts ***!
      \******************************************/

    /*! exports provided: CheckboxCheckedValidator, PasswordValidator, PhoneValidator, UsernameValidator */

    /***/
    function srcAppCoreValidatorsIndexTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony import */


      var _checkbox_checked_validator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! ./checkbox-checked.validator */
      "./src/app/core/validators/checkbox-checked.validator.ts");
      /* harmony reexport (safe) */


      __webpack_require__.d(__webpack_exports__, "CheckboxCheckedValidator", function () {
        return _checkbox_checked_validator__WEBPACK_IMPORTED_MODULE_0__["CheckboxCheckedValidator"];
      });
      /* harmony import */


      var _password_validator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ./password.validator */
      "./src/app/core/validators/password.validator.ts");
      /* harmony reexport (safe) */


      __webpack_require__.d(__webpack_exports__, "PasswordValidator", function () {
        return _password_validator__WEBPACK_IMPORTED_MODULE_1__["PasswordValidator"];
      });
      /* harmony import */


      var _phone_validator__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./phone.validator */
      "./src/app/core/validators/phone.validator.ts");
      /* harmony reexport (safe) */


      __webpack_require__.d(__webpack_exports__, "PhoneValidator", function () {
        return _phone_validator__WEBPACK_IMPORTED_MODULE_2__["PhoneValidator"];
      });
      /* harmony import */


      var _username_validator__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./username.validator */
      "./src/app/core/validators/username.validator.ts");
      /* harmony reexport (safe) */


      __webpack_require__.d(__webpack_exports__, "UsernameValidator", function () {
        return _username_validator__WEBPACK_IMPORTED_MODULE_3__["UsernameValidator"];
      });
      /***/

    },

    /***/
    "./src/app/core/validators/password.validator.ts":
    /*!*******************************************************!*\
      !*** ./src/app/core/validators/password.validator.ts ***!
      \*******************************************************/

    /*! exports provided: PasswordValidator */

    /***/
    function srcAppCoreValidatorsPasswordValidatorTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "PasswordValidator", function () {
        return PasswordValidator;
      });

      var PasswordValidator = /*#__PURE__*/function () {
        function PasswordValidator() {
          _classCallCheck(this, PasswordValidator);
        }

        _createClass(PasswordValidator, null, [{
          key: "areNotEqual",
          // If our validation fails, we return an object with a key for the error name and a value of true.
          // Otherwise, if the validation passes, we simply return null because there is no error.
          value: function areNotEqual(formGroup) {
            var firstControlValue;
            var valid = true;

            for (var key in formGroup.controls) {
              if (formGroup.controls.hasOwnProperty(key)) {
                var control = formGroup.controls[key];

                if (firstControlValue === undefined) {
                  firstControlValue = control.value;
                } else {
                  // check if the value of the first control is equal to the value of the second control
                  if (firstControlValue !== control.value) {
                    valid = false;
                    break;
                  }
                }
              }
            }

            if (valid) {
              return null;
            }

            return {
              areNotEqual: true
            };
          }
        }]);

        return PasswordValidator;
      }();
      /***/

    },

    /***/
    "./src/app/core/validators/phone.validator.ts":
    /*!****************************************************!*\
      !*** ./src/app/core/validators/phone.validator.ts ***!
      \****************************************************/

    /*! exports provided: PhoneValidator */

    /***/
    function srcAppCoreValidatorsPhoneValidatorTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "PhoneValidator", function () {
        return PhoneValidator;
      });
      /* harmony import */


      var google_libphonenumber__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! google-libphonenumber */
      "./node_modules/google-libphonenumber/dist/libphonenumber.js");
      /* harmony import */


      var google_libphonenumber__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(google_libphonenumber__WEBPACK_IMPORTED_MODULE_0__);

      var PhoneValidator = function PhoneValidator() {
        _classCallCheck(this, PhoneValidator);
      }; // Validate if a phone number belongs to a certain country.
      // If our validation fails, we return an object with a key for the error name and a value of true.
      // Otherwise, if the validation passes, we simply return null because there is no error.


      PhoneValidator.invalidCountryPhone = function (countryControl) {
        var subscribe = false;
        return function (phoneControl) {
          if (!subscribe) {
            subscribe = true;
            countryControl.valueChanges.subscribe(function () {
              phoneControl.updateValueAndValidity();
            });
          }

          if (phoneControl.value !== "") {
            try {
              var phoneUtil = google_libphonenumber__WEBPACK_IMPORTED_MODULE_0__["libphonenumber"].PhoneNumberUtil.getInstance();
              var phoneNumber = "" + phoneControl.value + "",
                  region = countryControl.value.iso,
                  number = phoneUtil.parse(phoneNumber, region),
                  isValidNumber = phoneUtil.isValidNumber(number);

              if (isValidNumber) {
                return null;
              }
            } catch (e) {
              return {
                invalidCountryPhone: true
              };
            }

            return {
              invalidCountryPhone: true
            };
          } else {
            return null;
          }
        };
      };
      /***/

    },

    /***/
    "./src/app/core/validators/username.validator.ts":
    /*!*******************************************************!*\
      !*** ./src/app/core/validators/username.validator.ts ***!
      \*******************************************************/

    /*! exports provided: UsernameValidator */

    /***/
    function srcAppCoreValidatorsUsernameValidatorTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "UsernameValidator", function () {
        return UsernameValidator;
      });

      var UsernameValidator = /*#__PURE__*/function () {
        function UsernameValidator() {
          _classCallCheck(this, UsernameValidator);
        }

        _createClass(UsernameValidator, null, [{
          key: "usernameNotAvailable",
          value: function usernameNotAvailable(fc) {
            // this is a dummy validator to check if the username is valid or not.
            // In a real app you should check against your DB if the username is already in use.
            // in this example we define two existing usernames: 'abc123' and '123abc'
            // If our validation fails, we return an object with a key for the error name and a value of true.
            // Otherwise, if the validation passes, we simply return null because there is no error.
            if (fc.value.toLowerCase() === 'abc123' || fc.value.toLowerCase() === '123abc') {
              return {
                usernameNotAvailable: true
              };
            } else {
              return null;
            }
          }
        }]);

        return UsernameValidator;
      }();
      /***/

    },

    /***/
    "./src/app/pages/auth/auth-routing.module.ts":
    /*!***************************************************!*\
      !*** ./src/app/pages/auth/auth-routing.module.ts ***!
      \***************************************************/

    /*! exports provided: AuthRoutingModule */

    /***/
    function srcAppPagesAuthAuthRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AuthRoutingModule", function () {
        return AuthRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_fire_auth_guard__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/fire/auth-guard */
      "./node_modules/@angular/fire/__ivy_ngcc__/fesm2015/angular-fire-auth-guard.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! rxjs/operators */
      "./node_modules/rxjs/_esm2015/operators/index.js");
      /* harmony import */


      var _login_login_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./login/login.page */
      "./src/app/pages/auth/login/login.page.ts");
      /* harmony import */


      var _signup_signup_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./signup/signup.page */
      "./src/app/pages/auth/signup/signup.page.ts"); // Firebase guard to redirect logged in users to profile


      var redirectLoggedInToProfile = function redirectLoggedInToProfile(next) {
        return Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (user) {
          // when queryParams['auth-redirect'] don't redirect because we want
          // the component to handle the redirection
          if (user !== null && !next.queryParams["auth-redirect"]) {
            return ["home"];
          } else {
            return true;
          }
        });
      };

      var routes = [{
        path: "",
        canActivate: [_angular_fire_auth_guard__WEBPACK_IMPORTED_MODULE_2__["AngularFireAuthGuard"]],
        data: {
          authGuardPipe: redirectLoggedInToProfile
        },
        children: [{
          path: "",
          redirectTo: "login",
          pathMatch: "full"
        }, {
          path: "login",
          component: _login_login_page__WEBPACK_IMPORTED_MODULE_5__["LoginPage"]
        }, {
          path: "signup",
          component: _signup_signup_page__WEBPACK_IMPORTED_MODULE_6__["SignupPage"]
        }]
      }];

      var AuthRoutingModule = function AuthRoutingModule() {
        _classCallCheck(this, AuthRoutingModule);
      };

      AuthRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"]]
      })], AuthRoutingModule);
      /***/
    },

    /***/
    "./src/app/pages/auth/auth.module.ts":
    /*!*******************************************!*\
      !*** ./src/app/pages/auth/auth.module.ts ***!
      \*******************************************/

    /*! exports provided: AuthModule */

    /***/
    function srcAppPagesAuthAuthModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AuthModule", function () {
        return AuthModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _core_services__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @core/services */
      "./src/app/core/services/index.ts");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _shared_lib_shared_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @shared-lib/shared.module */
      "./src/app/shared/shared.module.ts");
      /* harmony import */


      var _auth_routing_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ./auth-routing.module */
      "./src/app/pages/auth/auth-routing.module.ts");
      /* harmony import */


      var _login_login_page__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! ./login/login.page */
      "./src/app/pages/auth/login/login.page.ts");
      /* harmony import */


      var _privacy_policy__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
      /*! ./privacy-policy */
      "./src/app/pages/auth/privacy-policy/index.ts");
      /* harmony import */


      var _signup_signup_page__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
      /*! ./signup/signup.page */
      "./src/app/pages/auth/signup/signup.page.ts");
      /* harmony import */


      var _terms_of_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
      /*! ./terms-of-service */
      "./src/app/pages/auth/terms-of-service/index.ts");

      var AuthModule = function AuthModule() {
        _classCallCheck(this, AuthModule);
      };

      AuthModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
        declarations: [_login_login_page__WEBPACK_IMPORTED_MODULE_8__["LoginPage"], _signup_signup_page__WEBPACK_IMPORTED_MODULE_10__["SignupPage"], _terms_of_service__WEBPACK_IMPORTED_MODULE_11__["TermsOfServicePage"], _privacy_policy__WEBPACK_IMPORTED_MODULE_9__["PrivacyPolicyPage"], _privacy_policy__WEBPACK_IMPORTED_MODULE_9__["PrivacyGermanComponent"], _privacy_policy__WEBPACK_IMPORTED_MODULE_9__["PrivacyEnglishComponent"]],
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _auth_routing_module__WEBPACK_IMPORTED_MODULE_7__["AuthRoutingModule"], _shared_lib_shared_module__WEBPACK_IMPORTED_MODULE_6__["SharedModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"]],
        providers: [_core_services__WEBPACK_IMPORTED_MODULE_4__["FirebaseAuthService"]]
      })], AuthModule);
      /***/
    },

    /***/
    "./src/app/pages/auth/login/login.page.responsive.scss":
    /*!*************************************************************!*\
      !*** ./src/app/pages/auth/login/login.page.responsive.scss ***!
      \*************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppPagesAuthLoginLoginPageResponsiveScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "/* ----------- iPhone 4 and 4S ----------- */\n/* ----------- iPhone 5, 5S, 5C and 5SE ----------- */\n@media only screen and (min-device-width: 320px) and (max-device-width: 568px) and (-webkit-min-device-pixel-ratio: 2) and (device-aspect-ratio: 40/71) {\n  .auth-title {\n    font-size: 15px !important;\n  }\n\n  .form-container p {\n    font-size: 12px !important;\n    line-height: 15px !important;\n  }\n}\n/* ----------- iPhone 6, 6S, 7 and 8 ----------- */\n@media only screen and (min-device-width: 375px) and (max-device-width: 667px) and (-webkit-min-device-pixel-ratio: 2) {\n  .social-logins ion-button {\n    font-size: 11px;\n  }\n  .social-logins ion-button ion-label {\n    margin-left: 2.5px;\n  }\n}\n/* ----------- iPhone X ----------- */\n@media only screen and (min-device-width: 375px) and (max-device-width: 812px) and (-webkit-min-device-pixel-ratio: 3) {\n  .auth-title {\n    font-size: 18px !important;\n  }\n}\n/* ----------- iPhone 6+, 7+ and 8+ ----------- */\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvYXV0aC9sb2dpbi9sb2dpbi5wYWdlLnJlc3BvbnNpdmUuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFLQSw0Q0FBQTtBQUlBLHFEQUFBO0FBQ0E7RUFDRTtJQUNFLDBCQUFBO0VBUEY7O0VBVUU7SUFDRSwwQkFBQTtJQUNBLDRCQUFBO0VBUEo7QUFDRjtBQVdBLGtEQUFBO0FBQ0E7RUFFSTtJQUNFLGVBQUE7RUFWSjtFQVdJO0lBQ0Usa0JBQUE7RUFUTjtBQUNGO0FBY0EscUNBQUE7QUFDQTtFQUNFO0lBQ0UsMEJBQUE7RUFaRjtBQUNGO0FBZUEsaURBQUEiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9hdXRoL2xvZ2luL2xvZ2luLnBhZ2UucmVzcG9uc2l2ZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLy8gKE5vdGU6IERvbid0IGNoYW5nZSB0aGUgb3JkZXIgb2YgdGhlIGRldmljZXMgYXMgaXQgbWF5IGJyZWFrIHRoZSBjb3JyZWN0IGNzcyBwcmVjZWRlbmNlKVxyXG5cclxuLy8gKHNlZTogaHR0cHM6Ly9jc3MtdHJpY2tzLmNvbS9zbmlwcGV0cy9jc3MvbWVkaWEtcXVlcmllcy1mb3Itc3RhbmRhcmQtZGV2aWNlcy8jaXBob25lLXF1ZXJpZXMpXHJcbi8vIChzZWU6IGh0dHBzOi8vc3RhY2tvdmVyZmxvdy5jb20vYS80Nzc1MDI2MS8xMTE2OTU5KVxyXG5cclxuLyogLS0tLS0tLS0tLS0gaVBob25lIDQgYW5kIDRTIC0tLS0tLS0tLS0tICovXHJcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1pbi1kZXZpY2Utd2lkdGg6IDMyMHB4KSBhbmQgKG1heC1kZXZpY2Utd2lkdGg6IDQ4MHB4KSBhbmQgKC13ZWJraXQtbWluLWRldmljZS1waXhlbC1yYXRpbzogMikgYW5kIChkZXZpY2UtYXNwZWN0LXJhdGlvOiAyLzMpIHtcclxufVxyXG5cclxuLyogLS0tLS0tLS0tLS0gaVBob25lIDUsIDVTLCA1QyBhbmQgNVNFIC0tLS0tLS0tLS0tICovXHJcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1pbi1kZXZpY2Utd2lkdGg6IDMyMHB4KSBhbmQgKG1heC1kZXZpY2Utd2lkdGg6IDU2OHB4KSBhbmQgKC13ZWJraXQtbWluLWRldmljZS1waXhlbC1yYXRpbzogMikgYW5kIChkZXZpY2UtYXNwZWN0LXJhdGlvOiA0MCAvIDcxKSB7XHJcbiAgLmF1dGgtdGl0bGUge1xyXG4gICAgZm9udC1zaXplOiAxNXB4ICFpbXBvcnRhbnQ7XHJcbiAgfVxyXG4gIC5mb3JtLWNvbnRhaW5lciB7XHJcbiAgICBwIHtcclxuICAgICAgZm9udC1zaXplOiAxMnB4ICFpbXBvcnRhbnQ7XHJcbiAgICAgIGxpbmUtaGVpZ2h0OiAxNXB4ICFpbXBvcnRhbnQ7XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcblxyXG4vKiAtLS0tLS0tLS0tLSBpUGhvbmUgNiwgNlMsIDcgYW5kIDggLS0tLS0tLS0tLS0gKi9cclxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWluLWRldmljZS13aWR0aDogMzc1cHgpIGFuZCAobWF4LWRldmljZS13aWR0aDogNjY3cHgpIGFuZCAoLXdlYmtpdC1taW4tZGV2aWNlLXBpeGVsLXJhdGlvOiAyKSB7XHJcbiAgLnNvY2lhbC1sb2dpbnMge1xyXG4gICAgaW9uLWJ1dHRvbiB7XHJcbiAgICAgIGZvbnQtc2l6ZTogMTFweDtcclxuICAgICAgaW9uLWxhYmVsIHtcclxuICAgICAgICBtYXJnaW4tbGVmdDogMi41cHg7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcbn1cclxuXHJcbi8qIC0tLS0tLS0tLS0tIGlQaG9uZSBYIC0tLS0tLS0tLS0tICovXHJcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1pbi1kZXZpY2Utd2lkdGg6IDM3NXB4KSBhbmQgKG1heC1kZXZpY2Utd2lkdGg6IDgxMnB4KSBhbmQgKC13ZWJraXQtbWluLWRldmljZS1waXhlbC1yYXRpbzogMykge1xyXG4gIC5hdXRoLXRpdGxlIHtcclxuICAgIGZvbnQtc2l6ZTogMThweCAhaW1wb3J0YW50O1xyXG4gIH1cclxufVxyXG5cclxuLyogLS0tLS0tLS0tLS0gaVBob25lIDYrLCA3KyBhbmQgOCsgLS0tLS0tLS0tLS0gKi9cclxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWluLWRldmljZS13aWR0aDogNDE0cHgpIGFuZCAobWF4LWRldmljZS13aWR0aDogNzM2cHgpIGFuZCAoLXdlYmtpdC1taW4tZGV2aWNlLXBpeGVsLXJhdGlvOiAzKSB7XHJcbn1cclxuIl19 */";
      /***/
    },

    /***/
    "./src/app/pages/auth/login/login.page.scss":
    /*!**************************************************!*\
      !*** ./src/app/pages/auth/login/login.page.scss ***!
      \**************************************************/

    /*! exports provided: default */

    /***/
    function srcAppPagesAuthLoginLoginPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ":host {\n  --page-margin: var(--app-broad-margin);\n  --page-background: var(--app-background-shade);\n  --page-spacing: 10px;\n  --button-padding: 6px 26px;\n  --btn-socials: var(--ion-btn-socials);\n}\n\n.login-content {\n  --background: transparent;\n  --padding-start: var(--page-margin);\n  --padding-end: var(--page-margin);\n  --padding-top: var(--page-margin);\n  --padding-bottom: var(--page-margin);\n  background-image: linear-gradient(#28cc99, #2e7cf6);\n}\n\n.login-content .form-container {\n  padding: 40px 29px;\n  border-radius: 3.2px;\n  background-color: #ffffff;\n}\n\n.login-content .form-container p {\n  font-family: \"Inter\";\n  font-size: 15px;\n  font-weight: normal;\n  font-stretch: normal;\n  font-style: normal;\n  line-height: 17px;\n  letter-spacing: normal;\n  text-align: left;\n  color: var(--ion-color-lighter-grey-contrast);\n}\n\n.login-content .form-container p a {\n  font-weight: 600;\n  font-stretch: normal;\n  font-style: normal;\n  line-height: 1;\n  letter-spacing: normal;\n  text-align: left;\n  color: var(--ion-color-primary);\n}\n\n.login-content .form-container p .pulled-right {\n  float: right !important;\n}\n\n.login-content .auth-title {\n  color: var(--ion-color-dark);\n  font-weight: bold;\n  margin-top: calc(var(--page-margin) / 2);\n  margin-bottom: calc(var(--page-margin) * (3 / 2));\n  letter-spacing: 0.6px;\n  margin: 0 35px 16px 0;\n  font-family: \"Inter\";\n  font-size: 20px;\n  font-stretch: normal;\n  font-style: normal;\n  line-height: 1.25;\n  letter-spacing: normal;\n  text-align: left;\n}\n\n.login-content .inputs-list {\n  --ion-item-background: transparent;\n}\n\n.login-content .inputs-list .input-item {\n  --padding-start: 0px;\n  --padding-end: 0px;\n  --inner-padding-end: 0px;\n}\n\n.login-content .inputs-list .input-item ion-input {\n  margin: 8px 0 0;\n  padding: 6px 12px !important;\n  border-radius: 8px;\n  border: solid 1px var(--ion-border-gray-color);\n  background-color: #ffffff;\n}\n\n.login-content .inputs-list ion-input {\n  font-family: \"Inter\";\n  font-size: 13px;\n  font-weight: normal;\n  font-stretch: normal;\n  font-style: normal;\n  line-height: 1.54;\n  letter-spacing: normal;\n  text-align: left;\n  color: var(--ion-color-darkest);\n}\n\n.login-content .inputs-list .label-row {\n  margin-top: 24px;\n  margin-bottom: 14px;\n}\n\n.login-content .inputs-list .tl-auth-item {\n  --padding-start: 0px;\n  --inner-padding-end: 0px;\n  margin: 8px 0 0;\n  padding: 2px 12px !important;\n  border-radius: 8px;\n  border: solid 1px var(--ion-border-gray-color);\n  background-color: #ffffff;\n}\n\n.login-content .inputs-list .error-container .error-message {\n  margin: calc(var(--page-margin) / 2) 0px;\n  display: flex;\n  align-items: center;\n  color: var(--ion-color-danger);\n  font-size: 14px;\n}\n\n.login-content .inputs-list .error-container .error-message ion-icon {\n  -webkit-padding-end: calc(var(--page-margin) / 2);\n          padding-inline-end: calc(var(--page-margin) / 2);\n}\n\n.login-content .login-btn {\n  margin: 30px 0px;\n}\n\n.login-content .separator {\n  display: flex;\n  align-items: center;\n  text-align: center;\n  color: var(--ion-border-gray-color);\n  margin: 40px 0;\n}\n\n.login-content .separator::before, .login-content .separator::after {\n  content: \"\";\n  flex: 1;\n  border-bottom: 1px solid var(--ion-border-gray-color);\n}\n\n.login-content .separator::before {\n  margin-right: 0.5em;\n}\n\n.login-content .separator::after {\n  margin-left: 0.5em;\n}\n\n.login-content .other-auth-options-row {\n  justify-content: space-between;\n  align-items: center;\n}\n\n.login-content .other-auth-options-row .forgot-btn {\n  --color: var(--ion-color-medium);\n  margin: 0px;\n}\n\n.login-content .other-auth-options-row .forgot-btn:focus {\n  outline: none;\n}\n\n.login-content .other-auth-options-row .signup-btn {\n  --color: var(--ion-color-secondary);\n  margin: 0px;\n}\n\n.login-content .other-auth-options-row .signup-btn:focus {\n  outline: none;\n}\n\n.login-content .social-auth-options .options-divider {\n  color: var(--ion-color-medium);\n  margin: var(--page-margin) 0px;\n  text-align: center;\n}\n\n.login-content .social-auth-options .social-auth-btn {\n  margin: 0px;\n}\n\n.login-content .social-auth-options .social-auth-btn:not(:first-child) {\n  margin-top: var(--page-margin);\n}\n\n.login-content .social-logins ion-button {\n  font-size: 13px;\n}\n\n.login-content .social-logins ion-button ion-label {\n  margin-left: 5px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvYXV0aC9sb2dpbi9sb2dpbi5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBRUE7RUFFRSxzQ0FBQTtFQUNBLDhDQUFBO0VBQ0Esb0JBQUE7RUFDQSwwQkFBQTtFQUNBLHFDQUFBO0FBRkY7O0FBTUE7RUFDRSx5QkFBQTtFQUNBLG1DQUFBO0VBQ0EsaUNBQUE7RUFDQSxpQ0FBQTtFQUNBLG9DQUFBO0VBQ0EsbURBQUE7QUFIRjs7QUFLRTtFQUNFLGtCQUFBO0VBQ0Esb0JBQUE7RUFDQSx5QkFBQTtBQUhKOztBQUlJO0VBQ0Usb0JBQUE7RUFDQSxlQUFBO0VBQ0EsbUJBQUE7RUFDQSxvQkFBQTtFQUNBLGtCQUFBO0VBQ0EsaUJBQUE7RUFDQSxzQkFBQTtFQUNBLGdCQUFBO0VBQ0EsNkNBQUE7QUFGTjs7QUFHTTtFQUNFLGdCQUFBO0VBQ0Esb0JBQUE7RUFDQSxrQkFBQTtFQUNBLGNBQUE7RUFDQSxzQkFBQTtFQUNBLGdCQUFBO0VBQ0EsK0JBQUE7QUFEUjs7QUFJTTtFQUNFLHVCQUFBO0FBRlI7O0FBT0U7RUFDRSw0QkFBQTtFQUNBLGlCQUFBO0VBQ0Esd0NBQUE7RUFDQSxpREFBQTtFQUNBLHFCQUFBO0VBQ0EscUJBQUE7RUFDQSxvQkFBQTtFQUNBLGVBQUE7RUFDQSxvQkFBQTtFQUNBLGtCQUFBO0VBQ0EsaUJBQUE7RUFDQSxzQkFBQTtFQUNBLGdCQUFBO0FBTEo7O0FBUUU7RUFFRSxrQ0FBQTtBQVBKOztBQVNJO0VBQ0Usb0JBQUE7RUFDQSxrQkFBQTtFQUNBLHdCQUFBO0FBUE47O0FBUU07RUFDRSxlQUFBO0VBQ0EsNEJBQUE7RUFDQSxrQkFBQTtFQUNBLDhDQUFBO0VBQ0EseUJBQUE7QUFOUjs7QUFVSTtFQUNFLG9CQUFBO0VBQ0EsZUFBQTtFQUNBLG1CQUFBO0VBQ0Esb0JBQUE7RUFDQSxrQkFBQTtFQUNBLGlCQUFBO0VBQ0Esc0JBQUE7RUFDQSxnQkFBQTtFQUNBLCtCQUFBO0FBUk47O0FBV0k7RUFDRSxnQkFBQTtFQUNBLG1CQUFBO0FBVE47O0FBV0k7RUFDRSxvQkFBQTtFQUNBLHdCQUFBO0VBQ0EsZUFBQTtFQUNBLDRCQUFBO0VBQ0Esa0JBQUE7RUFDQSw4Q0FBQTtFQUNBLHlCQUFBO0FBVE47O0FBYU07RUFDRSx3Q0FBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtFQUNBLDhCQUFBO0VBQ0EsZUFBQTtBQVhSOztBQWFRO0VBQ0UsaURBQUE7VUFBQSxnREFBQTtBQVhWOztBQWlCRTtFQUVFLGdCQUFBO0FBaEJKOztBQWtCRTtFQUNFLGFBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsbUNBQUE7RUFDQSxjQUFBO0FBaEJKOztBQWlCSTtFQUVFLFdBQUE7RUFDQSxPQUFBO0VBQ0EscURBQUE7QUFoQk47O0FBa0JJO0VBQ0UsbUJBQUE7QUFoQk47O0FBa0JJO0VBQ0Usa0JBQUE7QUFoQk47O0FBb0JFO0VBQ0UsOEJBQUE7RUFDQSxtQkFBQTtBQWxCSjs7QUFvQkk7RUFDRSxnQ0FBQTtFQUNBLFdBQUE7QUFsQk47O0FBb0JNO0VBQ0UsYUFBQTtBQWxCUjs7QUFzQkk7RUFDRSxtQ0FBQTtFQUNBLFdBQUE7QUFwQk47O0FBc0JNO0VBQ0UsYUFBQTtBQXBCUjs7QUEwQkk7RUFDRSw4QkFBQTtFQUNBLDhCQUFBO0VBQ0Esa0JBQUE7QUF4Qk47O0FBMkJJO0VBQ0UsV0FBQTtBQXpCTjs7QUEyQk07RUFDRSw4QkFBQTtBQXpCUjs7QUErQkk7RUFDRSxlQUFBO0FBN0JOOztBQThCTTtFQUNFLGdCQUFBO0FBNUJSIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvYXV0aC9sb2dpbi9sb2dpbi5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIvLyBDdXN0b20gdmFyaWFibGVzXHJcbi8vIE5vdGU6ICBUaGVzZSBvbmVzIHdlcmUgYWRkZWQgYnkgdXMgYW5kIGhhdmUgbm90aGluZyB0byBkbyB3aXRoIElvbmljIENTUyBDdXN0b20gUHJvcGVydGllc1xyXG46aG9zdCB7XHJcbiAgLy8gRGVmYXVsdCB2YXJpYWJsZXNcclxuICAtLXBhZ2UtbWFyZ2luOiB2YXIoLS1hcHAtYnJvYWQtbWFyZ2luKTtcclxuICAtLXBhZ2UtYmFja2dyb3VuZDogdmFyKC0tYXBwLWJhY2tncm91bmQtc2hhZGUpO1xyXG4gIC0tcGFnZS1zcGFjaW5nOiAxMHB4O1xyXG4gIC0tYnV0dG9uLXBhZGRpbmc6IDZweCAyNnB4O1xyXG4gIC0tYnRuLXNvY2lhbHM6IHZhcigtLWlvbi1idG4tc29jaWFscyk7XHJcbn1cclxuXHJcbi8vIE5vdGU6ICBBbGwgdGhlIENTUyB2YXJpYWJsZXMgZGVmaW5lZCBiZWxvdyBhcmUgb3ZlcnJpZGVzIG9mIElvbmljIGVsZW1lbnRzIENTUyBDdXN0b20gUHJvcGVydGllc1xyXG4ubG9naW4tY29udGVudCB7XHJcbiAgLS1iYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcclxuICAtLXBhZGRpbmctc3RhcnQ6IHZhcigtLXBhZ2UtbWFyZ2luKTtcclxuICAtLXBhZGRpbmctZW5kOiB2YXIoLS1wYWdlLW1hcmdpbik7XHJcbiAgLS1wYWRkaW5nLXRvcDogdmFyKC0tcGFnZS1tYXJnaW4pO1xyXG4gIC0tcGFkZGluZy1ib3R0b206IHZhcigtLXBhZ2UtbWFyZ2luKTtcclxuICBiYWNrZ3JvdW5kLWltYWdlOiBsaW5lYXItZ3JhZGllbnQoIzI4Y2M5OSwgIzJlN2NmNik7XHJcblxyXG4gIC5mb3JtLWNvbnRhaW5lciB7XHJcbiAgICBwYWRkaW5nOiA0MHB4IDI5cHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiAzLjJweDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmZmZmY7XHJcbiAgICBwIHtcclxuICAgICAgZm9udC1mYW1pbHk6IFwiSW50ZXJcIjtcclxuICAgICAgZm9udC1zaXplOiAxNXB4O1xyXG4gICAgICBmb250LXdlaWdodDogbm9ybWFsO1xyXG4gICAgICBmb250LXN0cmV0Y2g6IG5vcm1hbDtcclxuICAgICAgZm9udC1zdHlsZTogbm9ybWFsO1xyXG4gICAgICBsaW5lLWhlaWdodDogMTdweDtcclxuICAgICAgbGV0dGVyLXNwYWNpbmc6IG5vcm1hbDtcclxuICAgICAgdGV4dC1hbGlnbjogbGVmdDtcclxuICAgICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1saWdodGVyLWdyZXktY29udHJhc3QpO1xyXG4gICAgICBhIHtcclxuICAgICAgICBmb250LXdlaWdodDogNjAwO1xyXG4gICAgICAgIGZvbnQtc3RyZXRjaDogbm9ybWFsO1xyXG4gICAgICAgIGZvbnQtc3R5bGU6IG5vcm1hbDtcclxuICAgICAgICBsaW5lLWhlaWdodDogMTtcclxuICAgICAgICBsZXR0ZXItc3BhY2luZzogbm9ybWFsO1xyXG4gICAgICAgIHRleHQtYWxpZ246IGxlZnQ7XHJcbiAgICAgICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KTtcclxuICAgICAgfVxyXG5cclxuICAgICAgLnB1bGxlZC1yaWdodCB7XHJcbiAgICAgICAgZmxvYXQ6IHJpZ2h0ICFpbXBvcnRhbnQ7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcblxyXG4gIC5hdXRoLXRpdGxlIHtcclxuICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItZGFyayk7XHJcbiAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgIG1hcmdpbi10b3A6IGNhbGModmFyKC0tcGFnZS1tYXJnaW4pIC8gMik7XHJcbiAgICBtYXJnaW4tYm90dG9tOiBjYWxjKHZhcigtLXBhZ2UtbWFyZ2luKSAqICgzIC8gMikpO1xyXG4gICAgbGV0dGVyLXNwYWNpbmc6IDAuNnB4O1xyXG4gICAgbWFyZ2luOiAwIDM1cHggMTZweCAwO1xyXG4gICAgZm9udC1mYW1pbHk6IFwiSW50ZXJcIjtcclxuICAgIGZvbnQtc2l6ZTogMjBweDtcclxuICAgIGZvbnQtc3RyZXRjaDogbm9ybWFsO1xyXG4gICAgZm9udC1zdHlsZTogbm9ybWFsO1xyXG4gICAgbGluZS1oZWlnaHQ6IDEuMjU7XHJcbiAgICBsZXR0ZXItc3BhY2luZzogbm9ybWFsO1xyXG4gICAgdGV4dC1hbGlnbjogbGVmdDtcclxuICB9XHJcblxyXG4gIC5pbnB1dHMtbGlzdCB7XHJcbiAgICAvLyAtLWlvbi1pdGVtLWJhY2tncm91bmQ6IHZhcigtLXBhZ2UtYmFja2dyb3VuZCk7XHJcbiAgICAtLWlvbi1pdGVtLWJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xyXG5cclxuICAgIC5pbnB1dC1pdGVtIHtcclxuICAgICAgLS1wYWRkaW5nLXN0YXJ0OiAwcHg7XHJcbiAgICAgIC0tcGFkZGluZy1lbmQ6IDBweDtcclxuICAgICAgLS1pbm5lci1wYWRkaW5nLWVuZDogMHB4O1xyXG4gICAgICBpb24taW5wdXQge1xyXG4gICAgICAgIG1hcmdpbjogOHB4IDAgMDtcclxuICAgICAgICBwYWRkaW5nOiA2cHggMTJweCAhaW1wb3J0YW50O1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDhweDtcclxuICAgICAgICBib3JkZXI6IHNvbGlkIDFweCB2YXIoLS1pb24tYm9yZGVyLWdyYXktY29sb3IpO1xyXG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmZmZmY7XHJcbiAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBpb24taW5wdXQge1xyXG4gICAgICBmb250LWZhbWlseTogXCJJbnRlclwiO1xyXG4gICAgICBmb250LXNpemU6IDEzcHg7XHJcbiAgICAgIGZvbnQtd2VpZ2h0OiBub3JtYWw7XHJcbiAgICAgIGZvbnQtc3RyZXRjaDogbm9ybWFsO1xyXG4gICAgICBmb250LXN0eWxlOiBub3JtYWw7XHJcbiAgICAgIGxpbmUtaGVpZ2h0OiAxLjU0O1xyXG4gICAgICBsZXR0ZXItc3BhY2luZzogbm9ybWFsO1xyXG4gICAgICB0ZXh0LWFsaWduOiBsZWZ0O1xyXG4gICAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLWRhcmtlc3QpO1xyXG4gICAgfVxyXG5cclxuICAgIC5sYWJlbC1yb3cge1xyXG4gICAgICBtYXJnaW4tdG9wOiAyNHB4O1xyXG4gICAgICBtYXJnaW4tYm90dG9tOiAxNHB4O1xyXG4gICAgfVxyXG4gICAgLnRsLWF1dGgtaXRlbSB7XHJcbiAgICAgIC0tcGFkZGluZy1zdGFydDogMHB4O1xyXG4gICAgICAtLWlubmVyLXBhZGRpbmctZW5kOiAwcHg7XHJcbiAgICAgIG1hcmdpbjogOHB4IDAgMDtcclxuICAgICAgcGFkZGluZzogMnB4IDEycHggIWltcG9ydGFudDtcclxuICAgICAgYm9yZGVyLXJhZGl1czogOHB4O1xyXG4gICAgICBib3JkZXI6IHNvbGlkIDFweCB2YXIoLS1pb24tYm9yZGVyLWdyYXktY29sb3IpO1xyXG4gICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmZmZmO1xyXG4gICAgfVxyXG5cclxuICAgIC5lcnJvci1jb250YWluZXIge1xyXG4gICAgICAuZXJyb3ItbWVzc2FnZSB7XHJcbiAgICAgICAgbWFyZ2luOiBjYWxjKHZhcigtLXBhZ2UtbWFyZ2luKSAvIDIpIDBweDtcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1kYW5nZXIpO1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMTRweDtcclxuXHJcbiAgICAgICAgaW9uLWljb24ge1xyXG4gICAgICAgICAgcGFkZGluZy1pbmxpbmUtZW5kOiBjYWxjKHZhcigtLXBhZ2UtbWFyZ2luKSAvIDIpO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgLmxvZ2luLWJ0biB7XHJcbiAgICAvLyBtYXJnaW46IGNhbGModmFyKC0tcGFnZS1tYXJnaW4pIC8gMikgMHB4O1xyXG4gICAgbWFyZ2luOiAzMHB4IDBweDtcclxuICB9XHJcbiAgLnNlcGFyYXRvciB7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIGNvbG9yOiB2YXIoLS1pb24tYm9yZGVyLWdyYXktY29sb3IpO1xyXG4gICAgbWFyZ2luOiA0MHB4IDA7XHJcbiAgICAmOjpiZWZvcmUsXHJcbiAgICAmOjphZnRlciB7XHJcbiAgICAgIGNvbnRlbnQ6IFwiXCI7XHJcbiAgICAgIGZsZXg6IDE7XHJcbiAgICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCB2YXIoLS1pb24tYm9yZGVyLWdyYXktY29sb3IpO1xyXG4gICAgfVxyXG4gICAgJjo6YmVmb3JlIHtcclxuICAgICAgbWFyZ2luLXJpZ2h0OiAwLjVlbTtcclxuICAgIH1cclxuICAgICY6OmFmdGVyIHtcclxuICAgICAgbWFyZ2luLWxlZnQ6IDAuNWVtO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgLm90aGVyLWF1dGgtb3B0aW9ucy1yb3cge1xyXG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuXHJcbiAgICAuZm9yZ290LWJ0biB7XHJcbiAgICAgIC0tY29sb3I6IHZhcigtLWlvbi1jb2xvci1tZWRpdW0pO1xyXG4gICAgICBtYXJnaW46IDBweDtcclxuXHJcbiAgICAgICY6Zm9jdXMge1xyXG4gICAgICAgIG91dGxpbmU6IG5vbmU7XHJcbiAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAuc2lnbnVwLWJ0biB7XHJcbiAgICAgIC0tY29sb3I6IHZhcigtLWlvbi1jb2xvci1zZWNvbmRhcnkpO1xyXG4gICAgICBtYXJnaW46IDBweDtcclxuXHJcbiAgICAgICY6Zm9jdXMge1xyXG4gICAgICAgIG91dGxpbmU6IG5vbmU7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcblxyXG4gIC5zb2NpYWwtYXV0aC1vcHRpb25zIHtcclxuICAgIC5vcHRpb25zLWRpdmlkZXIge1xyXG4gICAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLW1lZGl1bSk7XHJcbiAgICAgIG1hcmdpbjogdmFyKC0tcGFnZS1tYXJnaW4pIDBweDtcclxuICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgfVxyXG5cclxuICAgIC5zb2NpYWwtYXV0aC1idG4ge1xyXG4gICAgICBtYXJnaW46IDBweDtcclxuXHJcbiAgICAgICY6bm90KDpmaXJzdC1jaGlsZCkge1xyXG4gICAgICAgIG1hcmdpbi10b3A6IHZhcigtLXBhZ2UtbWFyZ2luKTtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgLnNvY2lhbC1sb2dpbnMge1xyXG4gICAgaW9uLWJ1dHRvbiB7XHJcbiAgICAgIGZvbnQtc2l6ZTogMTNweDtcclxuICAgICAgaW9uLWxhYmVsIHtcclxuICAgICAgICBtYXJnaW4tbGVmdDogNXB4O1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICAvLyBAbWVkaWEgc2NyZWVuIGFuZCAobWF4LWhlaWdodDogNTY4cHgpIHtcclxuICAvLyAgIC5hdXRoLXRpdGxlIHtcclxuICAvLyAgICAgZm9udC1zaXplOiAxNXB4O1xyXG4gIC8vICAgfVxyXG4gIC8vICAgLmZvcm0tY29udGFpbmVyIHtcclxuICAvLyAgICAgcCB7XHJcbiAgLy8gICAgICAgZm9udC1zaXplOiAxMnB4O1xyXG4gIC8vICAgICAgIGxpbmUtaGVpZ2h0OiAxNXB4O1xyXG4gIC8vICAgICB9XHJcbiAgLy8gICB9XHJcbiAgLy8gfVxyXG5cclxuICAvLyBAbWVkaWEgc2NyZWVuIGFuZCAobWF4LWhlaWdodDogNjQwcHgpIHtcclxuICAvLyAgIC5zb2NpYWwtbG9naW5zIHtcclxuICAvLyAgICAgaW9uLWJ1dHRvbiB7XHJcbiAgLy8gICAgICAgZm9udC1zaXplOiAxMXB4O1xyXG4gIC8vICAgICAgIGlvbi1sYWJlbCB7XHJcbiAgLy8gICAgICAgICBtYXJnaW4tbGVmdDogMi41cHg7XHJcbiAgLy8gICAgICAgfVxyXG4gIC8vICAgICB9XHJcbiAgLy8gICB9XHJcbiAgLy8gfVxyXG5cclxuICAvLyBAbWVkaWEgc2NyZWVuIGFuZCAobWF4LWhlaWdodDogNjY3cHgpIHtcclxuICAvLyB9XHJcbn1cclxuIl19 */";
      /***/
    },

    /***/
    "./src/app/pages/auth/login/login.page.ts":
    /*!************************************************!*\
      !*** ./src/app/pages/auth/login/login.page.ts ***!
      \************************************************/

    /*! exports provided: LoginPage */

    /***/
    function srcAppPagesAuthLoginLoginPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "LoginPage", function () {
        return LoginPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _core_helpers__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @core/helpers */
      "./src/app/core/helpers/index.ts");
      /* harmony import */


      var _core_services__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @core/services */
      "./src/app/core/services/index.ts");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _shared_lib_services__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! @shared-lib/services */
      "./src/app/shared/services/index.ts");
      /* harmony import */


      var lodash__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
      /*! lodash */
      "./node_modules/lodash/lodash.js");
      /* harmony import */


      var lodash__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_9__);

      var LoginPage = /*#__PURE__*/function () {
        function LoginPage(router, route, menu, storageService, authService, loadingController, location, ngZone) {
          var _this = this;

          _classCallCheck(this, LoginPage);

          this.router = router;
          this.route = route;
          this.menu = menu;
          this.storageService = storageService;
          this.authService = authService;
          this.loadingController = loadingController;
          this.location = location;
          this.ngZone = ngZone;
          this.validation_messages = _core_helpers__WEBPACK_IMPORTED_MODULE_5__["loginValidationMessages"];
          this.email = '';
          this.password = '';
          this.getUserEmail();
          this.loginForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormGroup"]({
            email: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].pattern("^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$")])),
            password: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].minLength(5), _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]))
          }); // Check if url contains our custom 'auth-redirect' param, then show a loader while we receive the getRedirectResult notification

          this.route.queryParams.subscribe(function (params) {
            var authProvider = params["auth-redirect"];

            if (authProvider) {
              _this.presentLoading(authProvider);
            }
          });
        }

        _createClass(LoginPage, [{
          key: "getUserEmail",
          value: function getUserEmail() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
              var userEmail;
              return regeneratorRuntime.wrap(function _callee$(_context) {
                while (1) {
                  switch (_context.prev = _context.next) {
                    case 0:
                      _context.next = 2;
                      return this.storageService.getItem('userEmail');

                    case 2:
                      userEmail = _context.sent;

                      if (userEmail != null) {
                        this.email = userEmail;
                      }

                      return _context.abrupt("return", userEmail);

                    case 5:
                    case "end":
                      return _context.stop();
                  }
                }
              }, _callee, this);
            }));
          } // Disable side menu for this page

        }, {
          key: "ionViewDidEnter",
          value: function ionViewDidEnter() {
            this.menu.enable(false);
          } // Restore to default when leaving this page

        }, {
          key: "ionViewDidLeave",
          value: function ionViewDidLeave() {
            this.menu.enable(true);
          }
        }, {
          key: "ngOnDestroy",
          value: function ngOnDestroy() {
            this.dismissLoading();
          }
        }, {
          key: "doLogin",
          value: function doLogin() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
              var hasDonetutorial;
              return regeneratorRuntime.wrap(function _callee2$(_context2) {
                while (1) {
                  switch (_context2.prev = _context2.next) {
                    case 0:
                      console.log("do Log In");
                      _context2.next = 3;
                      return this.storageService.getItem("tutorial");

                    case 3:
                      hasDonetutorial = _context2.sent;

                      if (!Object(lodash__WEBPACK_IMPORTED_MODULE_9__["isNil"])(hasDonetutorial)) {
                        this.router.navigate(["home"]);
                      } else {
                        this.storageService.setItem({
                          key: "tutorial",
                          value: JSON.stringify(1)
                        });
                        this.router.navigate(["walkthrough"]);
                      }

                    case 5:
                    case "end":
                      return _context2.stop();
                  }
                }
              }, _callee2, this);
            }));
          }
        }, {
          key: "goToForgotPassword",
          value: function goToForgotPassword() {
            console.log("redirect to forgot-password page");
          }
        }, {
          key: "presentLoading",
          value: function presentLoading(authProvider) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
              var _this2 = this;

              var authProviderCapitalized;
              return regeneratorRuntime.wrap(function _callee3$(_context3) {
                while (1) {
                  switch (_context3.prev = _context3.next) {
                    case 0:
                      authProviderCapitalized = authProvider[0].toUpperCase() + authProvider.slice(1);
                      this.loadingController.create({
                        message: authProvider ? "Signing in with " + authProviderCapitalized : "Signin in ..."
                      }).then(function (loader) {
                        var currentUrl = _this2.location.path();

                        if (currentUrl.includes("auth-redirect")) {
                          _this2.redirectLoader = loader;

                          _this2.redirectLoader.present();
                        }
                      });

                    case 2:
                    case "end":
                      return _context3.stop();
                  }
                }
              }, _callee3, this);
            }));
          }
        }, {
          key: "dismissLoading",
          value: function dismissLoading() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee4() {
              return regeneratorRuntime.wrap(function _callee4$(_context4) {
                while (1) {
                  switch (_context4.prev = _context4.next) {
                    case 0:
                      if (!this.redirectLoader) {
                        _context4.next = 3;
                        break;
                      }

                      _context4.next = 3;
                      return this.redirectLoader.dismiss();

                    case 3:
                    case "end":
                      return _context4.stop();
                  }
                }
              }, _callee4, this);
            }));
          } // Before invoking auth provider redirect flow, present a loading indicator and add a flag to the path.
          // The precense of the flag in the path indicates we should wait for the auth redirect to complete.

        }, {
          key: "prepareForAuthWithProvidersRedirection",
          value: function prepareForAuthWithProvidersRedirection(authProvider) {
            this.presentLoading(authProvider);
            this.location.replaceState(this.location.path(), "auth-redirect=" + authProvider, this.location.getState());
          }
        }, {
          key: "manageAuthWithProvidersErrors",
          value: function manageAuthWithProvidersErrors(errorMessage) {
            this.submitError = errorMessage; // remove auth-redirect param from url

            this.location.replaceState(this.router.url.split("?")[0], "");
            this.dismissLoading();
          }
        }, {
          key: "resetSubmitError",
          value: function resetSubmitError() {
            this.submitError = null;
          }
        }, {
          key: "signInWithEmail",
          value: function signInWithEmail() {
            var _this3 = this;

            this.resetSubmitError();

            var _ref = this.loginForm.value || {},
                email = _ref.email,
                password = _ref.password;

            this.storageService.setItem({
              key: "userEmail",
              value: JSON.stringify(this.loginForm.value.email)
            });
            var formValues = {
              email: email,
              password: password
            };
            this.authService.signInWithEmail(formValues).then(function (user) {
              var _ref2 = user || {},
                  code = _ref2.code,
                  message = _ref2.message;

              if (Object(lodash__WEBPACK_IMPORTED_MODULE_9__["isNil"])(code)) {
                _this3.redirectLoggedUserToProfilePage();
              } else {
                _this3.submitError = message;

                _this3.dismissLoading();
              }
            })["catch"](function (error) {
              _this3.submitError = error.message;

              _this3.dismissLoading();
            });
          }
        }, {
          key: "doFacebookLogin",
          value: function doFacebookLogin() {
            var _this4 = this;

            this.resetSubmitError();
            this.prepareForAuthWithProvidersRedirection("facebook");
            this.authService.signInWithFacebook().subscribe(function (result) {
              // This gives you a Facebook Access Token. You can use it to access the Facebook API.
              // const token = result.credential.accessToken;
              var _ref3 = result || {},
                  user = _ref3.user;

              _this4.authService.setUserStorage(user);

              var resultHandled = _this4.authService.handleOAuthResult(result, "facebook.com", "login");

              if (resultHandled) _this4.redirectLoggedUserToProfilePage();
            }, function (error) {
              console.log("doFacebookLogin error", error);

              _this4.manageAuthWithProvidersErrors(error.message);
            });
          }
        }, {
          key: "doGoogleLogin",
          value: function doGoogleLogin() {
            var _this5 = this;

            this.resetSubmitError();
            this.prepareForAuthWithProvidersRedirection("google");
            this.authService.signInWithGoogle().subscribe(function (result) {
              console.log("doGoogleLogin result", result); // This gives you a Facebook Access Token. You can use it to access the Facebook API.
              // const token = result.credential.accessToken;

              var _ref4 = result || {},
                  user = _ref4.user;

              _this5.authService.setUserStorage(user);

              var resultHandled = _this5.authService.handleOAuthResult(result, "google.com", "login");

              if (resultHandled) _this5.redirectLoggedUserToProfilePage();
            }, function (error) {
              console.log("doGoogleLogin error", error);

              _this5.manageAuthWithProvidersErrors(error.message);
            });
          }
        }, {
          key: "doTwitterLogin",
          value: function doTwitterLogin() {
            var _this6 = this;

            this.resetSubmitError();
            this.prepareForAuthWithProvidersRedirection("twitter");
            this.authService.signInWithTwitter().subscribe(function (result) {
              // This gives you a Facebook Access Token. You can use it to access the Facebook API.
              // const token = result.credential.accessToken;
              var _ref5 = result || {},
                  user = _ref5.user;

              _this6.authService.setUserStorage(user);

              var resultHandled = _this6.authService.handleOAuthResult(result, "twitter.com", "login");

              if (resultHandled) _this6.redirectLoggedUserToProfilePage();
            }, function (error) {
              console.log("doTwitterLogin error", error);

              _this6.manageAuthWithProvidersErrors(error.message);
            });
          } // Once the auth provider finished the authentication flow, and the auth redirect completes,
          // hide the loader and redirect the user to the profile page

        }, {
          key: "redirectLoggedUserToProfilePage",
          value: function redirectLoggedUserToProfilePage() {
            var _this7 = this;

            this.dismissLoading(); // As we are calling the Angular router navigation inside a subscribe method, the navigation will be triggered outside Angular zone.
            // That's why we need to wrap the router navigation call inside an ngZone wrapper

            this.ngZone.run(function () {
              return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this7, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee5() {
                var previousUrl, hasDonetutorial;
                return regeneratorRuntime.wrap(function _callee5$(_context5) {
                  while (1) {
                    switch (_context5.prev = _context5.next) {
                      case 0:
                        // Get previous URL from our custom History Helper
                        // If there's no previous page, then redirect to profile
                        // const previousUrl = this.historyHelper.previousUrl || 'firebase/auth/profile';
                        // const previousUrl = "firebase/auth/profile";
                        previousUrl = "home";
                        _context5.next = 3;
                        return this.storageService.getItem("tutorial");

                      case 3:
                        hasDonetutorial = _context5.sent;

                        if (Object(lodash__WEBPACK_IMPORTED_MODULE_9__["isNil"])(hasDonetutorial)) {
                          this.storageService.setItem({
                            key: "tutorial",
                            value: JSON.stringify(1)
                          });
                          previousUrl = "walkthrough";
                        } // No need to store in the navigation history the sign-in page with redirect params (it's justa a mandatory mid-step)
                        // Navigate to profile and replace current url with profile


                        this.router.navigate([previousUrl], {
                          replaceUrl: true
                        });

                      case 6:
                      case "end":
                        return _context5.stop();
                    }
                  }
                }, _callee5, this);
              }));
            });
          }
        }]);

        return LoginPage;
      }();

      LoginPage.ctorParameters = function () {
        return [{
          type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["MenuController"]
        }, {
          type: _shared_lib_services__WEBPACK_IMPORTED_MODULE_8__["StorageService"]
        }, {
          type: _core_services__WEBPACK_IMPORTED_MODULE_6__["FirebaseAuthService"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["LoadingController"]
        }, {
          type: _angular_common__WEBPACK_IMPORTED_MODULE_1__["Location"]
        }, {
          type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["NgZone"]
        }];
      };

      LoginPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
        selector: "app-login",
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./login.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/auth/login/login.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./login.page.scss */
        "./src/app/pages/auth/login/login.page.scss"))["default"], Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./login.page.responsive.scss */
        "./src/app/pages/auth/login/login.page.responsive.scss"))["default"]]
      }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"], _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["MenuController"], _shared_lib_services__WEBPACK_IMPORTED_MODULE_8__["StorageService"], _core_services__WEBPACK_IMPORTED_MODULE_6__["FirebaseAuthService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["LoadingController"], _angular_common__WEBPACK_IMPORTED_MODULE_1__["Location"], _angular_core__WEBPACK_IMPORTED_MODULE_2__["NgZone"]])], LoginPage);
      /***/
    },

    /***/
    "./src/app/pages/auth/privacy-policy/components/index.ts":
    /*!***************************************************************!*\
      !*** ./src/app/pages/auth/privacy-policy/components/index.ts ***!
      \***************************************************************/

    /*! exports provided: PrivacyGermanComponent, PrivacyEnglishComponent */

    /***/
    function srcAppPagesAuthPrivacyPolicyComponentsIndexTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony import */


      var _privacy_english_privacy_english_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! ./privacy-english/privacy-english.component */
      "./src/app/pages/auth/privacy-policy/components/privacy-english/privacy-english.component.ts");
      /* harmony reexport (safe) */


      __webpack_require__.d(__webpack_exports__, "PrivacyEnglishComponent", function () {
        return _privacy_english_privacy_english_component__WEBPACK_IMPORTED_MODULE_0__["PrivacyEnglishComponent"];
      });
      /* harmony import */


      var _privacy_german_privacy_german_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ./privacy-german/privacy-german.component */
      "./src/app/pages/auth/privacy-policy/components/privacy-german/privacy-german.component.ts");
      /* harmony reexport (safe) */


      __webpack_require__.d(__webpack_exports__, "PrivacyGermanComponent", function () {
        return _privacy_german_privacy_german_component__WEBPACK_IMPORTED_MODULE_1__["PrivacyGermanComponent"];
      });
      /***/

    },

    /***/
    "./src/app/pages/auth/privacy-policy/components/privacy-english/privacy-english.component.ts":
    /*!***************************************************************************************************!*\
      !*** ./src/app/pages/auth/privacy-policy/components/privacy-english/privacy-english.component.ts ***!
      \***************************************************************************************************/

    /*! exports provided: PrivacyEnglishComponent */

    /***/
    function srcAppPagesAuthPrivacyPolicyComponentsPrivacyEnglishPrivacyEnglishComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "PrivacyEnglishComponent", function () {
        return PrivacyEnglishComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

      var PrivacyEnglishComponent = /*#__PURE__*/function () {
        function PrivacyEnglishComponent() {
          _classCallCheck(this, PrivacyEnglishComponent);
        }

        _createClass(PrivacyEnglishComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }]);

        return PrivacyEnglishComponent;
      }();

      PrivacyEnglishComponent.ctorParameters = function () {
        return [];
      };

      PrivacyEnglishComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "app-privacy-english",
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./privacy-english.component.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/auth/privacy-policy/components/privacy-english/privacy-english.component.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ../../styles/privacy-policy.page.scss */
        "./src/app/pages/auth/privacy-policy/styles/privacy-policy.page.scss"))["default"]]
      }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [])], PrivacyEnglishComponent);
      /***/
    },

    /***/
    "./src/app/pages/auth/privacy-policy/components/privacy-german/privacy-german.component.ts":
    /*!*************************************************************************************************!*\
      !*** ./src/app/pages/auth/privacy-policy/components/privacy-german/privacy-german.component.ts ***!
      \*************************************************************************************************/

    /*! exports provided: PrivacyGermanComponent */

    /***/
    function srcAppPagesAuthPrivacyPolicyComponentsPrivacyGermanPrivacyGermanComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "PrivacyGermanComponent", function () {
        return PrivacyGermanComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

      var PrivacyGermanComponent = /*#__PURE__*/function () {
        function PrivacyGermanComponent() {
          _classCallCheck(this, PrivacyGermanComponent);
        }

        _createClass(PrivacyGermanComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }]);

        return PrivacyGermanComponent;
      }();

      PrivacyGermanComponent.ctorParameters = function () {
        return [];
      };

      PrivacyGermanComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "app-privacy-german",
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./privacy-german.component.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/auth/privacy-policy/components/privacy-german/privacy-german.component.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ../../styles/privacy-policy.page.scss */
        "./src/app/pages/auth/privacy-policy/styles/privacy-policy.page.scss"))["default"]]
      }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [])], PrivacyGermanComponent);
      /***/
    },

    /***/
    "./src/app/pages/auth/privacy-policy/index.ts":
    /*!****************************************************!*\
      !*** ./src/app/pages/auth/privacy-policy/index.ts ***!
      \****************************************************/

    /*! exports provided: PrivacyPolicyPage, PrivacyGermanComponent, PrivacyEnglishComponent */

    /***/
    function srcAppPagesAuthPrivacyPolicyIndexTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony import */


      var _components__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! ./components */
      "./src/app/pages/auth/privacy-policy/components/index.ts");
      /* harmony reexport (safe) */


      __webpack_require__.d(__webpack_exports__, "PrivacyGermanComponent", function () {
        return _components__WEBPACK_IMPORTED_MODULE_0__["PrivacyGermanComponent"];
      });
      /* harmony reexport (safe) */


      __webpack_require__.d(__webpack_exports__, "PrivacyEnglishComponent", function () {
        return _components__WEBPACK_IMPORTED_MODULE_0__["PrivacyEnglishComponent"];
      });
      /* harmony import */


      var _privacy_policy_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ./privacy-policy.page */
      "./src/app/pages/auth/privacy-policy/privacy-policy.page.ts");
      /* harmony reexport (safe) */


      __webpack_require__.d(__webpack_exports__, "PrivacyPolicyPage", function () {
        return _privacy_policy_page__WEBPACK_IMPORTED_MODULE_1__["default"];
      });
      /***/

    },

    /***/
    "./src/app/pages/auth/privacy-policy/privacy-policy.page.ts":
    /*!******************************************************************!*\
      !*** ./src/app/pages/auth/privacy-policy/privacy-policy.page.ts ***!
      \******************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppPagesAuthPrivacyPolicyPrivacyPolicyPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");

      var PrivacyPolicyPage = /*#__PURE__*/function () {
        function PrivacyPolicyPage(modalController) {
          _classCallCheck(this, PrivacyPolicyPage);

          this.modalController = modalController;
          this.isEnglish = true;
        }

        _createClass(PrivacyPolicyPage, [{
          key: "dismiss",
          value: function dismiss() {
            this.modalController.dismiss();
          }
        }, {
          key: "segmentChanged",
          value: function segmentChanged() {
            this.isEnglish = !this.isEnglish;
          }
        }]);

        return PrivacyPolicyPage;
      }();

      PrivacyPolicyPage.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]
        }];
      };

      PrivacyPolicyPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "app-privacy-policy-page",
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./privacy-policy.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/auth/privacy-policy/privacy-policy.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./styles/privacy-policy.page.scss */
        "./src/app/pages/auth/privacy-policy/styles/privacy-policy.page.scss"))["default"]]
      }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]])], PrivacyPolicyPage);
      /* harmony default export */

      __webpack_exports__["default"] = PrivacyPolicyPage;
      /***/
    },

    /***/
    "./src/app/pages/auth/privacy-policy/styles/privacy-policy.page.scss":
    /*!***************************************************************************!*\
      !*** ./src/app/pages/auth/privacy-policy/styles/privacy-policy.page.scss ***!
      \***************************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppPagesAuthPrivacyPolicyStylesPrivacyPolicyPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ":host {\n  --page-margin: var(--app-broad-margin);\n  --page-background: var(--app-background-shade);\n}\n\n.legal-content {\n  --background: var(--page-background);\n  --padding-start: var(--page-margin);\n  --padding-end: var(--page-margin);\n  --padding-top: var(--page-margin);\n  --padding-bottom: var(--page-margin);\n}\n\n.legal-content .legal-title {\n  color: var(--ion-color-primary);\n  margin: var(--page-margin) 0px calc(var(--page-margin) / 2);\n}\n\n.legal-content .legal-text {\n  color: var(--ion-color-medium);\n  margin: calc(var(--page-margin) / 2) 0px var(--page-margin);\n  font-size: 14px;\n  line-height: 20px;\n  text-align: justify;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvYXV0aC9wcml2YWN5LXBvbGljeS9zdHlsZXMvcHJpdmFjeS1wb2xpY3kucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVBO0VBQ0Usc0NBQUE7RUFDQSw4Q0FBQTtBQURGOztBQUtBO0VBQ0Usb0NBQUE7RUFDQSxtQ0FBQTtFQUNBLGlDQUFBO0VBQ0EsaUNBQUE7RUFDQSxvQ0FBQTtBQUZGOztBQUlFO0VBQ0UsK0JBQUE7RUFDQSwyREFBQTtBQUZKOztBQUtFO0VBQ0UsOEJBQUE7RUFDQSwyREFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtFQUNBLG1CQUFBO0FBSEoiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9hdXRoL3ByaXZhY3ktcG9saWN5L3N0eWxlcy9wcml2YWN5LXBvbGljeS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIvLyBDdXN0b20gdmFyaWFibGVzXHJcbi8vIE5vdGU6ICBUaGVzZSBvbmVzIHdlcmUgYWRkZWQgYnkgdXMgYW5kIGhhdmUgbm90aGluZyB0byBkbyB3aXRoIElvbmljIENTUyBDdXN0b20gUHJvcGVydGllc1xyXG46aG9zdCB7XHJcbiAgLS1wYWdlLW1hcmdpbjogdmFyKC0tYXBwLWJyb2FkLW1hcmdpbik7XHJcbiAgLS1wYWdlLWJhY2tncm91bmQ6IHZhcigtLWFwcC1iYWNrZ3JvdW5kLXNoYWRlKTtcclxufVxyXG5cclxuLy8gTm90ZTogIEFsbCB0aGUgQ1NTIHZhcmlhYmxlcyBkZWZpbmVkIGJlbG93IGFyZSBvdmVycmlkZXMgb2YgSW9uaWMgZWxlbWVudHMgQ1NTIEN1c3RvbSBQcm9wZXJ0aWVzXHJcbi5sZWdhbC1jb250ZW50IHtcclxuICAtLWJhY2tncm91bmQ6IHZhcigtLXBhZ2UtYmFja2dyb3VuZCk7XHJcbiAgLS1wYWRkaW5nLXN0YXJ0OiB2YXIoLS1wYWdlLW1hcmdpbik7XHJcbiAgLS1wYWRkaW5nLWVuZDogdmFyKC0tcGFnZS1tYXJnaW4pO1xyXG4gIC0tcGFkZGluZy10b3A6IHZhcigtLXBhZ2UtbWFyZ2luKTtcclxuICAtLXBhZGRpbmctYm90dG9tOiB2YXIoLS1wYWdlLW1hcmdpbik7XHJcblxyXG4gIC5sZWdhbC10aXRsZSB7XHJcbiAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpO1xyXG4gICAgbWFyZ2luOiB2YXIoLS1wYWdlLW1hcmdpbikgMHB4IGNhbGModmFyKC0tcGFnZS1tYXJnaW4pIC8gMik7XHJcbiAgfVxyXG5cclxuICAubGVnYWwtdGV4dCB7XHJcbiAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLW1lZGl1bSk7XHJcbiAgICBtYXJnaW46IGNhbGModmFyKC0tcGFnZS1tYXJnaW4pIC8gMikgMHB4IHZhcigtLXBhZ2UtbWFyZ2luKTtcclxuICAgIGZvbnQtc2l6ZTogMTRweDtcclxuICAgIGxpbmUtaGVpZ2h0OiAyMHB4O1xyXG4gICAgdGV4dC1hbGlnbjoganVzdGlmeTtcclxuICB9XHJcbn1cclxuIl19 */";
      /***/
    },

    /***/
    "./src/app/pages/auth/signup/signup.page.scss":
    /*!****************************************************!*\
      !*** ./src/app/pages/auth/signup/signup.page.scss ***!
      \****************************************************/

    /*! exports provided: default */

    /***/
    function srcAppPagesAuthSignupSignupPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ":host {\n  --page-margin: var(--app-broad-margin);\n  --page-background: var(--app-background-shade);\n}\n\n.signup-content {\n  --background: transparent;\n  --padding-start: var(--page-margin);\n  --padding-end: var(--page-margin);\n  --padding-top: var(--page-margin);\n  --padding-bottom: var(--page-margin);\n  background-image: linear-gradient(#28cc99, #2e7cf6);\n}\n\n.signup-content .form-container {\n  padding: 40px 29px;\n  border-radius: 3.2px;\n  background-color: #ffffff;\n}\n\n.signup-content .form-container p {\n  font-family: \"Inter\";\n  font-size: 15px;\n  font-weight: normal;\n  font-stretch: normal;\n  font-style: normal;\n  line-height: 17px;\n  letter-spacing: normal;\n  text-align: left;\n  color: var(--ion-color-lighter-grey-contrast);\n}\n\n.signup-content .form-container p a {\n  font-weight: 600;\n  font-stretch: normal;\n  font-style: normal;\n  line-height: 1;\n  letter-spacing: normal;\n  text-align: left;\n  color: var(--ion-color-primary);\n}\n\n.signup-content .form-container p .pulled-right {\n  float: right !important;\n}\n\n.signup-content .auth-title {\n  color: var(--ion-color-dark);\n  font-weight: bold;\n  margin-top: calc(var(--page-margin) / 2);\n  margin-bottom: calc(var(--page-margin) * (3 / 2));\n  letter-spacing: 0.6px;\n  margin: 0 35px 16px 0;\n  font-family: \"Inter\";\n  font-size: 20px;\n  font-stretch: normal;\n  font-style: normal;\n  line-height: 1.25;\n  letter-spacing: normal;\n  text-align: left;\n}\n\n.signup-content .inputs-list {\n  --ion-item-background: transparent;\n}\n\n.signup-content .inputs-list .half-width {\n  width: 50%;\n  display: inline-block;\n}\n\n.signup-content .inputs-list .input-item {\n  --padding-start: 0px;\n  --padding-end: 0px;\n  --inner-padding-end: 0px;\n}\n\n.signup-content .inputs-list .input-item ion-input {\n  --padding-start: 0px;\n  --padding-end: 0px;\n  margin: 8px 0 0;\n  padding: 6px 12px !important;\n  border-radius: 8px;\n  border: solid 1px var(--ion-border-gray-color);\n  background-color: #ffffff;\n}\n\n.signup-content .inputs-list .input-item ion-select {\n  padding: 14px;\n  border-radius: 8px;\n  border: solid 1px var(--ion-border-gray-color);\n  width: 100%;\n}\n\n.signup-content .inputs-list ion-input {\n  font-family: \"Inter\";\n  font-size: 13px;\n  font-weight: normal;\n  font-stretch: normal;\n  font-style: normal;\n  line-height: 1.54;\n  letter-spacing: normal;\n  text-align: left;\n  color: var(--ion-border-gray-color);\n}\n\n.signup-content .inputs-list .label-row {\n  margin-top: 24px;\n  margin-bottom: 14px;\n}\n\n.signup-content .inputs-list .tl-auth-item {\n  --padding-start: 0px;\n  --inner-padding-end: 0px;\n  margin: 8px 0 0;\n  padding: 2px 12px !important;\n  border-radius: 8px;\n  border: solid 1px var(--ion-border-gray-color);\n  background-color: #ffffff;\n}\n\n.signup-content .inputs-list .error-container .error-message {\n  margin: calc(var(--page-margin) / 2) 0px;\n  display: flex;\n  align-items: center;\n  color: var(--ion-color-danger);\n  font-size: 14px;\n}\n\n.signup-content .inputs-list .error-container .error-message ion-icon {\n  -webkit-padding-end: calc(var(--page-margin) / 2);\n          padding-inline-end: calc(var(--page-margin) / 2);\n}\n\n.signup-content .signup-btn {\n  margin: calc(var(--page-margin) / 2) 0px;\n}\n\n.signup-content .other-auth-options-row {\n  justify-content: flex-end;\n  align-items: center;\n}\n\n.signup-content .other-auth-options-row .login-btn {\n  --color: var(--ion-color-secondary);\n  margin: 0px;\n}\n\n.signup-content .other-auth-options-row .login-btn:focus {\n  outline: none;\n}\n\n.signup-content .social-auth-options .options-divider {\n  color: var(--ion-color-medium);\n  margin: var(--page-margin) 0px;\n  text-align: center;\n}\n\n.signup-content .social-auth-options .social-auth-btn {\n  margin: 0px;\n}\n\n.signup-content .social-auth-options .social-auth-btn:not(:first-child) {\n  margin-top: var(--page-margin);\n}\n\n.signup-content .legal-stuff {\n  margin: var(--page-margin) 0px;\n  font-family: \"Inter\";\n  font-size: 12px;\n  font-weight: normal;\n  font-stretch: normal;\n  font-style: normal;\n  line-height: 1.33;\n  letter-spacing: normal;\n  text-align: left;\n  color: #000000;\n  width: 90%;\n}\n\n.signup-content .legal-stuff .legal-action {\n  font-weight: 500;\n  color: #3b86ff;\n  cursor: pointer;\n}\n\n.signup-content .separator {\n  display: flex;\n  align-items: center;\n  text-align: center;\n  color: var(--ion-border-gray-color);\n  margin: 40px 0;\n}\n\n.signup-content .separator::before, .signup-content .separator::after {\n  content: \"\";\n  flex: 1;\n  border-bottom: 1px solid var(--ion-border-gray-color);\n}\n\n.signup-content .separator::before {\n  margin-right: 0.5em;\n}\n\n.signup-content .separator::after {\n  margin-left: 0.5em;\n}\n\n.signup-content .social-logins ion-button {\n  font-size: 13px;\n}\n\n.signup-content .social-logins ion-button ion-label {\n  margin-left: 5px;\n}\n\n@media screen and (max-height: 568px) {\n  .signup-content .auth-title {\n    font-size: 15px;\n  }\n  .signup-content .form-container p {\n    font-size: 12px;\n    line-height: 15px;\n  }\n}\n\n@media screen and (max-height: 640px) {\n  .signup-content .social-logins ion-button {\n    font-size: 11px;\n  }\n  .signup-content .social-logins ion-button ion-label {\n    margin-left: 2.5px;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvYXV0aC9zaWdudXAvc2lnbnVwLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFFQTtFQUNFLHNDQUFBO0VBQ0EsOENBQUE7QUFERjs7QUFLQTtFQUVFLHlCQUFBO0VBQ0EsbUNBQUE7RUFDQSxpQ0FBQTtFQUNBLGlDQUFBO0VBQ0Esb0NBQUE7RUFDQSxtREFBQTtBQUhGOztBQUtFO0VBQ0Usa0JBQUE7RUFDQSxvQkFBQTtFQUNBLHlCQUFBO0FBSEo7O0FBSUk7RUFDRSxvQkFBQTtFQUNBLGVBQUE7RUFDQSxtQkFBQTtFQUNBLG9CQUFBO0VBQ0Esa0JBQUE7RUFDQSxpQkFBQTtFQUNBLHNCQUFBO0VBQ0EsZ0JBQUE7RUFDQSw2Q0FBQTtBQUZOOztBQUdNO0VBQ0UsZ0JBQUE7RUFDQSxvQkFBQTtFQUNBLGtCQUFBO0VBQ0EsY0FBQTtFQUNBLHNCQUFBO0VBQ0EsZ0JBQUE7RUFDQSwrQkFBQTtBQURSOztBQUlNO0VBQ0UsdUJBQUE7QUFGUjs7QUFPRTtFQUNFLDRCQUFBO0VBQ0EsaUJBQUE7RUFDQSx3Q0FBQTtFQUNBLGlEQUFBO0VBQ0EscUJBQUE7RUFDQSxxQkFBQTtFQUNBLG9CQUFBO0VBQ0EsZUFBQTtFQUNBLG9CQUFBO0VBQ0Esa0JBQUE7RUFDQSxpQkFBQTtFQUNBLHNCQUFBO0VBQ0EsZ0JBQUE7QUFMSjs7QUFRRTtFQUVFLGtDQUFBO0FBUEo7O0FBUUk7RUFDRSxVQUFBO0VBQ0EscUJBQUE7QUFOTjs7QUFRSTtFQUNFLG9CQUFBO0VBQ0Esa0JBQUE7RUFDQSx3QkFBQTtBQU5OOztBQU9NO0VBQ0Usb0JBQUE7RUFDQSxrQkFBQTtFQUNBLGVBQUE7RUFDQSw0QkFBQTtFQUNBLGtCQUFBO0VBQ0EsOENBQUE7RUFDQSx5QkFBQTtBQUxSOztBQVFNO0VBQ0UsYUFBQTtFQUNBLGtCQUFBO0VBQ0EsOENBQUE7RUFDQSxXQUFBO0FBTlI7O0FBVUk7RUFDRSxvQkFBQTtFQUNBLGVBQUE7RUFDQSxtQkFBQTtFQUNBLG9CQUFBO0VBQ0Esa0JBQUE7RUFDQSxpQkFBQTtFQUNBLHNCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxtQ0FBQTtBQVJOOztBQVdJO0VBQ0UsZ0JBQUE7RUFDQSxtQkFBQTtBQVROOztBQVdJO0VBQ0Usb0JBQUE7RUFDQSx3QkFBQTtFQUNBLGVBQUE7RUFDQSw0QkFBQTtFQUNBLGtCQUFBO0VBQ0EsOENBQUE7RUFDQSx5QkFBQTtBQVROOztBQWFNO0VBQ0Usd0NBQUE7RUFDQSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSw4QkFBQTtFQUNBLGVBQUE7QUFYUjs7QUFhUTtFQUNFLGlEQUFBO1VBQUEsZ0RBQUE7QUFYVjs7QUFpQkU7RUFDRSx3Q0FBQTtBQWZKOztBQWtCRTtFQUNFLHlCQUFBO0VBQ0EsbUJBQUE7QUFoQko7O0FBa0JJO0VBQ0UsbUNBQUE7RUFDQSxXQUFBO0FBaEJOOztBQWtCTTtFQUNFLGFBQUE7QUFoQlI7O0FBc0JJO0VBQ0UsOEJBQUE7RUFDQSw4QkFBQTtFQUNBLGtCQUFBO0FBcEJOOztBQXVCSTtFQUNFLFdBQUE7QUFyQk47O0FBdUJNO0VBQ0UsOEJBQUE7QUFyQlI7O0FBMEJFO0VBQ0UsOEJBQUE7RUFDQSxvQkFBQTtFQUNBLGVBQUE7RUFDQSxtQkFBQTtFQUNBLG9CQUFBO0VBQ0Esa0JBQUE7RUFDQSxpQkFBQTtFQUNBLHNCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxjQUFBO0VBQ0EsVUFBQTtBQXhCSjs7QUF5Qkk7RUFDRSxnQkFBQTtFQUNBLGNBQUE7RUFDQSxlQUFBO0FBdkJOOztBQTBCRTtFQUNFLGFBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsbUNBQUE7RUFDQSxjQUFBO0FBeEJKOztBQXlCSTtFQUVFLFdBQUE7RUFDQSxPQUFBO0VBQ0EscURBQUE7QUF4Qk47O0FBMEJJO0VBQ0UsbUJBQUE7QUF4Qk47O0FBMEJJO0VBQ0Usa0JBQUE7QUF4Qk47O0FBNkJJO0VBQ0UsZUFBQTtBQTNCTjs7QUE0Qk07RUFDRSxnQkFBQTtBQTFCUjs7QUErQkU7RUFDRTtJQUNFLGVBQUE7RUE3Qko7RUFnQ0k7SUFDRSxlQUFBO0lBQ0EsaUJBQUE7RUE5Qk47QUFDRjs7QUFrQ0U7RUFFSTtJQUNFLGVBQUE7RUFqQ047RUFrQ007SUFDRSxrQkFBQTtFQWhDUjtBQUNGIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvYXV0aC9zaWdudXAvc2lnbnVwLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi8vIEN1c3RvbSB2YXJpYWJsZXNcclxuLy8gTm90ZTogIFRoZXNlIG9uZXMgd2VyZSBhZGRlZCBieSB1cyBhbmQgaGF2ZSBub3RoaW5nIHRvIGRvIHdpdGggSW9uaWMgQ1NTIEN1c3RvbSBQcm9wZXJ0aWVzXHJcbjpob3N0IHtcclxuICAtLXBhZ2UtbWFyZ2luOiB2YXIoLS1hcHAtYnJvYWQtbWFyZ2luKTtcclxuICAtLXBhZ2UtYmFja2dyb3VuZDogdmFyKC0tYXBwLWJhY2tncm91bmQtc2hhZGUpO1xyXG59XHJcblxyXG4vLyBOb3RlOiAgQWxsIHRoZSBDU1MgdmFyaWFibGVzIGRlZmluZWQgYmVsb3cgYXJlIG92ZXJyaWRlcyBvZiBJb25pYyBlbGVtZW50cyBDU1MgQ3VzdG9tIFByb3BlcnRpZXNcclxuLnNpZ251cC1jb250ZW50IHtcclxuICAvLyAtLWJhY2tncm91bmQ6IHZhcigtLXBhZ2UtYmFja2dyb3VuZCk7XHJcbiAgLS1iYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcclxuICAtLXBhZGRpbmctc3RhcnQ6IHZhcigtLXBhZ2UtbWFyZ2luKTtcclxuICAtLXBhZGRpbmctZW5kOiB2YXIoLS1wYWdlLW1hcmdpbik7XHJcbiAgLS1wYWRkaW5nLXRvcDogdmFyKC0tcGFnZS1tYXJnaW4pO1xyXG4gIC0tcGFkZGluZy1ib3R0b206IHZhcigtLXBhZ2UtbWFyZ2luKTtcclxuICBiYWNrZ3JvdW5kLWltYWdlOiBsaW5lYXItZ3JhZGllbnQoIzI4Y2M5OSwgIzJlN2NmNik7XHJcblxyXG4gIC5mb3JtLWNvbnRhaW5lciB7XHJcbiAgICBwYWRkaW5nOiA0MHB4IDI5cHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiAzLjJweDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmZmZmY7XHJcbiAgICBwIHtcclxuICAgICAgZm9udC1mYW1pbHk6IFwiSW50ZXJcIjtcclxuICAgICAgZm9udC1zaXplOiAxNXB4O1xyXG4gICAgICBmb250LXdlaWdodDogbm9ybWFsO1xyXG4gICAgICBmb250LXN0cmV0Y2g6IG5vcm1hbDtcclxuICAgICAgZm9udC1zdHlsZTogbm9ybWFsO1xyXG4gICAgICBsaW5lLWhlaWdodDogMTdweDtcclxuICAgICAgbGV0dGVyLXNwYWNpbmc6IG5vcm1hbDtcclxuICAgICAgdGV4dC1hbGlnbjogbGVmdDtcclxuICAgICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1saWdodGVyLWdyZXktY29udHJhc3QpO1xyXG4gICAgICBhIHtcclxuICAgICAgICBmb250LXdlaWdodDogNjAwO1xyXG4gICAgICAgIGZvbnQtc3RyZXRjaDogbm9ybWFsO1xyXG4gICAgICAgIGZvbnQtc3R5bGU6IG5vcm1hbDtcclxuICAgICAgICBsaW5lLWhlaWdodDogMTtcclxuICAgICAgICBsZXR0ZXItc3BhY2luZzogbm9ybWFsO1xyXG4gICAgICAgIHRleHQtYWxpZ246IGxlZnQ7XHJcbiAgICAgICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KTtcclxuICAgICAgfVxyXG5cclxuICAgICAgLnB1bGxlZC1yaWdodCB7XHJcbiAgICAgICAgZmxvYXQ6IHJpZ2h0ICFpbXBvcnRhbnQ7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcblxyXG4gIC5hdXRoLXRpdGxlIHtcclxuICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItZGFyayk7XHJcbiAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgIG1hcmdpbi10b3A6IGNhbGModmFyKC0tcGFnZS1tYXJnaW4pIC8gMik7XHJcbiAgICBtYXJnaW4tYm90dG9tOiBjYWxjKHZhcigtLXBhZ2UtbWFyZ2luKSAqICgzIC8gMikpO1xyXG4gICAgbGV0dGVyLXNwYWNpbmc6IDAuNnB4O1xyXG4gICAgbWFyZ2luOiAwIDM1cHggMTZweCAwO1xyXG4gICAgZm9udC1mYW1pbHk6IFwiSW50ZXJcIjtcclxuICAgIGZvbnQtc2l6ZTogMjBweDtcclxuICAgIGZvbnQtc3RyZXRjaDogbm9ybWFsO1xyXG4gICAgZm9udC1zdHlsZTogbm9ybWFsO1xyXG4gICAgbGluZS1oZWlnaHQ6IDEuMjU7XHJcbiAgICBsZXR0ZXItc3BhY2luZzogbm9ybWFsO1xyXG4gICAgdGV4dC1hbGlnbjogbGVmdDtcclxuICB9XHJcblxyXG4gIC5pbnB1dHMtbGlzdCB7XHJcbiAgICAvLyAtLWlvbi1pdGVtLWJhY2tncm91bmQ6IHZhcigtLXBhZ2UtYmFja2dyb3VuZCk7XHJcbiAgICAtLWlvbi1pdGVtLWJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xyXG4gICAgLmhhbGYtd2lkdGgge1xyXG4gICAgICB3aWR0aDogNTAlO1xyXG4gICAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICB9XHJcbiAgICAuaW5wdXQtaXRlbSB7XHJcbiAgICAgIC0tcGFkZGluZy1zdGFydDogMHB4O1xyXG4gICAgICAtLXBhZGRpbmctZW5kOiAwcHg7XHJcbiAgICAgIC0taW5uZXItcGFkZGluZy1lbmQ6IDBweDtcclxuICAgICAgaW9uLWlucHV0IHtcclxuICAgICAgICAtLXBhZGRpbmctc3RhcnQ6IDBweDtcclxuICAgICAgICAtLXBhZGRpbmctZW5kOiAwcHg7XHJcbiAgICAgICAgbWFyZ2luOiA4cHggMCAwO1xyXG4gICAgICAgIHBhZGRpbmc6IDZweCAxMnB4ICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogOHB4O1xyXG4gICAgICAgIGJvcmRlcjogc29saWQgMXB4IHZhcigtLWlvbi1ib3JkZXItZ3JheS1jb2xvcik7XHJcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZmZmZjtcclxuICAgICAgfVxyXG5cclxuICAgICAgaW9uLXNlbGVjdCB7XHJcbiAgICAgICAgcGFkZGluZzogMTRweDtcclxuICAgICAgICBib3JkZXItcmFkaXVzOiA4cHg7XHJcbiAgICAgICAgYm9yZGVyOiBzb2xpZCAxcHggdmFyKC0taW9uLWJvcmRlci1ncmF5LWNvbG9yKTtcclxuICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGlvbi1pbnB1dCB7XHJcbiAgICAgIGZvbnQtZmFtaWx5OiBcIkludGVyXCI7XHJcbiAgICAgIGZvbnQtc2l6ZTogMTNweDtcclxuICAgICAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcclxuICAgICAgZm9udC1zdHJldGNoOiBub3JtYWw7XHJcbiAgICAgIGZvbnQtc3R5bGU6IG5vcm1hbDtcclxuICAgICAgbGluZS1oZWlnaHQ6IDEuNTQ7XHJcbiAgICAgIGxldHRlci1zcGFjaW5nOiBub3JtYWw7XHJcbiAgICAgIHRleHQtYWxpZ246IGxlZnQ7XHJcbiAgICAgIGNvbG9yOiB2YXIoLS1pb24tYm9yZGVyLWdyYXktY29sb3IpO1xyXG4gICAgfVxyXG5cclxuICAgIC5sYWJlbC1yb3cge1xyXG4gICAgICBtYXJnaW4tdG9wOiAyNHB4O1xyXG4gICAgICBtYXJnaW4tYm90dG9tOiAxNHB4O1xyXG4gICAgfVxyXG4gICAgLnRsLWF1dGgtaXRlbSB7XHJcbiAgICAgIC0tcGFkZGluZy1zdGFydDogMHB4O1xyXG4gICAgICAtLWlubmVyLXBhZGRpbmctZW5kOiAwcHg7XHJcbiAgICAgIG1hcmdpbjogOHB4IDAgMDtcclxuICAgICAgcGFkZGluZzogMnB4IDEycHggIWltcG9ydGFudDtcclxuICAgICAgYm9yZGVyLXJhZGl1czogOHB4O1xyXG4gICAgICBib3JkZXI6IHNvbGlkIDFweCB2YXIoLS1pb24tYm9yZGVyLWdyYXktY29sb3IpO1xyXG4gICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmZmZmO1xyXG4gICAgfVxyXG5cclxuICAgIC5lcnJvci1jb250YWluZXIge1xyXG4gICAgICAuZXJyb3ItbWVzc2FnZSB7XHJcbiAgICAgICAgbWFyZ2luOiBjYWxjKHZhcigtLXBhZ2UtbWFyZ2luKSAvIDIpIDBweDtcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1kYW5nZXIpO1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMTRweDtcclxuXHJcbiAgICAgICAgaW9uLWljb24ge1xyXG4gICAgICAgICAgcGFkZGluZy1pbmxpbmUtZW5kOiBjYWxjKHZhcigtLXBhZ2UtbWFyZ2luKSAvIDIpO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgLnNpZ251cC1idG4ge1xyXG4gICAgbWFyZ2luOiBjYWxjKHZhcigtLXBhZ2UtbWFyZ2luKSAvIDIpIDBweDtcclxuICB9XHJcblxyXG4gIC5vdGhlci1hdXRoLW9wdGlvbnMtcm93IHtcclxuICAgIGp1c3RpZnktY29udGVudDogZmxleC1lbmQ7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG5cclxuICAgIC5sb2dpbi1idG4ge1xyXG4gICAgICAtLWNvbG9yOiB2YXIoLS1pb24tY29sb3Itc2Vjb25kYXJ5KTtcclxuICAgICAgbWFyZ2luOiAwcHg7XHJcblxyXG4gICAgICAmOmZvY3VzIHtcclxuICAgICAgICBvdXRsaW5lOiBub25lO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICAuc29jaWFsLWF1dGgtb3B0aW9ucyB7XHJcbiAgICAub3B0aW9ucy1kaXZpZGVyIHtcclxuICAgICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1tZWRpdW0pO1xyXG4gICAgICBtYXJnaW46IHZhcigtLXBhZ2UtbWFyZ2luKSAwcHg7XHJcbiAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIH1cclxuXHJcbiAgICAuc29jaWFsLWF1dGgtYnRuIHtcclxuICAgICAgbWFyZ2luOiAwcHg7XHJcblxyXG4gICAgICAmOm5vdCg6Zmlyc3QtY2hpbGQpIHtcclxuICAgICAgICBtYXJnaW4tdG9wOiB2YXIoLS1wYWdlLW1hcmdpbik7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcblxyXG4gIC5sZWdhbC1zdHVmZiB7XHJcbiAgICBtYXJnaW46IHZhcigtLXBhZ2UtbWFyZ2luKSAwcHg7XHJcbiAgICBmb250LWZhbWlseTogXCJJbnRlclwiO1xyXG4gICAgZm9udC1zaXplOiAxMnB4O1xyXG4gICAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcclxuICAgIGZvbnQtc3RyZXRjaDogbm9ybWFsO1xyXG4gICAgZm9udC1zdHlsZTogbm9ybWFsO1xyXG4gICAgbGluZS1oZWlnaHQ6IDEuMzM7XHJcbiAgICBsZXR0ZXItc3BhY2luZzogbm9ybWFsO1xyXG4gICAgdGV4dC1hbGlnbjogbGVmdDtcclxuICAgIGNvbG9yOiAjMDAwMDAwO1xyXG4gICAgd2lkdGg6IDkwJTtcclxuICAgIC5sZWdhbC1hY3Rpb24ge1xyXG4gICAgICBmb250LXdlaWdodDogNTAwO1xyXG4gICAgICBjb2xvcjogIzNiODZmZjtcclxuICAgICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gICAgfVxyXG4gIH1cclxuICAuc2VwYXJhdG9yIHtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgY29sb3I6IHZhcigtLWlvbi1ib3JkZXItZ3JheS1jb2xvcik7XHJcbiAgICBtYXJnaW46IDQwcHggMDtcclxuICAgICY6OmJlZm9yZSxcclxuICAgICY6OmFmdGVyIHtcclxuICAgICAgY29udGVudDogXCJcIjtcclxuICAgICAgZmxleDogMTtcclxuICAgICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIHZhcigtLWlvbi1ib3JkZXItZ3JheS1jb2xvcik7XHJcbiAgICB9XHJcbiAgICAmOjpiZWZvcmUge1xyXG4gICAgICBtYXJnaW4tcmlnaHQ6IDAuNWVtO1xyXG4gICAgfVxyXG4gICAgJjo6YWZ0ZXIge1xyXG4gICAgICBtYXJnaW4tbGVmdDogMC41ZW07XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICAuc29jaWFsLWxvZ2lucyB7XHJcbiAgICBpb24tYnV0dG9uIHtcclxuICAgICAgZm9udC1zaXplOiAxM3B4O1xyXG4gICAgICBpb24tbGFiZWwge1xyXG4gICAgICAgIG1hcmdpbi1sZWZ0OiA1cHg7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcblxyXG4gIEBtZWRpYSBzY3JlZW4gYW5kIChtYXgtaGVpZ2h0OiA1NjhweCkge1xyXG4gICAgLmF1dGgtdGl0bGUge1xyXG4gICAgICBmb250LXNpemU6IDE1cHg7XHJcbiAgICB9XHJcbiAgICAuZm9ybS1jb250YWluZXIge1xyXG4gICAgICBwIHtcclxuICAgICAgICBmb250LXNpemU6IDEycHg7XHJcbiAgICAgICAgbGluZS1oZWlnaHQ6IDE1cHg7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcblxyXG4gIEBtZWRpYSBzY3JlZW4gYW5kIChtYXgtaGVpZ2h0OiA2NDBweCkge1xyXG4gICAgLnNvY2lhbC1sb2dpbnMge1xyXG4gICAgICBpb24tYnV0dG9uIHtcclxuICAgICAgICBmb250LXNpemU6IDExcHg7XHJcbiAgICAgICAgaW9uLWxhYmVsIHtcclxuICAgICAgICAgIG1hcmdpbi1sZWZ0OiAyLjVweDtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcblxyXG4gIEBtZWRpYSBzY3JlZW4gYW5kIChtYXgtaGVpZ2h0OiA2NjdweCkge1xyXG4gIH1cclxufVxyXG4iXX0= */";
      /***/
    },

    /***/
    "./src/app/pages/auth/signup/signup.page.ts":
    /*!**************************************************!*\
      !*** ./src/app/pages/auth/signup/signup.page.ts ***!
      \**************************************************/

    /*! exports provided: SignupPage */

    /***/
    function srcAppPagesAuthSignupSignupPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "SignupPage", function () {
        return SignupPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _core_helpers__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @core/helpers */
      "./src/app/core/helpers/index.ts");
      /* harmony import */


      var _core_services__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @core/services */
      "./src/app/core/services/index.ts");
      /* harmony import */


      var _core_validators__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! @core/validators */
      "./src/app/core/validators/index.ts");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _shared_lib_services__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
      /*! @shared-lib/services */
      "./src/app/shared/services/index.ts");
      /* harmony import */


      var lodash__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
      /*! lodash */
      "./node_modules/lodash/lodash.js");
      /* harmony import */


      var lodash__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_10__);
      /* harmony import */


      var rxjs_operators__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
      /*! rxjs/operators */
      "./node_modules/rxjs/_esm2015/operators/index.js");
      /* harmony import */


      var _privacy_policy__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
      /*! ../privacy-policy */
      "./src/app/pages/auth/privacy-policy/index.ts");
      /* harmony import */


      var _terms_of_service__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(
      /*! ../terms-of-service */
      "./src/app/pages/auth/terms-of-service/index.ts");

      var SignupPage = /*#__PURE__*/function () {
        function SignupPage(modalController, routerOutlet, router, route, menu, authService, ngZone, loadingController, location, dataService, storageService) {
          _classCallCheck(this, SignupPage);

          this.modalController = modalController;
          this.routerOutlet = routerOutlet;
          this.router = router;
          this.route = route;
          this.menu = menu;
          this.authService = authService;
          this.ngZone = ngZone;
          this.loadingController = loadingController;
          this.location = location;
          this.dataService = dataService;
          this.storageService = storageService;
          this.validation_messages = _core_helpers__WEBPACK_IMPORTED_MODULE_5__["signUpValidationMessages"];
          this.matching_passwords_group = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormGroup"]({
            password: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].minLength(5), _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required])),
            confirm_password: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required)
          }, function (formGroup) {
            return _core_validators__WEBPACK_IMPORTED_MODULE_7__["PasswordValidator"].areNotEqual(formGroup);
          });
          this.signupForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormGroup"]({
            firstName: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required])),
            lastName: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required])),
            email: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].pattern("^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$")])),
            country: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
            terms: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](false, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].pattern("true")),
            matching_passwords: this.matching_passwords_group
          });
        }

        _createClass(SignupPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            this.getCountries();
          } // Disable side menu for this page

        }, {
          key: "ionViewDidEnter",
          value: function ionViewDidEnter() {
            this.menu.enable(false);
          } // Restore to default when leaving this page

        }, {
          key: "ionViewDidLeave",
          value: function ionViewDidLeave() {
            this.menu.enable(true);
            this.countriesSubscription.unsubscribe();
          }
        }, {
          key: "showTermsModal",
          value: function showTermsModal() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee6() {
              var modal;
              return regeneratorRuntime.wrap(function _callee6$(_context6) {
                while (1) {
                  switch (_context6.prev = _context6.next) {
                    case 0:
                      _context6.next = 2;
                      return this.modalController.create({
                        component: _terms_of_service__WEBPACK_IMPORTED_MODULE_13__["TermsOfServicePage"],
                        swipeToClose: true,
                        presentingElement: this.routerOutlet.nativeEl
                      });

                    case 2:
                      modal = _context6.sent;
                      _context6.next = 5;
                      return modal.present();

                    case 5:
                      return _context6.abrupt("return", _context6.sent);

                    case 6:
                    case "end":
                      return _context6.stop();
                  }
                }
              }, _callee6, this);
            }));
          }
        }, {
          key: "showPrivacyModal",
          value: function showPrivacyModal() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee7() {
              var modal;
              return regeneratorRuntime.wrap(function _callee7$(_context7) {
                while (1) {
                  switch (_context7.prev = _context7.next) {
                    case 0:
                      _context7.next = 2;
                      return this.modalController.create({
                        component: _privacy_policy__WEBPACK_IMPORTED_MODULE_12__["PrivacyPolicyPage"],
                        swipeToClose: true,
                        presentingElement: this.routerOutlet.nativeEl
                      });

                    case 2:
                      modal = _context7.sent;
                      _context7.next = 5;
                      return modal.present();

                    case 5:
                      return _context7.abrupt("return", _context7.sent);

                    case 6:
                    case "end":
                      return _context7.stop();
                  }
                }
              }, _callee7, this);
            }));
          } // Once the auth provider finished the authentication flow, and the auth redirect completes,
          // hide the loader and redirect the user to the profile page

        }, {
          key: "redirectLoggedUserToProfilePage",
          value: function redirectLoggedUserToProfilePage() {
            var _this8 = this;

            this.dismissLoading(); // As we are calling the Angular router navigation inside a subscribe method, the navigation will be triggered outside Angular zone.
            // That's why we need to wrap the router navigation call inside an ngZone wrapper

            this.ngZone.run(function () {
              return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this8, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee8() {
                var previousUrl, hasDonetutorial;
                return regeneratorRuntime.wrap(function _callee8$(_context8) {
                  while (1) {
                    switch (_context8.prev = _context8.next) {
                      case 0:
                        // Get previous URL from our custom History Helper
                        // If there's no previous page, then redirect to profile
                        // const previousUrl = this.historyHelper.previousUrl || 'firebase/auth/profile';
                        previousUrl = "home";
                        _context8.next = 3;
                        return this.storageService.getItem("tutorial");

                      case 3:
                        hasDonetutorial = _context8.sent;
                        console.log("hasDonetutorial", hasDonetutorial);

                        if (Object(lodash__WEBPACK_IMPORTED_MODULE_10__["isNil"])(hasDonetutorial)) {
                          this.storageService.setItem({
                            key: "tutorial",
                            value: JSON.stringify(1)
                          });
                          previousUrl = "walkthrough";
                        } // No need to store in the navigation history the sign-in page with redirect params (it's justa a mandatory mid-step)
                        // Navigate to profile and replace current url with profile


                        this.router.navigate([previousUrl], {
                          replaceUrl: true
                        });

                      case 7:
                      case "end":
                        return _context8.stop();
                    }
                  }
                }, _callee8, this);
              }));
            });
          }
        }, {
          key: "presentLoading",
          value: function presentLoading(authProvider) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee9() {
              var authProviderCapitalized;
              return regeneratorRuntime.wrap(function _callee9$(_context9) {
                while (1) {
                  switch (_context9.prev = _context9.next) {
                    case 0:
                      authProviderCapitalized = authProvider[0].toUpperCase() + authProvider.slice(1);
                      _context9.next = 3;
                      return this.loadingController.create({
                        message: authProvider ? "Signing up with " + authProviderCapitalized : "Signin up ..."
                      });

                    case 3:
                      this.redirectLoader = _context9.sent;
                      _context9.next = 6;
                      return this.redirectLoader.present();

                    case 6:
                    case "end":
                      return _context9.stop();
                  }
                }
              }, _callee9, this);
            }));
          }
        }, {
          key: "dismissLoading",
          value: function dismissLoading() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee10() {
              return regeneratorRuntime.wrap(function _callee10$(_context10) {
                while (1) {
                  switch (_context10.prev = _context10.next) {
                    case 0:
                      if (!this.redirectLoader) {
                        _context10.next = 3;
                        break;
                      }

                      _context10.next = 3;
                      return this.redirectLoader.dismiss();

                    case 3:
                    case "end":
                      return _context10.stop();
                  }
                }
              }, _callee10, this);
            }));
          }
        }, {
          key: "resetSubmitError",
          value: function resetSubmitError() {
            this.submitError = null;
          } // Before invoking auth provider redirect flow, present a loading indicator and add a flag to the path.
          // The precense of the flag in the path indicates we should wait for the auth redirect to complete.

        }, {
          key: "prepareForAuthWithProvidersRedirection",
          value: function prepareForAuthWithProvidersRedirection(authProvider) {
            this.presentLoading(authProvider);
            this.location.go(this.location.path(), "auth-redirect=" + authProvider, this.location.getState());
          }
        }, {
          key: "manageAuthWithProvidersErrors",
          value: function manageAuthWithProvidersErrors(errorMessage) {
            this.submitError = errorMessage; // remove auth-redirect param from url

            this.location.replaceState(this.router.url.split("?")[0], "");
            this.dismissLoading();
          }
        }, {
          key: "signUpWithEmail",
          value: function signUpWithEmail() {
            var _this9 = this;

            this.resetSubmitError();
            var _this$signupForm$valu = this.signupForm.value,
                firstName = _this$signupForm$valu.firstName,
                lastName = _this$signupForm$valu.lastName,
                email = _this$signupForm$valu.email,
                country = _this$signupForm$valu.country,
                password = _this$signupForm$valu.matching_passwords.password;
            var signupForm = {
              firstName: firstName,
              lastName: lastName,
              email: email,
              country: country,
              password: password
            };
            this.authService.signUpWithEmail(signupForm).then(function (user) {
              // navigate to user profile
              _this9.redirectLoggedUserToProfilePage();
            })["catch"](function (error) {
              _this9.submitError = error.message;
            });
          }
        }, {
          key: "doFacebookSignup",
          value: function doFacebookSignup() {
            var _this10 = this;

            this.resetSubmitError();
            this.prepareForAuthWithProvidersRedirection("facebook");
            this.authService.signInWithFacebook().subscribe(function (result) {
              // This gives you a Facebook Access Token. You can use it to access the Facebook API.
              // const token = result.credential.accessToken;
              var _ref6 = result || {},
                  user = _ref6.user;

              _this10.authService.setUserStorage(user);

              var resultHandled = _this10.authService.handleOAuthResult(result, "facebook.com");

              if (resultHandled) _this10.redirectLoggedUserToProfilePage();
            }, function (error) {
              console.log("doFacebookSignup error", error);

              _this10.manageAuthWithProvidersErrors(error.message);
            });
          }
        }, {
          key: "doGoogleSignup",
          value: function doGoogleSignup() {
            var _this11 = this;

            this.resetSubmitError();
            this.prepareForAuthWithProvidersRedirection("google");
            this.authService.signInWithGoogle().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_11__["tap"])(function (data) {
              console.log("signin with google", data);
            })).subscribe(function (result) {
              var _ref7 = result || {},
                  user = _ref7.user;

              _this11.authService.setUserStorage(user);

              var resultHandled = _this11.authService.handleOAuthResult(result, "google.com");

              if (resultHandled) _this11.redirectLoggedUserToProfilePage();
            }, function (error) {
              console.log("gogole signin error error", error);

              _this11.manageAuthWithProvidersErrors(error.message);
            });
          }
        }, {
          key: "doTwitterSignup",
          value: function doTwitterSignup() {
            var _this12 = this;

            this.resetSubmitError();
            this.prepareForAuthWithProvidersRedirection("twitter");
            this.authService.signInWithTwitter().subscribe(function (result) {
              console.log("result", result); // This gives you a Facebook Access Token. You can use it to access the Facebook API.
              // const token = result.credential.accessToken;

              var _ref8 = result || {},
                  user = _ref8.user;

              _this12.authService.setUserStorage(user);

              var resultHandled = _this12.authService.handleOAuthResult(result, "twitter.com");

              if (resultHandled) _this12.redirectLoggedUserToProfilePage();
            }, function (error) {
              console.log("doTwitterSignup error", error);

              _this12.manageAuthWithProvidersErrors(error.message);
            });
          }
        }, {
          key: "getCountries",
          value: function getCountries() {
            var _this13 = this;

            this.countriesSubscription = this.dataService.getCountries().subscribe(function (data) {
              _this13.countries = data;
            });
          }
        }]);

        return SignupPage;
      }();

      SignupPage.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_8__["ModalController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_8__["IonRouterOutlet"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_8__["MenuController"]
        }, {
          type: _core_services__WEBPACK_IMPORTED_MODULE_6__["FirebaseAuthService"]
        }, {
          type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["NgZone"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_8__["LoadingController"]
        }, {
          type: _angular_common__WEBPACK_IMPORTED_MODULE_1__["Location"]
        }, {
          type: _core_services__WEBPACK_IMPORTED_MODULE_6__["DataService"]
        }, {
          type: _shared_lib_services__WEBPACK_IMPORTED_MODULE_9__["StorageService"]
        }];
      };

      SignupPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
        selector: "app-signup",
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./signup.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/auth/signup/signup.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./signup.page.scss */
        "./src/app/pages/auth/signup/signup.page.scss"))["default"]]
      }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_8__["ModalController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_8__["IonRouterOutlet"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"], _ionic_angular__WEBPACK_IMPORTED_MODULE_8__["MenuController"], _core_services__WEBPACK_IMPORTED_MODULE_6__["FirebaseAuthService"], _angular_core__WEBPACK_IMPORTED_MODULE_2__["NgZone"], _ionic_angular__WEBPACK_IMPORTED_MODULE_8__["LoadingController"], _angular_common__WEBPACK_IMPORTED_MODULE_1__["Location"], _core_services__WEBPACK_IMPORTED_MODULE_6__["DataService"], _shared_lib_services__WEBPACK_IMPORTED_MODULE_9__["StorageService"]])], SignupPage);
      /***/
    },

    /***/
    "./src/app/pages/auth/terms-of-service/index.ts":
    /*!******************************************************!*\
      !*** ./src/app/pages/auth/terms-of-service/index.ts ***!
      \******************************************************/

    /*! exports provided: TermsOfServicePage */

    /***/
    function srcAppPagesAuthTermsOfServiceIndexTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony import */


      var _terms_of_service_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! ./terms-of-service.page */
      "./src/app/pages/auth/terms-of-service/terms-of-service.page.ts");
      /* harmony reexport (safe) */


      __webpack_require__.d(__webpack_exports__, "TermsOfServicePage", function () {
        return _terms_of_service_page__WEBPACK_IMPORTED_MODULE_0__["default"];
      });
      /***/

    },

    /***/
    "./src/app/pages/auth/terms-of-service/styles/terms-of-service.page.scss":
    /*!*******************************************************************************!*\
      !*** ./src/app/pages/auth/terms-of-service/styles/terms-of-service.page.scss ***!
      \*******************************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppPagesAuthTermsOfServiceStylesTermsOfServicePageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ":host {\n  --page-margin: var(--app-broad-margin);\n  --page-background: var(--app-background-shade);\n}\n\n.legal-content {\n  --background: var(--page-background);\n  --padding-start: var(--page-margin);\n  --padding-end: var(--page-margin);\n  --padding-top: var(--page-margin);\n  --padding-bottom: var(--page-margin);\n}\n\n.legal-content .legal-header {\n  color: var(--ion-color-darkest);\n}\n\n.legal-content .legal-title {\n  color: var(--ion-color-darkest);\n  margin: var(--page-margin) 0px calc(var(--page-margin) / 2);\n}\n\n.legal-content .legal-text {\n  color: var(--ion-color-medium);\n  margin: calc(var(--page-margin) / 2) 0px var(--page-margin);\n  font-size: 14px;\n  line-height: 20px;\n  text-align: justify;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvYXV0aC90ZXJtcy1vZi1zZXJ2aWNlL3N0eWxlcy90ZXJtcy1vZi1zZXJ2aWNlLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFFQTtFQUNFLHNDQUFBO0VBQ0EsOENBQUE7QUFERjs7QUFLQTtFQUNFLG9DQUFBO0VBQ0EsbUNBQUE7RUFDQSxpQ0FBQTtFQUNBLGlDQUFBO0VBQ0Esb0NBQUE7QUFGRjs7QUFJRTtFQUNFLCtCQUFBO0FBRko7O0FBS0U7RUFDRSwrQkFBQTtFQUNBLDJEQUFBO0FBSEo7O0FBTUU7RUFDRSw4QkFBQTtFQUNBLDJEQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0VBQ0EsbUJBQUE7QUFKSiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2F1dGgvdGVybXMtb2Ytc2VydmljZS9zdHlsZXMvdGVybXMtb2Ytc2VydmljZS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIvLyBDdXN0b20gdmFyaWFibGVzXHJcbi8vIE5vdGU6ICBUaGVzZSBvbmVzIHdlcmUgYWRkZWQgYnkgdXMgYW5kIGhhdmUgbm90aGluZyB0byBkbyB3aXRoIElvbmljIENTUyBDdXN0b20gUHJvcGVydGllc1xyXG46aG9zdCB7XHJcbiAgLS1wYWdlLW1hcmdpbjogdmFyKC0tYXBwLWJyb2FkLW1hcmdpbik7XHJcbiAgLS1wYWdlLWJhY2tncm91bmQ6IHZhcigtLWFwcC1iYWNrZ3JvdW5kLXNoYWRlKTtcclxufVxyXG5cclxuLy8gTm90ZTogIEFsbCB0aGUgQ1NTIHZhcmlhYmxlcyBkZWZpbmVkIGJlbG93IGFyZSBvdmVycmlkZXMgb2YgSW9uaWMgZWxlbWVudHMgQ1NTIEN1c3RvbSBQcm9wZXJ0aWVzXHJcbi5sZWdhbC1jb250ZW50IHtcclxuICAtLWJhY2tncm91bmQ6IHZhcigtLXBhZ2UtYmFja2dyb3VuZCk7XHJcbiAgLS1wYWRkaW5nLXN0YXJ0OiB2YXIoLS1wYWdlLW1hcmdpbik7XHJcbiAgLS1wYWRkaW5nLWVuZDogdmFyKC0tcGFnZS1tYXJnaW4pO1xyXG4gIC0tcGFkZGluZy10b3A6IHZhcigtLXBhZ2UtbWFyZ2luKTtcclxuICAtLXBhZGRpbmctYm90dG9tOiB2YXIoLS1wYWdlLW1hcmdpbik7XHJcblxyXG4gIC5sZWdhbC1oZWFkZXIge1xyXG4gICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1kYXJrZXN0KTtcclxuICB9XHJcblxyXG4gIC5sZWdhbC10aXRsZSB7XHJcbiAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLWRhcmtlc3QpO1xyXG4gICAgbWFyZ2luOiB2YXIoLS1wYWdlLW1hcmdpbikgMHB4IGNhbGModmFyKC0tcGFnZS1tYXJnaW4pIC8gMik7XHJcbiAgfVxyXG5cclxuICAubGVnYWwtdGV4dCB7XHJcbiAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLW1lZGl1bSk7XHJcbiAgICBtYXJnaW46IGNhbGModmFyKC0tcGFnZS1tYXJnaW4pIC8gMikgMHB4IHZhcigtLXBhZ2UtbWFyZ2luKTtcclxuICAgIGZvbnQtc2l6ZTogMTRweDtcclxuICAgIGxpbmUtaGVpZ2h0OiAyMHB4O1xyXG4gICAgdGV4dC1hbGlnbjoganVzdGlmeTtcclxuICB9XHJcbn1cclxuIl19 */";
      /***/
    },

    /***/
    "./src/app/pages/auth/terms-of-service/terms-of-service.page.ts":
    /*!**********************************************************************!*\
      !*** ./src/app/pages/auth/terms-of-service/terms-of-service.page.ts ***!
      \**********************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppPagesAuthTermsOfServiceTermsOfServicePageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");

      var TermsOfServicePage = /*#__PURE__*/function () {
        function TermsOfServicePage(modalController) {
          _classCallCheck(this, TermsOfServicePage);

          this.modalController = modalController;
        }

        _createClass(TermsOfServicePage, [{
          key: "dismiss",
          value: function dismiss() {
            this.modalController.dismiss();
          }
        }]);

        return TermsOfServicePage;
      }();

      TermsOfServicePage.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]
        }];
      };

      TermsOfServicePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "app-terms-of-service-page",
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./terms-of-service.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/auth/terms-of-service/terms-of-service.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./styles/terms-of-service.page.scss */
        "./src/app/pages/auth/terms-of-service/styles/terms-of-service.page.scss"))["default"]]
      }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]])], TermsOfServicePage);
      /* harmony default export */

      __webpack_exports__["default"] = TermsOfServicePage;
      /***/
    }
  }]);
})();
//# sourceMappingURL=pages-auth-auth-module-es5.js.map