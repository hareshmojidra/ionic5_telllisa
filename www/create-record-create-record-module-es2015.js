(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["create-record-create-record-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/recordings/create-record/create-record.page.html":
/*!**************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/recordings/create-record/create-record.page.html ***!
  \**************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\r\n  <ion-toolbar color=\"skip-btn\" class=\"header-toolbar\">\r\n    <ion-buttons slot=\"start\">\r\n      <ion-back-button></ion-back-button>\r\n    </ion-buttons>\r\n    <app-aspect-ratio [ratio]=\"{w:100, h:12}\">\r\n      <app-image-shell style=\"width:150px; height: 34px; margin: 0 auto;\" animation=\"spinner\"\r\n        [src]=\"'assets/tl-logo-simpel-white.png'\" [alt]=\"'walkthrough'\"></app-image-shell>\r\n    </app-aspect-ratio>\r\n    <ion-buttons slot=\"end\">\r\n      <div>&nbsp;</div>\r\n    </ion-buttons>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content class=\"forms-validations-content\">\r\n  <app-ios-speech-fab [showFab]=\"showFab\"></app-ios-speech-fab>\r\n\r\n  <form class=\"validations-form\" [formGroup]=\"createRecordForm\" (ngSubmit)=\"save(createRecordForm.value)\">\r\n    <ion-list class=\"inputs-list\" lines=\"none\">\r\n      <ion-list-header class=\"ion-margin-bottom\">\r\n        <ion-label class=\"header-title\">Type or record your task</ion-label>\r\n      </ion-list-header>\r\n\r\n      <!-- When this bug (https://github.com/ionic-team/ionic-framework/issues/22117) gets fixed, remove .item-label-floating class -->\r\n      <ion-item class=\"input-item\">\r\n        <textarea autofocus autosize [minRows]=\"8\" placeholder=\"Type a message...\" type=\"text\" class=\"message-input\"\r\n          formControlName=\"name\" required></textarea>\r\n      </ion-item>\r\n      <div class=\"error-container\">\r\n        <ng-container *ngFor=\"let validation of validations.name\">\r\n          <div class=\"error-message\"\r\n            *ngIf=\"createRecordForm.get('name').hasError(validation.type) && createRecordForm.get('name').dirty\">\r\n            <ion-icon name=\"information-circle-outline\"></ion-icon>\r\n            <span>{{ validation.message }}</span>\r\n          </div>\r\n        </ng-container>\r\n      </div>\r\n      <ion-item lines=\"none\" class=\"ion-margin-vertical\">\r\n        <div class=\"centered\">\r\n          <ion-button class=\"default-btn radio-icon\" shape=\"round\" (click)=\"openSpeechRecognition()\">\r\n            <ion-icon slot=\"icon-only\" name=\"mic-sharp\"></ion-icon>\r\n          </ion-button>\r\n        </div>\r\n      </ion-item>\r\n    </ion-list>\r\n  </form>\r\n  <div class=\"validations-form\">\r\n    <ion-list class=\"inputs-list\" lines=\"none\">\r\n      <ion-item>\r\n        <ion-buttons slot=\"start\">\r\n          <ion-button class=\"large-button\" size=\"large\" shape=\"round\" color=\"success\" [routerLink]=\"['/home']\"\r\n            routerDirection=\"backward\">\r\n            Cancel\r\n          </ion-button>\r\n        </ion-buttons>\r\n        <ion-buttons slot=\"end\">\r\n          <ion-button class=\"large-button\" color=\"tertiary\" (click)=\"save()\" size=\"large\" shape=\"round\">\r\n            Save\r\n          </ion-button>\r\n        </ion-buttons>\r\n      </ion-item>\r\n    </ion-list>\r\n  </div>\r\n</ion-content>");

/***/ }),

/***/ "./src/app/pages/recordings/create-record/create-record-routing.module.ts":
/*!********************************************************************************!*\
  !*** ./src/app/pages/recordings/create-record/create-record-routing.module.ts ***!
  \********************************************************************************/
/*! exports provided: CreateRecordPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CreateRecordPageRoutingModule", function() { return CreateRecordPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _create_record_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./create-record.page */ "./src/app/pages/recordings/create-record/create-record.page.ts");




const routes = [
    {
        path: '',
        component: _create_record_page__WEBPACK_IMPORTED_MODULE_3__["CreateRecordPage"]
    }
];
let CreateRecordPageRoutingModule = class CreateRecordPageRoutingModule {
};
CreateRecordPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], CreateRecordPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/recordings/create-record/create-record.module.ts":
/*!************************************************************************!*\
  !*** ./src/app/pages/recordings/create-record/create-record.module.ts ***!
  \************************************************************************/
/*! exports provided: CreateRecordPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CreateRecordPageModule", function() { return CreateRecordPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _shared_lib_shared_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @shared-lib/shared.module */ "./src/app/shared/shared.module.ts");
/* harmony import */ var ngx_autosize__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-autosize */ "./node_modules/ngx-autosize/__ivy_ngcc__/fesm2015/ngx-autosize.js");
/* harmony import */ var _create_record_routing_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./create-record-routing.module */ "./src/app/pages/recordings/create-record/create-record-routing.module.ts");
/* harmony import */ var _create_record_page__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./create-record.page */ "./src/app/pages/recordings/create-record/create-record.page.ts");









let CreateRecordPageModule = class CreateRecordPageModule {
};
CreateRecordPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _create_record_routing_module__WEBPACK_IMPORTED_MODULE_7__["CreateRecordPageRoutingModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
            _shared_lib_shared_module__WEBPACK_IMPORTED_MODULE_5__["SharedModule"],
            ngx_autosize__WEBPACK_IMPORTED_MODULE_6__["AutosizeModule"],
        ],
        declarations: [_create_record_page__WEBPACK_IMPORTED_MODULE_8__["CreateRecordPage"]],
    })
], CreateRecordPageModule);



/***/ }),

/***/ "./src/app/pages/recordings/create-record/create-record.page.scss":
/*!************************************************************************!*\
  !*** ./src/app/pages/recordings/create-record/create-record.page.scss ***!
  \************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (":host {\n  --page-margin: var(--app-fair-margin);\n  --page-background: var(--app-background-shade);\n  --app-header-height: 30px;\n}\n\nion-content {\n  position: absolute;\n  top: 0;\n  border-top: calc(var(--app-header-height) + var(--ion-safe-area-top));\n  border-top-style: solid;\n  border-top-color: var(--ion-color-skip-btn);\n}\n\n.centered {\n  margin: 0 auto;\n}\n\n.forms-validations-content {\n  --background: var(--page-background);\n}\n\n.forms-validations-content .validations-form {\n  margin-bottom: calc(var(--page-margin) * 2);\n}\n\n.forms-validations-content .validations-form .inputs-list {\n  --ion-item-background: var(--page-background);\n  padding: var(--page-margin) var(--page-margin) calc(var(--page-margin) * 2);\n}\n\n.forms-validations-content .validations-form .inputs-list ion-list-header {\n  -webkit-padding-start: 0px;\n          padding-inline-start: 0px;\n}\n\n.forms-validations-content .validations-form .inputs-list ion-list-header .header-title {\n  text-transform: uppercase;\n  font-size: 16px;\n  color: var(--ion-color-primary);\n  margin-top: initial;\n}\n\n.forms-validations-content .validations-form .inputs-list .input-item {\n  --padding-start: 0px;\n  --padding-end: 0px;\n  --inner-padding-end: 0px;\n}\n\n.forms-validations-content .validations-form .inputs-list .input-item ion-input,\n.forms-validations-content .validations-form .inputs-list .input-item ion-textarea {\n  --padding-start: 0px;\n  --padding-end: 0px;\n  margin: 8px 0 0;\n  padding: 6px 12px !important;\n  border-radius: 8px;\n  border: solid 1px var(--ion-border-gray-color);\n  background-color: #ffffff;\n}\n\n.forms-validations-content .validations-form .inputs-list .input-item .message-input {\n  width: 100%;\n  border: 0;\n  resize: none;\n  padding: 12px !important;\n}\n\n.forms-validations-content .validations-form .inputs-list .input-item ion-select {\n  padding: 14px;\n  border-radius: 8px;\n  border: solid 1px var(--ion-border-gray-color);\n  width: 100%;\n}\n\n.forms-validations-content .validations-form .inputs-list .terms-item {\n  --border-width: 0px;\n  --inner-padding-end: 0px;\n}\n\n.forms-validations-content .validations-form .inputs-list .error-container .error-message {\n  margin: calc(var(--page-margin) / 2) 0px;\n  display: flex;\n  align-items: center;\n  color: var(--ion-color-danger);\n  font-size: 14px;\n}\n\n.forms-validations-content .validations-form .inputs-list .error-container .error-message ion-icon {\n  -webkit-padding-end: calc(var(--page-margin) / 2);\n          padding-inline-end: calc(var(--page-margin) / 2);\n  flex-shrink: 0;\n}\n\n.forms-validations-content .validations-form .inputs-list .counter-item app-counter-input {\n  --counter-background: transparent;\n  --counter-color: var(--ion-color-primary);\n  --counter-border-color: var(--ion-color-primary);\n}\n\n.forms-validations-content .validations-form .inputs-list .counter-item .counter-value {\n  text-align: center;\n}\n\n.forms-validations-content .validations-form .submit-btn {\n  margin: var(--page-margin);\n}\n\n::ng-deep .select-alert {\n  --select-alert-color: #000;\n  --select-alert-background: #FFF;\n  --select-alert-margin: 16px;\n  --select-alert-color: var(--ion-color-lightest);\n  --select-alert-background: var(--ion-color-primary);\n  --select-alert-margin: 16px;\n}\n\n::ng-deep .select-alert .alert-head {\n  padding-top: calc((var(--select-alert-margin) / 4) * 3);\n  padding-bottom: calc((var(--select-alert-margin) / 4) * 3);\n  -webkit-padding-start: var(--select-alert-margin);\n          padding-inline-start: var(--select-alert-margin);\n  -webkit-padding-end: var(--select-alert-margin);\n          padding-inline-end: var(--select-alert-margin);\n}\n\n::ng-deep .select-alert .alert-title {\n  color: var(--select-alert-color);\n}\n\n::ng-deep .select-alert .alert-head,\n::ng-deep .select-alert .alert-message {\n  background-color: var(--select-alert-background);\n}\n\n::ng-deep .select-alert .alert-wrapper.sc-ion-alert-ios .alert-title {\n  margin: 0px;\n}\n\n::ng-deep .select-alert .alert-wrapper.sc-ion-alert-md .alert-title {\n  font-size: 18px;\n  font-weight: 400;\n}\n\n::ng-deep .select-alert .alert-wrapper.sc-ion-alert-md .alert-button {\n  --padding-top: 0;\n  --padding-start: 0.9em;\n  --padding-end: 0.9em;\n  --padding-bottom: 0;\n  height: 2.1em;\n  font-size: 13px;\n}\n\n::ng-deep .select-alert .alert-message {\n  display: none;\n}\n\n.header-toolbar {\n  padding: 16px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvcmVjb3JkaW5ncy9jcmVhdGUtcmVjb3JkL2NyZWF0ZS1yZWNvcmQucGFnZS5zY3NzIiwic3JjL3RoZW1lL21peGlucy9pbnB1dHMvc2VsZWN0LWFsZXJ0LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBSUE7RUFDRSxxQ0FBQTtFQUNBLDhDQUFBO0VBQ0EseUJBQUE7QUFIRjs7QUFZQTtFQUNFLGtCQUFBO0VBQ0EsTUFBQTtFQUNBLHFFQUFBO0VBQ0EsdUJBQUE7RUFDQSwyQ0FBQTtBQVRGOztBQVlBO0VBQ0UsY0FBQTtBQVRGOztBQXNCQTtFQUNFLG9DQUFBO0FBbkJGOztBQXFCRTtFQUNFLDJDQUFBO0FBbkJKOztBQXFCSTtFQUNFLDZDQUFBO0VBRUEsMkVBQUE7QUFwQk47O0FBdUJNO0VBQ0UsMEJBQUE7VUFBQSx5QkFBQTtBQXJCUjs7QUF1QlE7RUFDRSx5QkFBQTtFQUNBLGVBQUE7RUFDQSwrQkFBQTtFQUNBLG1CQUFBO0FBckJWOztBQXlCTTtFQUNFLG9CQUFBO0VBQ0Esa0JBQUE7RUFDQSx3QkFBQTtBQXZCUjs7QUF3QlE7O0VBRUUsb0JBQUE7RUFDQSxrQkFBQTtFQUNBLGVBQUE7RUFDQSw0QkFBQTtFQUNBLGtCQUFBO0VBQ0EsOENBQUE7RUFDQSx5QkFBQTtBQXRCVjs7QUF5QlE7RUFDRSxXQUFBO0VBQ0EsU0FBQTtFQUNBLFlBQUE7RUFFQSx3QkFBQTtBQXhCVjs7QUEyQlE7RUFDRSxhQUFBO0VBQ0Esa0JBQUE7RUFDQSw4Q0FBQTtFQUNBLFdBQUE7QUF6QlY7O0FBNkJNO0VBQ0UsbUJBQUE7RUFDQSx3QkFBQTtBQTNCUjs7QUErQlE7RUFDRSx3Q0FBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtFQUNBLDhCQUFBO0VBQ0EsZUFBQTtBQTdCVjs7QUErQlU7RUFDRSxpREFBQTtVQUFBLGdEQUFBO0VBQ0EsY0FBQTtBQTdCWjs7QUFtQ1E7RUFDRSxpQ0FBQTtFQUNBLHlDQUFBO0VBQ0EsZ0RBQUE7QUFqQ1Y7O0FBb0NRO0VBQ0Usa0JBQUE7QUFsQ1Y7O0FBdUNJO0VBQ0UsMEJBQUE7QUFyQ047O0FBNENBO0VDbklFLDBCQUFBO0VBQ0EsK0JBQUE7RUFDQSwyQkFBQTtFRHFJQSwrQ0FBQTtFQUNBLG1EQUFBO0VBQ0EsMkJBQUE7QUF6Q0Y7O0FDNUZFO0VBQ0UsdURBQUE7RUFDQSwwREFBQTtFQUNBLGlEQUFBO1VBQUEsZ0RBQUE7RUFDQSwrQ0FBQTtVQUFBLDhDQUFBO0FEOEZKOztBQzNGRTtFQUNFLGdDQUFBO0FENkZKOztBQzFGRTs7RUFFRSxnREFBQTtBRDRGSjs7QUN2Rkk7RUFDRSxXQUFBO0FEeUZOOztBQ25GSTtFQUNFLGVBQUE7RUFDQSxnQkFBQTtBRHFGTjs7QUNsRkk7RUFFRSxnQkFBQTtFQUNBLHNCQUFBO0VBQ0Esb0JBQUE7RUFDQSxtQkFBQTtFQUVBLGFBQUE7RUFDQSxlQUFBO0FEa0ZOOztBQWVFO0VBQ0UsYUFBQTtBQWJKOztBQWlCQTtFQUNFLGFBQUE7QUFkRiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3JlY29yZGluZ3MvY3JlYXRlLXJlY29yZC9jcmVhdGUtcmVjb3JkLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIkBpbXBvcnQgXCIuLi8uLi8uLi8uLi90aGVtZS9taXhpbnMvaW5wdXRzL3NlbGVjdC1hbGVydFwiO1xyXG5cclxuLy8gQ3VzdG9tIHZhcmlhYmxlc1xyXG4vLyBOb3RlOiAgVGhlc2Ugb25lcyB3ZXJlIGFkZGVkIGJ5IHVzIGFuZCBoYXZlIG5vdGhpbmcgdG8gZG8gd2l0aCBJb25pYyBDU1MgQ3VzdG9tIFByb3BlcnRpZXNcclxuOmhvc3Qge1xyXG4gIC0tcGFnZS1tYXJnaW46IHZhcigtLWFwcC1mYWlyLW1hcmdpbik7XHJcbiAgLS1wYWdlLWJhY2tncm91bmQ6IHZhcigtLWFwcC1iYWNrZ3JvdW5kLXNoYWRlKTtcclxuICAtLWFwcC1oZWFkZXItaGVpZ2h0OiAzMHB4O1xyXG59XHJcblxyXG4vLyBVc2UgYSBjb2xvcmVkIGJvcmRlci10b3AgdG8gZml4IHdlaXJkIHRyYW5zaXRpb25zIGJldHdlZW4gdG9vbGJhcnMgdGhhdCBoYXZlIGRpZmZlcmVudCBiYWNrZ3JvdW5kIGNvbG9yc1xyXG4vLyBpb24taGVhZGVyIHtcclxuLy8gICBpb24tdG9vbGJhciB7XHJcbi8vICAgICAtLWJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xyXG4vLyAgIH1cclxuLy8gfVxyXG5pb24tY29udGVudCB7XHJcbiAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gIHRvcDogMDtcclxuICBib3JkZXItdG9wOiBjYWxjKHZhcigtLWFwcC1oZWFkZXItaGVpZ2h0KSArIHZhcigtLWlvbi1zYWZlLWFyZWEtdG9wKSk7XHJcbiAgYm9yZGVyLXRvcC1zdHlsZTogc29saWQ7XHJcbiAgYm9yZGVyLXRvcC1jb2xvcjogdmFyKC0taW9uLWNvbG9yLXNraXAtYnRuKTtcclxufVxyXG5cclxuLmNlbnRlcmVkIHtcclxuICBtYXJnaW46IDAgYXV0bztcclxufVxyXG5cclxuLy8gLnJhZGlvLWljb24ge1xyXG4vLyAgIHdpZHRoOiA1MHB4O1xyXG4vLyAgIGhlaWdodDogNTBweDtcclxuLy8gfVxyXG4vLyAubGFyZ2UtYnV0dG9uIHtcclxuLy8gICB3aWR0aDogMTAwJTtcclxuLy8gICBoZWlnaHQ6IDcwcHg7XHJcbi8vIH1cclxuXHJcbi8vIE5vdGU6ICBBbGwgdGhlIENTUyB2YXJpYWJsZXMgZGVmaW5lZCBiZWxvdyBhcmUgb3ZlcnJpZGVzIG9mIElvbmljIGVsZW1lbnRzIENTUyBDdXN0b20gUHJvcGVydGllc1xyXG4uZm9ybXMtdmFsaWRhdGlvbnMtY29udGVudCB7XHJcbiAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1wYWdlLWJhY2tncm91bmQpO1xyXG5cclxuICAudmFsaWRhdGlvbnMtZm9ybSB7XHJcbiAgICBtYXJnaW4tYm90dG9tOiBjYWxjKHZhcigtLXBhZ2UtbWFyZ2luKSAqIDIpO1xyXG5cclxuICAgIC5pbnB1dHMtbGlzdCB7XHJcbiAgICAgIC0taW9uLWl0ZW0tYmFja2dyb3VuZDogdmFyKC0tcGFnZS1iYWNrZ3JvdW5kKTtcclxuXHJcbiAgICAgIHBhZGRpbmc6IHZhcigtLXBhZ2UtbWFyZ2luKSB2YXIoLS1wYWdlLW1hcmdpbilcclxuICAgICAgICBjYWxjKHZhcigtLXBhZ2UtbWFyZ2luKSAqIDIpO1xyXG5cclxuICAgICAgaW9uLWxpc3QtaGVhZGVyIHtcclxuICAgICAgICBwYWRkaW5nLWlubGluZS1zdGFydDogMHB4O1xyXG5cclxuICAgICAgICAuaGVhZGVyLXRpdGxlIHtcclxuICAgICAgICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XHJcbiAgICAgICAgICBmb250LXNpemU6IDE2cHg7XHJcbiAgICAgICAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpO1xyXG4gICAgICAgICAgbWFyZ2luLXRvcDogaW5pdGlhbDtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIC5pbnB1dC1pdGVtIHtcclxuICAgICAgICAtLXBhZGRpbmctc3RhcnQ6IDBweDtcclxuICAgICAgICAtLXBhZGRpbmctZW5kOiAwcHg7XHJcbiAgICAgICAgLS1pbm5lci1wYWRkaW5nLWVuZDogMHB4O1xyXG4gICAgICAgIGlvbi1pbnB1dCxcclxuICAgICAgICBpb24tdGV4dGFyZWEge1xyXG4gICAgICAgICAgLS1wYWRkaW5nLXN0YXJ0OiAwcHg7XHJcbiAgICAgICAgICAtLXBhZGRpbmctZW5kOiAwcHg7XHJcbiAgICAgICAgICBtYXJnaW46IDhweCAwIDA7XHJcbiAgICAgICAgICBwYWRkaW5nOiA2cHggMTJweCAhaW1wb3J0YW50O1xyXG4gICAgICAgICAgYm9yZGVyLXJhZGl1czogOHB4O1xyXG4gICAgICAgICAgYm9yZGVyOiBzb2xpZCAxcHggdmFyKC0taW9uLWJvcmRlci1ncmF5LWNvbG9yKTtcclxuICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmZmZmY7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAubWVzc2FnZS1pbnB1dCB7XHJcbiAgICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICAgIGJvcmRlcjogMDtcclxuICAgICAgICAgIHJlc2l6ZTogbm9uZTtcclxuICAgICAgICAgIC8vIHBhZGRpbmc6IDZweCAwcHggMHB4IDBweDtcclxuICAgICAgICAgIHBhZGRpbmc6IDEycHggIWltcG9ydGFudDtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlvbi1zZWxlY3Qge1xyXG4gICAgICAgICAgcGFkZGluZzogMTRweDtcclxuICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDhweDtcclxuICAgICAgICAgIGJvcmRlcjogc29saWQgMXB4IHZhcigtLWlvbi1ib3JkZXItZ3JheS1jb2xvcik7XHJcbiAgICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIC50ZXJtcy1pdGVtIHtcclxuICAgICAgICAtLWJvcmRlci13aWR0aDogMHB4O1xyXG4gICAgICAgIC0taW5uZXItcGFkZGluZy1lbmQ6IDBweDtcclxuICAgICAgfVxyXG5cclxuICAgICAgLmVycm9yLWNvbnRhaW5lciB7XHJcbiAgICAgICAgLmVycm9yLW1lc3NhZ2Uge1xyXG4gICAgICAgICAgbWFyZ2luOiBjYWxjKHZhcigtLXBhZ2UtbWFyZ2luKSAvIDIpIDBweDtcclxuICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1kYW5nZXIpO1xyXG4gICAgICAgICAgZm9udC1zaXplOiAxNHB4O1xyXG5cclxuICAgICAgICAgIGlvbi1pY29uIHtcclxuICAgICAgICAgICAgcGFkZGluZy1pbmxpbmUtZW5kOiBjYWxjKHZhcigtLXBhZ2UtbWFyZ2luKSAvIDIpO1xyXG4gICAgICAgICAgICBmbGV4LXNocmluazogMDtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIC5jb3VudGVyLWl0ZW0ge1xyXG4gICAgICAgIGFwcC1jb3VudGVyLWlucHV0IHtcclxuICAgICAgICAgIC0tY291bnRlci1iYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcclxuICAgICAgICAgIC0tY291bnRlci1jb2xvcjogdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpO1xyXG4gICAgICAgICAgLS1jb3VudGVyLWJvcmRlci1jb2xvcjogdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLmNvdW50ZXItdmFsdWUge1xyXG4gICAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC5zdWJtaXQtYnRuIHtcclxuICAgICAgbWFyZ2luOiB2YXIoLS1wYWdlLW1hcmdpbik7XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcblxyXG4vLyBBbGVydHMgYW5kIGluIGdlbmVyYWwgYWxsIG92ZXJsYXlzIGFyZSBhdHRhY2hlZCB0byB0aGUgYm9keSBvciBpb24tYXBwIGRpcmVjdGx5XHJcbi8vIFdlIG5lZWQgdG8gdXNlIDo6bmctZGVlcCB0byBhY2Nlc3MgaXQgZnJvbSBoZXJlXHJcbjo6bmctZGVlcCAuc2VsZWN0LWFsZXJ0IHtcclxuICBAaW5jbHVkZSBzZWxlY3QtYWxlcnQoKTtcclxuXHJcbiAgLy8gVmFyaWFibGVzIHNob3VsZCBiZSBpbiBhIGRlZXBlciBzZWxlY3RvciBvciBhZnRlciB0aGUgbWl4aW4gaW5jbHVkZSB0byBvdmVycmlkZSBkZWZhdWx0IHZhbHVlc1xyXG4gIC0tc2VsZWN0LWFsZXJ0LWNvbG9yOiB2YXIoLS1pb24tY29sb3ItbGlnaHRlc3QpO1xyXG4gIC0tc2VsZWN0LWFsZXJ0LWJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KTtcclxuICAtLXNlbGVjdC1hbGVydC1tYXJnaW46IDE2cHg7XHJcblxyXG4gIC5hbGVydC1tZXNzYWdlIHtcclxuICAgIGRpc3BsYXk6IG5vbmU7XHJcbiAgfVxyXG59XHJcblxyXG4uaGVhZGVyLXRvb2xiYXIge1xyXG4gIHBhZGRpbmc6IDE2cHg7XHJcbn1cclxuIiwiQG1peGluIHNlbGVjdC1hbGVydCgpIHtcclxuICAvLyBEZWZhdWx0IHZhbHVlc1xyXG4gIC0tc2VsZWN0LWFsZXJ0LWNvbG9yOiAjMDAwO1xyXG4gIC0tc2VsZWN0LWFsZXJ0LWJhY2tncm91bmQ6ICNGRkY7XHJcbiAgLS1zZWxlY3QtYWxlcnQtbWFyZ2luOiAxNnB4O1xyXG5cclxuICAuYWxlcnQtaGVhZCB7XHJcbiAgICBwYWRkaW5nLXRvcDogY2FsYygodmFyKC0tc2VsZWN0LWFsZXJ0LW1hcmdpbikgLyA0KSAqIDMpO1xyXG4gICAgcGFkZGluZy1ib3R0b206IGNhbGMoKHZhcigtLXNlbGVjdC1hbGVydC1tYXJnaW4pIC8gNCkgKiAzKTtcclxuICAgIHBhZGRpbmctaW5saW5lLXN0YXJ0OiB2YXIoLS1zZWxlY3QtYWxlcnQtbWFyZ2luKTtcclxuICAgIHBhZGRpbmctaW5saW5lLWVuZDogdmFyKC0tc2VsZWN0LWFsZXJ0LW1hcmdpbik7XHJcbiAgfVxyXG5cclxuICAuYWxlcnQtdGl0bGUge1xyXG4gICAgY29sb3I6IHZhcigtLXNlbGVjdC1hbGVydC1jb2xvcik7XHJcbiAgfVxyXG5cclxuICAuYWxlcnQtaGVhZCxcclxuICAuYWxlcnQtbWVzc2FnZSB7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB2YXIoLS1zZWxlY3QtYWxlcnQtYmFja2dyb3VuZCk7XHJcbiAgfVxyXG5cclxuICAvLyBpT1Mgc3R5bGVzXHJcbiAgLmFsZXJ0LXdyYXBwZXIuc2MtaW9uLWFsZXJ0LWlvcyB7XHJcbiAgICAuYWxlcnQtdGl0bGUge1xyXG4gICAgICBtYXJnaW46IDBweDtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIC8vIE1hdGVyaWFsIHN0eWxlc1xyXG4gIC5hbGVydC13cmFwcGVyLnNjLWlvbi1hbGVydC1tZCB7XHJcbiAgICAuYWxlcnQtdGl0bGUge1xyXG4gICAgICBmb250LXNpemU6IDE4cHg7XHJcbiAgICAgIGZvbnQtd2VpZ2h0OiA0MDA7XHJcbiAgICB9XHJcblxyXG4gICAgLmFsZXJ0LWJ1dHRvbiB7XHJcbiAgICAgIC8vIFZhbHVlcyB0YWtlbiBmcm9tIElvbmljIHNtYWxsIGJ1dHRvbiBwcmVzZXRcclxuICAgICAgLS1wYWRkaW5nLXRvcDogMDtcclxuICAgICAgLS1wYWRkaW5nLXN0YXJ0OiAwLjllbTtcclxuICAgICAgLS1wYWRkaW5nLWVuZDogMC45ZW07XHJcbiAgICAgIC0tcGFkZGluZy1ib3R0b206IDA7XHJcblxyXG4gICAgICBoZWlnaHQ6IDIuMWVtO1xyXG4gICAgICBmb250LXNpemU6IDEzcHg7XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcbiJdfQ== */");

/***/ }),

/***/ "./src/app/pages/recordings/create-record/create-record.page.ts":
/*!**********************************************************************!*\
  !*** ./src/app/pages/recordings/create-record/create-record.page.ts ***!
  \**********************************************************************/
/*! exports provided: CreateRecordPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CreateRecordPage", function() { return CreateRecordPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _capacitor_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @capacitor/core */ "./node_modules/@capacitor/core/dist/esm/index.js");
/* harmony import */ var _core_helpers__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @core/helpers */ "./src/app/core/helpers/index.ts");
/* harmony import */ var _core_services__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @core/services */ "./src/app/core/services/index.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _shared_lib_services__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @shared-lib/services */ "./src/app/shared/services/index.ts");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_9__);










const { isNative } = _capacitor_core__WEBPACK_IMPORTED_MODULE_4__["Capacitor"];
let CreateRecordPage = class CreateRecordPage {
    constructor(recordingsService, fb, messagesService, toast, mailService, router, platform, languageService) {
        this.recordingsService = recordingsService;
        this.fb = fb;
        this.messagesService = messagesService;
        this.toast = toast;
        this.mailService = mailService;
        this.router = router;
        this.platform = platform;
        this.languageService = languageService;
        this.validations = {
            name: [{ type: "required", message: "Text is required." }],
        };
        this.showFab = false;
        this.createRecordForm = this.fb.group({
            name: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required])],
        });
    }
    ngOnInit() {
        this.board = this.messagesService.getBoard;
        if (Object(lodash__WEBPACK_IMPORTED_MODULE_9__["isEmpty"])(this.board)) {
            this.router.navigate(["home"], { replaceUrl: true });
        }
    }
    ionViewWillLeave() {
        this.recordingsService.recordings = null;
    }
    openSpeechRecognition() {
        if (isNative) {
            if (this.platform.is("ios")) {
                this.showFab = true;
            }
            const { languageTag } = this.languageService.getSelectedLanguage || {};
            const options = {
                language: languageTag ? languageTag : "en-US",
            };
            this.recordingsService.speechRecognition
                .startListening(options)
                .subscribe((matches) => this.handleSpeechToText(matches), (onerror) => {
                console.log("one error", onerror);
            });
        }
        else {
            this.toast.showBottomShort("Please run on actual device", 3000);
        }
    }
    handleSpeechToText(matches) {
        this.matchesArr = matches;
    }
    save() {
        if (this.createRecordForm.valid) {
            const { value } = this.createRecordForm || {};
            const { email } = this.board || {};
            const record = !Object(lodash__WEBPACK_IMPORTED_MODULE_9__["isEmpty"])(this.matchesArr)
                ? value.name + ", " + this.matchesArr.join(", ")
                : value.name;
            const message = {
                title: Object(_core_helpers__WEBPACK_IMPORTED_MODULE_5__["convertDateToFirebaseTimestamp"])(),
                creationDate: Object(_core_helpers__WEBPACK_IMPORTED_MODULE_5__["convertDateToFirebaseTimestamp"])(),
                text: record,
            };
            if (email) {
                const mail = {
                    to: email,
                    message: {
                        subject: "New card",
                        html: `${record}`,
                    },
                };
                Object(_core_helpers__WEBPACK_IMPORTED_MODULE_5__["tryCatchHelper"])(this.mailService.addMail(mail));
            }
            Object(_core_helpers__WEBPACK_IMPORTED_MODULE_5__["tryCatchHelper"])(this.messagesService.addMessage(message));
            this.toast.showBottomShort("Stored Message.", 500);
            setTimeout(() => {
                this.router.navigate(["home"], { replaceUrl: true });
            }, 1000);
        }
        else {
            this.toast.showBottomShort("Text is required.", 500);
        }
    }
};
CreateRecordPage.ctorParameters = () => [
    { type: _core_services__WEBPACK_IMPORTED_MODULE_6__["RecordingsService"] },
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
    { type: _core_services__WEBPACK_IMPORTED_MODULE_6__["MessagesService"] },
    { type: _shared_lib_services__WEBPACK_IMPORTED_MODULE_8__["ToastService"] },
    { type: _core_services__WEBPACK_IMPORTED_MODULE_6__["MailService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["Platform"] },
    { type: _core_services__WEBPACK_IMPORTED_MODULE_6__["LanguageService"] }
];
CreateRecordPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "app-create-record",
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./create-record.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/recordings/create-record/create-record.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./create-record.page.scss */ "./src/app/pages/recordings/create-record/create-record.page.scss")).default]
    }),
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_core_services__WEBPACK_IMPORTED_MODULE_6__["RecordingsService"],
        _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
        _core_services__WEBPACK_IMPORTED_MODULE_6__["MessagesService"],
        _shared_lib_services__WEBPACK_IMPORTED_MODULE_8__["ToastService"],
        _core_services__WEBPACK_IMPORTED_MODULE_6__["MailService"],
        _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["Platform"],
        _core_services__WEBPACK_IMPORTED_MODULE_6__["LanguageService"]])
], CreateRecordPage);



/***/ })

}]);
//# sourceMappingURL=create-record-create-record-module-es2015.js.map