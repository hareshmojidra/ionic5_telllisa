(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-walkthrough-walkthrough-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/walkthrough/walkthrough.page.html":
    /*!***********************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/walkthrough/walkthrough.page.html ***!
      \***********************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppPagesWalkthroughWalkthroughPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header class=\"ion-no-border\">\r\n  <ion-toolbar>\r\n    <ion-buttons slot=\"end\">\r\n      <!-- <ion-button class=\"skip-walkthrough-button\" fill=\"\" color=\"skip-btn\" (click)=\"skipWalkthrough()\">skip</ion-button> -->\r\n    </ion-buttons>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content scrollY=\"false\">\r\n  <ion-slides class=\"walkthrough-slides\" pager=\"false\" [options]=\"slidesOptions\" (ionSlideDidChange)=\"slideChanged()\">\r\n    <ion-slide class=\"first-slide illustration-and-decoration-slide\">\r\n      <ion-row class=\"slide-inner-row\">\r\n        <ion-col class=\"illustration-col\">\r\n          <app-aspect-ratio [ratio]=\"{w:915, h:849}\">\r\n            <app-image-shell class=\"illustration-image\" animation=\"spinner\"\r\n              [src]=\"'./assets/sample-images/walkthrough/walkthrough-illustration-1.svg'\" [alt]=\"'walkthrough'\">\r\n            </app-image-shell>\r\n          </app-aspect-ratio>\r\n        </ion-col>\r\n        <ion-col class=\"decoration-col\">\r\n          <!-- http://jxnblk.com/paths/?d=M0%200%20V5%20H5%20C25%205%2025%2020%2045%2020%20S65%205%2085%205%20H90%20V0%20Z -->\r\n          <svg class=\"vector-decoration\" version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\"\r\n            xmlns:xlink=\"http://www.w3.org/1999/xlink\" x=\"0px\" y=\"0px\" height=\"100px\" width=\"100%\" viewBox=\"0 0 90 20\"\r\n            style=\"enable-background: new 0 0 90 20\" xml:space=\"preserve\" preserveAspectRatio=\"none\">\r\n            <path d=\"M0 0 V5 H5 C25 5 25 20 45 20 S65 5 85 5 H90 V0 Z\" />\r\n          </svg>\r\n        </ion-col>\r\n        <ion-col class=\"info-col\">\r\n          <div class=\"info-wrapper\">\r\n            <h3 class=\"info-title\">\r\n              Hi! I'm Lisa and I'm happy to be your digital voice assistance for\r\n              Trello.\r\n            </h3>\r\n            <p class=\"info-paragraph\">\r\n              For now I can create cards for you while you're on the road or too\r\n              busy to work in your Trello board.\r\n            </p>\r\n            <p class=\"info-paragraph\">\r\n              To understand you correctly, please select your language on the\r\n              next page\r\n            </p>\r\n          </div>\r\n        </ion-col>\r\n      </ion-row>\r\n    </ion-slide>\r\n    <ion-slide class=\"second-slide illustration-and-decoration-slide\">\r\n    </ion-slide>\r\n\r\n    <ng-container *ngIf=\"boards$ | async; let boards;\">\r\n      <ion-slide class=\"last-slide illustration-and-decoration-slide\">\r\n        <ion-row class=\"slide-inner-row\">\r\n          <ion-col class=\"illustration-col\">\r\n            <app-aspect-ratio [ratio]=\"{w:924, h:819}\">\r\n              <app-image-shell class=\"illustration-image\" animation=\"spinner\"\r\n                [src]=\"'./assets/tl-images/TL-Symbol-Simpel.png'\" [alt]=\"'lisa-web'\"></app-image-shell>\r\n            </app-aspect-ratio>\r\n          </ion-col>\r\n          <ion-col class=\"decoration-col\">\r\n            <!-- http://jxnblk.com/paths/?d=M0%200%20L64%200%20L64%2010%20Q56%2024%2046%2012%20Q34%200%2026%208%20Q10%2022%200%208%20Z -->\r\n            <svg class=\"vector-decoration\" version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\"\r\n              xmlns:xlink=\"http://www.w3.org/1999/xlink\" x=\"0px\" y=\"0px\" height=\"100px\" width=\"100%\" viewBox=\"0 0 64 18\"\r\n              style=\"enable-background: new 0 0 64 24\" xml:space=\"preserve\" preserveAspectRatio=\"none\">\r\n              <path d=\"M0 0 L64 0 L64 10 Q56 24 46 12 Q34 0 26 8 Q10 22 0 8 Z\" />\r\n            </svg>\r\n          </ion-col>\r\n          <ion-col class=\"info-col\">\r\n            <ion-row class=\"info-outer\">\r\n              <ion-col>\r\n                <ng-container *ngIf=\"!boards.length;\">\r\n                  <div class=\"info-wrapper\">\r\n                    <h3 class=\"info-title\">\r\n                      You need to add a Trello board so I know where to create new\r\n                      cards for you.\r\n                    </h3>\r\n                    <p class=\"info-paragraph\">\r\n                      Please use the web app to add boards to my files. You can open\r\n                      the webapp by tap the button below.\r\n                    </p>\r\n                  </div>\r\n                  <div class=\"call-to-actions-wrapper\">\r\n                    <ion-button class=\"get-started-button\" expand=\"block\" (click)=\"presentAlertConfirm();\">Open Web App\r\n                    </ion-button>\r\n                  </div>\r\n                </ng-container>\r\n                <ng-container *ngIf=\"boards.length > 0;\">\r\n                  <div class=\"info-wrapper\">\r\n                    <h3 class=\"info-title\">\r\n                      You already have {{ boards.length }} boards.\r\n                    </h3>\r\n                  </div>\r\n                  <div class=\"call-to-actions-wrapper\">\r\n                    <ion-button class=\"get-started-button\" expand=\"block\" (click)=\"getStarted();\">Let's get Started.\r\n                    </ion-button>\r\n                  </div>\r\n                </ng-container>\r\n              </ion-col>\r\n            </ion-row>\r\n          </ion-col>\r\n        </ion-row>\r\n      </ion-slide>\r\n    </ng-container>\r\n  </ion-slides>\r\n</ion-content>\r\n<ion-footer class=\"ion-no-border\" style=\"bottom: 0px; position: absolute;\">\r\n  <ion-toolbar *ngIf=\"!isLastSlide\">\r\n    <ion-buttons slot=\"start\">\r\n      <ion-button size=\"large\" (click)=\"prev()\" *ngIf=\"isNotStart && activeSlide !== 1\">\r\n        <ion-icon slot=\"icon-only\" name=\"arrow-back-circle-outline\"></ion-icon>\r\n      </ion-button>\r\n    </ion-buttons>\r\n\r\n    <ion-buttons slot=\"end\">\r\n      <ion-button size=\"large\" (click)=\"next()\" *ngIf=\"activeSlide !== 1\" style=\"width: 60px;\">\r\n        <ion-icon slot=\"icon-only\" name=\"arrow-forward-circle-outline\" style=\"font-size: 32px;\"></ion-icon>\r\n      </ion-button>\r\n    </ion-buttons>\r\n  </ion-toolbar>\r\n</ion-footer>";
      /***/
    },

    /***/
    "./src/app/pages/walkthrough/styles/walkthrough.page.scss":
    /*!****************************************************************!*\
      !*** ./src/app/pages/walkthrough/styles/walkthrough.page.scss ***!
      \****************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppPagesWalkthroughStylesWalkthroughPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ":host {\n  --page-margin: var(--app-broad-margin);\n  --page-background: var(--app-background);\n  --page-swiper-pagination-space: 40px;\n  --page-swiper-pagination-height: 18px;\n  --page-pagination-bullet-size: 10px;\n  --page-first-slide-background: #c1ebff;\n  --page-second-slide-background: #a9ebd2;\n  --page-third-slide-background: #f0cbe5;\n  --page-last-slide-background: #eef3ff;\n  --page-vector-decoration-fill: var(--ion-color-light-shade);\n}\n\nion-header ion-toolbar {\n  --ion-toolbar-background: transparent;\n}\n\nion-header ion-toolbar ion-button {\n  --color: var(--ion-color-lightest);\n}\n\nion-content {\n  position: absolute;\n  top: 0;\n}\n\n.walkthrough-slides {\n  --bullet-background: var(--ion-color-dark);\n  --bullet-background-active: var(--ion-color-dark);\n  height: 100%;\n}\n\n.walkthrough-slides .slide-inner-row {\n  height: 100%;\n  width: 100%;\n  padding: 0px;\n  background-clip: padding-box;\n  background-color: var(--page-vector-decoration-fill);\n}\n\n.illustration-and-decoration-slide .slide-inner-row {\n  --ion-grid-column-padding: 0px;\n  flex-flow: column;\n  justify-content: flex-start;\n  align-items: center;\n}\n\n.illustration-and-decoration-slide .illustration-col {\n  flex-grow: 0;\n  flex-shrink: 0;\n  min-height: auto;\n  min-height: -webkit-fit-content;\n  min-height: -moz-fit-content;\n  min-height: fit-content;\n  max-width: 30vh;\n  padding: 0px;\n}\n\n.illustration-and-decoration-slide .decoration-col {\n  flex-grow: 0;\n  flex-shrink: 1;\n  min-height: 12vh;\n  transform: translate3d(0px, 0px, 0px);\n}\n\n.illustration-and-decoration-slide .decoration-col .vector-decoration {\n  fill: currentColor;\n  color: var(--page-vector-decoration-fill);\n  background-color: var(--page-background);\n  padding-bottom: 4px;\n  transform: translate3d(0px, 0px, 0px);\n  shape-rendering: geometricprecision;\n  height: calc(100% + 1px);\n}\n\n.illustration-and-decoration-slide .info-col {\n  flex-grow: 1;\n  flex-shrink: 0;\n  min-height: auto;\n  background-color: var(--page-background);\n  margin-bottom: -1px;\n  transform: translate3d(0px, 0px, 0px);\n}\n\n.illustration-and-decoration-slide .info-col .info-wrapper {\n  margin: calc(var(--page-margin) * -1) var(--page-margin) 0px;\n  margin-top: calc(var(--page-margin) * 2);\n  text-align: left;\n}\n\n.illustration-and-decoration-slide .info-col .info-wrapper .info-title {\n  margin: 0px;\n  margin-bottom: var(--page-margin);\n  color: var(--ion-color-dark);\n  font-family: \"Inter\";\n  font-size: 22px;\n  font-weight: normal;\n  font-stretch: normal;\n  font-style: normal;\n  line-height: 1.27;\n  letter-spacing: normal;\n  text-align: left;\n}\n\n.illustration-and-decoration-slide .info-col .info-wrapper .info-paragraph {\n  color: var(--ion-color-medium-shade);\n  margin: 0px 0px calc(var(--page-margin) / 2);\n  font-family: \"Inter\";\n  font-size: 13px;\n  font-weight: normal;\n  font-stretch: normal;\n  font-style: normal;\n  line-height: 1.54;\n  letter-spacing: normal;\n  text-align: center;\n}\n\n.illustration-and-decoration-slide .info-col .info-wrapper .info-paragraph:last-child {\n  margin-bottom: 0px;\n}\n\n.first-slide {\n  --page-vector-decoration-fill: var(--page-first-slide-background);\n}\n\n.second-slide {\n  --page-vector-decoration-fill: var(--page-second-slide-background);\n}\n\n.third-slide {\n  --page-vector-decoration-fill: var(--page-last-slide-background);\n}\n\n.last-slide {\n  --page-vector-decoration-fill: var(--page-last-slide-background);\n}\n\n.last-slide .slide-inner-row {\n  border-width: 0px;\n}\n\n.last-slide .info-col {\n  padding: var(--page-margin);\n}\n\n.last-slide .info-col .info-outer {\n  height: 100%;\n  align-items: flex-end;\n  flex-direction: column;\n}\n\n.last-slide .info-col .info-outer .info-wrapper {\n  margin: calc(var(--page-margin) * -1) var(--page-margin) 0px;\n}\n\n.last-slide .info-col .info-outer .call-to-actions-wrapper {\n  max-height: -webkit-fit-content;\n  max-height: -moz-fit-content;\n  max-height: fit-content;\n  position: absolute;\n  bottom: 15%;\n  width: 100%;\n}\n\n.last-slide .info-col .get-started-button {\n  margin: 0px;\n}\n\n.last-slide .info-col .alt-call-to-action-row {\n  padding-top: 5px;\n  align-items: center;\n  justify-content: space-evenly;\n}\n\n.last-slide .info-col .alt-call-to-action-row .cta-leading-text {\n  color: var(--ion-color-medium);\n  font-size: 16px;\n}\n\n.last-slide .info-col .alt-call-to-action-row .login-button {\n  --color: var(--ion-color-secondary);\n  margin: 0px;\n}\n\n.last-slide .info-col .alt-call-to-action-row .login-button:focus {\n  outline: none;\n}\n\n:host(.first-slide-active) .skip-walkthrough-button {\n  visibility: hidden;\n}\n\n:host(.last-slide-active) ::ng-deep .walkthrough-slides .swiper-pagination {\n  display: none;\n}\n\n:host(.last-slide-active) .skip-walkthrough-button {\n  visibility: hidden;\n}\n\n:host ::ng-deep .walkthrough-slides .swiper-pagination {\n  height: var(--page-swiper-pagination-height);\n  line-height: 1;\n  bottom: calc( ( var(--page-swiper-pagination-space) - var(--page-swiper-pagination-height) ) / 2 );\n}\n\n:host ::ng-deep .walkthrough-slides .swiper-pagination .swiper-pagination-bullet {\n  width: var(--page-pagination-bullet-size);\n  height: var(--page-pagination-bullet-size);\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvd2Fsa3Rocm91Z2gvc3R5bGVzL3dhbGt0aHJvdWdoLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFFQTtFQUNFLHNDQUFBO0VBQ0Esd0NBQUE7RUFFQSxvQ0FBQTtFQUNBLHFDQUFBO0VBQ0EsbUNBQUE7RUFFQSxzQ0FBQTtFQUNBLHVDQUFBO0VBQ0Esc0NBQUE7RUFDQSxxQ0FBQTtFQUVBLDJEQUFBO0FBSkY7O0FBU0U7RUFDRSxxQ0FBQTtBQU5KOztBQVNJO0VBQ0Usa0NBQUE7QUFQTjs7QUFZQTtFQUVFLGtCQUFBO0VBQ0EsTUFBQTtBQVZGOztBQWFBO0VBQ0UsMENBQUE7RUFDQSxpREFBQTtFQUVBLFlBQUE7QUFYRjs7QUFhRTtFQUNFLFlBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUtBLDRCQUFBO0VBQ0Esb0RBQUE7QUFmSjs7QUFvQkU7RUFDRSw4QkFBQTtFQUVBLGlCQUFBO0VBQ0EsMkJBQUE7RUFDQSxtQkFBQTtBQWxCSjs7QUFxQkU7RUFDRSxZQUFBO0VBQ0EsY0FBQTtFQUNBLGdCQUFBO0VBQ0EsK0JBQUE7RUFBQSw0QkFBQTtFQUFBLHVCQUFBO0VBQ0EsZUFBQTtFQUNBLFlBQUE7QUFuQko7O0FBc0JFO0VBQ0UsWUFBQTtFQUNBLGNBQUE7RUFHQSxnQkFBQTtFQUNBLHFDQUFBO0FBdEJKOztBQXdCSTtFQUNFLGtCQUFBO0VBQ0EseUNBQUE7RUFDQSx3Q0FBQTtFQUNBLG1CQUFBO0VBR0EscUNBQUE7RUFDQSxtQ0FBQTtFQUNBLHdCQUFBO0FBeEJOOztBQTRCRTtFQUNFLFlBQUE7RUFDQSxjQUFBO0VBQ0EsZ0JBQUE7RUFDQSx3Q0FBQTtFQUdBLG1CQUFBO0VBRUEscUNBQUE7QUE3Qko7O0FBK0JJO0VBQ0UsNERBQUE7RUFDQSx3Q0FBQTtFQUVBLGdCQUFBO0FBOUJOOztBQWdDTTtFQUNFLFdBQUE7RUFDQSxpQ0FBQTtFQUNBLDRCQUFBO0VBQ0Esb0JBQUE7RUFDQSxlQUFBO0VBQ0EsbUJBQUE7RUFDQSxvQkFBQTtFQUNBLGtCQUFBO0VBQ0EsaUJBQUE7RUFDQSxzQkFBQTtFQUNBLGdCQUFBO0FBOUJSOztBQWlDTTtFQUNFLG9DQUFBO0VBQ0EsNENBQUE7RUFDQSxvQkFBQTtFQUNBLGVBQUE7RUFDQSxtQkFBQTtFQUNBLG9CQUFBO0VBQ0Esa0JBQUE7RUFDQSxpQkFBQTtFQUNBLHNCQUFBO0VBQ0Esa0JBQUE7QUEvQlI7O0FBaUNRO0VBQ0Usa0JBQUE7QUEvQlY7O0FBc0NBO0VBQ0UsaUVBQUE7QUFuQ0Y7O0FBc0NBO0VBQ0Usa0VBQUE7QUFuQ0Y7O0FBc0NBO0VBRUUsZ0VBQUE7QUFwQ0Y7O0FBdUNBO0VBQ0UsZ0VBQUE7QUFwQ0Y7O0FBc0NFO0VBRUUsaUJBQUE7QUFyQ0o7O0FBd0NFO0VBQ0UsMkJBQUE7QUF0Q0o7O0FBd0NJO0VBQ0UsWUFBQTtFQUNBLHFCQUFBO0VBQ0Esc0JBQUE7QUF0Q047O0FBd0NNO0VBQ0UsNERBQUE7QUF0Q1I7O0FBeUNNO0VBQ0UsK0JBQUE7RUFBQSw0QkFBQTtFQUFBLHVCQUFBO0VBQ0Esa0JBQUE7RUFDQSxXQUFBO0VBQ0EsV0FBQTtBQXZDUjs7QUEyQ0k7RUFDRSxXQUFBO0FBekNOOztBQTRDSTtFQUNFLGdCQUFBO0VBQ0EsbUJBQUE7RUFDQSw2QkFBQTtBQTFDTjs7QUE0Q007RUFDRSw4QkFBQTtFQUNBLGVBQUE7QUExQ1I7O0FBNkNNO0VBQ0UsbUNBQUE7RUFDQSxXQUFBO0FBM0NSOztBQTZDUTtFQUNFLGFBQUE7QUEzQ1Y7O0FBbURFO0VBQ0Usa0JBQUE7QUFoREo7O0FBc0RJO0VBQ0UsYUFBQTtBQW5ETjs7QUF1REU7RUFDRSxrQkFBQTtBQXJESjs7QUE2REU7RUFDRSw0Q0FBQTtFQUNBLGNBQUE7RUFFQSxrR0FBQTtBQTNESjs7QUFrRUk7RUFDRSx5Q0FBQTtFQUNBLDBDQUFBO0FBaEVOIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvd2Fsa3Rocm91Z2gvc3R5bGVzL3dhbGt0aHJvdWdoLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi8vIEN1c3RvbSB2YXJpYWJsZXNcclxuLy8gTm90ZTogIFRoZXNlIG9uZXMgd2VyZSBhZGRlZCBieSB1cyBhbmQgaGF2ZSBub3RoaW5nIHRvIGRvIHdpdGggSW9uaWMgQ1NTIEN1c3RvbSBQcm9wZXJ0aWVzXHJcbjpob3N0IHtcclxuICAtLXBhZ2UtbWFyZ2luOiB2YXIoLS1hcHAtYnJvYWQtbWFyZ2luKTtcclxuICAtLXBhZ2UtYmFja2dyb3VuZDogdmFyKC0tYXBwLWJhY2tncm91bmQpO1xyXG5cclxuICAtLXBhZ2Utc3dpcGVyLXBhZ2luYXRpb24tc3BhY2U6IDQwcHg7XHJcbiAgLS1wYWdlLXN3aXBlci1wYWdpbmF0aW9uLWhlaWdodDogMThweDtcclxuICAtLXBhZ2UtcGFnaW5hdGlvbi1idWxsZXQtc2l6ZTogMTBweDtcclxuXHJcbiAgLS1wYWdlLWZpcnN0LXNsaWRlLWJhY2tncm91bmQ6ICNjMWViZmY7XHJcbiAgLS1wYWdlLXNlY29uZC1zbGlkZS1iYWNrZ3JvdW5kOiAjYTllYmQyO1xyXG4gIC0tcGFnZS10aGlyZC1zbGlkZS1iYWNrZ3JvdW5kOiAjZjBjYmU1O1xyXG4gIC0tcGFnZS1sYXN0LXNsaWRlLWJhY2tncm91bmQ6ICNlZWYzZmY7XHJcblxyXG4gIC0tcGFnZS12ZWN0b3ItZGVjb3JhdGlvbi1maWxsOiB2YXIoLS1pb24tY29sb3ItbGlnaHQtc2hhZGUpO1xyXG59XHJcblxyXG4vLyBOb3RlOiAgQWxsIHRoZSBDU1MgdmFyaWFibGVzIGRlZmluZWQgYmVsb3cgYXJlIG92ZXJyaWRlcyBvZiBJb25pYyBlbGVtZW50cyBDU1MgQ3VzdG9tIFByb3BlcnRpZXNcclxuaW9uLWhlYWRlciB7XHJcbiAgaW9uLXRvb2xiYXIge1xyXG4gICAgLS1pb24tdG9vbGJhci1iYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcclxuXHJcbiAgICAvLyBGb3IgdGhlIHNraXAgYnV0dG9uXHJcbiAgICBpb24tYnV0dG9uIHtcclxuICAgICAgLS1jb2xvcjogdmFyKC0taW9uLWNvbG9yLWxpZ2h0ZXN0KTtcclxuICAgIH1cclxuICB9XHJcbn1cclxuXHJcbmlvbi1jb250ZW50IHtcclxuICAvLyBUbyBnZXQgdGhlIGlvbi1jb250ZW50IGJlaGluZCB0aGUgaW9uLWhlYWRlclxyXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICB0b3A6IDA7XHJcbn1cclxuXHJcbi53YWxrdGhyb3VnaC1zbGlkZXMge1xyXG4gIC0tYnVsbGV0LWJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1kYXJrKTtcclxuICAtLWJ1bGxldC1iYWNrZ3JvdW5kLWFjdGl2ZTogdmFyKC0taW9uLWNvbG9yLWRhcmspO1xyXG5cclxuICBoZWlnaHQ6IDEwMCU7XHJcblxyXG4gIC5zbGlkZS1pbm5lci1yb3cge1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBwYWRkaW5nOiAwcHg7XHJcbiAgICAvLyBIZWFkZXIgc3BhY2VcclxuICAgIC8vIHBhZGRpbmctdG9wOiBjYWxjKHZhcigtLWFwcC1oZWFkZXItaGVpZ2h0KSArIHZhcigtLWlvbi1zYWZlLWFyZWEtdG9wKSk7XHJcbiAgICAvLyAuc3dpcGVyLXBhZ2luYXRpb24gc3BhY2VcclxuICAgIC8vIGJvcmRlci1ib3R0b206IHZhcigtLXBhZ2Utc3dpcGVyLXBhZ2luYXRpb24tc3BhY2UpIHNvbGlkIHRyYW5zcGFyZW50O1xyXG4gICAgYmFja2dyb3VuZC1jbGlwOiBwYWRkaW5nLWJveDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHZhcigtLXBhZ2UtdmVjdG9yLWRlY29yYXRpb24tZmlsbCk7XHJcbiAgfVxyXG59XHJcblxyXG4uaWxsdXN0cmF0aW9uLWFuZC1kZWNvcmF0aW9uLXNsaWRlIHtcclxuICAuc2xpZGUtaW5uZXItcm93IHtcclxuICAgIC0taW9uLWdyaWQtY29sdW1uLXBhZGRpbmc6IDBweDtcclxuXHJcbiAgICBmbGV4LWZsb3c6IGNvbHVtbjtcclxuICAgIGp1c3RpZnktY29udGVudDogZmxleC1zdGFydDtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgfVxyXG5cclxuICAuaWxsdXN0cmF0aW9uLWNvbCB7XHJcbiAgICBmbGV4LWdyb3c6IDA7XHJcbiAgICBmbGV4LXNocmluazogMDtcclxuICAgIG1pbi1oZWlnaHQ6IGF1dG87XHJcbiAgICBtaW4taGVpZ2h0OiBmaXQtY29udGVudDtcclxuICAgIG1heC13aWR0aDogMzB2aDtcclxuICAgIHBhZGRpbmc6IDBweDtcclxuICB9XHJcblxyXG4gIC5kZWNvcmF0aW9uLWNvbCB7XHJcbiAgICBmbGV4LWdyb3c6IDA7XHJcbiAgICBmbGV4LXNocmluazogMTtcclxuXHJcbiAgICAvLyBUbyBwcmV2ZW50IHRyYW5zcGFyZW50IGxpbmUgY2F1c2VkIGJ5IHBpeGVsIGZyYWN0aW9uc1xyXG4gICAgbWluLWhlaWdodDogMTJ2aDtcclxuICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlM2QoMHB4LCAwcHgsIDBweCk7XHJcblxyXG4gICAgLnZlY3Rvci1kZWNvcmF0aW9uIHtcclxuICAgICAgZmlsbDogY3VycmVudENvbG9yO1xyXG4gICAgICBjb2xvcjogdmFyKC0tcGFnZS12ZWN0b3ItZGVjb3JhdGlvbi1maWxsKTtcclxuICAgICAgYmFja2dyb3VuZC1jb2xvcjogdmFyKC0tcGFnZS1iYWNrZ3JvdW5kKTtcclxuICAgICAgcGFkZGluZy1ib3R0b206IDRweDtcclxuXHJcbiAgICAgIC8vIFRvIHByZXZlbnQgdHJhbnNwYXJlbnQgbGluZSBjYXVzZWQgYnkgcGl4ZWwgZnJhY3Rpb25zXHJcbiAgICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlM2QoMHB4LCAwcHgsIDBweCk7XHJcbiAgICAgIHNoYXBlLXJlbmRlcmluZzogZ2VvbWV0cmljcHJlY2lzaW9uO1xyXG4gICAgICBoZWlnaHQ6IGNhbGMoMTAwJSArIDFweCk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICAuaW5mby1jb2wge1xyXG4gICAgZmxleC1ncm93OiAxO1xyXG4gICAgZmxleC1zaHJpbms6IDA7XHJcbiAgICBtaW4taGVpZ2h0OiBhdXRvO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogdmFyKC0tcGFnZS1iYWNrZ3JvdW5kKTtcclxuXHJcbiAgICAvLyBUbyBwcmV2ZW50IHRyYW5zcGFyZW50IGxpbmUgY2F1c2VkIGJ5IHBpeGVsIGZyYWN0aW9uc1xyXG4gICAgbWFyZ2luLWJvdHRvbTogLTFweDtcclxuICAgIC8vIFByZXZlbnQgLmluZm8td3JhcHBlciB0ZXh0IHRvIGJsaW5rIGFzIGl0J3Mgb3ZlciB0aGUgcHJldmlvdXMgZWxlbWVudCAoU2FmYXJpIGlzc3VlKVxyXG4gICAgdHJhbnNmb3JtOiB0cmFuc2xhdGUzZCgwcHgsIDBweCwgMHB4KTtcclxuXHJcbiAgICAuaW5mby13cmFwcGVyIHtcclxuICAgICAgbWFyZ2luOiBjYWxjKHZhcigtLXBhZ2UtbWFyZ2luKSAqIC0xKSB2YXIoLS1wYWdlLW1hcmdpbikgMHB4O1xyXG4gICAgICBtYXJnaW4tdG9wOiBjYWxjKHZhcigtLXBhZ2UtbWFyZ2luKSAqIDIpO1xyXG5cclxuICAgICAgdGV4dC1hbGlnbjogbGVmdDtcclxuXHJcbiAgICAgIC5pbmZvLXRpdGxlIHtcclxuICAgICAgICBtYXJnaW46IDBweDtcclxuICAgICAgICBtYXJnaW4tYm90dG9tOiB2YXIoLS1wYWdlLW1hcmdpbik7XHJcbiAgICAgICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1kYXJrKTtcclxuICAgICAgICBmb250LWZhbWlseTogXCJJbnRlclwiO1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMjJweDtcclxuICAgICAgICBmb250LXdlaWdodDogbm9ybWFsO1xyXG4gICAgICAgIGZvbnQtc3RyZXRjaDogbm9ybWFsO1xyXG4gICAgICAgIGZvbnQtc3R5bGU6IG5vcm1hbDtcclxuICAgICAgICBsaW5lLWhlaWdodDogMS4yNztcclxuICAgICAgICBsZXR0ZXItc3BhY2luZzogbm9ybWFsO1xyXG4gICAgICAgIHRleHQtYWxpZ246IGxlZnQ7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIC5pbmZvLXBhcmFncmFwaCB7XHJcbiAgICAgICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1tZWRpdW0tc2hhZGUpO1xyXG4gICAgICAgIG1hcmdpbjogMHB4IDBweCBjYWxjKHZhcigtLXBhZ2UtbWFyZ2luKSAvIDIpO1xyXG4gICAgICAgIGZvbnQtZmFtaWx5OiBcIkludGVyXCI7XHJcbiAgICAgICAgZm9udC1zaXplOiAxM3B4O1xyXG4gICAgICAgIGZvbnQtd2VpZ2h0OiBub3JtYWw7XHJcbiAgICAgICAgZm9udC1zdHJldGNoOiBub3JtYWw7XHJcbiAgICAgICAgZm9udC1zdHlsZTogbm9ybWFsO1xyXG4gICAgICAgIGxpbmUtaGVpZ2h0OiAxLjU0O1xyXG4gICAgICAgIGxldHRlci1zcGFjaW5nOiBub3JtYWw7XHJcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG5cclxuICAgICAgICAmOmxhc3QtY2hpbGQge1xyXG4gICAgICAgICAgbWFyZ2luLWJvdHRvbTogMHB4O1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxufVxyXG5cclxuLmZpcnN0LXNsaWRlIHtcclxuICAtLXBhZ2UtdmVjdG9yLWRlY29yYXRpb24tZmlsbDogdmFyKC0tcGFnZS1maXJzdC1zbGlkZS1iYWNrZ3JvdW5kKTtcclxufVxyXG5cclxuLnNlY29uZC1zbGlkZSB7XHJcbiAgLS1wYWdlLXZlY3Rvci1kZWNvcmF0aW9uLWZpbGw6IHZhcigtLXBhZ2Utc2Vjb25kLXNsaWRlLWJhY2tncm91bmQpO1xyXG59XHJcblxyXG4udGhpcmQtc2xpZGUge1xyXG4gIC8vIC0tcGFnZS12ZWN0b3ItZGVjb3JhdGlvbi1maWxsOiB2YXIoLS1wYWdlLXRoaXJkLXNsaWRlLWJhY2tncm91bmQpO1xyXG4gIC0tcGFnZS12ZWN0b3ItZGVjb3JhdGlvbi1maWxsOiB2YXIoLS1wYWdlLWxhc3Qtc2xpZGUtYmFja2dyb3VuZCk7XHJcbn1cclxuXHJcbi5sYXN0LXNsaWRlIHtcclxuICAtLXBhZ2UtdmVjdG9yLWRlY29yYXRpb24tZmlsbDogdmFyKC0tcGFnZS1sYXN0LXNsaWRlLWJhY2tncm91bmQpO1xyXG5cclxuICAuc2xpZGUtaW5uZXItcm93IHtcclxuICAgIC8vIEluIHRoZSBsYXN0IHNsaWRlIC5zd2lwZXItcGFnaW5hdGlvbiBpcyBoaWRkZW5cclxuICAgIGJvcmRlci13aWR0aDogMHB4O1xyXG4gIH1cclxuXHJcbiAgLmluZm8tY29sIHtcclxuICAgIHBhZGRpbmc6IHZhcigtLXBhZ2UtbWFyZ2luKTtcclxuICAgIC8vIG1hcmdpbi1ib3R0b206IGNhbGModmFyKC0tcGFnZS1tYXJnaW4pICogMyk7XHJcbiAgICAuaW5mby1vdXRlciB7XHJcbiAgICAgIGhlaWdodDogMTAwJTtcclxuICAgICAgYWxpZ24taXRlbXM6IGZsZXgtZW5kO1xyXG4gICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG5cclxuICAgICAgLmluZm8td3JhcHBlciB7XHJcbiAgICAgICAgbWFyZ2luOiBjYWxjKHZhcigtLXBhZ2UtbWFyZ2luKSAqIC0xKSB2YXIoLS1wYWdlLW1hcmdpbikgMHB4O1xyXG4gICAgICB9XHJcblxyXG4gICAgICAuY2FsbC10by1hY3Rpb25zLXdyYXBwZXIge1xyXG4gICAgICAgIG1heC1oZWlnaHQ6IGZpdC1jb250ZW50O1xyXG4gICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICBib3R0b206IDE1JTtcclxuICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC5nZXQtc3RhcnRlZC1idXR0b24ge1xyXG4gICAgICBtYXJnaW46IDBweDtcclxuICAgIH1cclxuXHJcbiAgICAuYWx0LWNhbGwtdG8tYWN0aW9uLXJvdyB7XHJcbiAgICAgIHBhZGRpbmctdG9wOiA1cHg7XHJcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtZXZlbmx5O1xyXG5cclxuICAgICAgLmN0YS1sZWFkaW5nLXRleHQge1xyXG4gICAgICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItbWVkaXVtKTtcclxuICAgICAgICBmb250LXNpemU6IDE2cHg7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIC5sb2dpbi1idXR0b24ge1xyXG4gICAgICAgIC0tY29sb3I6IHZhcigtLWlvbi1jb2xvci1zZWNvbmRhcnkpO1xyXG4gICAgICAgIG1hcmdpbjogMHB4O1xyXG5cclxuICAgICAgICAmOmZvY3VzIHtcclxuICAgICAgICAgIG91dGxpbmU6IG5vbmU7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcblxyXG46aG9zdCguZmlyc3Qtc2xpZGUtYWN0aXZlKSB7XHJcbiAgLnNraXAtd2Fsa3Rocm91Z2gtYnV0dG9uIHtcclxuICAgIHZpc2liaWxpdHk6IGhpZGRlbjtcclxuICB9XHJcbn1cclxuXHJcbjpob3N0KC5sYXN0LXNsaWRlLWFjdGl2ZSkge1xyXG4gIDo6bmctZGVlcCAud2Fsa3Rocm91Z2gtc2xpZGVzIHtcclxuICAgIC5zd2lwZXItcGFnaW5hdGlvbiB7XHJcbiAgICAgIGRpc3BsYXk6IG5vbmU7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICAuc2tpcC13YWxrdGhyb3VnaC1idXR0b24ge1xyXG4gICAgdmlzaWJpbGl0eTogaGlkZGVuO1xyXG4gIH1cclxufVxyXG5cclxuLy8gSVNTVUU6IC5zd2lwZXItcGFnZ2luYXRpb24gZ2V0cyByZW5kZXJlZCBkeW5hbWljYWxseS4gVGhhdCBwcmV2ZW50cyBzdHlsaW5nIHRoZSBlbGVtZW50cyB3aGVuIHVzaW5nIHRoZSBkZWZhdWx0IEFuZ3VsYXIgVmlld0VuY2Fwc3VsYXRpb24uTm9uZVxyXG4vLyAgICAgICAgKEFuZ3VsYXIgZG9lc24ndCBhZGQgYW4gJ19uZ2NvbnRlbnQnIGF0dHJpYnV0ZSB0byB0aGUgLnN3aXBlci1wYWdnaW5hdGlvbiBiZWNhdXNlIGl0J3MgZHluYW1pY2FsbHkgcmVuZGVyZWQpXHJcbi8vIEZJWDogICBTZWU6IGh0dHBzOi8vc3RhY2tvdmVyZmxvdy5jb20vYS8zNjI2NTA3Mi8xMTE2OTU5XHJcbjpob3N0IDo6bmctZGVlcCAud2Fsa3Rocm91Z2gtc2xpZGVzIHtcclxuICAuc3dpcGVyLXBhZ2luYXRpb24ge1xyXG4gICAgaGVpZ2h0OiB2YXIoLS1wYWdlLXN3aXBlci1wYWdpbmF0aW9uLWhlaWdodCk7XHJcbiAgICBsaW5lLWhlaWdodDogMTtcclxuICAgIC8vIC5zd2lwZXItcGFnaW5hdGlvbiBpcyAxOHB4IGhlaWdodCwgLnNsaWRlLWlubmVyLXJvdyBoYXMgNDBweCBvZiBwYWRkaW5nLWJvdHRvbSA9PiBib3R0b20gPSAoNDBweCAtIDE4cHgpLzIgPSAxMXB4XHJcbiAgICBib3R0b206IGNhbGMoXHJcbiAgICAgIChcclxuICAgICAgICAgIHZhcigtLXBhZ2Utc3dpcGVyLXBhZ2luYXRpb24tc3BhY2UpIC1cclxuICAgICAgICAgICAgdmFyKC0tcGFnZS1zd2lwZXItcGFnaW5hdGlvbi1oZWlnaHQpXHJcbiAgICAgICAgKSAvIDJcclxuICAgICk7XHJcblxyXG4gICAgLnN3aXBlci1wYWdpbmF0aW9uLWJ1bGxldCB7XHJcbiAgICAgIHdpZHRoOiB2YXIoLS1wYWdlLXBhZ2luYXRpb24tYnVsbGV0LXNpemUpO1xyXG4gICAgICBoZWlnaHQ6IHZhcigtLXBhZ2UtcGFnaW5hdGlvbi1idWxsZXQtc2l6ZSk7XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcbiJdfQ== */";
      /***/
    },

    /***/
    "./src/app/pages/walkthrough/styles/walkthrough.responsive.scss":
    /*!**********************************************************************!*\
      !*** ./src/app/pages/walkthrough/styles/walkthrough.responsive.scss ***!
      \**********************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppPagesWalkthroughStylesWalkthroughResponsiveScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "/* ----------- iPhone 4 and 4S ----------- */\n@media only screen and (min-device-width: 320px) and (max-device-width: 480px) and (-webkit-min-device-pixel-ratio: 2) and (device-aspect-ratio: 2/3) {\n  .illustration-and-decoration-slide .illustration-col {\n    max-width: 30vh;\n    padding: 0px;\n  }\n  .illustration-and-decoration-slide .decoration-col {\n    min-height: 12vh;\n  }\n  .illustration-and-decoration-slide .info-col .info-wrapper .info-title {\n    margin-bottom: calc(var(--page-margin) / 2);\n    font-size: 20px;\n  }\n}\n/* ----------- iPhone 5, 5S, 5C and 5SE ----------- */\n@media only screen and (min-device-width: 320px) and (max-device-width: 568px) and (-webkit-min-device-pixel-ratio: 2) and (device-aspect-ratio: 40/71) {\n  .illustration-and-decoration-slide .illustration-col {\n    max-width: 32vh;\n    padding: 0px;\n  }\n  .illustration-and-decoration-slide .decoration-col {\n    min-height: 12vh;\n  }\n  .illustration-and-decoration-slide .info-col .info-wrapper .info-title {\n    margin-bottom: calc(var(--page-margin) / 2);\n    font-size: 20px;\n  }\n}\n/* ----------- iPhone 6, 6S, 7 and 8 ----------- */\n@media only screen and (min-device-width: 375px) and (max-device-width: 667px) and (-webkit-min-device-pixel-ratio: 2) {\n  .illustration-and-decoration-slide .illustration-col {\n    max-width: 36vh;\n    padding: 2vh 0px;\n  }\n  .illustration-and-decoration-slide .decoration-col {\n    min-height: 14vh;\n  }\n}\n/* ----------- iPhone X ----------- */\n@media only screen and (min-device-width: 375px) and (max-device-width: 812px) and (-webkit-min-device-pixel-ratio: 3) {\n  .illustration-and-decoration-slide .illustration-col {\n    max-width: 34vh;\n    padding: 6vh 0px;\n  }\n  .illustration-and-decoration-slide .decoration-col {\n    min-height: 12vh;\n  }\n}\n/* ----------- iPhone 6+, 7+ and 8+ ----------- */\n@media only screen and (min-device-width: 414px) and (max-device-width: 736px) and (-webkit-min-device-pixel-ratio: 3) {\n  .illustration-and-decoration-slide .illustration-col {\n    max-width: 38vh;\n    padding: 4vh 0px;\n  }\n  .illustration-and-decoration-slide .decoration-col {\n    min-height: 14vh;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvd2Fsa3Rocm91Z2gvc3R5bGVzL3dhbGt0aHJvdWdoLnJlc3BvbnNpdmUuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFLQSw0Q0FBQTtBQUNBO0VBV0k7SUFDRSxlQUFBO0lBQ0EsWUFBQTtFQWRKO0VBaUJFO0lBQ0UsZ0JBQUE7RUFmSjtFQW9CTTtJQUNFLDJDQUFBO0lBQ0EsZUFBQTtFQWxCUjtBQUNGO0FBd0JBLHFEQUFBO0FBQ0E7RUFXSTtJQUNFLGVBQUE7SUFDQSxZQUFBO0VBaENKO0VBbUNFO0lBQ0UsZ0JBQUE7RUFqQ0o7RUFzQ007SUFDRSwyQ0FBQTtJQUNBLGVBQUE7RUFwQ1I7QUFDRjtBQTBDQSxrREFBQTtBQUNBO0VBVUk7SUFDRSxlQUFBO0lBQ0EsZ0JBQUE7RUFqREo7RUFvREU7SUFDRSxnQkFBQTtFQWxESjtBQUNGO0FBc0RBLHFDQUFBO0FBQ0E7RUFVSTtJQUNFLGVBQUE7SUFDQSxnQkFBQTtFQTdESjtFQWdFRTtJQUNFLGdCQUFBO0VBOURKO0FBQ0Y7QUFrRUEsaURBQUE7QUFDQTtFQVVJO0lBQ0UsZUFBQTtJQUNBLGdCQUFBO0VBekVKO0VBNEVFO0lBQ0UsZ0JBQUE7RUExRUo7QUFDRiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3dhbGt0aHJvdWdoL3N0eWxlcy93YWxrdGhyb3VnaC5yZXNwb25zaXZlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIvLyAoTm90ZTogRG9uJ3QgY2hhbmdlIHRoZSBvcmRlciBvZiB0aGUgZGV2aWNlcyBhcyBpdCBtYXkgYnJlYWsgdGhlIGNvcnJlY3QgY3NzIHByZWNlZGVuY2UpXHJcblxyXG4vLyAoc2VlOiBodHRwczovL2Nzcy10cmlja3MuY29tL3NuaXBwZXRzL2Nzcy9tZWRpYS1xdWVyaWVzLWZvci1zdGFuZGFyZC1kZXZpY2VzLyNpcGhvbmUtcXVlcmllcylcclxuLy8gKHNlZTogaHR0cHM6Ly9zdGFja292ZXJmbG93LmNvbS9hLzQ3NzUwMjYxLzExMTY5NTkpXHJcblxyXG4vKiAtLS0tLS0tLS0tLSBpUGhvbmUgNCBhbmQgNFMgLS0tLS0tLS0tLS0gKi9cclxuQG1lZGlhIG9ubHkgc2NyZWVuXHJcbiAgYW5kIChtaW4tZGV2aWNlLXdpZHRoIDogMzIwcHgpXHJcbiAgYW5kIChtYXgtZGV2aWNlLXdpZHRoIDogNDgwcHgpXHJcbiAgYW5kICgtd2Via2l0LW1pbi1kZXZpY2UtcGl4ZWwtcmF0aW86IDIpXHJcbiAgYW5kIChkZXZpY2UtYXNwZWN0LXJhdGlvOiAyLzMpXHJcbiAgLy8gdW5jb21tZW50IGZvciBvbmx5IHBvcnRyYWl0OlxyXG4gIC8vIGFuZCAob3JpZW50YXRpb246IHBvcnRyYWl0KVxyXG4gIC8vIHVuY29tbWVudCBmb3Igb25seSBsYW5kc2NhcGU6XHJcbiAgLy8gYW5kIChvcmllbnRhdGlvbjogbGFuZHNjYXBlKVxyXG57XHJcbiAgLmlsbHVzdHJhdGlvbi1hbmQtZGVjb3JhdGlvbi1zbGlkZSB7XHJcbiAgICAuaWxsdXN0cmF0aW9uLWNvbCB7XHJcbiAgICAgIG1heC13aWR0aDogMzB2aDtcclxuICAgICAgcGFkZGluZzogMHB4O1xyXG4gICAgfVxyXG5cclxuICAgIC5kZWNvcmF0aW9uLWNvbCB7XHJcbiAgICAgIG1pbi1oZWlnaHQ6IDEydmg7XHJcbiAgICB9XHJcblxyXG4gICAgLmluZm8tY29sIHtcclxuICAgICAgLmluZm8td3JhcHBlciB7XHJcbiAgICAgICAgLmluZm8tdGl0bGUge1xyXG4gICAgICAgICAgbWFyZ2luLWJvdHRvbTogY2FsYyh2YXIoLS1wYWdlLW1hcmdpbikgLyAyKTtcclxuICAgICAgICAgIGZvbnQtc2l6ZTogMjBweDtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcbn1cclxuXHJcbi8qIC0tLS0tLS0tLS0tIGlQaG9uZSA1LCA1UywgNUMgYW5kIDVTRSAtLS0tLS0tLS0tLSAqL1xyXG5AbWVkaWEgb25seSBzY3JlZW5cclxuICBhbmQgKG1pbi1kZXZpY2Utd2lkdGggOiAzMjBweClcclxuICBhbmQgKG1heC1kZXZpY2Utd2lkdGggOiA1NjhweClcclxuICBhbmQgKC13ZWJraXQtbWluLWRldmljZS1waXhlbC1yYXRpbzogMilcclxuICBhbmQgKGRldmljZS1hc3BlY3QtcmF0aW86IDQwIC8gNzEpXHJcbiAgLy8gdW5jb21tZW50IGZvciBvbmx5IHBvcnRyYWl0OlxyXG4gIC8vIGFuZCAob3JpZW50YXRpb246IHBvcnRyYWl0KVxyXG4gIC8vIHVuY29tbWVudCBmb3Igb25seSBsYW5kc2NhcGU6XHJcbiAgLy8gYW5kIChvcmllbnRhdGlvbjogbGFuZHNjYXBlKVxyXG57XHJcbiAgLmlsbHVzdHJhdGlvbi1hbmQtZGVjb3JhdGlvbi1zbGlkZSB7XHJcbiAgICAuaWxsdXN0cmF0aW9uLWNvbCB7XHJcbiAgICAgIG1heC13aWR0aDogMzJ2aDtcclxuICAgICAgcGFkZGluZzogMHB4O1xyXG4gICAgfVxyXG5cclxuICAgIC5kZWNvcmF0aW9uLWNvbCB7XHJcbiAgICAgIG1pbi1oZWlnaHQ6IDEydmg7XHJcbiAgICB9XHJcblxyXG4gICAgLmluZm8tY29sIHtcclxuICAgICAgLmluZm8td3JhcHBlciB7XHJcbiAgICAgICAgLmluZm8tdGl0bGUge1xyXG4gICAgICAgICAgbWFyZ2luLWJvdHRvbTogY2FsYyh2YXIoLS1wYWdlLW1hcmdpbikgLyAyKTtcclxuICAgICAgICAgIGZvbnQtc2l6ZTogMjBweDtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcbn1cclxuXHJcbi8qIC0tLS0tLS0tLS0tIGlQaG9uZSA2LCA2UywgNyBhbmQgOCAtLS0tLS0tLS0tLSAqL1xyXG5AbWVkaWEgb25seSBzY3JlZW5cclxuICBhbmQgKG1pbi1kZXZpY2Utd2lkdGggOiAzNzVweClcclxuICBhbmQgKG1heC1kZXZpY2Utd2lkdGggOiA2NjdweClcclxuICBhbmQgKC13ZWJraXQtbWluLWRldmljZS1waXhlbC1yYXRpbzogMilcclxuICAvLyB1bmNvbW1lbnQgZm9yIG9ubHkgcG9ydHJhaXQ6XHJcbiAgLy8gYW5kIChvcmllbnRhdGlvbjogcG9ydHJhaXQpXHJcbiAgLy8gdW5jb21tZW50IGZvciBvbmx5IGxhbmRzY2FwZTpcclxuICAvLyBhbmQgKG9yaWVudGF0aW9uOiBsYW5kc2NhcGUpXHJcbntcclxuICAuaWxsdXN0cmF0aW9uLWFuZC1kZWNvcmF0aW9uLXNsaWRlIHtcclxuICAgIC5pbGx1c3RyYXRpb24tY29sIHtcclxuICAgICAgbWF4LXdpZHRoOiAzNnZoO1xyXG4gICAgICBwYWRkaW5nOiAydmggMHB4O1xyXG4gICAgfVxyXG5cclxuICAgIC5kZWNvcmF0aW9uLWNvbCB7XHJcbiAgICAgIG1pbi1oZWlnaHQ6IDE0dmg7XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcblxyXG4vKiAtLS0tLS0tLS0tLSBpUGhvbmUgWCAtLS0tLS0tLS0tLSAqL1xyXG5AbWVkaWEgb25seSBzY3JlZW5cclxuICBhbmQgKG1pbi1kZXZpY2Utd2lkdGggOiAzNzVweClcclxuICBhbmQgKG1heC1kZXZpY2Utd2lkdGggOiA4MTJweClcclxuICBhbmQgKC13ZWJraXQtbWluLWRldmljZS1waXhlbC1yYXRpbyA6IDMpXHJcbiAgLy8gdW5jb21tZW50IGZvciBvbmx5IHBvcnRyYWl0OlxyXG4gIC8vIGFuZCAob3JpZW50YXRpb246IHBvcnRyYWl0KVxyXG4gIC8vIHVuY29tbWVudCBmb3Igb25seSBsYW5kc2NhcGU6XHJcbiAgLy8gYW5kIChvcmllbnRhdGlvbjogbGFuZHNjYXBlKVxyXG57XHJcbiAgLmlsbHVzdHJhdGlvbi1hbmQtZGVjb3JhdGlvbi1zbGlkZSB7XHJcbiAgICAuaWxsdXN0cmF0aW9uLWNvbCB7XHJcbiAgICAgIG1heC13aWR0aDogMzR2aDtcclxuICAgICAgcGFkZGluZzogNnZoIDBweDtcclxuICAgIH1cclxuXHJcbiAgICAuZGVjb3JhdGlvbi1jb2wge1xyXG4gICAgICBtaW4taGVpZ2h0OiAxMnZoO1xyXG4gICAgfVxyXG4gIH1cclxufVxyXG5cclxuLyogLS0tLS0tLS0tLS0gaVBob25lIDYrLCA3KyBhbmQgOCsgLS0tLS0tLS0tLS0gKi9cclxuQG1lZGlhIG9ubHkgc2NyZWVuXHJcbiAgYW5kIChtaW4tZGV2aWNlLXdpZHRoIDogNDE0cHgpXHJcbiAgYW5kIChtYXgtZGV2aWNlLXdpZHRoIDogNzM2cHgpXHJcbiAgYW5kICgtd2Via2l0LW1pbi1kZXZpY2UtcGl4ZWwtcmF0aW86IDMpXHJcbiAgLy8gdW5jb21tZW50IGZvciBvbmx5IHBvcnRyYWl0OlxyXG4gIC8vIGFuZCAob3JpZW50YXRpb246IHBvcnRyYWl0KVxyXG4gIC8vIHVuY29tbWVudCBmb3Igb25seSBsYW5kc2NhcGU6XHJcbiAgLy8gYW5kIChvcmllbnRhdGlvbjogbGFuZHNjYXBlKVxyXG57XHJcbiAgLmlsbHVzdHJhdGlvbi1hbmQtZGVjb3JhdGlvbi1zbGlkZSB7XHJcbiAgICAuaWxsdXN0cmF0aW9uLWNvbCB7XHJcbiAgICAgIG1heC13aWR0aDogMzh2aDtcclxuICAgICAgcGFkZGluZzogNHZoIDBweDtcclxuICAgIH1cclxuXHJcbiAgICAuZGVjb3JhdGlvbi1jb2wge1xyXG4gICAgICBtaW4taGVpZ2h0OiAxNHZoO1xyXG4gICAgfVxyXG4gIH1cclxufVxyXG4iXX0= */";
      /***/
    },

    /***/
    "./src/app/pages/walkthrough/styles/walkthrough.shell.scss":
    /*!*****************************************************************!*\
      !*** ./src/app/pages/walkthrough/styles/walkthrough.shell.scss ***!
      \*****************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppPagesWalkthroughStylesWalkthroughShellScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "app-image-shell.illustration-image {\n  --image-shell-loading-background: transparent;\n  --image-shell-spinner-color: var(--ion-color-lightest);\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvd2Fsa3Rocm91Z2gvc3R5bGVzL3dhbGt0aHJvdWdoLnNoZWxsLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSw2Q0FBQTtFQUNBLHNEQUFBO0FBQ0YiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy93YWxrdGhyb3VnaC9zdHlsZXMvd2Fsa3Rocm91Z2guc2hlbGwuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImFwcC1pbWFnZS1zaGVsbC5pbGx1c3RyYXRpb24taW1hZ2Uge1xyXG4gIC0taW1hZ2Utc2hlbGwtbG9hZGluZy1iYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcclxuICAtLWltYWdlLXNoZWxsLXNwaW5uZXItY29sb3I6IHZhcigtLWlvbi1jb2xvci1saWdodGVzdCk7XHJcbn1cclxuIl19 */";
      /***/
    },

    /***/
    "./src/app/pages/walkthrough/walkthrough.module.ts":
    /*!*********************************************************!*\
      !*** ./src/app/pages/walkthrough/walkthrough.module.ts ***!
      \*********************************************************/

    /*! exports provided: WalkthroughPageModule */

    /***/
    function srcAppPagesWalkthroughWalkthroughModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "WalkthroughPageModule", function () {
        return WalkthroughPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _core_services__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @core/services */
      "./src/app/core/services/index.ts");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _shared_lib_shared_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! @shared-lib/shared.module */
      "./src/app/shared/shared.module.ts");
      /* harmony import */


      var _walkthrough_page__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! ./walkthrough.page */
      "./src/app/pages/walkthrough/walkthrough.page.ts");

      var routes = [{
        path: "",
        component: _walkthrough_page__WEBPACK_IMPORTED_MODULE_8__["WalkthroughPage"]
      }];

      var WalkthroughPageModule = function WalkthroughPageModule() {
        _classCallCheck(this, WalkthroughPageModule);
      };

      WalkthroughPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["IonicModule"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes), _shared_lib_shared_module__WEBPACK_IMPORTED_MODULE_7__["SharedModule"]],
        declarations: [_walkthrough_page__WEBPACK_IMPORTED_MODULE_8__["WalkthroughPage"]],
        providers: [_core_services__WEBPACK_IMPORTED_MODULE_5__["LanguageService"]]
      })], WalkthroughPageModule);
      /***/
    },

    /***/
    "./src/app/pages/walkthrough/walkthrough.page.ts":
    /*!*******************************************************!*\
      !*** ./src/app/pages/walkthrough/walkthrough.page.ts ***!
      \*******************************************************/

    /*! exports provided: WalkthroughPage */

    /***/
    function srcAppPagesWalkthroughWalkthroughPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "WalkthroughPage", function () {
        return WalkthroughPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _capacitor_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @capacitor/core */
      "./node_modules/@capacitor/core/dist/esm/index.js");
      /* harmony import */


      var _core_services__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @core/services */
      "./src/app/core/services/index.ts");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! @ngx-translate/core */
      "./node_modules/@ngx-translate/core/__ivy_ngcc__/fesm2015/ngx-translate-core.js");
      /* harmony import */


      var _shared_lib_components__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! @shared-lib/components */
      "./src/app/shared/components/index.ts");

      var Browser = _capacitor_core__WEBPACK_IMPORTED_MODULE_4__["Plugins"].Browser;

      var WalkthroughPage = /*#__PURE__*/function () {
        function WalkthroughPage(platformId, menu, translate, languageService, alertController, modalController, router, authService, boardService) {
          _classCallCheck(this, WalkthroughPage);

          this.platformId = platformId;
          this.menu = menu;
          this.translate = translate;
          this.languageService = languageService;
          this.alertController = alertController;
          this.modalController = modalController;
          this.router = router;
          this.authService = authService;
          this.boardService = boardService;
          this.slidesOptions = {
            zoom: {
              toggle: false
            }
          };
          this.isNotStart = false;
          this.available_languages = [];
          this.isFirstSlide = true;
          this.isLastSlide = false;
        }

        _createClass(WalkthroughPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            this.getTranslations();

            var _ref = this.authService.currentUser || {},
                uid = _ref.uid;

            if (uid) {
              this.boardService.setUserId(uid);
              this.boards$ = this.boardService.getBoards(uid);
            }
          } // Disable side menu for this page

        }, {
          key: "ionViewDidEnter",
          value: function ionViewDidEnter() {
            this.menu.enable(false);
          } // Restore to default when leaving this page

        }, {
          key: "ionViewDidLeave",
          value: function ionViewDidLeave() {
            this.menu.enable(true);
          }
        }, {
          key: "ngAfterViewInit",
          value: function ngAfterViewInit() {
            var _this = this;

            // Accessing slides in server platform throw errors
            if (Object(_angular_common__WEBPACK_IMPORTED_MODULE_1__["isPlatformBrowser"])(this.platformId)) {
              // ViewChild is set
              this.slides.isBeginning().then(function (isBeginning) {
                _this.isFirstSlide = isBeginning;
              });
              this.slides.isEnd().then(function (isEnd) {
                _this.isLastSlide = isEnd;
              }); // Subscribe to changes

              this.slides.ionSlideWillChange.subscribe(function (changes) {
                _this.slides.isBeginning().then(function (isBeginning) {
                  _this.isFirstSlide = isBeginning;
                });

                _this.slides.isEnd().then(function (isEnd) {
                  _this.isLastSlide = isEnd;
                });
              });
              this.translate.onLangChange.subscribe(function () {
                return _this.getTranslations();
              });
            }
          }
        }, {
          key: "skipWalkthrough",
          value: function skipWalkthrough() {
            var _this2 = this;

            // Skip to the last slide
            this.slides.length().then(function (length) {
              _this2.slides.slideTo(length);
            });
          }
        }, {
          key: "getTranslations",
          value: function getTranslations() {
            var _this3 = this;

            // get translations for this page to use in the Language Chooser Alert
            this.translate.getTranslation(this.translate.currentLang).subscribe(function (translations) {
              return _this3.translations = translations;
            });
          }
        }, {
          key: "openLanguageChooser",
          value: function openLanguageChooser() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
              var _this4 = this;

              var componentProps, modal;
              return regeneratorRuntime.wrap(function _callee$(_context) {
                while (1) {
                  switch (_context.prev = _context.next) {
                    case 0:
                      this.available_languages = this.languageService.getLanguages().map(function (item) {
                        return {
                          label: item.name,
                          value: item.code,
                          checked: item.code === _this4.translate.currentLang
                        };
                      });
                      componentProps = {
                        header: this.translations.SELECT_LANGUAGE,
                        subHeader: this.translations.ADD_LANGUAGE,
                        options: this.available_languages,
                        closeBtn: true
                      };
                      _context.next = 4;
                      return this.modalController.create({
                        componentProps: componentProps,
                        showBackdrop: true,
                        component: _shared_lib_components__WEBPACK_IMPORTED_MODULE_8__["CustomModalComponent"],
                        cssClass: "custom-modal",
                        mode: "ios"
                      });

                    case 4:
                      modal = _context.sent;
                      modal.onDidDismiss().then(function (data) {
                        if (data["data"]) {
                          var selected = data.data.selected;

                          if (selected) {
                            var selectedLanguage = _this4.languageService.getLanguages().filter(function (obj) {
                              return obj.code === selected;
                            })[0];

                            _this4.languageService.setSelectedLanguage(selectedLanguage);

                            _this4.translate.use(selected);

                            _this4.slides.slideNext();
                          }

                          if (data["data"] == "forward") {
                            _this4.slides.slideNext();
                          }
                        } else {
                          _this4.slides.slidePrev();
                        }
                      });
                      _context.next = 8;
                      return modal.present();

                    case 8:
                      return _context.abrupt("return", _context.sent);

                    case 9:
                    case "end":
                      return _context.stop();
                  }
                }
              }, _callee, this);
            }));
          }
        }, {
          key: "slideChanged",
          value: function slideChanged() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
              var slide;
              return regeneratorRuntime.wrap(function _callee2$(_context2) {
                while (1) {
                  switch (_context2.prev = _context2.next) {
                    case 0:
                      _context2.next = 2;
                      return this.slides.getActiveIndex();

                    case 2:
                      slide = _context2.sent;
                      this.activeSlide = slide;

                      if (slide > 0) {
                        this.isNotStart = true;
                      } else {
                        this.isNotStart = false;
                      }

                      if (slide == 1) {
                        this.openLanguageChooser();
                      }

                    case 6:
                    case "end":
                      return _context2.stop();
                  }
                }
              }, _callee2, this);
            }));
          }
        }, {
          key: "next",
          value: function next() {
            this.slides.slideNext();
          }
        }, {
          key: "prev",
          value: function prev() {
            this.slides.slidePrev();
          }
        }, {
          key: "presentAlertConfirm",
          value: function presentAlertConfirm() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee4() {
              var _this5 = this;

              var alert;
              return regeneratorRuntime.wrap(function _callee4$(_context4) {
                while (1) {
                  switch (_context4.prev = _context4.next) {
                    case 0:
                      _context4.next = 2;
                      return this.alertController.create({
                        cssClass: "my-custom-class",
                        header: "Confirm!",
                        message: "Open Tell Lisa via webapp?",
                        buttons: [{
                          text: "Cancel",
                          role: "cancel",
                          cssClass: "secondary",
                          handler: function handler(blah) {
                            console.log("Confirm Cancel: blah");
                          }
                        }, {
                          text: "Okay",
                          handler: function handler() {
                            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this5, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
                              return regeneratorRuntime.wrap(function _callee3$(_context3) {
                                while (1) {
                                  switch (_context3.prev = _context3.next) {
                                    case 0:
                                      console.log("Confirm Okay");
                                      _context3.next = 3;
                                      return Browser.open({
                                        url: "https://tell-lisa.web.app/home/boards/"
                                      });

                                    case 3:
                                      this.router.navigate(["home"]);

                                    case 4:
                                    case "end":
                                      return _context3.stop();
                                  }
                                }
                              }, _callee3, this);
                            }));
                          }
                        }]
                      });

                    case 2:
                      alert = _context4.sent;
                      _context4.next = 5;
                      return alert.present();

                    case 5:
                    case "end":
                      return _context4.stop();
                  }
                }
              }, _callee4, this);
            }));
          }
        }, {
          key: "getStarted",
          value: function getStarted() {
            this.router.navigate(["home"]);
          }
        }]);

        return WalkthroughPage;
      }();

      WalkthroughPage.ctorParameters = function () {
        return [{
          type: Object,
          decorators: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["Inject"],
            args: [_angular_core__WEBPACK_IMPORTED_MODULE_2__["PLATFORM_ID"]]
          }]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["MenuController"]
        }, {
          type: _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__["TranslateService"]
        }, {
          type: _core_services__WEBPACK_IMPORTED_MODULE_5__["LanguageService"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["AlertController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["ModalController"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]
        }, {
          type: _core_services__WEBPACK_IMPORTED_MODULE_5__["FirebaseAuthService"]
        }, {
          type: _core_services__WEBPACK_IMPORTED_MODULE_5__["BoardsService"]
        }];
      };

      WalkthroughPage.propDecorators = {
        slides: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["ViewChild"],
          args: [_ionic_angular__WEBPACK_IMPORTED_MODULE_6__["IonSlides"], {
            "static": true
          }]
        }],
        isFirstSlide: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["HostBinding"],
          args: ["class.first-slide-active"]
        }],
        isLastSlide: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["HostBinding"],
          args: ["class.last-slide-active"]
        }]
      };
      WalkthroughPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
        selector: "app-walkthrough",
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./walkthrough.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/walkthrough/walkthrough.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./styles/walkthrough.page.scss */
        "./src/app/pages/walkthrough/styles/walkthrough.page.scss"))["default"], Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./styles/walkthrough.shell.scss */
        "./src/app/pages/walkthrough/styles/walkthrough.shell.scss"))["default"], Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./styles/walkthrough.responsive.scss */
        "./src/app/pages/walkthrough/styles/walkthrough.responsive.scss"))["default"]]
      }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [Object, _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["MenuController"], _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__["TranslateService"], _core_services__WEBPACK_IMPORTED_MODULE_5__["LanguageService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["AlertController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["ModalController"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _core_services__WEBPACK_IMPORTED_MODULE_5__["FirebaseAuthService"], _core_services__WEBPACK_IMPORTED_MODULE_5__["BoardsService"]])], WalkthroughPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=pages-walkthrough-walkthrough-module-es5.js.map