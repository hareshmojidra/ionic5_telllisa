// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyCpFbjpy44H8zQ7gIqlZ1HP9tRnLyMdR0I",
    authDomain: "tell-lisa.firebaseapp.com",
    projectId: "tell-lisa",
    storageBucket: "tell-lisa.appspot.com",
    messagingSenderId: "918368608867",
    appId: "1:918368608867:web:2d9331ac5c33625fd5ce56",
    measurementId: "G-CHBQBEXVS4",
  },
  appShellConfig: {
    debug: false,
    networkDelay: 500,
  },
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
