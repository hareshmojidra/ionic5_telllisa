export const environment = {
  production: true,
  firebase: {
    apiKey: "AIzaSyCpFbjpy44H8zQ7gIqlZ1HP9tRnLyMdR0I",
    authDomain: "tell-lisa.firebaseapp.com",
    projectId: "tell-lisa",
    storageBucket: "tell-lisa.appspot.com",
    messagingSenderId: "918368608867",
    appId: "1:918368608867:web:2d9331ac5c33625fd5ce56",
    measurementId: "G-CHBQBEXVS4",
  },
  appShellConfig: {
    debug: false,
    networkDelay: 500,
  },
};
