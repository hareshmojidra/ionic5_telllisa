import { PrivacyEnglishComponent } from './privacy-english/privacy-english.component';
import { PrivacyGermanComponent } from './privacy-german/privacy-german.component';

export { PrivacyGermanComponent, PrivacyEnglishComponent };
