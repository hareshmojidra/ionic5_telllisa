import { Component } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: "app-privacy-policy-page",
  templateUrl: "privacy-policy.page.html",
  styleUrls: ["./styles/privacy-policy.page.scss"],
})
export default class PrivacyPolicyPage {
  isEnglish: boolean = true;
  constructor(private modalController: ModalController) {}

  dismiss(): void {
    this.modalController.dismiss();
  }

  segmentChanged() {
    this.isEnglish = !this.isEnglish;
  }
}
