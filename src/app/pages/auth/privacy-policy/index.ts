import { PrivacyEnglishComponent, PrivacyGermanComponent } from './components';
import PrivacyPolicyPage from './privacy-policy.page';

export { PrivacyPolicyPage, PrivacyGermanComponent, PrivacyEnglishComponent };
