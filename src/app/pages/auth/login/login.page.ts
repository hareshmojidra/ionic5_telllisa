import { Location } from '@angular/common';
import { Component, NgZone } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { loginValidationMessages } from '@core/helpers';
import { FirebaseAuthService } from '@core/services';
import { LoadingController, MenuController } from '@ionic/angular';
import { StorageService } from '@shared-lib/services';
import { isNil } from 'lodash';
import { Subscription } from 'rxjs';

@Component({
  selector: "app-login",
  templateUrl: "./login.page.html",
  styleUrls: ["./login.page.scss", "./login.page.responsive.scss"],
})
export class LoginPage {
  loginForm: FormGroup;
  authRedirectResult: Subscription;
  redirectLoader: HTMLIonLoadingElement;
  submitError: string;
  validation_messages = loginValidationMessages;
  email: any = '';
  password:any = '';

  constructor(
    public router: Router,
    public route: ActivatedRoute,
    public menu: MenuController,
    private storageService: StorageService,
    public authService: FirebaseAuthService,
    public loadingController: LoadingController,
    public location: Location,
    private ngZone: NgZone
  ) {
    this.getUserEmail();
    this.loginForm = new FormGroup({
      email: new FormControl(
        Validators.compose([
          Validators.required,
          Validators.pattern("^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$"),
        ])
      ),
      password: new FormControl(
        "",
        Validators.compose([Validators.minLength(5), Validators.required])
      ),
    });

    // Check if url contains our custom 'auth-redirect' param, then show a loader while we receive the getRedirectResult notification
    this.route.queryParams.subscribe((params) => {
      const authProvider = params["auth-redirect"];
      if (authProvider) {
        this.presentLoading(authProvider);
      }
    });
  }

  async getUserEmail() {
    let userEmail = await this.storageService.getItem('userEmail');
    if(userEmail != null) {
    this.email = userEmail
    }
   return userEmail;
  }

  // Disable side menu for this page
  ionViewDidEnter(): void {
    this.menu.enable(false);
  }

  // Restore to default when leaving this page
  ionViewDidLeave(): void {
    this.menu.enable(true);
  }

  ngOnDestroy(): void {
    this.dismissLoading();
  }

  async doLogin(): Promise<void> {
    console.log("do Log In");
    const hasDonetutorial = await this.storageService.getItem("tutorial");
    if (!isNil(hasDonetutorial)) {
      this.router.navigate(["home"]);
    } else {
      this.storageService.setItem({
        key: "tutorial",
        value: JSON.stringify(1),
      });
      this.router.navigate(["walkthrough"]);
    }
  }

  goToForgotPassword(): void {
    console.log("redirect to forgot-password page");
  }

  async presentLoading(authProvider?: string) {
    const authProviderCapitalized =
      authProvider[0].toUpperCase() + authProvider.slice(1);

    this.loadingController
      .create({
        message: authProvider
          ? "Signing in with " + authProviderCapitalized
          : "Signin in ...",
      })
      .then((loader) => {
        const currentUrl = this.location.path();
        if (currentUrl.includes("auth-redirect")) {
          this.redirectLoader = loader;
          this.redirectLoader.present();
        }
      });
  }

  async dismissLoading() {
    if (this.redirectLoader) {
      await this.redirectLoader.dismiss();
    }
  }
  // Before invoking auth provider redirect flow, present a loading indicator and add a flag to the path.
  // The precense of the flag in the path indicates we should wait for the auth redirect to complete.
  prepareForAuthWithProvidersRedirection(authProvider: string) {
    this.presentLoading(authProvider);

    this.location.replaceState(
      this.location.path(),
      "auth-redirect=" + authProvider,
      this.location.getState()
    );
  }

  manageAuthWithProvidersErrors(errorMessage: string) {
    this.submitError = errorMessage;
    // remove auth-redirect param from url
    this.location.replaceState(this.router.url.split("?")[0], "");
    this.dismissLoading();
  }

  resetSubmitError() {
    this.submitError = null;
  }

  signInWithEmail() {
    this.resetSubmitError();
    const { email, password } = this.loginForm.value || {};
    this.storageService.setItem({
      key: "userEmail",
      value: JSON.stringify(this.loginForm.value.email),
    })
    const formValues = { email, password };
    this.authService
      .signInWithEmail(formValues)
      .then((user) => {
        const { code, message } = user || {};
        if (isNil(code)) {
          this.redirectLoggedUserToProfilePage();
        } else {
          this.submitError = message;
          this.dismissLoading();
        }
      })
      .catch((error) => {
        this.submitError = error.message;
        this.dismissLoading();
      });
  }

  doFacebookLogin(): void {
    this.resetSubmitError();
    this.prepareForAuthWithProvidersRedirection("facebook");

    this.authService.signInWithFacebook().subscribe(
      (result) => {
        // This gives you a Facebook Access Token. You can use it to access the Facebook API.
        // const token = result.credential.accessToken;
        const { user } = result || {};
        this.authService.setUserStorage(user);
        const resultHandled = this.authService.handleOAuthResult(
          result,
          "facebook.com",
          "login"
        );
        if (resultHandled) this.redirectLoggedUserToProfilePage();
      },
      (error) => {
        console.log("doFacebookLogin error", error);
        this.manageAuthWithProvidersErrors(error.message);
      }
    );
  }

  doGoogleLogin(): void {
    this.resetSubmitError();
    this.prepareForAuthWithProvidersRedirection("google");

    this.authService.signInWithGoogle().subscribe(
      (result) => {
        console.log("doGoogleLogin result", result);
        // This gives you a Facebook Access Token. You can use it to access the Facebook API.
        // const token = result.credential.accessToken;
        const { user } = result || {};
        this.authService.setUserStorage(user);
        const resultHandled = this.authService.handleOAuthResult(
          result,
          "google.com",
          "login"
        );
        if (resultHandled) this.redirectLoggedUserToProfilePage();
      },
      (error) => {
        console.log("doGoogleLogin error", error);
        this.manageAuthWithProvidersErrors(error.message);
      }
    );
  }

  doTwitterLogin(): void {
    this.resetSubmitError();
    this.prepareForAuthWithProvidersRedirection("twitter");

    this.authService.signInWithTwitter().subscribe(
      (result) => {
        // This gives you a Facebook Access Token. You can use it to access the Facebook API.
        // const token = result.credential.accessToken;
        const { user } = result || {};
        this.authService.setUserStorage(user);
        const resultHandled = this.authService.handleOAuthResult(
          result,
          "twitter.com",
          "login"
        );
        if (resultHandled) this.redirectLoggedUserToProfilePage();
      },
      (error) => {
        console.log("doTwitterLogin error", error);
        this.manageAuthWithProvidersErrors(error.message);
      }
    );
  }

  // Once the auth provider finished the authentication flow, and the auth redirect completes,
  // hide the loader and redirect the user to the profile page
  redirectLoggedUserToProfilePage() {
    this.dismissLoading();
    // As we are calling the Angular router navigation inside a subscribe method, the navigation will be triggered outside Angular zone.
    // That's why we need to wrap the router navigation call inside an ngZone wrapper
    this.ngZone.run(async () => {
      // Get previous URL from our custom History Helper
      // If there's no previous page, then redirect to profile
      // const previousUrl = this.historyHelper.previousUrl || 'firebase/auth/profile';
      // const previousUrl = "firebase/auth/profile";
      let previousUrl = "home";
      const hasDonetutorial = await this.storageService.getItem("tutorial");
      if (isNil(hasDonetutorial)) {
        this.storageService.setItem({
          key: "tutorial",
          value: JSON.stringify(1),
        });
        previousUrl = "walkthrough";
      }
      // No need to store in the navigation history the sign-in page with redirect params (it's justa a mandatory mid-step)
      // Navigate to profile and replace current url with profile
      this.router.navigate([previousUrl], { replaceUrl: true });
    });
  }
}
