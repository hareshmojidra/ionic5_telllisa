import { NgModule } from '@angular/core';
import { AngularFireAuthGuard } from '@angular/fire/auth-guard';
import { RouterModule, Routes } from '@angular/router';
import { map } from 'rxjs/operators';

import { LoginPage } from './login/login.page';
import { SignupPage } from './signup/signup.page';

// Firebase guard to redirect logged in users to profile
const redirectLoggedInToProfile = (next) =>
  map((user) => {
    // when queryParams['auth-redirect'] don't redirect because we want
    // the component to handle the redirection
    if (user !== null && !next.queryParams["auth-redirect"]) {
      return ["home"];
    } else {
      return true;
    }
  });

const routes: Routes = [
  {
    path: "",
    canActivate: [AngularFireAuthGuard],
    data: { authGuardPipe: redirectLoggedInToProfile },
    children: [
      {
        path: "",
        redirectTo: "login",
        pathMatch: "full",
      },
      {
        path: "login",
        component: LoginPage,
      },
      {
        path: "signup",
        component: SignupPage,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AuthRoutingModule {}
