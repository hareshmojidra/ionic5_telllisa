import { Location } from '@angular/common';
import { Component, NgZone, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { signUpValidationMessages } from '@core/helpers';
import { Country, SignUpForm } from '@core/models';
import { DataService, FirebaseAuthService } from '@core/services';
import { PasswordValidator } from '@core/validators';
import { IonRouterOutlet, LoadingController, MenuController, ModalController } from '@ionic/angular';
import { StorageService } from '@shared-lib/services';
import { isNil } from 'lodash';
import { Subscription } from 'rxjs';
import { tap } from 'rxjs/operators';

import { PrivacyPolicyPage } from '../privacy-policy';
import { TermsOfServicePage } from '../terms-of-service';

@Component({
  selector: "app-signup",
  templateUrl: "./signup.page.html",
  styleUrls: ["./signup.page.scss"],
})
export class SignupPage implements OnInit {
  signupForm: FormGroup;
  matching_passwords_group: FormGroup;
  submitError: string;
  redirectLoader: HTMLIonLoadingElement;
  authRedirectResult: Subscription;
  countriesSubscription: Subscription;
  countries: Array<Country>;
  validation_messages = signUpValidationMessages;

  constructor(
    public modalController: ModalController,
    private routerOutlet: IonRouterOutlet,
    public router: Router,
    public route: ActivatedRoute,
    public menu: MenuController,
    public authService: FirebaseAuthService,
    private ngZone: NgZone,
    public loadingController: LoadingController,
    public location: Location,
    public dataService: DataService,
    public storageService: StorageService
  ) {
    this.matching_passwords_group = new FormGroup(
      {
        password: new FormControl(
          "",
          Validators.compose([Validators.minLength(5), Validators.required])
        ),
        confirm_password: new FormControl("", Validators.required),
      },
      (formGroup: FormGroup) => {
        return PasswordValidator.areNotEqual(formGroup);
      }
    );

    this.signupForm = new FormGroup({
      firstName: new FormControl("", Validators.compose([Validators.required])),
      lastName: new FormControl("", Validators.compose([Validators.required])),
      email: new FormControl(
        "",
        Validators.compose([
          Validators.required,
          Validators.pattern("^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$"),
        ])
      ),
      country: new FormControl("", Validators.required),
      terms: new FormControl(false, Validators.pattern("true")),
      matching_passwords: this.matching_passwords_group,
    });
  }

  ngOnInit(): void {
    this.getCountries();
  }

  // Disable side menu for this page
  ionViewDidEnter(): void {
    this.menu.enable(false);
  }

  // Restore to default when leaving this page
  ionViewDidLeave(): void {
    this.menu.enable(true);
    this.countriesSubscription.unsubscribe();
  }

  async showTermsModal() {
    const modal = await this.modalController.create({
      component: TermsOfServicePage,
      swipeToClose: true,
      presentingElement: this.routerOutlet.nativeEl,
    });
    return await modal.present();
  }

  async showPrivacyModal() {
    const modal = await this.modalController.create({
      component: PrivacyPolicyPage,
      swipeToClose: true,
      presentingElement: this.routerOutlet.nativeEl,
    });
    return await modal.present();
  }

  // Once the auth provider finished the authentication flow, and the auth redirect completes,
  // hide the loader and redirect the user to the profile page
  redirectLoggedUserToProfilePage() {
    this.dismissLoading();

    // As we are calling the Angular router navigation inside a subscribe method, the navigation will be triggered outside Angular zone.
    // That's why we need to wrap the router navigation call inside an ngZone wrapper
    this.ngZone.run(async () => {
      // Get previous URL from our custom History Helper
      // If there's no previous page, then redirect to profile
      // const previousUrl = this.historyHelper.previousUrl || 'firebase/auth/profile';
      let previousUrl = "home";
      const hasDonetutorial = await this.storageService.getItem("tutorial");
      console.log("hasDonetutorial", hasDonetutorial);
      if (isNil(hasDonetutorial)) {
        this.storageService.setItem({
          key: "tutorial",
          value: JSON.stringify(1),
        });
        previousUrl = "walkthrough";
      }
      // No need to store in the navigation history the sign-in page with redirect params (it's justa a mandatory mid-step)
      // Navigate to profile and replace current url with profile
      this.router.navigate([previousUrl], { replaceUrl: true });
    });
  }

  async presentLoading(authProvider?: string) {
    const authProviderCapitalized =
      authProvider[0].toUpperCase() + authProvider.slice(1);
    this.redirectLoader = await this.loadingController.create({
      message: authProvider
        ? "Signing up with " + authProviderCapitalized
        : "Signin up ...",
    });
    await this.redirectLoader.present();
  }

  async dismissLoading() {
    if (this.redirectLoader) {
      await this.redirectLoader.dismiss();
    }
  }

  resetSubmitError() {
    this.submitError = null;
  }

  // Before invoking auth provider redirect flow, present a loading indicator and add a flag to the path.
  // The precense of the flag in the path indicates we should wait for the auth redirect to complete.
  prepareForAuthWithProvidersRedirection(authProvider: string) {
    this.presentLoading(authProvider);

    this.location.go(
      this.location.path(),
      "auth-redirect=" + authProvider,
      this.location.getState()
    );
  }

  manageAuthWithProvidersErrors(errorMessage: string) {
    this.submitError = errorMessage;
    // remove auth-redirect param from url
    this.location.replaceState(this.router.url.split("?")[0], "");
    this.dismissLoading();
  }

  signUpWithEmail(): void {
    this.resetSubmitError();
    const {
      firstName,
      lastName,
      email,
      country,
      matching_passwords: { password },
    } = this.signupForm.value;
    const signupForm: SignUpForm = {
      firstName,
      lastName,
      email,
      country,
      password,
    };
    this.authService
      .signUpWithEmail(signupForm)
      .then((user) => {
        // navigate to user profile
        this.redirectLoggedUserToProfilePage();
      })
      .catch((error) => {
        this.submitError = error.message;
      });
  }

  doFacebookSignup(): void {
    this.resetSubmitError();
    this.prepareForAuthWithProvidersRedirection("facebook");
    this.authService.signInWithFacebook().subscribe(
      (result) => {
        // This gives you a Facebook Access Token. You can use it to access the Facebook API.
        // const token = result.credential.accessToken;
        const { user } = result || {};
        this.authService.setUserStorage(user);
        const resultHandled = this.authService.handleOAuthResult(
          result,
          "facebook.com"
        );
        if (resultHandled) this.redirectLoggedUserToProfilePage();
      },
      (error) => {
        console.log("doFacebookSignup error", error);
        this.manageAuthWithProvidersErrors(error.message);
      }
    );
  }

  doGoogleSignup(): void {
    this.resetSubmitError();
    this.prepareForAuthWithProvidersRedirection("google");

    this.authService
      .signInWithGoogle()
      .pipe(
        tap((data) => {
          console.log("signin with google", data);
        })
      )
      .subscribe(
        (result) => {
          const { user } = result || {};
          this.authService.setUserStorage(user);
          const resultHandled = this.authService.handleOAuthResult(
            result,
            "google.com"
          );
          if (resultHandled) this.redirectLoggedUserToProfilePage();
        },
        (error) => {
          console.log("gogole signin error error", error);
          this.manageAuthWithProvidersErrors(error.message);
        }
      );
  }

  doTwitterSignup(): void {
    this.resetSubmitError();
    this.prepareForAuthWithProvidersRedirection("twitter");

    this.authService.signInWithTwitter().subscribe(
      (result) => {
        console.log("result", result);
        // This gives you a Facebook Access Token. You can use it to access the Facebook API.
        // const token = result.credential.accessToken;
        const { user } = result || {};
        this.authService.setUserStorage(user);
        const resultHandled = this.authService.handleOAuthResult(
          result,
          "twitter.com"
        );
        if (resultHandled) this.redirectLoggedUserToProfilePage();
      },
      (error) => {
        console.log("doTwitterSignup error", error);
        this.manageAuthWithProvidersErrors(error.message);
      }
    );
  }

  getCountries() {
    this.countriesSubscription = this.dataService
      .getCountries()
      .subscribe((data) => {
        this.countries = data;
      });
  }
}
