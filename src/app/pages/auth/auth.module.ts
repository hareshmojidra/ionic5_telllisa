import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FirebaseAuthService } from '@core/services';
import { IonicModule } from '@ionic/angular';
import { SharedModule } from '@shared-lib/shared.module';

import { AuthRoutingModule } from './auth-routing.module';
import { LoginPage } from './login/login.page';
import { PrivacyEnglishComponent, PrivacyGermanComponent, PrivacyPolicyPage } from './privacy-policy';
import { SignupPage } from './signup/signup.page';
import { TermsOfServicePage } from './terms-of-service';

@NgModule({
  declarations: [
    LoginPage,
    SignupPage,
    TermsOfServicePage,
    PrivacyPolicyPage,
    PrivacyGermanComponent,
    PrivacyEnglishComponent,
  ],
  imports: [
    CommonModule,
    AuthRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
  ],
  providers: [FirebaseAuthService],
})
export class AuthModule {}
