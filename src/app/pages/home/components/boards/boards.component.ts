import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Capacitor } from '@capacitor/core';
import { convertDateToFirebaseTimestamp, tryCatchHelper } from '@core/helpers';
import { Boards, defaultColor, Mail } from '@core/models';
import { LanguageService, MailService, MessagesService, RecordingsService } from '@core/services';
import { Platform } from '@ionic/angular';
import { ToastService } from '@shared-lib/services';

const { isNative } = Capacitor;
@Component({
  selector: "app-boards",
  templateUrl: "./boards.component.html",
  styleUrls: ["./boards.component.scss"],
})
export class BoardsComponent implements OnInit {
  public readonly defaultColor = defaultColor;

  longPressActive: boolean = false;
  showFab: boolean = false;
  @Input() board: Boards;
  @Input() boards: Boards[];
  constructor(
    private router: Router,
    private readonly recordingsService: RecordingsService,
    private readonly messagesService: MessagesService,
    private readonly toast: ToastService,
    private readonly mailService: MailService,
    private readonly platform: Platform,
    private readonly languageService: LanguageService
  ) {}

  ngOnInit() {
    const { $id: boardId } = this.board;
    this.messagesService.setBoardId(boardId);
    this.recordingsService.initSpeechRecognition();
  }

  openSpeechRecognition() {
    if (isNative) {
      if (this.platform.is("ios")) {
        this.showFab = true;
      }
      const { languageTag } = this.languageService.getSelectedLanguage || {};
      const options = {
        language: languageTag ? languageTag : "en-US",
      };
      this.recordingsService.options = options;
      this.recordingsService.speechRecognition
        .startListening(options)
        .subscribe(
          (matches: string[]) => this.handleSpeechToText(matches),
          (onerror) => {
            console.log("one error", onerror);
          }
        );
    } else {
      this.toast.showBottomShort("Please run on actual device", 3000);
    }
  }

  handleSpeechToText(matches: Array<string>) {
    console.log("matches", matches);
    const { email } = this.board;
    const record = matches.join(", ");
    const message = {
      title: convertDateToFirebaseTimestamp(),
      creationDate: convertDateToFirebaseTimestamp(),
      text: record,
    };
    const mail: Mail = {
      to: email,
      message: {
        subject: "New card",
        html: `${record}`,
      },
    };
    tryCatchHelper(this.mailService.addMail(mail));
    tryCatchHelper(this.messagesService.addMessage(message));
    this.toast.showBottomShort("Stored Message.", 3000);
    if (this.platform.is("ios")) {
      this.showFab = !this.showFab;
    }
  }

  showRecordings(board) {
    alert(JSON.stringify(board.recordings));
  }

  gotoPage(board: Boards, url: Array<string>) {
    this.messagesService.setBoard(board);
    this.router.navigate(url);
  }

  dragged(event, board) {
    this.recordingsService.recordings = board.recordings;
    if (event.detail.amount < 0) {
      this.gotoPage(board, ["recordings"]);
    } else {
      this.gotoPage(board, ["recordings", "create-record"]);
    }
  }
}
