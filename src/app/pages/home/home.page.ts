import { Component, HostListener, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Boards } from '@core/models';
import { BoardsService, FirebaseAuthService, LanguageService } from '@core/services';
import { IonRefresher } from '@ionic/angular';
import { SearchService } from '@shared-lib/services';
import { Observable, Subscription } from 'rxjs';

@Component({
  selector: "app-home",
  templateUrl: "home.page.html",
  styleUrls: ["home.page.scss"],
})
export class HomePage implements OnInit, OnDestroy {
  searchQuery: string;
  public searchLoader: boolean = false;
  public boards$: Observable<Boards[]>;
  boardSubscription: Subscription;
  private searchInputSubscription: Subscription;
  private searchInputValueSubscription: Subscription;

  @ViewChild("refresher") refresher: IonRefresher;

  constructor(
    private authService: FirebaseAuthService,
    private boardService: BoardsService,
    public readonly searchService: SearchService,
    private languageService: LanguageService
  ) {}

  ngOnInit() {
    this.searchQuery = "";
    const { uid } = this.authService.currentUser || {};
    if (uid) {
      this.boardService.setUserId(uid);
      this.boards$ = this.boardService.getBoards(uid);
    }

    this.searchInputValueSubscription = this.searchService.searchedWord$.subscribe(
      (searched) => {
        this.searchQuery = searched;
      }
    );
    this.handleDefaultLanguage();
  }
  @HostListener("window:beforeunload")
  ngOnDestroy() {
    if (this.searchInputSubscription) {
      this.searchInputSubscription.unsubscribe();
    }
    if (this.searchInputValueSubscription) {
      this.searchInputValueSubscription.unsubscribe();
    }
  }

  public ngAfterViewInit() {}

  refresh(ev) {
    setTimeout(() => {
      ev.detail.complete();
    }, 3000);
  }

  searchList(event) {
    this.searchService.setSearchedWord(event.detail.value);
    this.searchLoader = false;
  }
  handleDefaultLanguage() {
    this.languageService.setTranslations();
  }
}
