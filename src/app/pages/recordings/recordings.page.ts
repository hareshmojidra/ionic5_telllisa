import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Boards, Message } from '@core/models';
import { MessagesService, RecordingsService } from '@core/services';
import { isEmpty } from 'lodash';
import { Observable } from 'rxjs';

@Component({
  selector: "app-recordings",
  templateUrl: "./recordings.page.html",
  styleUrls: ["./recordings.page.scss"],
})
export class RecordingsPage implements OnInit, OnDestroy {
  public messages$: Observable<Message[]>;
  board: Boards;
  constructor(
    private readonly recordingsService: RecordingsService,
    private readonly messagesService: MessagesService,
    private readonly router: Router
  ) {}

  ngOnInit() {
    this.board = this.messagesService.getBoard;
    if (isEmpty(this.board)) {
      this.router.navigate(["home"], { replaceUrl: true });
    }
    this.messages$ = this.messagesService.getMessages();
  }

  ngOnDestroy() {
    this.recordingsService.recordings = null;
  }
}
