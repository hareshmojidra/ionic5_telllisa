import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Capacitor } from '@capacitor/core';
import { convertDateToFirebaseTimestamp, tryCatchHelper } from '@core/helpers';
import { Boards, Mail } from '@core/models';
import { LanguageService, MailService, MessagesService, RecordingsService } from '@core/services';
import { Platform } from '@ionic/angular';
import { ToastService } from '@shared-lib/services';
import { isEmpty } from 'lodash';

const { isNative } = Capacitor;

@Component({
  selector: "app-create-record",
  templateUrl: "./create-record.page.html",
  styleUrls: ["./create-record.page.scss"],
})
export class CreateRecordPage implements OnInit {
  createRecordForm: FormGroup;
  validations = {
    name: [{ type: "required", message: "Text is required." }],
  };
  board: Boards;
  matchesArr: Array<string>;
  showFab: boolean = false;
  constructor(
    private recordingsService: RecordingsService,
    public fb: FormBuilder,
    private readonly messagesService: MessagesService,
    private readonly toast: ToastService,
    private readonly mailService: MailService,
    private readonly router: Router,
    private readonly platform: Platform,
    private readonly languageService: LanguageService
  ) {
    this.createRecordForm = this.fb.group({
      name: ["", Validators.compose([Validators.required])],
    });
  }

  ngOnInit() {
    this.board = this.messagesService.getBoard;
    if (isEmpty(this.board)) {
      this.router.navigate(["home"], { replaceUrl: true });
    }
  }

  ionViewWillLeave() {
    this.recordingsService.recordings = null;
  }

  openSpeechRecognition() {
    if (isNative) {
      if (this.platform.is("ios")) {
        this.showFab = true;
      }
      const { languageTag } = this.languageService.getSelectedLanguage || {};
      const options = {
        language: languageTag ? languageTag : "en-US",
      };
      this.recordingsService.speechRecognition
        .startListening(options)
        .subscribe(
          (matches: string[]) => this.handleSpeechToText(matches),
          (onerror) => {
            console.log("one error", onerror);
          }
        );
    } else {
      this.toast.showBottomShort("Please run on actual device", 3000);
    }
  }

  handleSpeechToText(matches: Array<string>) {
    this.matchesArr = matches;
  }

  save() {
    if (this.createRecordForm.valid) {
      const { value } = this.createRecordForm || {};
      const { email } = this.board || {};
      const record = !isEmpty(this.matchesArr)
        ? value.name + ", " + this.matchesArr.join(", ")
        : value.name;

      const message = {
        title: convertDateToFirebaseTimestamp(),
        creationDate: convertDateToFirebaseTimestamp(),
        text: record,
      };
      if (email) {
        const mail: Mail = {
          to: email,
          message: {
            subject: "New card",
            html: `${record}`,
          },
        };
        tryCatchHelper(this.mailService.addMail(mail));
      }
      tryCatchHelper(this.messagesService.addMessage(message));
      this.toast.showBottomShort("Stored Message.", 500);
      setTimeout(() => {
        this.router.navigate(["home"], { replaceUrl: true });
      }, 1000);
    } else {
      this.toast.showBottomShort("Text is required.", 500);
    }
  }
}
