import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { SharedModule } from '@shared-lib/shared.module';
import { AutosizeModule } from 'ngx-autosize';

import { CreateRecordPageRoutingModule } from './create-record-routing.module';
import { CreateRecordPage } from './create-record.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CreateRecordPageRoutingModule,
    ReactiveFormsModule,
    SharedModule,
    AutosizeModule,
  ],
  declarations: [CreateRecordPage],
})
export class CreateRecordPageModule {}
