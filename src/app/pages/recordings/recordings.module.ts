import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { SharedModule } from '@shared-lib/shared.module';

import { RecordingsPageRoutingModule } from './recordings-routing.module';
import { RecordingsPage } from './recordings.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RecordingsPageRoutingModule,
    SharedModule,
  ],
  declarations: [RecordingsPage],
})
export class RecordingsPageModule {}
