import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { LanguageService } from '@core/services';
import { IonicModule } from '@ionic/angular';
import { SharedModule } from '@shared-lib/shared.module';

import { WalkthroughPage } from './walkthrough.page';

const routes: Routes = [
  {
    path: "",
    component: WalkthroughPage,
  },
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    SharedModule,
  ],
  declarations: [WalkthroughPage],
  providers: [LanguageService],
})
export class WalkthroughPageModule {}
