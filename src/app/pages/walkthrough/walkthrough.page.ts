import { isPlatformBrowser } from '@angular/common';
import { AfterViewInit, Component, HostBinding, Inject, OnInit, PLATFORM_ID, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Plugins } from '@capacitor/core';
import { Boards } from '@core/models';
import { BoardsService, FirebaseAuthService, LanguageService } from '@core/services';
import { AlertController, IonSlides, MenuController, ModalController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { CustomModalComponent } from '@shared-lib/components';
import { Observable } from 'rxjs';

const { Browser } = Plugins;
@Component({
  selector: "app-walkthrough",
  templateUrl: "./walkthrough.page.html",
  styleUrls: [
    "./styles/walkthrough.page.scss",
    "./styles/walkthrough.shell.scss",
    "./styles/walkthrough.responsive.scss",
  ],
})
export class WalkthroughPage implements AfterViewInit, OnInit {
  slidesOptions: any = {
    zoom: {
      toggle: false, // Disable zooming to prevent weird double tap zomming on slide images
    },
  };
  isNotStart: boolean = false;
  available_languages = [];
  translations;
  activeSlide: number;
  public boards$: Observable<Boards[]>;

  @ViewChild(IonSlides, { static: true }) slides: IonSlides;

  @HostBinding("class.first-slide-active") isFirstSlide = true;

  @HostBinding("class.last-slide-active") isLastSlide = false;

  constructor(
    @Inject(PLATFORM_ID) private platformId: object,
    private readonly menu: MenuController,
    private readonly translate: TranslateService,
    private readonly languageService: LanguageService,
    private readonly alertController: AlertController,
    private readonly modalController: ModalController,
    private readonly router: Router,

    private readonly authService: FirebaseAuthService,
    private readonly boardService: BoardsService
  ) {}

  ngOnInit(): void {
    this.getTranslations();
    const { uid } = this.authService.currentUser || {};
    if (uid) {
      this.boardService.setUserId(uid);
      this.boards$ = this.boardService.getBoards(uid);
    }
  }
  // Disable side menu for this page
  ionViewDidEnter(): void {
    this.menu.enable(false);
  }

  // Restore to default when leaving this page
  ionViewDidLeave(): void {
    this.menu.enable(true);
  }

  ngAfterViewInit(): void {
    // Accessing slides in server platform throw errors
    if (isPlatformBrowser(this.platformId)) {
      // ViewChild is set
      this.slides.isBeginning().then((isBeginning) => {
        this.isFirstSlide = isBeginning;
      });
      this.slides.isEnd().then((isEnd) => {
        this.isLastSlide = isEnd;
      });

      // Subscribe to changes
      this.slides.ionSlideWillChange.subscribe((changes) => {
        this.slides.isBeginning().then((isBeginning) => {
          this.isFirstSlide = isBeginning;
        });
        this.slides.isEnd().then((isEnd) => {
          this.isLastSlide = isEnd;
        });
      });
      this.translate.onLangChange.subscribe(() => this.getTranslations());
    }
  }

  skipWalkthrough(): void {
    // Skip to the last slide
    this.slides.length().then((length) => {
      this.slides.slideTo(length);
    });
  }

  getTranslations() {
    // get translations for this page to use in the Language Chooser Alert
    this.translate
      .getTranslation(this.translate.currentLang)
      .subscribe((translations) => (this.translations = translations));
  }

  async openLanguageChooser() {
    this.available_languages = this.languageService
      .getLanguages()
      .map((item) => ({
        label: item.name,
        value: item.code,
        checked: item.code === this.translate.currentLang,
      }));

    const componentProps = {
      header: this.translations.SELECT_LANGUAGE,
      subHeader: this.translations.ADD_LANGUAGE,
      options: this.available_languages,
      closeBtn: true,
    };
    const modal = await this.modalController.create({
      componentProps,
      showBackdrop: true,
      component: CustomModalComponent,
      cssClass: "custom-modal",
      mode: "ios",
    });
    modal.onDidDismiss().then((data) => {
      if (data["data"]) {
        const {
          data: { selected },
        } = data;
        if (selected) {
          const selectedLanguage = this.languageService
            .getLanguages()
            .filter((obj) => obj.code === selected)[0];
          this.languageService.setSelectedLanguage(selectedLanguage);
          this.translate.use(selected);
          this.slides.slideNext();
        }
        if (data["data"] == "forward") {
          this.slides.slideNext();
        }
      } else {
        this.slides.slidePrev();
      }
    });
    return await modal.present();
  }

  async slideChanged() {
    const slide = await this.slides.getActiveIndex();
    this.activeSlide = slide;
    if (slide > 0) {
      this.isNotStart = true;
    } else {
      this.isNotStart = false;
    }
    if (slide == 1) {
      this.openLanguageChooser();
    }
  }

  next() {
    this.slides.slideNext();
  }

  prev() {
    this.slides.slidePrev();
  }

  async presentAlertConfirm() {
    const alert = await this.alertController.create({
      cssClass: "my-custom-class",
      header: "Confirm!",
      message: "Open Tell Lisa via webapp?",
      buttons: [
        {
          text: "Cancel",
          role: "cancel",
          cssClass: "secondary",
          handler: (blah) => {
            console.log("Confirm Cancel: blah");
          },
        },
        {
          text: "Okay",
          handler: async () => {
            console.log("Confirm Okay");
            await Browser.open({
              url: "https://tell-lisa.web.app/home/boards/",
            });
            this.router.navigate(["home"]);
          },
        },
      ],
    });

    await alert.present();
  }
  getStarted() {
    this.router.navigate(["home"]);
  }
}
