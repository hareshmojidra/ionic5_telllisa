import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { AutosizeModule } from 'ngx-autosize';

import {
  AlertComponent,
  AspectRatioComponent,
  CheckboxWrapperComponent,
  CountdownTimerComponent,
  CounterInputComponent,
  CustomModalComponent,
  ImageShellComponent,
  IosSpeechFabComponent,
  RatingInputComponent,
  ShowHidePasswordComponent,
  TextShellComponent,
} from './components';
import { NamePipe } from './pipes';

@NgModule({
  imports: [CommonModule, FormsModule, IonicModule, AutosizeModule],
  declarations: [
    CheckboxWrapperComponent,
    ShowHidePasswordComponent,
    CountdownTimerComponent,
    CounterInputComponent,
    RatingInputComponent,
    AspectRatioComponent,
    ImageShellComponent,
    TextShellComponent,
    AlertComponent,
    NamePipe,
    IosSpeechFabComponent,
    CustomModalComponent,
  ],
  exports: [
    CheckboxWrapperComponent,
    ShowHidePasswordComponent,
    CountdownTimerComponent,
    CounterInputComponent,
    RatingInputComponent,
    AspectRatioComponent,
    ImageShellComponent,
    TextShellComponent,
    AlertComponent,
    NamePipe,
    IosSpeechFabComponent,
    CustomModalComponent,
  ],
})
export class SharedModule {}
