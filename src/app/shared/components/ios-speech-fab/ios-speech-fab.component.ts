import { Component, Input, OnInit } from '@angular/core';
import { RecordingsService } from '@core/services';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: "app-ios-speech-fab",
  templateUrl: "./ios-speech-fab.component.html",
  styleUrls: ["./ios-speech-fab.component.scss"],
})
export class IosSpeechFabComponent implements OnInit {
  @Input() showFab: boolean = false;
  constructor(
    private readonly recordingService: RecordingsService,
    private loadingController: LoadingController
  ) {}

  ngOnInit() {}

  stopListening() {
    this.showFab = false;
    this.recordingService.stopSpeechRecognition();
    // this.presentLoadingWithOptions();
  }

  async presentLoadingWithOptions() {
    const loading = await this.loadingController.create({
      spinner: null,
      message: "Processing your recording...",
      translucent: true,
      cssClass: "custom-class custom-loading",
      backdropDismiss: false,
    });
    await loading.present();

    setTimeout(() => {
      loading.dismiss();
    }, 5000);
  }
}
