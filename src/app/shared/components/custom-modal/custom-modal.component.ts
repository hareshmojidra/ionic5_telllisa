import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { Options } from '@core/models';
import { IonInfiniteScroll, ModalController } from '@ionic/angular';
import { uniqBy } from 'lodash';

@Component({
  selector: "app-custom-modal",
  templateUrl: "./custom-modal.component.html",
  styleUrls: ["./custom-modal.component.scss"],
})
export class CustomModalComponent implements OnInit {
  @Input() header: string;
  @Input() subHeader: string;
  @Input() options: Array<Options>;
  @Input() closeBtn: boolean = false;
  @Input() showFooter: boolean = false;
  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;
  selected: Options;
  selectedLanguage: string;
  pageLimit: number = 10;
  page: number = 1;
  limitedOptions: Array<Options> = [];
  constructor(private readonly modalController: ModalController) {}

  ngOnInit() {
    this.getOptions();
  }

  getOptions() {
    if (this.options) {
      this.selected = this.options.filter((o) => o.checked)[0];
      this.limitedOptions = this.options.slice(0, this.pageLimit);
      this.limitedOptions.push(this.selected);
      this.limitedOptions = uniqBy(this.limitedOptions, "value");
      this.pageLimit += 5;
    }
  }

  selectedValue(event) {
    const selected: string = event.detail.value;
    this.selectedLanguage = selected;
  }
  selectLanguage() {
    let selected = this.selectedLanguage;
    if (!selected) {
      selected = this.selected.value;
    }
    this.modalController.dismiss({ selected });
  }
  loadData(event) {
    setTimeout(() => {
      this.getOptions();
      event.target.complete();
    }, 500);
  }

  closeModal(action) {
    this.modalController.dismiss(action);
  }
}
