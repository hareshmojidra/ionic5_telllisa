import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: "root",
})
export default class SearchService {
  public searchedWord$: BehaviorSubject<string>;

  constructor() {
    this.searchedWord$ = new BehaviorSubject("");
  }

  public setSearchedWord(searchedWord: string): void {
    this.searchedWord$.next(searchedWord);
  }
}
