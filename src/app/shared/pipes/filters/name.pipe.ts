import { Pipe, PipeTransform } from '@angular/core';
import { Boards } from '@core/models';

@Pipe({
  name: "name",
})
export class NamePipe implements PipeTransform {
  transform(boards: Boards[], searchedName: string): unknown {
    // if boards is empty or search input is empty
    if (searchedName === "" || !boards.length) {
      return boards;
    }
    // filter boards by name
    const filteredBoards = boards.filter((board: Boards) =>
      board.insensitiveName.includes(searchedName.toUpperCase())
    );
    // if there're no results after filter reuturn -1 value else return filteredBoards
    return filteredBoards.length ? filteredBoards : [-1];
  }
}
