import { Component } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { LanguageService } from '@core/services';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Platform } from '@ionic/angular';

@Component({
  selector: "app-root",
  templateUrl: "app.component.html",
  styleUrls: ["app.component.scss"],
})
export class AppComponent {
  textDir = "ltr";
  user: any;
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private readonly angularFire: AngularFireAuth,
    private readonly languageService: LanguageService
  ) {
    this.initializeApp();
    this.handleUserState();
    this.handleDefaultLanguage();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  handleUserState() {
    this.angularFire.onAuthStateChanged((user) => {
      if (user) {
        // User is signed in.
        this.user = user;
      } else {
        // No user is signed in.
        this.user = null;
      }
    });
  }
  handleDefaultLanguage() {
    this.languageService.setTranslations();
  }
}
