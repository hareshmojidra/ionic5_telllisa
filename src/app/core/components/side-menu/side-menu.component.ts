import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Plugins } from '@capacitor/core';
import { FirebaseAuthService, LanguageService } from '@core/services';
import { MenuController, ModalController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { CustomModalComponent } from '@shared-lib/components';
import { StorageService } from '@shared-lib/services';
import { Subscription } from 'rxjs';

const { Browser } = Plugins;

@Component({
  selector: "app-side-menu",
  templateUrl: "./side-menu.component.html",
  styleUrls: ["./side-menu.component.scss"],
})
export class SideMenuComponent implements OnInit, OnDestroy {
  appPages = [
    {
      title: "Boards",
      url: "/app/boards",
      ionicIcon: "list-outline",
    },
    {
      title: "Profile",
      url: "/app/user",
      ionicIcon: "person-outline",
    },
    {
      title: "Contact Card",
      url: "/contact-card",
      customIcon: "./assets/custom-icons/side-menu/contact-card.svg",
    },
    {
      title: "Notifications",
      url: "/app/notifications",
      ionicIcon: "notifications-outline",
    },
  ];
  accountPages = [
    {
      title: "Tutorial",
      url: "/walkthrough",
      ionicIcon: "school-outline",
    },
  ];

  available_languages = [];
  translations;
  @Input() user: any;
  logoutSubscription: Subscription;
  constructor(
    private readonly translate: TranslateService,
    private readonly router: Router,
    private readonly menu: MenuController,
    private readonly languageService: LanguageService,
    private readonly authService: FirebaseAuthService,
    private readonly storageService: StorageService,
    private readonly modalController: ModalController
  ) {
    this.setLanguage();
  }

  ngOnInit(): void {
    this.getTranslations();
    // this.openEntryModal();
  }

  ngAfterViewInit() {
    this.translate.onLangChange.subscribe(() => this.getTranslations());
  }

  ngOnDestroy() {
    if (this.logoutSubscription) {
      this.logoutSubscription.unsubscribe();
    }
  }

  setLanguage(language = "en") {
    // console.log("setLang");
    // the lang to use, if the lang isn't available, it will use the current loader to get them
    this.translate.setDefaultLang(language);
    this.translate.use(language);
    // this is to determine the text direction depending on the selected language
    // for the purpose of this example we determine that only arabic and hebrew are RTL.
    // this.translate.onLangChange.subscribe((event: LangChangeEvent) => {
    //   this.textDir = (event.lang === 'ar' || event.lang === 'iw') ? 'rtl' : 'ltr';
    // });
  }

  async logout() {
    const closed = await this.menu.close();
    if (closed) {
      this.authService.signOut().subscribe(
        () => {
          // Sign-out successful.
          // Replace state as we are no longer authorized to access profile page.
          this.storageService.removeItem("userData");
          this.router.navigate([""], { replaceUrl: true });
        },
        (error) => {
          console.log("signout error", error);
        }
      );
    }
  }

  goHome() {
    this.router.navigate(["home"]);
  }

  async goToWebApp() {
    await Browser.open({ url: "https://tell-lisa.web.app/home/boards/" });
  }

  async getTranslations() {
    const { code } = (await this.storageService.getItem("language")) || {};
    const language = code;
    if (language) {
      this.setLanguage(language);
    }
    const { currentLang } = this.translate;
    // get translations for this page to use in the Language Chooser Alert
    this.translate
      .getTranslation(currentLang)
      .subscribe((translations) => (this.translations = translations));
  }

  async openLanguageChooser() {
    this.available_languages = this.languageService
      .getLanguages()
      .map((item) => ({
        label: item.name,
        value: item.code,
        checked: item.code === this.translate.currentLang,
      }));

    const componentProps = {
      header: this.translations.SELECT_LANGUAGE,
      subHeader: this.translations.ADD_LANGUAGE,
      options: this.available_languages,
    };
    const modal = await this.modalController.create({
      componentProps,
      showBackdrop: true,
      component: CustomModalComponent,
      cssClass: "custom-modal",
      mode: "ios",
      backdropDismiss: false,
    });
    modal.onDidDismiss().then((data) => {
      const {
        data: { selected },
      } = data;
      if (selected) {
        const selectedLanguage = this.languageService
          .getLanguages()
          .filter((obj) => obj.code === selected)[0];
        this.languageService.setSelectedLanguage(selectedLanguage);
        this.translate.use(selected);
      }
    });
    return await modal.present();
  }
}
