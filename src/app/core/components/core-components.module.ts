import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { SharedModule } from '@shared-lib/shared.module';

import { AppRoutingModule } from './../../app-routing.module';
import { SideMenuComponent } from './side-menu/side-menu.component';

@NgModule({
  declarations: [SideMenuComponent],
  imports: [CommonModule, IonicModule, SharedModule, AppRoutingModule],

  exports: [SideMenuComponent],
})
export class CoreComponentsModule {}
