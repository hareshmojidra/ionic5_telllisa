import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Boards } from '@core/models';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: "root",
})
export default class BoardsService {
  private userId: string;

  constructor(private readonly angularFirestore: AngularFirestore) {}

  // get all user board
  public getBoards(userId: string): Observable<Boards[]> {
    return this.angularFirestore
      .collection<Boards>(`Boards/${userId}/myBoards`, (ref) =>
        ref.orderBy("insensitiveName")
      )
      .valueChanges({ idField: "$id" });
  }

  // set userId to use in future calls
  public setUserId(userId: string): void {
    this.userId = userId;
  }

  // get userId
  private get getUserId(): string {
    return this.userId;
  }
}
