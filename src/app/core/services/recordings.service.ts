import { Injectable } from '@angular/core';
import { Capacitor } from '@capacitor/core';
import { SpeechRecognition } from '@ionic-native/speech-recognition/ngx';
import { BehaviorSubject } from 'rxjs';

const { isNative } = Capacitor;

@Injectable({
  providedIn: "root",
})
export default class RecordingsService {
  recordings: any;

  options = {
    language: "en-US",
  };

  subject = new BehaviorSubject(false);

  constructor(public speechRecognition: SpeechRecognition) {}

  async initSpeechRecognition() {
    if (isNative) {
      try {
        // Check feature available
        const available = await this.speechRecognition.isRecognitionAvailable();
        if (available) {
          // Get the list of supported languages
          const supportedLanguages =
            this.speechRecognition.getSupportedLanguages();
          console.log("supportedLanguages", supportedLanguages);
          // Check permission
          const hasPermissions = await this.speechRecognition.hasPermission();
          if (!hasPermissions) {
            // Request permissions
            const permission = await this.speechRecognition.requestPermission();
            console.log("permission", permission);
          }
        }
      } catch (error) {
        console.log("init error", error);
      }
    } else {
      console.log("please run on actual device");
    }
  }

  public async stopSpeechRecognition() {
    if (isNative) {
      // // Stop the recognition process (iOS only)
      await this.speechRecognition.stopListening();
    }
  }
}
