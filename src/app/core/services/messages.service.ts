import { Injectable } from '@angular/core';
import { AngularFirestore, DocumentReference } from '@angular/fire/firestore';
import { Boards, Message } from '@core/models';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: "root",
})
export default class MessagesService {
  private boardId: string;
  private board: Boards;

  constructor(private readonly angularFirestore: AngularFirestore) {}

  // add a message
  public addMessage(message: Message): Promise<DocumentReference<Message>> {
    return this.angularFirestore
      .collection<Message>(`Messages/${this.getBoardId}/myMessages`)
      .add(message);
  }

  // get all messages
  public getMessages(): Observable<Message[]> {
    return this.angularFirestore
      .collection<Message>(`Messages/${this.getBoardId}/myMessages`, (ref) =>
        ref.orderBy("title")
      )
      .valueChanges({ idField: "$id" });
  }

  // set BoardId to use in future calls
  public setBoardId(boardId: string): void {
    this.boardId = boardId;
  }

  // get BoardId
  private get getBoardId(): string {
    return this.boardId;
  }

  // set board to use in future calls
  public setBoard(board: Boards): void {
    const { $id: boardId } = board;
    this.board = board;
    this.boardId = boardId;
  }

  // get board
  public get getBoard(): Boards {
    return this.board;
  }
}
