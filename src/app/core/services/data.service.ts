import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ErrorService } from '@shared-lib/services';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

@Injectable({
  providedIn: "root",
})
export default class DataService {
  countriesFile: string = "../../assets/data/countries.json";
  constructor(private http: HttpClient, private errorService: ErrorService) {}

  public getCountries(): Observable<any> {
    return this.http
      .get(this.countriesFile)
      .pipe(
        map((data) => {
          return data;
        })
      )
      .pipe(catchError((error) => this.errorService.handleError(error)));
  }
}
