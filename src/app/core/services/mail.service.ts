import { Injectable } from '@angular/core';
import { AngularFirestore, DocumentReference } from '@angular/fire/firestore';
import { Mail } from '@core/models';

@Injectable({
  providedIn: "root",
})
export default class MailService {
  constructor(private readonly angularFirestore: AngularFirestore) {}

  // add a mail
  public addMail(mail: Mail): Promise<DocumentReference<Mail>> {
    return this.angularFirestore.collection<Mail>(`mail`).add(mail);
  }
}
