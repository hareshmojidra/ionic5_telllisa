import { Injectable } from '@angular/core';
import { LanguageModel } from '@core/models';
import { TranslateService } from '@ngx-translate/core';
import { StorageService } from '@shared-lib/services';

@Injectable({
  providedIn: "root",
})
export default class LanguageService {
  languages: Array<LanguageModel> = new Array<LanguageModel>();
  private selectedLanguage: LanguageModel;
  constructor(
    private readonly storageService: StorageService,
    private readonly translate: TranslateService
  ) {
    this.languages.push(
      { name: "English ( US )", code: "en", languageTag: "en-US" },
      { name: "English ( UK )", code: "en-gb", languageTag: "en-GB" },
      { name: "English ( AU )", code: "en-au", languageTag: "en-AU" },
      { name: "Spanish", code: "es", languageTag: "es-ES" },
      { name: "French", code: "fr", languageTag: "fr-FR" },
      { name: "German", code: "de", languageTag: "de-DE" },
      { name: "Italian", code: "it", languageTag: "it-IT" }
    );
  }

  getLanguages() {
    return this.languages;
  }

  // set selected language to use in future calls
  public setSelectedLanguage(selectedLanguage: LanguageModel): void {
    this.selectedLanguage = selectedLanguage;
    if (selectedLanguage) {
      this.storageService.setItem({
        key: "language",
        value: JSON.stringify(selectedLanguage),
      });
    }
  }

  // get get selected language
  public get getSelectedLanguage(): LanguageModel {
    return this.selectedLanguage;
  }

  async setTranslations() {
    const { code } = (await this.storageService.getItem("language")) || {};
    const language = code;
    console.log("language", language);
    this.setLanguage(language);
  }
  setLanguage(language = "en") {
    // console.log("setLang");
    // the lang to use, if the lang isn't available, it will use the current loader to get them
    this.translate.setDefaultLang(language);
    this.translate.use(language);
    console.log("translations set");
    // this is to determine the text direction depending on the selected language
    // for the purpose of this example we determine that only arabic and hebrew are RTL.
    // this.translate.onLangChange.subscribe((event: LangChangeEvent) => {
    //   this.textDir = (event.lang === 'ar' || event.lang === 'iw') ? 'rtl' : 'ltr';
    // });
  }
}
