import { isPlatformBrowser } from '@angular/common';
import { Inject, Injectable, PLATFORM_ID } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { convertDateToFirebaseTimestamp } from '@core/helpers';
import { AdditionalValues, AppUserModel, FirebaseCredentials, SignUpForm, UserModel } from '@core/models';
import { DataStore } from '@core/stores';
import { Platform } from '@ionic/angular';
import { StorageService } from '@shared-lib/services';
import { cfaSignIn, cfaSignOut, mapUserToUserInfo } from 'capacitor-firebase-auth';
import { auth } from 'firebase/app';
import { isEmpty } from 'lodash';
import { from, Observable, Subject } from 'rxjs';

type AdditionalInfo = "firstName" | "lastName";

@Injectable({
  providedIn: "root",
})
export default class FirebaseAuthService {
  currentUser;
  userProviderAdditionalInfo: any;
  profileDataStore: DataStore<UserModel>;
  redirectResult: Subject<any> = new Subject<any>();
  public readonly user$: Observable<AppUserModel>;
  // public readonly authLoaders$: BehaviorSubject<boolean>;

  constructor(
    private readonly platform: Platform,
    private readonly angularFire: AngularFireAuth,
    private readonly angularFireStore: AngularFirestore,
    @Inject(PLATFORM_ID) private platformId: object,
    private readonly storageService: StorageService
  ) {
    if (isPlatformBrowser(this.platformId)) {
      this.angularFire.onAuthStateChanged((user) => {
        if (user) {
          // User is signed in.
          this.currentUser = user;
          this.setUserStorage(user);
        } else {
          // No user is signed in.
          this.currentUser = null;
        }
      });

      if (!this.platform.is("capacitor")) {
        // when using signInWithRedirect, this listens for the redirect results
        this.angularFire.getRedirectResult().then(
          (result) => {
            // result.credential.accessToken gives you the Provider Access Token. You can use it to access the Provider API.
            if (result.user) {
              this.userProviderAdditionalInfo =
                result.additionalUserInfo.profile;
              this.redirectResult.next(result);
            }
          },
          (error) => {
            this.redirectResult.next({ error: error.code });
          }
        );
      }
    }
  }

  // retrieve redirect result
  getRedirectResult(): Observable<any> {
    return this.redirectResult.asObservable();
  }

  // logout user
  signOut(): Observable<any> {
    if (this.platform.is("capacitor")) {
      return cfaSignOut();
    } else {
      return from(this.angularFire.signOut());
    }
  }

  // sign in using email and password
  async signInWithEmail(
    formValues: SignUpForm,
    fbCreds?: FirebaseCredentials
  ): Promise<any> {
    const { email, password } = formValues;
    try {
      const result: FirebaseCredentials = await this.angularFire.signInWithEmailAndPassword(
        email,
        password
      );
      if (fbCreds) {
        const { firstName, lastName } = formValues;
        result.formValues = { firstName, lastName } as AdditionalValues;
      }
      return this.updateUserInfo(result);
    } catch (error) {
      return error;
    } finally {
      // end process here / remove loaders
    }
  }

  // sign up/register using email address and password
  async signUpWithEmail(signupForm: SignUpForm): Promise<auth.UserCredential> {
    try {
      const { email, password } = signupForm;
      const result: FirebaseCredentials = await this.angularFire.createUserWithEmailAndPassword(
        email,
        password
      );
      return await this.signInWithEmail(signupForm, result);
    } catch (error) {
      // console.log('emailSignUp error', error);
    } finally {
      // end process here / remove loaders
    }
  }

  socialSignIn(providerName: string, scopes?: Array<string>): Observable<any> {
    if (this.platform.is("capacitor")) {
      return cfaSignIn(providerName).pipe(mapUserToUserInfo());
    } else {
      const provider = new auth.OAuthProvider(providerName);

      if (scopes) {
        scopes.forEach((scope) => {
          provider.addScope(scope);
        });
      }

      // if (this.platform.is("desktop")) {
      //   console.log("desktop");
      return from(this.angularFire.signInWithPopup(provider));
      // } else {
      //   console.log("pwa");
      //   // web but not desktop, for example mobile PWA
      //   return from(this.angularFire.signInWithRedirect(provider));
      // }
    }
  }

  signInWithFacebook() {
    const provider = new auth.FacebookAuthProvider();
    return this.socialSignIn(provider.providerId);
  }

  signInWithGoogle() {
    const provider = new auth.GoogleAuthProvider();
    const scopes = ["profile", "email"];
    return this.socialSignIn(provider.providerId, scopes);
  }

  signInWithTwitter() {
    const provider = new auth.TwitterAuthProvider();
    return this.socialSignIn(provider.providerId);
  }

  // add user info if new user or update `lastLogin` if old user
  public updateUserInfo(userMetadata: FirebaseCredentials): Promise<any> {
    const { uid } = userMetadata.user;
    // check if email is in user else check additionalUserInfo (this is for twitter) finally if email is not found use ''
    const email = userMetadata.user.email
      ? userMetadata.user.email
      : userMetadata.additionalUserInfo.profile["email"]
      ? userMetadata.additionalUserInfo.profile["email"]
      : "";
    const created = convertDateToFirebaseTimestamp(
      userMetadata.user.metadata.creationTime
    );
    const lastLogin = convertDateToFirebaseTimestamp(
      userMetadata.user.metadata.lastSignInTime
    );
    const authMethod = userMetadata?.additionalUserInfo?.providerId;
    const userRef: AngularFirestoreDocument<any> = this.angularFireStore.doc(
      `users/${uid}`
    );
    if (userMetadata.formValues) {
      const { firstName, lastName } = userMetadata.formValues
        ? userMetadata.formValues
        : null;
      const userInfo: AppUserModel = {
        uid,
        firstName,
        lastName,
        email,
        created,
        lastLogin,
        authMethod,
      };
      return userRef.set(userInfo, { merge: true });
    } else {
      return userRef.set({ lastLogin }, { merge: true });
    }
  }

  public updateUserOAuthInfo(data, mode?: string) {
    const { uid, email, formValues, provider: authMethod } = data;
    const created = convertDateToFirebaseTimestamp();
    const lastLogin = convertDateToFirebaseTimestamp();
    const userRef: AngularFirestoreDocument<any> = this.angularFireStore.doc(
      `users/${uid}`
    );
    if (mode !== "login") {
      const { firstName, lastName, country } = formValues;
      const userInfo: AppUserModel = {
        uid,
        firstName,
        lastName,
        email,
        created,
        lastLogin,
        authMethod,
        country,
      };
      return userRef.set(userInfo, { merge: true });
    } else {
      return userRef.set({ lastLogin }, { merge: true });
    }
  }

  public getProfileStore(
    dataSource: Observable<UserModel>
  ): DataStore<UserModel> {
    // Initialize the model specifying that it is a shell model
    const shellModel: UserModel = new UserModel();
    this.profileDataStore = new DataStore(shellModel);
    // Trigger the loading mechanism (with shell) in the dataStore
    this.profileDataStore.load(dataSource);
    return this.profileDataStore;
  }

  // currate OAuth values to have `firstName` and `lastName` instead of provider specific variables
  public getOAuthAdditionalInfo(
    additionalInfo: firebase.auth.AdditionalUserInfo,
    type: AdditionalInfo
  ): string {
    switch (type) {
      case "firstName":
        return (
          additionalInfo.profile["first_name"] ||
          additionalInfo.profile["given_name"] ||
          additionalInfo.profile["name"] ||
          ""
        );
      case "lastName":
        return (
          additionalInfo.profile["last_name"] ||
          additionalInfo.profile["family_name"] ||
          ""
        );
    }
  }
  public handleOAuthResult(result, provider, mode?: string) {
    let displayName;
    let oauthTriggerFromWeb = false;
    if (!isEmpty(result["additionalUserInfo"])) {
      oauthTriggerFromWeb = true;
      displayName = result["additionalUserInfo"]["profile"]["name"].split(" ");
    } else {
      displayName = result["displayName"].split(" ");
    }
    const firstName = displayName.slice(0, -1).join(" ");
    const lastName = displayName[displayName.length - 1];
    result.formValues = {
      firstName,
      lastName,
    } as AdditionalValues;
    result["provider"] = provider;
    if (oauthTriggerFromWeb) {
      this.updateUserInfo(result);
    } else {
      this.updateUserOAuthInfo(result, mode);
    }
    return true;
  }

  setUserStorage(user) {
    this.storageService.setItem({
      key: "userData",
      value: JSON.stringify(user),
    });
  }
}
