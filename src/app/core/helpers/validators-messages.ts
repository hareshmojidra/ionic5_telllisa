export const signUpValidationMessages = {
  firstName: [{ type: "required", message: "Name is required." }],
  lastName: [{ type: "required", message: "Last name is required." }],
  terms: [
    { type: "pattern", message: "You must accept terms and conditions." },
  ],
  email: [
    { type: "required", message: "Email is required." },
    { type: "pattern", message: "Enter a valid email." },
  ],
  password: [
    { type: "required", message: "Password is required." },
    {
      type: "minlength",
      message: "Password must be at least 5 characters long.",
    },
  ],
  confirm_password: [
    { type: "required", message: "Confirm password is required" },
  ],
  matching_passwords: [{ type: "areNotEqual", message: "Password mismatch" }],
};

export const loginValidationMessages = {
  email: [
    { type: "required", message: "Email is required." },
    { type: "pattern", message: "Enter a valid email." },
  ],
  password: [
    { type: "required", message: "Password is required." },
    {
      type: "minlength",
      message: "Password must be at least 5 characters long.",
    },
  ],
};
