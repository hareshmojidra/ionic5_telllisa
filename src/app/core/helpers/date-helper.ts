import { FirebaseTimestamp } from "@core/models";
import firebase from "firebase/app";

export function convertDateToFirebaseTimestamp(
  date?: number | string | Date
): FirebaseTimestamp {
  date = date ? new Date(date) : new Date();
  return firebase.firestore.Timestamp.fromDate(date);
}
