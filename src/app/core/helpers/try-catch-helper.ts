// static try catch for update and add boards and close loaders if there are any
const tryCatchHelper = async (promise: Promise<any>): Promise<any> => {
  try {
    await promise;
  } catch (error) {
    console.log("error", error);
    return error;
  } finally {
    // close loader
  }
};

export default tryCatchHelper;
