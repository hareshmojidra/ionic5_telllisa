export enum Colors {
  DarkBlue = "#0079bf",
  Orange = "#d29034",
  DarkGreen = "#519839",
  Red = "#b04632",
  Purple = "#89609e",
  Pink = "#cd5a91",
  LightGreen = "#4bbf6b",
  LightBlue = "#00aecc",
  Gray = "#838c91",
}

export interface UserBackgroundValues {
  backgroundColor?: Colors | "default";
  backgroundImage?: string;
  $id?: string;
}

export const defaultColor = Colors.DarkBlue;
/* tslint:disable:max-line-length */
export const defaultColors: Array<Colors> = [
  Colors.DarkBlue,
  Colors.Orange,
  Colors.DarkGreen,
  Colors.Red,
  Colors.Purple,
  Colors.Pink,
  Colors.LightGreen,
  Colors.LightBlue,
  Colors.Gray,
];
/* tslint:enable:max-line-length */
