export interface SignUpForm {
  firstName?: string;
  lastName?: string;
  email: string;
  country?: string;
  password: string;
}
