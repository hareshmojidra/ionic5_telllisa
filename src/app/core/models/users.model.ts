import { ShellModel } from '@core/stores';

import { Boards } from './boards.model';
import { Country } from './country.model';
import { BackgroundRef } from './file.model';
import { FirebaseTimestamp } from './firebase.model';

export interface UserFormValues {
  name: string;
  email: string;
  action?: "add" | "edit";
  $id: string;
}

export interface BoardsFormData {
  title: string;
  formValues?: Boards;
}

export class UserModel extends ShellModel {
  created: FirebaseTimestamp | Date;
  lastLogin?: FirebaseTimestamp | Date;
  authMethod?: string;
  uploadedBackgroundImages?: BackgroundRef[];

  uid: string;
  firstName: string;
  lastName: string;
  email: string;

  constructor() {
    super();
  }
}

export interface AppUserModel {
  uid?: string;
  firstName?: string;
  lastName?: string;
  email: string;
  created: FirebaseTimestamp | Date;
  lastLogin?: FirebaseTimestamp | Date;
  authMethod?: string;
  country?: Country;
  uploadedBackgroundImages?: BackgroundRef[];
}
