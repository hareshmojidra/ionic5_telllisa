import { Colors } from '@core/enums';

import { FirebaseTimestamp } from './firebase.model';

export interface Boards {
  $id?: string;
  name: string;
  email: string;
  creationDate: FirebaseTimestamp;
  backgroundColor?: Colors | "default";
  backgroundImage?: string;
  insensitiveName?: string;
}
