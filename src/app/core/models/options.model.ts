export interface Options {
  value: string;
  label: string;
  checked: boolean;
}
