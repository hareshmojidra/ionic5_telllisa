import { FirebaseTimestamp } from './firebase.model';

export interface Message {
  $id?: string;
  title: FirebaseTimestamp;
  text: string;
  creationDate: FirebaseTimestamp;
}
