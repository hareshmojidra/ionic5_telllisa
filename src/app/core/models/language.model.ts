export class LanguageModel {
  name: string;
  code: string;
  languageTag: string;
}
