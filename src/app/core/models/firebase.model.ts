import firebase from 'firebase/app';
export type AdditionalValues = {
    firstName: string;
    lastName: string;
};
export type FirebaseTimestamp = firebase.firestore.Timestamp;
export type FirebaseCredentials  = firebase.auth.UserCredential & {formValues?: AdditionalValues};
export type OAuths = firebase.auth.FacebookAuthProvider | firebase.auth.GoogleAuthProvider | firebase.auth.TwitterAuthProvider;
export type OAuthProvider = 'google' | 'facebook' | 'twitter';
