export interface Images {
  images?: Array<File>;
}

export interface BackgroundRef {
  url: string;
  storagePath: string;
}

export interface UserUploadModel {
  file: File;
  uploadedBackgroundImages: BackgroundRef[];
  userId: string;
}
