import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { FirebaseAuthService } from '@core/services';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: "root",
})
export class AuthGuard implements CanActivate {
  constructor(
    private readonly authService: FirebaseAuthService,
    private readonly router: Router
  ) {}
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    return this.authService.user$.pipe(
      map((userInfo) => {
        const requiresAuth = route.data.requiresAuth || false;
        if (requiresAuth) {
          if (userInfo) {
            return true;
          }
          this.router.navigate(["/auth/login"]);
          return false;
        }
        if (!userInfo) {
          return true;
        }
        this.router.navigate(["/home"]);
        return false;
      })
    );
  }
}
