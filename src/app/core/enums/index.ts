export enum ImageShellState {
  SSR = "ssr-loaded",
  BROWSER_FROM_SSR = "browser-loaded-from-ssr",
  NOT_FOUND = "not-found",
}

export enum Colors {
  DarkBlue = "#0079bf",
  Orange = "#d29034",
  DarkGreen = "#519839",
  Red = "#b04632",
  Purple = "#89609e",
  Pink = "#cd5a91",
  LightGreen = "#4bbf6b",
  LightBlue = "#00aecc",
  Gray = "#838c91",
}
